# Project Name

Cathedral Bistro - Pizza App

# Introduction

TODO: Give a short introduction of your project. Let this section explain the objectives or the motivation behind this project.

# Getting Started

TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:

1. Installation process
2. Software dependencies
3. Latest releases
4. API references

# Build and Test

TODO: Describe and show how to build your code and run the tests.

# Contribute

TODO: Explain how other users and developers can contribute to make your code better.

If you want to learn more about creating good readme files then refer the following [guidelines](https://docs.microsoft.com/en-us/azure/devops/repos/git/create-a-readme?view=azure-devops). You can also seek inspiration from the below readme files:

- [ASP.NET Core](https://github.com/aspnet/Home)
- [Visual Studio Code](https://github.com/Microsoft/vscode)
- [Chakra Core](https://github.com/Microsoft/ChakraCore)


# Creating ipa for iOS app using XCode

- Change the Bundle version string to new version in "info.plist"
- In Xcode change emulator to "Any iOS Device"
- Make sure "bundle identifier" in "General" tab in Xcode matches the App ID in apple developer profile. Ex: com.miviewis.bistro
- Make sure to select the right provisioning profile in "Signing and Capabilities".
- Make sure Build Phases > "Link binary with Libraries" includes "libSwiftWebKit.tbd"
- Click on Product > Archive
- Select the newly created Archive and click on "Distribute App"
- Click on Ad hoc
- Click on Manually manage signing.
- Select the right profile and certificate and click next.
- may need to go on to developer.apple.com > select profile > edit profile > select the certificate we want to use and save it.
- On Xcode download the profile again and select the certificate we choose on developer account.
- Click export and use the ipa exported to release app in app center