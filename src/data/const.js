const CONST = {
  baseUrl: "https://cathedralbistro.azurewebsites.net/",
  // baseUrl: "https://mi2.duceapps.com/",
  privacyPolicyUrl:
    "https://admin-cathedralbistro.azurewebsites.net/privacy-policy",
  termsAndConditionsUrl:
    "https://admin-cathedralbistro.azurewebsites.net/terms-and-condition",
  vipBenefitsUrl: `https://admin-cathedralbistro.azurewebsites.net/membership-benefits/`,
  additionalPizzaItems: "ADDITIONAL_PIZZA_ITEMS",
  myOrdersList: [
    {
      orderId: "12345",
      status: "Pending",
      date: "15/08/2021",
      time: "11:54AM",
      totalAmount: "4.07",
      address: "1234,Street,Dallas, Texas(TX),75225",
    },

    {
      orderId: "34343",
      status: "Pending",
      date: "15/08/2021",
      time: "11:54AM",
      totalAmount: "6",
      address: "1234,Street,Dallas, Texas(TX),75225",
    },
    {
      orderId: "123544545",
      status: "Completed",
      date: "15/08/2021",
      time: "11:54AM",
      totalAmount: "3",
      address: "1234,Street,Dallas, Texas(TX),75225",
    },
    {
      orderId: "165652345",
      status: "Completed",
      date: "15/08/2021",
      time: "11:54AM",
      totalAmount: "6.07",
      address: "1234,Street,Dallas, Texas(TX),75225",
    },
    {
      orderId: "122323345",
      status: "Completed",
      date: "15/08/2021",
      time: "11:54AM",
      totalAmount: "2.06",
      address: "1234,Street,Dallas, Texas(TX),75225",
    },
    {
      orderId: "887787",
      status: "Completed",
      date: "15/08/2021",
      time: "11:54AM",
      totalAmount: "4.45",
      address: "1234,Street,Dallas, Texas(TX),75225",
    },
  ],
};

export const HEADER_TITLE_FONT_SIZE = 20;

export default CONST;
