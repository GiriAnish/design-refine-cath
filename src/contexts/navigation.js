import React, { useState, createContext, useContext, useMemo } from "react";

export const navigationContext = createContext({});

export const NavigationProvider = (props) => {
  const [tabVisible, setTabVisible] = useState(true);
  const memoizedValue = useMemo(() => {
    return {
      tabVisible,
      setTabVisible,
    };
  }, [tabVisible]);
  return (
    <navigationContext.Provider value={memoizedValue}>
      {props.children}
    </navigationContext.Provider>
  );
};

export function useTabVisible() {
  const tabVisible = useContext(navigationContext);
  return tabVisible;
}
