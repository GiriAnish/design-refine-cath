import React, {
  useState,
  useMemo,
  createContext,
  useContext,
  useRef,
} from "react";
import { Alert } from "react-native";
import CONST from "../../data/const";
import { useEffect } from "react";

import { useSelector } from "react-redux";

const getBasicSelections = (basicSelectionHolder) => {
  return basicSelectionHolder.reduce((acc, currentItem, currentIndex) => {
    const selectedItem = currentItem.items[currentItem.selectedIndex];
    if (selectedItem) {
      acc.push([currentIndex, [selectedItem]]);
    }
    return acc;
  }, []);
};

const getAdditionalSelections = (additionalQuestionHolder) => {
  return additionalQuestionHolder.reduce((acc, currentItem, sectionIndex) => {
    const selectedItems = currentItem.items.filter(
      (item) => item.selectedIndex !== -1
    );
    if (selectedItems.length) {
      acc.push([sectionIndex, selectedItems]);
    }
    return acc;
  }, []);
};

const isBasicIndexSelected = (sectionaName, items, fromCartBasic) => {
  let sectionNameSele = fromCartBasic.filter((item) => item[0] == sectionaName);
  if (sectionNameSele.length) {
    let ingre = sectionNameSele[0][1][0];
    let a = items.map((item, index) => {
      if (item.name == ingre) {
        return index;
      }
    });
    let c = a.filter((c) => c != null);
    return c[0];
  }
  return -1;
};
const isAdditionalIndexSelected = (
  sectionaName,
  itemName,
  fromCartAdditional
) => {
  let sectionNameSele = fromCartAdditional.filter(
    (item) => item[0] == sectionaName
  );
  if (sectionNameSele.length) {
    let ingreList = sectionNameSele[0][1];
    let e = ingreList.map((it) => {
      let a = it?.label?.split("(");
      let leftName = a[0].trim();
      if (itemName == leftName) {
        switch (it.mode) {
          case 1:
            return 0;
          case 2:
            return 1;
          case 3:
            return 2;
          default:
            return -1;
        }
      }
    });
    let c = e.filter((c) => c != null);
    return c[0];
  }
  return -1;
};

const basicSelectionsInitial = (basic, fromCartBasic, uuid) =>
  basic.map((section) => {
    return {
      title: section.name,
      minimum: section.minimum,
      maximum: section.maximum,
      items: section.ingredients.map((item) => {
        return { id: item.id, label: item.name, imageUrl: item.imageUrl };
      }),
      selectedIndex: uuid
        ? isBasicIndexSelected(section.name, section.ingredients, fromCartBasic)
        : section.ingredients?.findIndex((item) => item.isDefault),
    };
  });
const additionalSelectionInitial = (additional, fromCartAdditional, uuid) =>
  additional.map((section) => {
    return {
      title: section.name,
      minimum: section.minimum,
      maximum: section.maximum,
      items: section.ingredients.map((item) => {
        return {
          id: item.id,
          label: item.name,
          image: item.imageUrl,
          selectedIndex: uuid
            ? isAdditionalIndexSelected(
                section.name,
                item.name,
                fromCartAdditional
              )
            : item.selected
            ? item.mode - 1
            : item.isDefault
            ? 2
            : -1,
        };
      }),
    };
  });

const getBasicSelectionPreview = (
  basicQuestionHolder = [],
  basicSelectedItems
) => {
  return basicSelectedItems.reduce((acc, [rowindex, selectedItems]) => {
    const currentItem = basicQuestionHolder[rowindex];
    const selectedItem = selectedItems[0];
    if (selectedItem) {
      const secondaryText = selectedItem.secondary
        ? ` (${selectedItem.secondary})`
        : "";
      acc.push([
        currentItem?.title,
        [`${selectedItem.label}${secondaryText}`],
        selectedItem.id,
      ]);
    }
    return acc;
  }, []);
};
const getAdditionalSelectionPreview = (
  additionalQuestionHolder,
  additionalSelectedItems
) => {
  return additionalSelectedItems.reduce(
    (acc, [sectionIndex, selectedItems]) => {
      const selectedLabels = selectedItems.map((item) => {
        if (item.selectedIndex === 0) {
          //side_left
          return {
            id: item.id,
            label: `${item.label} (Left Half)`,
            mode: item.selectedIndex + 1,
          };
        }
        if (item.selectedIndex === 1) {
          //side_right
          return {
            id: item.id,
            label: `${item.label} (Right Half)`,
            mode: item.selectedIndex + 1,
          };
        }
        if (item.selectedIndex === 2) {
          //side_full
          return {
            id: item.id,
            label: item.label,
            mode: item.selectedIndex + 1,
          };
        }
      });
      let selectedLabelsFiltered = selectedLabels.filter((c) => c != null);
      if (selectedLabelsFiltered.length) {
        acc.push([
          additionalQuestionHolder?.[sectionIndex]?.title,
          selectedLabelsFiltered,
          CONST.additionalPizzaItems,
        ]);
      }
      return acc;
    },
    []
  );
};

export const getSelectionPreviewFromServerData = (data) => {
  const serverData = data.ingredientGroups;
  const basic = serverData.filter((item) => item.type == 1);
  const additional = serverData.filter((item) => item.type == 3);
  const basicQuestionHolder = basicSelectionsInitial(basic);
  const additionalQuestionHolder = additionalSelectionInitial(additional);
  const basicSelectedItems = getBasicSelections(basicQuestionHolder);
  const additionalSelectedItems = getAdditionalSelections(
    additionalQuestionHolder
  );
  const basicSelectionPreview = getBasicSelectionPreview(
    basicQuestionHolder,
    basicSelectedItems
  );
  const additionalSelectionPreview = getAdditionalSelectionPreview(
    additionalQuestionHolder,
    additionalSelectedItems
  );
  const selectedItemsPreview = [
    ...basicSelectionPreview,
    ...additionalSelectionPreview,
  ];
  return selectedItemsPreview;
};

export const CustomisePizzaContext = createContext({});
export const CustomiseScreenProvider = (props) => {
  const basicQuestionHolder = useRef([]);
  const additionalQuestionHolder = useRef([]);
  const [price, setPrice] = useState(2);
  const [name, setName] = useState("");
  const [image, setImage] = useState("");
  const [id, setId] = useState("");
  const [quantity, setQuantity] = useState(1);
  const cart = useSelector((state) => state.cart.cartItems);
  const loginInfo = useSelector((state) => state.loggedInUser.loggedInUser);
  useEffect(() => {
    fetch(
      CONST.baseUrl +
        "api/v1/item/" +
        (props.id || loginInfo?.userDetails?.customItemId)
    )
      .then((res) => {
        if (res?.status === 200) {
          return res.json();
        } else {
          return { ingredientGroups: [] };
        }
      })
      .then((data) => {
        const serverData = data?.ingredientGroups;
        const basic = serverData.filter((item) => item.type == 1);
        const additional = serverData.filter((item) => item.type == 3);
        let fromCart;
        let fromCartBasic;
        let fromCartAdditional;
        if (props.uuid) {
          fromCart = cart.filter((item) => item.uuid == props.uuid);
          setQuantity(fromCart[0].quantity);
          fromCartBasic = fromCart[0].ingredients.filter(
            (item) => item[2] !== "ADDITIONAL_PIZZA_ITEMS"
          );
          fromCartAdditional = fromCart[0].ingredients.filter(
            (item) => item[2] == "ADDITIONAL_PIZZA_ITEMS"
          );
        } else {
          setQuantity(1);
        }

        basicQuestionHolder.current = basicSelectionsInitial(
          basic,
          fromCartBasic,
          props.uuid
        );
        additionalQuestionHolder.current = additionalSelectionInitial(
          additional,
          fromCartAdditional,
          props.uuid
        );
        setPrice(data.price);
        setName(data.name);
        setId(data.id);
        setImage(data.imageUrl);
        setBasicSelectedItems(getBasicSelections(basicQuestionHolder.current));
        setAdditionalSelectedItems(
          getAdditionalSelections(additionalQuestionHolder?.current)
        );
      })
      .catch((e) => {
        console.log(e);
      });
  }, [props]);

  const [basicSelectedItems, setBasicSelectedItems] = useState(
    getBasicSelections(basicQuestionHolder.current)
  );

  const [additionalSelectedItems, setAdditionalSelectedItems] = useState(
    getAdditionalSelections(additionalQuestionHolder?.current)
  );

  const updateQuantity = React.useCallback((quantity) => {
    setQuantity(quantity);
  }, []);

  const updateBasicSelection = React.useCallback((rowIndex, selectedIndex) => {
    basicQuestionHolder.current[rowIndex].selectedIndex = selectedIndex;
    setBasicSelectedItems(getBasicSelections(basicQuestionHolder.current));
  }, []);

  const updateAdditionalSelection = React.useCallback(
    (sectionIndex, rowIndex, selectedIndex) => {
      additionalQuestionHolder.current[sectionIndex].items[
        rowIndex
      ].selectedIndex = selectedIndex;
      setAdditionalSelectedItems(
        getAdditionalSelections(additionalQuestionHolder?.current)
      );
    },
    []
  );

  const updateAdditionalSelectionAlreadySelectedIndex = React.useCallback(
    (sectionIndex, rowIndex) => {
      return additionalQuestionHolder.current[sectionIndex].items[rowIndex]
        .selectedIndex;
    },
    []
  );

  const updateAdditionalSelectionTotalSelection = React.useCallback(
    (sectionIndex) => {
      return additionalQuestionHolder.current[sectionIndex].items.reduce(
        (acc, currentItem, currentIndex) => {
          if (currentItem.selectedIndex >= 0) {
            acc.push([currentIndex, currentItem.selectedIndex]);
          }
          return acc;
        },
        []
      );
    }
  );

  const basicSelectionPreview = useMemo(() => {
    return getBasicSelectionPreview(
      basicQuestionHolder.current,
      basicSelectedItems
    );
  }, [basicQuestionHolder.current, basicSelectedItems]);

  const additionalSelectionPreview = useMemo(() => {
    return getAdditionalSelectionPreview(
      additionalQuestionHolder?.current,
      additionalSelectedItems
    );
  }, [additionalQuestionHolder?.current, additionalSelectedItems]);

  const selectedItemsPreview = useMemo(() => {
    return [...basicSelectionPreview, ...additionalSelectionPreview];
  }, [basicSelectionPreview, additionalSelectionPreview]);

  const memoizedValue = useMemo(
    () => ({
      basicSelections: basicQuestionHolder.current,
      additionalSelections: additionalQuestionHolder?.current,
      selectedItemsPreview,
      price,
      id,
      name,
      image,
      quantity,
      updateQuantity,
      updateBasicSelection,
      updateAdditionalSelection,
      updateAdditionalSelectionAlreadySelectedIndex,
      updateAdditionalSelectionTotalSelection,
      basicSelectedItems,
    }),
    [
      basicQuestionHolder.current,
      additionalQuestionHolder?.current,
      selectedItemsPreview,
      price,
      id,
      name,
      image,
      quantity,
      updateQuantity,
      updateBasicSelection,
      updateAdditionalSelection,
      updateAdditionalSelectionAlreadySelectedIndex,
      updateAdditionalSelectionTotalSelection,
      basicSelectedItems,
    ]
  );
  return (
    <CustomisePizzaContext.Provider value={memoizedValue}>
      {props.children}
    </CustomisePizzaContext.Provider>
  );
};

export function useCustomisePizza() {
  const customisePizza = useContext(CustomisePizzaContext);
  return customisePizza;
}
