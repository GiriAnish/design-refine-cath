import "react-native-gesture-handler";
import React, { useEffect, useState } from "react";
import { Linking } from "react-native";
import { rootReducer } from "./store/rootReducers";
import {
  NavigationContainer,
  useNavigationContainerRef,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Toast from "react-native-toast-message";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import ReduxThunk from "redux-thunk";
import SplashScreen from "./Screen/SplashScreen";
import DrawerNavigationRoutes from "./navigation/DrawerNavigationRoutes";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { ThemeProvider } from "@emotion/react";
import theme from "./assets/css/commonThemes";
import { toastConfig } from "./Screen/Components/ToastConfig";
import Auth from "./navigation/AuthNavigation";
import { NavigationProvider } from "./contexts/navigation";

import AsyncStorage from "@react-native-community/async-storage";
import { persistStore, persistReducer } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import { StripeProvider } from "@stripe/stripe-react-native";
import codePush from "react-native-code-push";
import API_LIST from "./apiServices/apiList";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  blacklist: ["loading"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const Stack = createStackNavigator();

const store = createStore(persistedReducer, applyMiddleware(ReduxThunk));

const persistor = persistStore(store);

const App = () => {
  const [url, setUrl] = useState(null);
  const navigationRef = useNavigationContainerRef();
  const [stripePublishableKey, setStripePublishableKey] = useState(null);

  useEffect(() => {
    async function fetchVersion() {
      const [{ appVersion }, update] = await Promise.all([
        codePush.getConfiguration(),
        codePush.getUpdateMetadata(),
      ]);
      let app = "version";
      if (!update) {
        app = `v${appVersion}`;
        await AsyncStorage.setItem("appVersion", app);
      } else {
        const label = update.label.substring(1);
        app = `v${appVersion} rev.${label}`;
        await AsyncStorage.setItem("appVersion", app);
      }
    }
    fetchVersion();

    Linking.getInitialURL().then((url) => {
      setUrl(url);
    });
    Linking.addEventListener("url", handleOpenURL);
    return () => {
      Linking.removeEventListener("url", handleOpenURL);
    };
  }, []);

  const handleOpenURL = (event) => {
    setUrl(event?.url);
    if (event?.url?.includes("trackorder")) {
      const params = event.url.match(/\d+/g);
      if (params?.length) {
        navigationRef.navigate("DrawerNavigationRoutes", {
          screen: "homeScreenStack",
          params: {
            screen: "OrdersMenu",
            params: {
              screen: "OrderDetails",
              params: {
                orderId: params[0],
                id: params[0],
              },
            },
          },
        });
      }
    }
  };

  useEffect(() => {
    const getStripePublishableKey = async () => {
      try {
        const response = await fetch(
          API_LIST.BASE_URL + API_LIST.GET_STRIPE_PUBLISHABLE_KEY
        );
        if (!response.ok) {
          setStripePublishableKey(null);
          console.log("Unable to get stripe publishable key", err);
          return;
        }
        const publishableKey = await response.text();
        setStripePublishableKey(publishableKey);
      } catch (err) {
        setStripePublishableKey(null);
        console.log("Unable to get stripe publishable key", err);
      }
    };
    getStripePublishableKey();
  }, []);

  return (
    <StripeProvider publishableKey={stripePublishableKey}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <SafeAreaProvider>
            <NavigationProvider>
              <ThemeProvider theme={theme}>
                <NavigationContainer ref={navigationRef}>
                  <Stack.Navigator initialRouteName={"Auth"}>
                    <Stack.Screen
                      name="SplashScreen"
                      component={SplashScreen}
                      options={{ headerShown: false }}
                    />
                    <Stack.Screen
                      name="Auth"
                      component={Auth}
                      initialParams={{ url: url }}
                      options={{ headerShown: false }}
                    />
                    <Stack.Screen
                      name="DrawerNavigationRoutes"
                      component={DrawerNavigationRoutes}
                      options={{ headerShown: false }}
                    />
                  </Stack.Navigator>
                </NavigationContainer>
              </ThemeProvider>
            </NavigationProvider>
            <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
          </SafeAreaProvider>
        </PersistGate>
      </Provider>
    </StripeProvider>
  );
};

const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE,
};

export default codePush(codePushOptions)(App);
