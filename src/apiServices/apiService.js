import API_LIST from "./apiList";
import useAxios from "axios-hooks";
export const fetchAllStaffs = () =>
  useAxios(API_LIST.BASE_URL + API_LIST.GET_STAFFS);
