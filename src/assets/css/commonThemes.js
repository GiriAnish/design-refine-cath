const theme = {
  Colors: {
    purple: "#673ab7",
    white: "#ffffff",
    black: "#000000",
    Atomic: "#37474F",

    aliceBlue: "#e1f5fe",
    nero: "#212121",
    sandal: "#a38862",
    darkOrchid: "#9c27b0",
    oxfordBlue: "#263238",

    // dark green
    bismark: "#546e7a",
    sanJuan: "#455a64",
    bermudaGrey: "#78909c",
    pattensBlue: "#cfd8dc",
    hoki: "#607d8b",
    fruitSalad: "#4CAF50",

    dodgerBlue: "#2196f3",

    //Orange
    redOrange: "#f44336",
    sunsetOrange: "#ef5350",

    // light pink
    magnolia: "#ede7f6",
    selago: "#f3e5f5",
    amour: "#ffebee",
  },
  fontFamily: "Roboto",
};

export default theme;
