import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";

dayjs.extend(utc);

export const convertLocalDateFormat = (date) => {
  if (!date) return;
  return dayjs(date).format("DD-MMM-YYYY, h:mm A");
};

export const convertTime = (date) => {
  if (!date) return;
  return dayjs(date).format("h:mm A");
};

export const convertUTCtoLocal = (date) => {
  if (!date) return;
  return dayjs.utc(dayjs(date)).local().format("MM/DD/YYYY");
};

export const convertUTCtoAPI = (date) => {
  if (!date) return;
  return dayjs.utc(dayjs(date)).local().format("YYYY-MM-DD");
};

export const timeToNumber = (timeString) => {
  const [timeText, amPM] = timeString.split(" ");
  let offset = 0;
  if (amPM == "PM") {
    offset = 12;
  }
  const [hour, minute] = timeText.split(":");
  const hours = Number(hour) + offset;
  return hours * 100 + Number(minute);
}

export const timeCompare = (min, max, ref) => {
  return (
    timeToNumber(min) <= timeToNumber(convertTime(ref)) &&
    timeToNumber(convertTime(ref)) <= timeToNumber(max)
  );
}
