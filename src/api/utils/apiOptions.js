import { getToken, getRootUrl } from "./";

export const apiOptions = async ({
  params = {},
  url = "",
  method = "get",
  cancelToken,
  noToken,
  data,
  etag,
  headers,
} = {}) => {
  let rootUrl = "";
  if (!url.includes("http")) {
    rootUrl = await getRootUrl();
  }

  const options = {
    method: method.toLowerCase(),
    url: `${rootUrl}${url}`,
    headers: {
      "Content-Type": "application/json",
    },
    params,
  };

  if (etag) {
    options.headers["if-none-match"] = etag;
  }

  if (cancelToken) {
    options.cancelToken = cancelToken;
  }

  if (headers) {
    options.headers = { ...options.headers, ...headers };
  }

  if (!noToken) {
    const userToken = await getToken();
    options.headers.Authorization = `Bearer ${userToken}`;
  }

  switch (options.method) {
    case "post":
    case "patch":
    case "put":
      return {
        ...options,
        data,
      };
    case "delete":
    case "get":
    default:
      return options;
  }
};
