import { navigate } from "../utils";
//fix toasts
// import { createToast } from "@miview/toast";
// import { TOAST_TYPES } from "../constants";

const reportIssue = (error, url, toastMessage) => {
  const errorObj = {
    message: error?.response?.data?.errorBody,
    endpoint: url,
    error: true,
    code: error?.code,
    status: error?.response?.status,
    statusCode: error?.response?.status,
    errorBody:
      error?.response?.data?.errorBody ||
      "An error occured while updating or retrieving data",
  };

  if (process.env.NODE_ENV === "development") {
    console.log("Network Error: ", errorObj);
  }

  if (toastMessage?.message) {
    // createToast(
    //   toastMessage.message,
    //   toastMessage?.type ? toastMessage.type : TOAST_TYPES.ERROR
    // );
  } else if (!toastMessage?.hideMessage) {
    // createToast(errorObj.errorBody, TOAST_TYPES.ERROR);
  }

  return errorObj;
};

const handle401 = () => {
  //TODO: wire up toast messages
  //createToast("Session expired. Please log in.", "");
  navigate("Login");
};

export const handleError = (error, request, toastMessage = null) => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx

    const { status } = error.response;

    if (status === 401) {
      handle401();
    }
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
  } else {
    // Something happened in setting up the request that triggered an Error
  }

  return reportIssue(error, request.url, toastMessage);
};
