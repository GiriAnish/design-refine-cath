import { StackActions, DrawerActions } from "@react-navigation/native";
import React from "react";

export const navigationRef = React.createRef();

export const popToTop = (actions = StackActions) => {
  return navigationRef.current?.dispatch(actions.popToTop());
};

export const push = (routeName, params, actions = StackActions) => {
  return navigationRef.current?.dispatch(
    actions.push({
      routeName,
      params,
    })
  );
};

export const navigate = (name, params) => {
  navigationRef.current?.navigate(name, params);
};

export const getRouteName = () => {
  return navigationRef.current.getCurrentRoute().name;
};

export const openDrawer = () => {
  navigationRef.current?.dispatch(DrawerActions.openDrawer());
};
