import AsyncStorage from "@react-native-community/async-storage";

export const getUserId = async () => {
  const user = await AsyncStorage.getItem("user");

  return JSON.parse(user).userId;
};
