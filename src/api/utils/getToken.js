import AsyncStorage from "@react-native-community/async-storage";

export const getToken = async () => {
  return AsyncStorage.getItem("token");
};
