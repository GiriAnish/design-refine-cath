import AsyncStorage from "@react-native-community/async-storage";

export const getRootUrl = async () => {
  return AsyncStorage.getItem("url");
};
