import { apiOptions, apiRequest } from "../../utils";
import { API_V3 } from "../../constants";

export const communityService = {
  getAll: async ({ filters, sort, params, cancelToken }) => {
    const options = await apiOptions({
      url: `${API_V3}/communities`,
      method: "get",
      cancelToken,
      params,
      filters,
      sort,
    });

    return apiRequest(options);
  },

  get: async ({ id, cancelToken }) => {
    const options = await apiOptions({
      url: `${API_V3}/communities/${id}`,
      method: "get",
      cancelToken,
    });

    return apiRequest(options);
  },

  create: async ({ data, cancelToken }) => {
    const options = await apiOptions({
      url: `${API_V3}/communities`,
      method: "post",
      data,
      cancelToken,
    });

    const toastMessageConfig = {
      failure: {
        message: "Failure saving community",
      },
      success: {
        message: "Community saved successfully",
      },
    };
    return apiRequest(options, toastMessageConfig);
  },

  update: async ({ id, data, cancelToken }) => {
    const options = await apiOptions({
      url: `${API_V3}/communities/${id}`,
      method: "patch",
      data,
      cancelToken,
    });

    const toastMessageConfig = {
      failure: {
        message: "Failure saving community",
      },
      success: {
        message: "Community saved successfully",
      },
    };
    return apiRequest(options, toastMessageConfig);
  },

  delete: async ({ id, cancelToken }) => {
    const options = await apiOptions({
      url: `${API_V3}/communities/${id}`,
      method: "delete",
      cancelToken,
    });

    return apiRequest(options);
  },

  getContacts: async ({ id, cancelToken, params }) => {
    const options = await apiOptions({
      url: `${API_V3}/communities/${id}/contacts`,
      method: "get",
      params,
      cancelToken,
    });

    return apiRequest(options);
  },

  getBuilders: async ({ id, cancelToken }) => {
    const options = await apiOptions({
      url: `${API_V3}/communities/${id}/builders`,
      method: "get",
      cancelToken,
    });

    return apiRequest(options);
  },
};
