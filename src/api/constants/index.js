export const API_V2 = "/api/v2";
export const API_V3 = "/api/v3";

export const USER_TOKEN_NATIVE = "userToken";

export const USER_TOKEN = "userToken";

export const POST = "POST";
export const PATCH = "PATCH";
export const PUT = "PUT";
export const GET = "GET";
export const DELETE = "DELETE";

export const API_CONTENT_JSON = "application/json";

//TODO: Remove this constant and implementation when userService and gateway fixed
export const API_HEADERS_TEMP = {
  "Miview-TenantId": 1,
  "Miview-UserId": "c2eb3b1e-1a3c-4d9e-b1f2-f9a7d7b6559b",
  "Miview-Permissions": "1",
};

//TODO: Remove this url like the headers above at the same time
export const API_INTEGRATION_SERVICE_URL =
  "https://miv-func-integrationservice-dev-southcentralus-001.azurewebsites.net/api";

export const TOAST_TYPES = {
  ERROR: "error",
  WARNING: "warning",
  SUCCESS: "success",
  INFO: "info",
  LINK: "link",
};
