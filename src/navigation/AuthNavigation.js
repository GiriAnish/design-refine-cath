import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "../Screen/LoginScreen";
import Signup from "../Screen/DrawerScreens/Signup";
import RegistrationBuilder from "../Screen/DrawerScreens/RegistrationBuilder";
import RegForm from "../Screen/DrawerScreens/RegistrationForm";
import VerifyEmailSuccess from "../Screen/DrawerScreens/VerifyEmailSuccess";
import ForgotPassword from "../Screen/DrawerScreens/ForgotPassword";
import ForgotPasswordEmailSend from "../Screen/DrawerScreens/ForgotPasswordEmailSend";
import ForgotPasswordReset from "../Screen/DrawerScreens/ForgotPasswordReset";
import ForgotPasswordResetSuccess from "../Screen/DrawerScreens/ForgotPasswordResetSuccess";
import RegistrationCreatePassword from "../Screen/DrawerScreens/RegistrationCreatePassword";
import RegistrationAddPhoneNo from "../Screen/DrawerScreens/RegistrationAddPhoneNo";
import RegistrationSmsVerifyCode from "../Screen/DrawerScreens/RegistrationSmsVerifyCode";
import RegistrationSuccess from "../Screen/DrawerScreens/RegistrationSuccess";
import RegEmailVerify from "../Screen/DrawerScreens/RegEmailVerify";
import { HEADER_TITLE_FONT_SIZE } from "../data/const";

const Stack = createStackNavigator();

const Auth = (props) => {
  return (
    <Stack.Navigator
      initialRouteName={
        props?.route?.params?.url ? "VerifyEmail" : "LoginScreen"
      }
    >
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Registration"
        component={Signup}
        options={{
          title: "Registration",
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="RegistrationBuilder"
        component={RegistrationBuilder}
        options={{
          title: "Registration",
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="RegForm"
        component={RegForm}
        options={{
          title: "Registration",
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="RegEmailVerify"
        component={RegEmailVerify}
        options={{
          title: "Verify Email Address",
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="VerifyEmailSuccess"
        component={VerifyEmailSuccess}
        options={{
          title: "Verify Email Address",
          headerLeft:null,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="RegistrationCreatePassword"
        component={RegistrationCreatePassword}
        options={{
          title: "Create Password",
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="RegistrationAddPhoneNo"
        component={RegistrationAddPhoneNo}
        options={{
          title: "Add Phone Number",
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="RegistrationSmsVerifyCode"
        component={RegistrationSmsVerifyCode}
        options={{
          title: "Phone Number",
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="RegistrationSuccess"
        component={RegistrationSuccess}
        options={{
          title: "Registration Complete",
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />

      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{
          title: "Reset Password",
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 20,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="ForgotPasswordEmailSend"
        component={ForgotPasswordEmailSend}
        options={{
          title: "Check Email for Instructions",
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 6,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="ForgotPasswordReset"
        component={ForgotPasswordReset}
        options={{
          title: "Reset Password",
          headerLeft:null,
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 6,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="ForgotPasswordResetSuccess"
        component={ForgotPasswordResetSuccess}
        options={{
          title: "Reset Password",
         
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 6,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
    </Stack.Navigator>
  );
};

export default Auth;
