import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import theme from "../assets/css/commonThemes";
import HomeScreen from "../Screen/DrawerScreens/HomeScreen";
import SettingsScreen from "../Screen/DrawerScreens/SettingScreen";
import OurStaffScreen from "../Screen/DrawerScreens/OurStaff";
import OurStaffDetails from "../Screen/DrawerScreens/OurStaffDetails";
import CustomSidebarMenu from "../Screen/Components/CustomSidebarMenu";
import NavigationDrawerHeader from "../Screen/Components/NavigationDrawerHeader";
import TabNavigation from "./TabNavigation";
import SavedPizzaScreen from "../Screen/DrawerScreens/SavedPizza";
import MyOrders from "../Screen/DrawerScreens/MyOrders";
import { HEADER_TITLE_FONT_SIZE } from "../data/const";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const homeScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="HomeScreen">
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          title: "Home", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white, //Set Header color
          },
          headerTintColor: theme.Colors.sanJuan, //Set Header text color
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const settingScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="SettingsScreen"
        component={SettingsScreen}
        options={{
          title: "Settings",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white,
          },
          headerTintColor: theme.Colors.sanJuan,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const savedPizzaScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="SavedPizza"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerStyle: {
          backgroundColor: theme.Colors.white, //Set Header color
        },
        headerTintColor: theme.Colors.sanJuan, //Set Header text color
        headerTitleStyle: {
          fontWeight: "normal",
          fontSize: HEADER_TITLE_FONT_SIZE,
        },
      }}
    >
      <Stack.Screen
        name="SavedPizza"
        component={SavedPizzaScreen}
        options={{
          title: "Saved Pizzas",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white,
          },
          headerTintColor: theme.Colors.sanJuan,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};
const myOrdersScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="MyOrders"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerStyle: {
          backgroundColor: theme.Colors.white, //Set Header color
        },
        headerTintColor: theme.Colors.sanJuan, //Set Header text color
        headerTitleStyle: {
          fontWeight: "normal",
          fontSize: HEADER_TITLE_FONT_SIZE,
        },
      }}
    >
      <Stack.Screen
        name="MyOrders"
        component={MyOrders}
        options={{
          title: "My Orders",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white,
          },
          headerTintColor: theme.Colors.sanJuan,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const myLocationScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="SettingsScreen"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerStyle: {
          backgroundColor: theme.Colors.white, //Set Header color
        },
        headerTintColor: theme.Colors.sanJuan, //Set Header text color
        headerTitleStyle: {
          fontWeight: "normal",
          fontSize: HEADER_TITLE_FONT_SIZE,
        },
      }}
    >
      <Stack.Screen
        name="SettingsScreen"
        component={SettingsScreen}
        options={{
          title: "Settings", //Set Header Title
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const ourStaffScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="OurStaffScreen">
      <Stack.Screen
        name="OurStaffScreen"
        component={OurStaffScreen}
        options={{
          title: "Our Staff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white,
          },
          headerTintColor: theme.Colors.sanJuan,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};
const ourStaffDetailsScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="OurStaffDetailsScreen">
      <Stack.Screen
        name="OurStaffDetailsScreen"
        component={OurStaffDetails}
        options={{
          title: "Our Staff Details",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white,
          },
          headerTintColor: theme.Colors.sanJuan,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const DrawerNavigatorRoutes = (props) => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
        itemStyle: { marginVertical: 5 },
        drawerStyle: { backgroundColor: "transparent", width: "75%" },
      }}
      drawerContent={(props) => <CustomSidebarMenu {...props} />}
    >
      <Drawer.Screen
        name="homeScreenStack"
        options={{ drawerLabel: "Home" }}
        component={TabNavigation}
      />
      <Drawer.Screen
        name="homeStack"
        options={{ drawerLabel: "Home" }}
        component={homeScreenStack}
      />
      <Drawer.Screen
        name="savedPizzaScreenStack"
        options={{ drawerLabel: "Saved Pizzas" }}
        component={savedPizzaScreenStack}
      />
      <Drawer.Screen
        name="myOrdersScreenStack"
        options={{ drawerLabel: "My Orders" }}
        component={myOrdersScreenStack}
      />
      <Drawer.Screen
        name="myLocationScreenStack"
        options={{ drawerLabel: "My Locations" }}
        component={myLocationScreenStack}
      />
      <Drawer.Screen
        name="ourStaffScreenStack"
        options={{ drawerLabel: "Our Staff" }}
        component={ourStaffScreenStack}
      />
      <Drawer.Screen
        name="ourStaffDetailsScreenStack"
        options={{ drawerLabel: "Our Staff Details" }}
        component={OurStaffDetails}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigatorRoutes;
