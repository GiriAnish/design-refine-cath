import React, { useEffect, useState } from "react";
import styled from "@emotion/native";
import {
  createBottomTabNavigator,
  BottomTabBar,
} from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import theme from "../assets/css/commonThemes";
import HomeVipScreen from "../Screen/DrawerScreens/HomeVipScreen";
import SettingsScreen from "../Screen/DrawerScreens/SettingScreen";
import NavigationDrawerHeader from "../Screen/Components/NavigationDrawerHeader";
import OurStaffScreen from "../Screen/DrawerScreens/OurStaff";
import OurStaffDetails from "../Screen/DrawerScreens/OurStaffDetails";
import MyLocation from "../Screen/DrawerScreens/MyLocation";
import MyProfile from "../Screen/DrawerScreens/MyProfile";
import ChangeLanguage from "../Screen/DrawerScreens/ChangeLanguage";
import AboutApplication from "../Screen/DrawerScreens/AboutApplication";
import PrivacyPolicy from "../Screen/DrawerScreens/PrivacyPolicy";
import Notification from "../Screen/DrawerScreens/Notification";
import TermsAndCondition from "../Screen/DrawerScreens/TermsAndCondition";
import CustomisePizzaScreen from "../Screen/CustomisePizza";
import PizzaDetails from "../Screen/DrawerScreens/PizzaDetails";
import PizzaMenuScreen from "../Screen/PizzaMenuScreen";
import CartScreen from "../Screen/DrawerScreens/CartScreen";
import SavedPizzaScreen from "../Screen/DrawerScreens/SavedPizza";
import MyOrders from "../Screen/DrawerScreens/MyOrders";
import CheckoutScreen from "../Screen/DrawerScreens/CheckoutScreen";
import CheckoutVIPScreen from "../Screen/DrawerScreens/CheckoutVIPScreen";
import DeliveryAddress from "../Screen/DrawerScreens/DeliveryAddess";
import OrderSuccessScreen from "../Screen/DrawerScreens/OrderSuccess";
import DeliveryTrackingScreen from "../Screen/DrawerScreens/DeliveryTracking";
import OrderDetails from "../Screen/DrawerScreens/OrderDetails";
import CartTabIcon from "../Screen/Components/CartTabIcon";
import CartTabText from "../Screen/Components/CartTabText";
import Cog from "../Screen/Components/svg/Cog";
import Pizza from "../Screen/Components/svg/Pizza";
import Home from "../Screen/Components/svg/Home";
import OrderTab from "../Screen/Components/svg/OrderTab";
import ResetEmail from "../Screen/DrawerScreens/ResetEmail";
import EmailVerifyCode from "../Screen/DrawerScreens/EmailVerifyCode";
import SettingsResetPassword from "../Screen/DrawerScreens/SettingsResetPassword";
import SettingsResetPasswordSuccess from "../Screen/DrawerScreens/SettingsResetPasswordSuccess";
import { useTabVisible } from "../contexts/navigation";
import MakePayment from "../Screen/DrawerScreens/MakePayment";
import { HEADER_TITLE_FONT_SIZE } from "../data/const";
import { useSelector } from "react-redux";

const Stack = createStackNavigator();

const Tab = createBottomTabNavigator();
const PizzaIcon = styled((props) => <Pizza {...props} />)`
  transform: rotate(45deg);
`;
const homeScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="HomeVipScreen">
      <Stack.Screen
        name="HomeVipScreen"
        component={HomeVipScreen}
        options={{
          title: "Home", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white, //Set Header color
          },
          headerTintColor: theme.Colors.sanJuan, //Set Header text color
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};
const settingScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="SettingsScreen">
      <Stack.Screen
        name="SettingsScreen"
        component={SettingsScreen}
        options={{
          title: "Settings", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
        }}
      />
      <Stack.Screen
        name="MyLocations"
        component={MyLocation}
        options={{
          title: "My Location", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="ResetPassword"
        component={ResetEmail}
        options={{
          title: "Reset Password",
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 20,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="EmailVerifyCode"
        component={EmailVerifyCode}
        options={{
          title: "Check Email for Instructions",
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 20,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="SettingsResetPassword"
        component={SettingsResetPassword}
        options={{
          title: "Reset Password", //Set Header Title
          headerLeft: null,
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 20,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="SettingsResetPasswordSuccess"
        component={SettingsResetPasswordSuccess}
        options={{
          title: "Reset Password", //Set Header Title
          headerLeft: null,
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 20,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="MyProfile"
        component={MyProfile}
        options={{
          title: "My Profile", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="ChangeLanguage"
        component={ChangeLanguage}
        options={{
          title: "Change Language", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="AboutApplication"
        component={AboutApplication}
        options={{
          title: "About Application", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="PrivacyPolicy"
        component={PrivacyPolicy}
        options={{
          title: "Privacy Policy", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="Terms&Conditions" //Terms & Conditions
        component={TermsAndCondition}
        options={{
          title: "Terms & Condition", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="Notifications" //Terms & Conditions
        component={Notification}
        options={{
          title: "Notifications", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};
const cartScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="cartScreenHome"
      screenOptions={{
        headerStyle: {
          backgroundColor: theme.Colors.white, //Set Header color
        },
        headerTintColor: theme.Colors.sanJuan, //Set Header text color
        headerTitleStyle: {
          fontWeight: "normal", //Set Header text style
          fontSize: HEADER_TITLE_FONT_SIZE,
        },
      }}
    >
      <Stack.Screen
        name="cartScreenHome"
        component={CartScreen}
        options={{
          headerTitle: () => {
            return <CartTabText />;
          }, //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white, //Set Header color
          },
          headerTintColor: theme.Colors.sanJuan, //Set Header text color
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="checkoutVIPScreen"
        component={CheckoutVIPScreen}
        options={{
          title: "Checkout", //Set Header Title
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 20,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="checkoutScreenHome"
        component={CheckoutScreen}
        options={{
          title: "Checkout", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerStyle: {
            backgroundColor: theme.Colors.white, //Set Header color
          },
          headerTintColor: theme.Colors.sanJuan, //Set Header text color
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="deliveryAddressHome"
        component={DeliveryAddress}
        options={{
          title: "Delivery Address", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerStyle: {
            backgroundColor: theme.Colors.white, //Set Header color
          },
          headerTintColor: theme.Colors.sanJuan, //Set Header text color
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="MakePayment"
        component={MakePayment}
        options={{
          title: "Make Payment", //Set Header Title
          headerShown: true,
          headerStyle: {
            backgroundColor: "#673ab7",
            elevation: 0,
            fontSize: 20,
          },
          headerTintColor: "#fff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize: HEADER_TITLE_FONT_SIZE,
            fontWeight: "normal",
          },
        }}
      />
      <Stack.Screen
        name="orderSuccessScreen"
        component={OrderSuccessScreen}
        options={{
          title: "Order Confirmation", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: null,
          gestureEnabled: false,
          headerStyle: {
            backgroundColor: theme.Colors.white, //Set Header color
            elevation: 0,
          },
          headerTintColor: theme.Colors.sanJuan, //Set Header text color
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="OrderDetails"
        component={OrderDetails}
        options={{
          title: "Order Details", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="deliveryTrackingHome"
        component={DeliveryTrackingScreen}
        options={{
          title: "Delivery Tracking", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerStyle: {
            backgroundColor: theme.Colors.white, //Set Header color
          },
          headerTintColor: theme.Colors.sanJuan, //Set Header text color
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const ourStaffScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="OurStaffScreen">
      <Stack.Screen
        name="OurStaffScreen"
        component={OurStaffScreen}
        options={{
          title: "Our Staff",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white,
          },
          headerTintColor: theme.Colors.sanJuan,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="ourstaffdetails"
        component={OurStaffDetails}
        options={{
          title: "Our Staff", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const pizzaMenuStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="PizzaMenuScreenHome"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerStyle: {
          backgroundColor: theme.Colors.white, //Set Header color
        },
        headerTintColor: theme.Colors.sanJuan, //Set Header text color
        headerTitleStyle: {
          fontWeight: "normal", //Set Header text style
          fontSize: HEADER_TITLE_FONT_SIZE,
        },
      }}
    >
      <Stack.Screen
        name="PizzaMenuScreenHome"
        component={PizzaMenuScreen}
        options={{
          title: "Pizza Menu", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="pizzaScreenCustomise"
        component={CustomisePizzaScreen}
        options={{
          title: "Pizza", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="pizzaDetails"
        component={PizzaDetails}
        options={{
          title: "Details", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontWeight: "normal",
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};
const myOrdersScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="MyOrders"
      screenOptions={{
        headerStyle: {
          backgroundColor: theme.Colors.white, //Set Header color
        },
        headerTintColor: theme.Colors.sanJuan, //Set Header text color
        headerTitleStyle: {
          fontWeight: "normal", //Set Header text style
          fontSize: HEADER_TITLE_FONT_SIZE,
        },
      }}
    >
      <Stack.Screen
        name="MyOrders"
        component={MyOrders}
        options={{
          title: "My Orders", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="OrderDetails"
        component={OrderDetails}
        options={{
          title: "Order Details", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerStyle: {
            elevation: 0,
            shadowOpacity: 0,
            borderBottomWidth: 0,
          },
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
      <Stack.Screen
        name="deliveryTrackingHome"
        component={DeliveryTrackingScreen}
        options={{
          title: "Delivery Tracking", //Set Header Title
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerStyle: {
            backgroundColor: theme.Colors.white, //Set Header color
          },
          headerTintColor: theme.Colors.sanJuan, //Set Header text color
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const MyProfileScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="MyProfileScreen">
      <Stack.Screen
        name="MyProfileScreen"
        component={MyProfile}
        options={{
          title: "My Profile",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white,
          },
          headerTintColor: theme.Colors.sanJuan,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const MyLocationScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="MyLocationScreen">
      <Stack.Screen
        name="MyLocationScreen"
        component={MyLocation}
        options={{
          title: "My Location",
          headerTitleAlign: "center",
          headerBackTitleVisible: false,
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: theme.Colors.white,
          },
          headerTintColor: theme.Colors.sanJuan,
          headerTitleStyle: {
            fontWeight: "normal", //Set Header text style
            fontSize: HEADER_TITLE_FONT_SIZE,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const BottomTabNavigator = (props) => {
  const { tabVisible } = useTabVisible();
  const [isTab, setIsTrue] = useState(false);
  const loginInfo = useSelector((state) => state.loggedInUser.loggedInUser);

  useEffect(() => {
    setIsTrue(tabVisible);
  }, [tabVisible]);

  function isOrderingFeatureUnavailable() {
    return (
      loginInfo?.userDetails?.maxItemPerCart === 0 ||
      loginInfo?.userDetails?.maxOrderPerDay === 0
    );
  }

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          height: 70,
          borderTopWidth: 0,
          elevation: 0,
          backgroundColor: "#fff",
        },
      }}
      tabBar={(tabProps) => {
        if (!isTab) {
          return null;
        }
        return <BottomTabBar {...tabProps} />;
      }}
    >
      <Tab.Screen
        name="Home"
        component={homeScreenStack}
        options={{
          tabBarLabel: "Home",
          tabBarIcon: ({ focused }) => {
            if (!focused) {
              return <Home width={24} height={24} color={"#607d8b"} />;
            }
            return <Home width={24} height={24} color={theme.Colors.purple} />;
          },
          tabBarActiveBackgroundColor: theme.Colors.magnolia,
          tabBarActiveTintColor: theme.Colors.purple,
          tabBarItemStyle: {
            borderRadius: 5,
            height: 60,
            paddingVertical: 8,
            marginTop: 5,
            marginLeft: 10,
          },
        }}
      />
      {!isOrderingFeatureUnavailable() && (
        <>
          <Tab.Screen
            name="PizzaMenu"
            component={pizzaMenuStack}
            listeners={({ navigation }) => ({
              tabPress: (e) => {
                e.preventDefault();
                navigation.navigate("DrawerNavigationRoutes", {
                  screen: "homeScreenStack",
                  params: {
                    screen: "PizzaMenu",
                    params: {
                      screen: "PizzaMenuScreenHome",
                    },
                  },
                });
              },
            })}
            options={{
              tabBarLabel: "Pizza",
              tabBarIcon: ({ focused }) => {
                if (!focused) {
                  return <PizzaIcon width={24} height={24} color={"#607d8b"} />;
                }
                return (
                  <PizzaIcon
                    width={24}
                    height={24}
                    color={theme.Colors.purple}
                  />
                );
              },
              tabBarActiveBackgroundColor: theme.Colors.magnolia,
              tabBarActiveTintColor: theme.Colors.purple,
              tabBarItemStyle: {
                borderRadius: 5,
                height: 60,
                paddingVertical: 8,
                marginTop: 5,
              },
            }}
          />
          <Tab.Screen
            name="Cart"
            component={cartScreenStack}
            listeners={({ navigation }) => ({
              tabPress: (e) => {
                e.preventDefault();
                navigation.navigate("DrawerNavigationRoutes", {
                  screen: "homeScreenStack",
                  params: {
                    screen: "Cart",
                    params: {
                      screen: "cartScreenHome",
                    },
                  },
                });
              },
            })}
            options={{
              tabBarLabel: "Cart",
              tabBarIcon: ({ focused }) => {
                return <CartTabIcon focused={focused} />;
              },
              tabBarActiveBackgroundColor: theme.Colors.magnolia,
              tabBarActiveTintColor: theme.Colors.purple,
              tabBarItemStyle: {
                borderRadius: 5,
                height: 60,
                paddingVertical: 8,
                marginTop: 5,
              },
            }}
          />
          <Tab.Screen
            name="OrdersMenu"
            component={myOrdersScreenStack}
            listeners={({ navigation }) => ({
              tabPress: (e) => {
                e.preventDefault();
                navigation.navigate("DrawerNavigationRoutes", {
                  screen: "homeScreenStack",
                  params: {
                    screen: "OrdersMenu",
                    params: {
                      screen: "MyOrders",
                    },
                  },
                });
              },
            })}
            options={{
              tabBarLabel: "Orders",
              tabBarIcon: ({ focused }) => {
                if (!focused) {
                  return <OrderTab width={24} height={24} color={"#607d8b"} />;
                }
                return (
                  <OrderTab
                    width={24}
                    height={24}
                    color={theme.Colors.purple}
                  />
                );
              },
              tabBarActiveBackgroundColor: theme.Colors.magnolia,
              tabBarActiveTintColor: theme.Colors.purple,
              tabBarItemStyle: {
                borderRadius: 5,
                height: 60,
                paddingVertical: 8,
                marginTop: 5,
              },
            }}
          />
        </>
      )}

      <Tab.Screen
        name="Settings"
        component={settingScreenStack}
        options={{
          tabBarLabel: "Settings",
          tabBarIcon: ({ focused }) => {
            if (!focused) {
              return <Cog width={24} height={24} color={"#607d8b"} />;
            }
            return <Cog width={24} height={24} color={theme.Colors.purple} />;
          },
          tabBarActiveBackgroundColor: theme.Colors.magnolia,
          tabBarActiveTintColor: theme.Colors.purple,
          tabBarItemStyle: {
            borderRadius: 5,
            height: 60,
            paddingVertical: 8,
            marginTop: 5,
            marginRight: 10,
          },
        }}
      />
      <Tab.Screen
        name="Staff"
        component={ourStaffScreenStack}
        options={{
          tabBarButton: () => null,
          tabBarVisible: false,
        }}
      />
      <Tab.Screen
        name="MyOrders"
        component={myOrdersScreenStack}
        options={{
          tabBarButton: () => null,
          tabBarVisible: true,
        }}
      />
      <Tab.Screen
        name="OrderDetails"
        component={OrderDetails}
        options={{
          tabBarButton: () => null,
          tabBarVisible: true,
        }}
      />
      <Tab.Screen
        name="MyProfileScreenStack"
        component={MyProfileScreenStack}
        options={{
          tabBarButton: () => null,
          tabBarVisible: false,
        }}
      />
      <Tab.Screen
        name="MyLocationScreenStack"
        component={MyLocationScreenStack}
        options={{
          tabBarButton: () => null,
          tabBarVisible: false,
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;
