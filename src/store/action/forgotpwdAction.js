import * as errorAction from "./error";
import * as loadingAction from "./loading";
import API_LIST from "../../apiServices/apiList";

export const PASSWORD_RESET_REQUEST = "PASSWORD_RESET_REQUEST";
export const PASSWORD_RESET_REQUEST_DEFAULT = "PASSWORD_RESET_REQUEST_DEFAULT";
export const VALIDATE_OTP = "VALIDATE_OTP";
export const VALIDATE_OTP_RESET = "VALIDATE_OTP_RESET";
export const SET_USER_NEWPASSWORD = "SET_USER_NEWPASSWORD";
export const SET_USER_NEWPASSWORD_TO_DEFAULT =
  "SET_USER_NEWPASSWORD_TO_DEFAULT";
export const RESET_FORGET_ALL_PASSWORD = "RESET_FORGET_ALL_PASSWORD";

export const pwdResetReq = (emailId) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: PASSWORD_RESET_REQUEST_DEFAULT });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.PASSWORD_RESET_REQUEST + "/" + emailId,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: PASSWORD_RESET_REQUEST_DEFAULT,
          passwordResetReq: (await response.text()) || null,
        });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: PASSWORD_RESET_REQUEST, passwordResetReq: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};

export const otpVerify = (emailId, otp) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: VALIDATE_OTP_RESET });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.VALIDATE_OTP + "/" + emailId + "/" + otp,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: VALIDATE_OTP_RESET,
          validateOTPRes: (await response.text()) || null,
        });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: VALIDATE_OTP, validateOTPRes: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};

export const resetPwdCreation = (emailid, payload) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(payload),
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: SET_USER_NEWPASSWORD_TO_DEFAULT });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.SET_USER_NEWPASSWORD + "/" + emailid,
        requestOptions
      );
      if (!response.ok) {
        try {
          dispatch(errorAction.setError(response));
          dispatch(loadingAction.setLoading(false));
          dispatch({
            type: SET_USER_NEWPASSWORD_TO_DEFAULT,
            passwordResetResponse: (await response.text()) || null,
          });
        } catch (err) {
          dispatch(errorAction.setError(err));
          throw err;
        }
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_USER_NEWPASSWORD, passwordResetResponse: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
