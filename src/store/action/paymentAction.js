import * as errorAction from "./error";
import * as loadingAction from "./loading";
import API_LIST from "../../apiServices/apiList";

export const GET_CLIENT_SECRET = "GET_CLIENT_SECRET";
export const RESET_CLIENT_SECRET = "RESET_CLIENT_SECRET";
export const PAYMENT_CONFIRMED = "PAYMENT_CONFIRMED";
export const RESET_PAYMENT_CONFIRMED = "RESET_PAYMENT_CONFIRMED";
export const GET_CLIENT_PAYMENT_CARDS = "GET_CLIENT_PAYMENT_CARDS";
export const RESET_CLIENT_PAYMENT_CARDS = "RESET_CLIENT_PAYMENT_CARDS";

export const getPaymentIntent = (orderUUID, jwtToken) => {
  const requestParams = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwtToken}`,
    },
    body: JSON.stringify({ orderUUID: orderUUID }),
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_CLIENT_SECRET });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL}${API_LIST.GET_PAYMENT_INTENT}`,
        requestParams
      );
      
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: RESET_CLIENT_SECRET, paymentIntentDetails: (await response.text()) } );
      } else {
        const resData = await response.json();
        const data = resData;

        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: GET_CLIENT_SECRET,
          paymentIntentDetails: data,
        });
      }
    } catch (err) {
      dispatch(errorAction.setError(err));
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: RESET_CLIENT_SECRET });
      throw err;
    }
  };
};
export const confirmOrderPayment = (orderUUID, jwtToken) => {
  const requestParams = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwtToken}`,
    },
    body: JSON.stringify({
      orderUuid: orderUUID,
    })
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_PAYMENT_CONFIRMED });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL}${API_LIST.PAYMENT_CONFIRMED}`,
        requestParams
      );
      
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        console.log()
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: RESET_PAYMENT_CONFIRMED, paymentConfirmed: (await response.text()) } );
      } else {
        const resData = await response.json();
        const data = resData;

        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: PAYMENT_CONFIRMED,
          paymentConfirmed: data,
        });
      }
    } catch (err) {
      dispatch(errorAction.setError(err));
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: RESET_PAYMENT_CONFIRMED });
      throw err;
    }
  };
};
export const getSavedCards = (jwtToken) => {
  const requestParams = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwtToken}`,
    }
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_CLIENT_PAYMENT_CARDS });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL}${API_LIST.GET_PAYMENT_CARDS}`,
        requestParams
      );
      console.log(response);
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: RESET_CLIENT_PAYMENT_CARDS });
      } else {
        const resData = await response.json();
        const data = resData;

        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: GET_CLIENT_PAYMENT_CARDS,
          cards: data,
        });
      }
    } catch (err) {
      dispatch(errorAction.setError(err));
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: RESET_CLIENT_PAYMENT_CARDS });
      throw err;
    }
  };
};