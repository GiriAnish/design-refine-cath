export const ADD_ITEMS_TO_CART = "ADD_ITEMS_TO_CART";
export const RESET_CART = "RESET_CART";
export const REMOVE_ITEMS_FROM_CART = "REMOVE_ITEMS_FROM_CART";
export const UPDATE_ITEMS_TO_CART = "UPDATE_ITEMS_TO_CART";

export const addItemsToCart = (item) => {
  return {
    type: ADD_ITEMS_TO_CART,
    cartItems: item,
  };
};

export const resetCart = () => {
  return {
    type: RESET_CART,
  };
};

export const removeItemsFromCart = (id) => {
  return {
    type: REMOVE_ITEMS_FROM_CART,
    itemUuid: id,
  };
};

export const updateItemsToCart = (item) => {
  return {
    type: UPDATE_ITEMS_TO_CART,
    cartItem: item,
  };
};
