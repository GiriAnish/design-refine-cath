import * as errorAction from "./error";
import * as loadingAction from "./loading";
import API_LIST from "../../apiServices/apiList";

export const SET_PIZZA_MENU = "SET_PIZZA_MENU";
export const RESET_PIZZA_MENU = "RESET_PIZZA_MENU";

export const fetchPizzaMenu = (menuId, pageNo = 1, size = 100) => {
  const params = `?menuId=${menuId}&itemType=standard&pageNo=${pageNo}&size=${size}`;
  const requestUrl = API_LIST.BASE_URL + API_LIST.PIZZA_MENU + params;
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: SET_PIZZA_MENU, pizzaMenu: [] });
      dispatch(errorAction.resetError());
      const response = await fetch(requestUrl);
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: SET_PIZZA_MENU, pizzaMenu: [] });
        //throw new Error('Something went wrong');
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_PIZZA_MENU, pizzaMenu: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};

export const resetPizzaMenu = () => {
  return {
    type: RESET_PIZZA_MENU,
  };
};
