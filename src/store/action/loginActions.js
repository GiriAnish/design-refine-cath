import * as errorAction from "./error";
import * as loadingAction from "./loading";
import API_LIST from "../../apiServices/apiList";

export const SET_USER_LOGIN = "SET_USER_LOGIN";
const getResponseData = async (response)=>{
  try {
    const responseContentType = response.headers.get("content-type");
    if (responseContentType.includes('json')) {
      const responseData =  await response.json();
      return responseData;
    }
    if (responseContentType.includes('text/plain')) {
      const message = await response.text()
      return {
        message
      }
    }

  } catch (error) {
    return {
      message: 'Internal Server Error'
    }
  }
}
export const getUserLogin = (userName, password) => {
  const payload = {
    userName: userName,
    password: password,
  };
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(payload),
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_USER_LOGIN });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.USER_LOGIN,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: RESET_USER_LOGIN, loggedInUser: null });
        //throw new Error('Something went wrong');
      }
      const data = await getResponseData(response);
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_USER_LOGIN, loggedInUser: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
export const RESET_USER_LOGIN = "RESET_USER_LOGIN";
export const resetUserLogin = () => {
  return {
    type: RESET_USER_LOGIN,
  };
};
