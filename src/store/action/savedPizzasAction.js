import * as errorAction from "./error";
import * as loadingAction from "./loading";
import API_LIST from "../../apiServices/apiList";

export const SET_SAVED_PIZZAS = "SET_SAVED_PIZZAS";
export const fetchSavedPizzas = (contactId, pageNo = 1, size = 100) => {
  const requestParams = `?orderTypes=favorite,standard&contactId=${contactId}&pageNo=${pageNo}&size=${size}`;
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_SAVED_PIZZAS });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.PIZZA_ORDERS + requestParams
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: SET_SAVED_PIZZAS, savedPizzas: [] });
        //throw new Error('Something went wrong');
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({
        type: SET_SAVED_PIZZAS,
        savedPizzas: data?.length ? data[0] : [],
      });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
export const RESET_SAVED_PIZZAS = "RESET_SAVED_PIZZAS";
export const resetSavedPizzas = () => {
  return {
    type: RESET_SAVED_PIZZAS,
  };
};
