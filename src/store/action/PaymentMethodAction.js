import * as errorAction from "./error";
import * as loadingAction from "./loading";
import API_LIST from "../../apiServices/apiList";

export const GET_LOCATION = "GET_LOCATION";
export const SET_DEFAULT_LOCALLY = "SET_DEFAULT_LOCALLY";

export const getLocation = (token) => {
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch(errorAction.resetError());
      const response = await fetch(API_LIST.BASE_URL + API_LIST.LOCATION, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: GET_LOCATION, locations: null });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: GET_LOCATION, locations: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      //throw err;
    }
  };
};

export const setDefaultLocally = (id, token) => {
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.LOCATION + "/" + id,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-type": "application/json; charset=UTF-8",
          },
          body: JSON.stringify({
            isDefault: true,
          }),
        }
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_DEFAULT_LOCALLY, id: id });
    } catch (err) {
      dispatch(errorAction.setError(err));
      dispatch(loadingAction.setLoading(false));
      //throw err;
    }
  };
};

export const setLocally = (id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: SET_DEFAULT_LOCALLY, id: id });
    } catch (err) {
      dispatch(errorAction.setError(err));
      dispatch(loadingAction.setLoading(false));
      //throw err;
    }
  };
};
