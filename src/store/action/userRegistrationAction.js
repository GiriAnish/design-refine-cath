import * as errorAction from "./error";
import * as loadingAction from "./loading";
import API_LIST from "../../apiServices/apiList";

export const SET_NEW_USER = "SET_NEW_USER";
export const setNewUser = (payload) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(payload),
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_NEW_USER });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.SIGNUP,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: RESET_NEW_USER, registeredUser: (await response.text()) });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_NEW_USER, registeredUser: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
export const SET_NEW_PASSWORD = "SET_NEW_PASSWORD";
export const createPassword = (payload, userId) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(payload),
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_NEW_PASSWORD });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.CREATEPASSWORD + "/" + userId,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: RESET_NEW_PASSWORD,
          createPasswordRes: (await response.text()) || null,
        });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_NEW_PASSWORD, createPasswordRes: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
export const RESEND_EMAIL_VERIFICATION = "RESEND_EMAIL_VERIFICATION";
export const resendEmailVerification = (email) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_EMAIL_RESP });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.RESEND_EMAIL + `?email=${email}`,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: RESET_EMAIL_RESP,
          emailResp: (await response.text()) || null,
        });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: RESEND_EMAIL_VERIFICATION, emailResp: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
export const SET_PHONE_NUMBER = "SET_PHONE_NUMBER";
export const addPhoneNumber = (phoneNo, userId) => {
  // For demo chnage this needs to handled
  var numberPattern = /\d+/g;
  phoneNo = phoneNo
    .match(numberPattern)
    .toString()
    .replace(",", "")
    .replace(",", "");
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_PHONE_NUMBER });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL +
          API_LIST.UPDATE_PHONE_NUMBER +
          "/" +
          userId +
          "/" +
          phoneNo,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: RESET_PHONE_NUMBER,
          phoneResp: (await response.text()) || null,
        });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_PHONE_NUMBER, phoneResp: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
export const VERIFY_PHONE_OTP = "VERIFY_PHONE_OTP";
export const smsOtpVerification = (userId, phoneNo, phoneOTP) => {
  // For demo chnage this needs to handled
  var numberPattern = /\d+/g;
  phoneNo = phoneNo
    .match(numberPattern)
    .toString()
    .replace(",", "")
    .replace(",", "");
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_VERIFY_PHONE_OTP });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL +
          API_LIST.UPDATE_PHONE_NUMBER +
          "/" +
          userId +
          "/" +
          phoneNo +
          "/" +
          phoneOTP,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: RESET_VERIFY_PHONE_OTP,
          otpResp: (await response.text()) || null,
        });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: VERIFY_PHONE_OTP, otpResp: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
export const RESEND_PHONE_OTP_VERIFICATION = "RESEND_PHONE_OTP_VERIFICATION";
export const resendPhoneOtpVerification = (email, phoneNumber) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_RESEND_PHONE_OTP });
      dispatch(errorAction.resetError());

      const response = await fetch(
        API_LIST.BASE_URL +
          API_LIST.RESEND_PHONE_OTP +
          `?email=${email}` +
          `&phoneNumber=${phoneNumber}`,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: RESET_RESEND_PHONE_OTP,
          otpResendResp: (await response.text()) || null,
        });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: RESEND_PHONE_OTP_VERIFICATION, otpResendResp: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};

//verify email otp

export const VERIFY_EMAIL_OTP_ACTION = "VERIFY_EMAIL_OTP_ACTION";
export const emailOtpVerify = (emailId, otp) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    // body: JSON.stringify("")
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_EMAIL_OTP_ACTION });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL}${API_LIST.VERIFY_EMAIL_OTP}/${emailId}/${otp}`,
        requestOptions
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: RESET_EMAIL_OTP_ACTION,
          verifyEmailOtp: (await response.text()) || null,
        });
        //throw new Error('Something went wrong');
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: VERIFY_EMAIL_OTP_ACTION, verifyEmailOtp: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};

export const SET_TERMS_AND_CONDITION = "SET_TERMS_AND_CONDITION";
export const setTermsAndCondition = () => {
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_TERMS_AND_CONDITION });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.TERMS_AND_CONDITION
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: RESET_TERMS_AND_CONDITION, userTerms: null });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_TERMS_AND_CONDITION, userTerms: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
//privacy policy

export const SET_PRIVACY_POLICY = "SET_PRIVACY_POLICY";
export const setPrivacyPolicy = () => {
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_PRIVACY_POLICY });
      dispatch(errorAction.resetError());
      const response = await fetch(API_LIST.BASE_URL + API_LIST.PRIVACY_POLICY);
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: RESET_PRIVACY_POLICY, userPrivacy: null });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_PRIVACY_POLICY, userPrivacy: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};
//vip membership benifits
export const SET_VIP_MEMBERSHIP_BENIFITS = "SET_VIP_MEMBERSHIP_BENIFITS";
export const setVipmembershipbenifits = () => {
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_VIP_MEMBERSHIP_BENIFITS });
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.VIP_MEMBERSHIP_BENIFITS
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({
          type: RESET_VIP_MEMBERSHIP_BENIFITS,
          userVipbenifits: null,
        });
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_VIP_MEMBERSHIP_BENIFITS, userVipbenifits: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
      throw err;
    }
  };
};

export const RESET_NEW_USER = "RESET_NEW_USER";
export const resetNewUser = () => {
  return {
    type: RESET_NEW_USER,
  };
};
export const RESET_NEW_PASSWORD = "RESET_NEW_PASSWORD";
export const RESET_EMAIL_RESP = "RESET_EMAIL_RESP";
export const RESET_PHONE_NUMBER = "RESET_PHONE_NUMBER";
export const RESET_VERIFY_PHONE_OTP = "RESET_VERIFY_PHONE_OTP";
export const RESET_RESEND_PHONE_OTP = "RESET_RESEND_PHONE_OTP";
export const RESET_EMAIL_OTP_ACTION = "RESET_EMAIL_OTP_ACTION";
export const RESET_ALL_USER_REGISTRATION = "RESET_ALL_USER_REGISTRATION";
export const RESET_TERMS_AND_CONDITION = "RESET_TERMS_AND_CONDITION";
export const REGISTRATION_ERROR = "REGISTRATION_ERROR";
