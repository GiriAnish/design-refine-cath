import * as errorAction from "./error";
import * as loadingAction from "./loading";
import API_LIST from "../../apiServices/apiList";

//my order list
export const SET_MY_ORDERS = "SET_MY_ORDERS";
export const fetchMyOrders = (userId) => {
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_MY_ORDERS });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL}${API_LIST.PIZZA_ORDERS}/${userId}`
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch(loadingAction.setLoading(false));
        dispatch({ type: RESET_MY_ORDERS, myOrders: null });
        return;
      }
      const resData = await response.json();
      const data = resData;
      dispatch(loadingAction.setLoading(false));
      dispatch({ type: SET_MY_ORDERS, myOrders: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
    } finally {
      dispatch(loadingAction.setLoading(false));
    }
  };
};
//get one order detail
export const GET_ONE_ORDER_DETAIL = "GET_ONE_ORDER_DETAIL";
export const getOneOrderDetail = (orderId) => {
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_GET_ONE_ORDER_DETAILS });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL}${API_LIST.PIZZA_ORDER_DETAIL}/${orderId}`
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch({ type: RESET_GET_ONE_ORDER_DETAILS, getOneOrderDetail: [] });
        return;
      }
      const resData = await response.json();
      const data = resData;
      dispatch({ type: GET_ONE_ORDER_DETAIL, getOneOrderDetail: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
    } finally {
      dispatch(loadingAction.setLoading(false));
    }
  };
};

//create order
export const CREATE_NEW_ORDER = "CREATE_NEW_ORDER";
export const createNewOrder = (payload, jwtToken) => {
  const requestParams = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwtToken}`,
    },
    body: JSON.stringify(payload),
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch(errorAction.resetError());
      const response = await fetch(
        API_LIST.BASE_URL + API_LIST.PIZZA_NEW_ORDER,
        requestParams
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        return;
      }
      const resData = await response.json();
      const data = resData;
      dispatch({ type: CREATE_NEW_ORDER, newOrder: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
    } finally {
      dispatch(loadingAction.setLoading(false));
    }
  };
};

//cancel order
export const CANCEL_ORDER_CASE = "CANCEL_ORDER_CASE";
export const cancelOrder = (orderId) => {
  const requestParams = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      // dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_CANCEL_ORDER });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL + API_LIST.PIZZA_CANCEL_ORDER}/${orderId}`,
        requestParams
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch({ type: RESET_CANCEL_ORDER });
        return;
      }
      const resData = await response.json();
      const data = resData;
      dispatch({ type: CANCEL_ORDER_CASE, cancelOrder: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
    } finally {
      dispatch(loadingAction.setLoading(false));
    }
  };
};

//Re-order
export const SET_RE_ORDER = "SET_RE_ORDER";
export const reOrder = (orderId) => {
  const requestParams = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_RE_ORDER });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL + API_LIST.PIZZA_RE_ORDER}/${orderId}`,
        requestParams
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch({
          type: RESET_RE_ORDER,
          reOrder: (await response.text()) || null,
        });
        return;
      }
      const resData = await response.json();
      const data = resData;
      dispatch({ type: SET_RE_ORDER, reOrder: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
    } finally {
      dispatch(loadingAction.setLoading(false));
    }
  };
};

//track order
export const TRACK_ORDER_CASE = "TRACK_ORDER_CASE";
export const trackOrder = (orderId) => {
  const requestParams = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_TRACK_ORDER });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL}${API_LIST.PIZZA_TRACK_ORDER}/${orderId}`,
        requestParams
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch({ type: RESET_TRACK_ORDER });
        return;
      }
      const resData = await response.json();
      const data = resData;
      dispatch({ type: TRACK_ORDER_CASE, trackOrder: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
    } finally {
      dispatch(loadingAction.setLoading(false));
    }
  };
};

//Check whether selected date is valid or not
//create order
export const ORDER_DATE_VALID_OR_NOT = "ORDER_DATE_VALID_OR_NOT";
export const orderDateIsValidOrNot = (deliveryDate, jwtToken) => {
  const requestParams = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwtToken}`,
    },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_ORDER_DATE_VALID_OR_NOT });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${
          API_LIST.BASE_URL + API_LIST.PIZZA_VALIDATE_DATE
        }?deliveryDate=${deliveryDate}`,
        requestParams
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch({
          type: RESET_ORDER_DATE_VALID_OR_NOT,
          isValidDate: (await response.text()) || null,
        });
        return;
      }
      dispatch({ type: ORDER_DATE_VALID_OR_NOT, isValidDate: true });
    } catch (err) {
      dispatch(errorAction.setError(err));
    } finally {
      dispatch(loadingAction.setLoading(false));
    }
  };
};

//Check VIP code
export const CHECK_VIP_CODE = "CHECK_VIP_CODE";
export const checkVIPCode = (id, code) => {
  const requestParams = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch({ type: RESET_VIP_CODE });
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL}${API_LIST.CHECK_VIP_CODE}?userId=${id}&currentCode=${code}`,
        requestParams
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch({ type: RESET_VIP_CODE, vipCode: await response.text() });
        return;
      }
      dispatch({ type: CHECK_VIP_CODE, vipCode: true });
    } catch (err) {
      dispatch(errorAction.setError(err));
      dispatch({ type: RESET_VIP_CODE, vipCode: err });
    } finally {
      dispatch(loadingAction.setLoading(false));
    }
  };
};

export const UPDATE_TEMP_ORDER = "UPDATE_TEMP_ORDER";
export const RESET_UPDATE_TEMP_ORDER = "RESET_UPDATE_TEMP_ORDER";
export const updateTempOrder = (orderId, updatedOrder, jwtToken) => {
  const requestParams = {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${jwtToken}`,
    },
    body: JSON.stringify(updatedOrder),
  };
  return async (dispatch) => {
    try {
      dispatch(loadingAction.setLoading(true));
      dispatch(errorAction.resetError());
      const response = await fetch(
        `${API_LIST.BASE_URL}${API_LIST.PIZZA_NEW_ORDER}/${orderId}`,
        requestParams
      );
      if (!response.ok) {
        dispatch(errorAction.setError(response));
        dispatch({
          type: RESET_UPDATE_TEMP_ORDER,
          updatedOrder: (await response.text()) || null,
        });
        return;
      }
      const resData = await response.json();
      const data = resData;
      dispatch({ type: UPDATE_TEMP_ORDER, updatedOrder: data });
    } catch (err) {
      dispatch(errorAction.setError(err));
    } finally {
      dispatch(loadingAction.setLoading(false));
    }
  };
};

export const RESET_ORDER_DATE_VALID_OR_NOT = "RESET_ORDER_DATE_VALID_OR_NOT";
export const RESET_TRACK_ORDER = "RESET_TRACK_ORDER";
export const RESET_GET_ONE_ORDER_DETAILS = "RESET_GET_ONE_ORDER_DETAILS";
export const RESET_CANCEL_ORDER = "RESET_CANCEL_ORDER";
export const RESET_MY_ORDERS = "RESET_MY_ORDERS";
export const RESET_RE_ORDER = "RESET_RE_ORDER";
export const resetSavedPizzas = () => {
  return {
    type: RESET_MY_ORDERS,
  };
};
export const RESET_NEW_ORDER = "RESET_NEW_ORDER";
export const resetNewOrder = () => {
  return {
    type: RESET_NEW_ORDER,
  };
};
export const RESET_VIP_CODE = "RESET_VIP_CODE";
export const resetVIPCode = () => {
  return {
    type: RESET_VIP_CODE,
  };
};
