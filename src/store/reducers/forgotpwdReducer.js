import {
  PASSWORD_RESET_REQUEST,
  VALIDATE_OTP,
  SET_USER_NEWPASSWORD,
  PASSWORD_RESET_REQUEST_DEFAULT,
  VALIDATE_OTP_RESET,
  SET_USER_NEWPASSWORD_TO_DEFAULT,
  RESET_FORGET_ALL_PASSWORD,
} from "../action/forgotpwdAction";

const initialState = {
  passwordResetReq: {
    errorBody: null,
    successBody: null,
  },
  validateOTPRes: {
    errorBody: null,
    successBody: null,
  },
  passwordResetResponse: {
    errorBody: null,
    successBody: null,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PASSWORD_RESET_REQUEST:
      return {
        ...state,
        passwordResetReq: {
          ...state.passwordResetReq,
          successBody: action.passwordResetReq,
          errorBody: null,
        },
      };
    case PASSWORD_RESET_REQUEST_DEFAULT:
      return {
        ...state,
        passwordResetReq: {
          ...state.passwordResetReq,
          successBody: null,
          errorBody: action.passwordResetReq,
        },
      };

    case VALIDATE_OTP:
      return {
        ...state,
        validateOTPRes: {
          ...state.validateOTPRes,
          successBody: action.validateOTPRes,
          errorBody: null,
        },
      };
    case VALIDATE_OTP_RESET:
      return {
        ...state,
        validateOTPRes: {
          ...state.validateOTPRes,
          successBody: null,
          errorBody: action.validateOTPRes,
        },
      };
    case SET_USER_NEWPASSWORD:
      return {
        ...state,
        passwordResetResponse: {
          ...state.passwordResetResponse,
          successBody: action.passwordResetResponse,
          errorBody: null,
        },
      };
    case SET_USER_NEWPASSWORD_TO_DEFAULT:
      return {
        ...state,
        passwordResetResponse: {
          ...state.passwordResetResponse,
          errorBody: action.passwordResetResponse,
          successBody: null,
        },
      };
    case RESET_FORGET_ALL_PASSWORD:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
