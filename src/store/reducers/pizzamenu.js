import { SET_PIZZA_MENU, RESET_PIZZA_MENU } from "../action/pizzamenu";

const initialState = {
  pizzaMenu: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PIZZA_MENU:
      return {
        pizzaMenu: action.pizzaMenu,
      };
    case RESET_PIZZA_MENU:
      return {
        pizzaMenu: [],
      };
  }
  return state;
};
