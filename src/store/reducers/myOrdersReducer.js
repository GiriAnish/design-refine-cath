import {
  SET_MY_ORDERS,
  RESET_MY_ORDERS,
  RESET_NEW_ORDER,
  CREATE_NEW_ORDER,
  ORDER_DATE_VALID_OR_NOT,
  RESET_ORDER_DATE_VALID_OR_NOT,
  CANCEL_ORDER_CASE,
  RESET_CANCEL_ORDER,
  GET_ONE_ORDER_DETAIL,
  RESET_GET_ONE_ORDER_DETAILS,
  RESET_TRACK_ORDER,
  TRACK_ORDER_CASE,
  SET_RE_ORDER,
  RESET_RE_ORDER,
  RESET_VIP_CODE,
  CHECK_VIP_CODE,
  UPDATE_TEMP_ORDER,
  RESET_UPDATE_TEMP_ORDER,
} from "../action/myOrdersAction";
const initialState = {
  myOrders: null,
  newOrder: {
    errorBody: null,
    successBody: null,
  },
  updatedOrder: {
    errorBody: null,
    successBody: null,
  },
  cancelOrder: {
    errorBody: null,
    successBody: null,
  },
  getOneOrderDetail: null,
  trackOrder: null,
  reOrder: {
    errorBody: null,
    successBody: null,
  },
  vipCode: {
    errorBody: null,
    successBody: null,
  },
  orderDateValidation: {
    errorBody: null,
    successBody: null,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_MY_ORDERS:
      return {
        ...state,
        myOrders: action.myOrders,
      };
    case RESET_MY_ORDERS:
      return {
        ...state,
        myOrders: null,
      };
    case CREATE_NEW_ORDER:
      return {
        ...state,
        newOrder: {
          ...state.newOrder,
          successBody: action.newOrder,
          errorBody: null,
        },
      };
    case UPDATE_TEMP_ORDER:
      return {
        ...state,
        updatedOrder: {
          ...state.updatedOrder,
          successBody: action.updatedOrder,
          errorBody: null,
        },
      };
    case RESET_UPDATE_TEMP_ORDER:
      return {
        ...state,
        updatedOrder: {
          ...state.updatedOrder,
          successBody: null,
          errorBody: action.updatedOrder,
        },
      };
    case RESET_NEW_ORDER:
      return {
        ...state,
        newOrder: {
          ...state.newOrder,
          successBody: null,
          errorBody: action.newOrder,
        },
      };

    case ORDER_DATE_VALID_OR_NOT:
      return {
        ...state,
        orderDateValidation: {
          ...state.isValidDate,
          successBody: action.isValidDate,
          errorBody: null,
        },
      };
    case RESET_ORDER_DATE_VALID_OR_NOT:
      return {
        ...state,
        orderDateValidation: {
          ...state.isValidDate,
          successBody: null,
          errorBody: action.isValidDate,
        },
      };
    case CANCEL_ORDER_CASE:
      return {
        ...state,
        cancelOrder: {
          ...state.cancelOrder,
          successBody: action.cancelOrder,
          errorBody: null,
        },
      };
    case RESET_CANCEL_ORDER:
      return {
        ...state,
        cancelOrder: {
          ...state.cancelOrder,
          successBody: null,
          errorBody: action.cancelOrder,
        },
      };
    case GET_ONE_ORDER_DETAIL:
      return {
        ...state,
        getOneOrderDetail: action.getOneOrderDetail,
      };
    case RESET_GET_ONE_ORDER_DETAILS:
      return {
        ...state,
        getOneOrderDetail: null,
      };
    case TRACK_ORDER_CASE:
      return {
        ...state,
        trackOrder: action.trackOrder,
      };
    case RESET_TRACK_ORDER:
      return {
        ...state,
        trackOrder: null,
      };
    case SET_RE_ORDER:
      return {
        ...state,
        reOrder: {
          ...state.reOrder,
          successBody: action.reOrder,
          errorBody: null,
        },
      };
    case RESET_RE_ORDER:
      return {
        ...state,
        reOrder: {
          ...state.reOrder,
          successBody: null,
          errorBody: action.reOrder,
        },
      };
    case CHECK_VIP_CODE:
      return {
        ...state,
        vipCode: {
          ...state.vipCode,
          successBody: action.vipCode,
          errorBody: null,
        },
      };
    case RESET_VIP_CODE:
      return {
        ...state,
        vipCode: {
          ...state.vipCode,
          successBody: null,
          errorBody: action.vipCode,
        },
      };

    default:
      return state;
  }
};
