import {
  ADD_ITEMS_TO_CART,
  RESET_CART,
  REMOVE_ITEMS_FROM_CART,
  UPDATE_ITEMS_TO_CART,
} from "../action/cart";

const initialState = {
  cartItems: [],
};

const removeItems = (items, id) => {
  let newItems = [...items];
  newItems = newItems.filter((item) => item.uuid != id);
  return newItems;
};

const updateItems = (items, updatedItem) => {
  let newItems = [...items];
  newItems = newItems.map((item) => {
    if (item.uuid == updatedItem.uuid) {
      return updatedItem;
    } else {
      return item;
    }
  });
  return newItems;
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEMS_TO_CART:
      const newCartItems = Array.isArray(action.cartItems)
        ? action.cartItems
        : [action.cartItems];

      let newItems = [...state.cartItems, ...newCartItems];
      return {
        cartItems: newItems,
      };
    case RESET_CART:
      return {
        cartItems: [],
      };
    case REMOVE_ITEMS_FROM_CART:
      let removedItems = removeItems(state.cartItems, action.itemUuid);
      return {
        ...state,
        cartItems: removedItems,
      };
    case UPDATE_ITEMS_TO_CART:
      let updatedItems = updateItems(state.cartItems, action.cartItem);
      return {
        ...state,
        cartItems: updatedItems,
      };
  }
  return state;
};
