import {
  SET_NEW_USER,
  RESET_NEW_USER,
  SET_NEW_PASSWORD,
  RESET_NEW_PASSWORD,
  RESEND_EMAIL_VERIFICATION,
  RESET_EMAIL_RESP,
  SET_PHONE_NUMBER,
  RESET_PHONE_NUMBER,
  VERIFY_PHONE_OTP,
  RESET_VERIFY_PHONE_OTP,
  RESET_RESEND_PHONE_OTP,
  RESEND_PHONE_OTP_VERIFICATION,
  RESET_ALL_USER_REGISTRATION,
  RESET_EMAIL_OTP_ACTION,
  VERIFY_EMAIL_OTP_ACTION,
  SET_TERMS_AND_CONDITION,
  RESET_TERMS_AND_CONDITION,
  SET_PRIVACY_POLICY,
  RESET_PRIVACY_POLICY,
  SET_VIP_MEMBERSHIP_BENIFITS,
  RESET_VIP_MEMBESHIP_BENIFITS,
} from "../action/userRegistrationAction";

const initialState = {
  registeredUser: {
    errorBody: null,
    successBody: null,
  },
  createPasswordRes: {
    errorBody: null,
    successBody: null,
  },
  emailResp: {
    errorBody: null,
    successBody: null,
  },
  phoneResp: {
    errorBody: null,
    successBody: null,
  },
  otpResp: {
    errorBody: null,
    successBody: null,
  },
  verifyEmailOtp: {
    errorBody: null,
    successBody: null,
  },
  userTerms: null,
  userError: null,
  userPrivacy: null,
  userVipbenifits: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_NEW_USER:
      return {
        ...state,
        registeredUser: {
          ...state.registeredUser,
          successBody: action.registeredUser,
          errorBody: null,
        },
      };
    case RESET_NEW_USER:
      return {
        ...state,
        userError: null,
        registeredUser: {
			...state.registeredUser,
          	successBody: null,
          	errorBody: action.registeredUser,
        },
      };
    case SET_NEW_PASSWORD:
      return {
        ...state,
        createPasswordRes: {
          ...state.createPasswordRes,
          successBody: action.createPasswordRes,
          errorBody: null,
        },
      };
    case RESET_NEW_PASSWORD:
      return {
        ...state,
        createPasswordRes: {
          ...state.createPasswordRes,
          successBody: null,
          errorBody: action.createPasswordRes,
        },
      };
    case RESEND_EMAIL_VERIFICATION:
      return {
        ...state,
        emailResp: {
          ...state.emailResp,
          successBody: action.emailResp,
          errorBody: null,
        },
      };
    case RESET_EMAIL_RESP:
      return {
        ...state,
        emailResp: {
          ...state.emailResp,
          successBody: null,
          errorBody: action.emailResp,
        },
      };
    case SET_PHONE_NUMBER:
      return {
        ...state,
        phoneResp: {
          ...state.phoneResp,
          successBody: action.phoneResp,
          errorBody: null,
        },
      };
    case RESET_PHONE_NUMBER:
      return {
        ...state,
        phoneResp: {
          ...state.phoneResp,
          successBody: null,
          errorBody: action.phoneResp,
        },
      };
    case VERIFY_PHONE_OTP:
      return {
        ...state,
        otpResp: {
          ...state.otpResp,
          successBody: action.otpResp,
          errorBody: null,
        },
      };
    case RESET_VERIFY_PHONE_OTP:
      return {
        ...state,
        otpResp: {
          ...state.otpResp,
          successBody: null,
          errorBody: action.otpResp,
        },
      };
    case RESEND_PHONE_OTP_VERIFICATION:
      return {
        ...state,
        otpResendResp: {
          ...state.otpResendResp,
          successBody: action.otpResendResp,
          errorBody: null,
        },
      };
    case RESET_RESEND_PHONE_OTP:
      return {
        ...state,
        otpResendResp: {
          ...state.otpResendResp,
          successBody: null,
          errorBody: action.otpResendResp,
        },
      };
    case RESET_ALL_USER_REGISTRATION:
      return {
        ...initialState,
      };
    case VERIFY_EMAIL_OTP_ACTION:
      return {
        ...state,
        verifyEmailOtp: {
          ...state.verifyEmailOtp,
          successBody: action.verifyEmailOtp,
          errorBody: null,
        },
      };
    case RESET_EMAIL_OTP_ACTION:
      return {
        ...state,
        verifyEmailOtp: {
          ...state.verifyEmailOtp,
          successBody: null,
          errorBody: action.verifyEmailOtp,
        },
      };
    case SET_TERMS_AND_CONDITION:
      return {
        ...state,
        userTerms: action.userTerms,
      };
    case RESET_TERMS_AND_CONDITION:
      return {
        ...state,
        userTerms: null,
      };
    case SET_PRIVACY_POLICY:
      return {
        ...state,
        userPrivacy: action.userPrivacy,
      };
    case RESET_PRIVACY_POLICY:
      return {
        ...state,
        userPrivacy: null,
      };
    case SET_VIP_MEMBERSHIP_BENIFITS:
      return {
        ...state,
        userVipbenifits: action.userVipbenifits,
      };
    case RESET_VIP_MEMBESHIP_BENIFITS:
      return {
        ...state,
        userVipbenifits: null,
      };
    default:
      return state;
  }
};
