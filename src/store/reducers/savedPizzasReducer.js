import {
  SET_SAVED_PIZZAS,
  RESET_SAVED_PIZZAS,
} from "../action/savedPizzasAction";

const initialState = {
  savedPizzas: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_SAVED_PIZZAS:
      return {
        ...state,
        savedPizzas: action.savedPizzas,
      };
    case RESET_SAVED_PIZZAS:
      return {
        ...state,
        savedPizzas: [],
      };
    default:
      return state;
  }
};
