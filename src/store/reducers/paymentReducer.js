import {
  GET_CLIENT_SECRET,
  RESET_CLIENT_SECRET,
  PAYMENT_CONFIRMED,
  RESET_PAYMENT_CONFIRMED,
  GET_CLIENT_PAYMENT_CARDS,
  RESET_CLIENT_PAYMENT_CARDS,
} from "../action/paymentAction";

const initialState = {
  paymentIntentDetails: {
    errorBody: null,
    successBody: null,
  },
  cards: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CLIENT_SECRET:
      return {
        ...state,
        paymentIntentDetails: {
          ...state.paymentIntentDetails,
          errorBody: null,
          successBody: action.paymentIntentDetails,
        },
      };

    case RESET_CLIENT_SECRET:
      return {
        ...state,
        paymentIntentDetails: {
          ...state.paymentIntentDetails,
          successBody: null,
          errorBody: action.paymentIntentDetails,
        },
      };
    case GET_CLIENT_PAYMENT_CARDS:
      return {
        ...state,
        cards: action.cards,
      };
    case RESET_CLIENT_PAYMENT_CARDS:
      return {
        ...state,
        cards: null,
      };
    case PAYMENT_CONFIRMED:
      return {
        ...state,
        paymentConfirmed: {
          ...state.paymentConfirmed,
          successBody: action.paymentConfirmed,
          errorBody: null,
        },
      };
    case RESET_PAYMENT_CONFIRMED:
      return {
        ...state,
        paymentConfirmed: {
          ...state.paymentConfirmed,
          successBody: null,
          errorBody: action.paymentConfirmed,
        },
      };

    default:
      return state;
  }
};
