import { GET_LOCATION, SET_DEFAULT_LOCALLY } from "../action/location";

const initialState = {
  locations: null,
};

const updatedefault = (items, id) => {
  let newItems = [...items];
  newItems = newItems.map((item) => {
    if (item.communityId == id) {
      return { ...item, isDefault: true };
    } else {
      return { ...item, isDefault: false };
    }
  });
  return newItems;
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_LOCATION:
      return {
        locations: action.locations,
      };
    case SET_DEFAULT_LOCALLY:
      let updatedItems = updatedefault(state.locations.communities, action.id);
      return {
        locations: {
          ...state.locations,
          communities: updatedItems,
        },
      };
  }
  return state;
};
