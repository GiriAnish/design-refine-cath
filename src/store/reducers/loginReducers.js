import { SET_USER_LOGIN, RESET_USER_LOGIN } from "../action/loginActions";

const initialState = {
  loggedInUser: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_LOGIN:
      return {
        ...state,
        loggedInUser: action.loggedInUser,
      };
    case RESET_USER_LOGIN:
      return {
        ...state,
        loggedInUser: null,
      };
    default:
      return state;
  }
};
