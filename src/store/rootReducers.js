import { combineReducers } from "redux";
//Common Reducers
import errorReducer from "./reducers/error";
import loadingReducer from "./reducers/loading";

//Screen Reducers
import pizzamenuReducer from "./reducers/pizzamenu";
import cartReducer from "./reducers/cart";
import savedPizzasReducer from "./reducers/savedPizzasReducer";
import myOrdersReducer from "./reducers/myOrdersReducer";
import loginReducers from "./reducers/loginReducers";
import locationReducers from "./reducers/location";
import userRegistrationReducer from "./reducers/userRegistrationReducer";
import forgotpwdReducer from "./reducers/forgotpwdReducer";
import paymentReducer from "./reducers/paymentReducer";

export const rootReducer = combineReducers({
  pizzaMenu: pizzamenuReducer,
  error: errorReducer,
  loading: loadingReducer,
  cart: cartReducer,
  savedPizzas: savedPizzasReducer,
  myOrders: myOrdersReducer,
  loggedInUser: loginReducers,
  locations: locationReducers,
  registeredUser: userRegistrationReducer,
  forgotPwd: forgotpwdReducer,
  payment: paymentReducer,
});
