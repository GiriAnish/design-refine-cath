import React, { useEffect, useCallback } from "react";
import {
  View,
  StyleSheet,
  ImageBackground,
  Text,
  TouchableOpacity,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import LogoPrimary from "./Components/svg/CB_Primary";
import CBPartnership from "./Components/svg/CB_Partnership";
import { useSelector, useDispatch } from "react-redux";
import { setLoading } from "../store/action/loading";

const SplashScreen = ({ navigation }) => {
  const loggedIn = useSelector((state) => state.loggedInUser.loggedInUser);

  const dispatch = useDispatch();
  const setInitialLoader = useCallback(() => {
    try {
      dispatch(setLoading(false));
    } catch (err) {}
  });
  useEffect(() => {
    setInitialLoader();
    if (loggedIn) {
      navigation.replace("DrawerNavigationRoutes");
    }
  }, []);

  return (
    <View style={styles.container}>
      {!loggedIn && (
        <ImageBackground
          source={require("../assets/images/splash/newBg.png")}
          style={styles.topContainer}
        >
          <View style={styles.headerCont}>
            <Text style={styles.titleCont}>Welcome VIP!</Text>
          </View>
          <View style={styles.centerCont}>
            <LogoPrimary width={800} height={130} viewBox="0 0 600 300" />
          </View>
          <View style={styles.centerContPartner}>
            <CBPartnership width={400} height={115} viewBox="0 0 400 140" />
          </View>
          <View style={styles.bottomCont}>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={() => {
                AsyncStorage.getItem("user_id").then((value) =>
                  navigation.replace(
                    value === null ? "Auth" : "DrawerNavigationRoutes"
                  )
                );
              }}
            >
              <Text style={styles.buttonTextStyle}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.registerButtonStyle}
              activeOpacity={0.5}
              onPress={() => {
                navigation.navigate("Auth", {
                  screen: "Registration",
                });
              }}
            >
              <Text style={styles.registerTextStyle}>Sign Up</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      )}
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  headerCont: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    marginTop: "25%",
  },
  titleCont: {
    color: "#a38862",
    fontSize: 38,
    fontWeight: "300",
    marginBottom: 20,
  },
  centerCont: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    width: "100%",
    marginTop: 20,
    marginBottom: 20,
  },
  centerContPartner: {
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    width: "100%",
    marginTop: 10,
    marginBottom: 20,
  },
  logo: {
    width: "90%",
    resizeMode: "contain",
    marginHorizontal: 30,
    marginVertical: 0,
  },
  partText: {
    color: "#5e35b1",
    fontSize: 23,
    fontWeight: "normal",
    marginVertical: 30,
    marginLeft: 10,
  },
  logoCont: {
    width: "80%",
    resizeMode: "contain",
    marginHorizontal: 30,
    marginVertical: 10,
  },
  topContainer: {
    flex: 1,
    width: "100%",
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#FFF",
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    color: "#FFFFFF",
    borderColor: "#673ab7",
    borderWidth: 1,
    height: 45,
    alignItems: "center",
    width: "90%",
    marginLeft: "5%",
    marginTop: 20,
    borderRadius: 5,
  },
  registerButtonStyle: {
    backgroundColor: "#fff",
    color: "#FFFFFF",
    borderColor: "#a38862",
    borderWidth: 1,
    height: 45,
    alignItems: "center",
    width: "90%",
    marginLeft: "5%",
    marginTop: 20,
    borderRadius: 5,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  registerTextStyle: {
    color: "#a38862",
    textAlign: "center",
    fontWeight: "normal",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  bottomCont: { position: "absolute", bottom: 30, width: "100%" },
  leftcol: {paddingLeft: 10}
});
