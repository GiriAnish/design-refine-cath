import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import * as CartActions from "../store/action/cart";
import Modal from "./Components/Modal";
import {
  CustomiseScreenProvider,
  useCustomisePizza,
} from "../contexts/customise";
import { TouchableOpacity } from "react-native-gesture-handler";
import CallToStore from "./Components/CallToStore";
import CartModal from "./Components/CartModal";
import buildPizzaImage from "../assets/images/pizzamenu/build-pizza-image.png";
import { PizzaPreview, PizzaPreviewImage } from "./DrawerScreens/PizzaDetails";
import CBLogoNoFill from "./Components/svg/CBLogoGoldNoFill";
import CircleLeft from "./Components/svg/CircleLeft";
import CircleRight from "./Components/svg/CircleRight";
import CircleFull from "./Components/svg/CircleFull";
import Toast from "react-native-toast-message";
import Minus from "./Components/svg/Minus.js";
import Plus from "./Components/svg/Plus.js";
import { useFocusEffect } from "@react-navigation/native";
import { useTabVisible } from "../contexts/navigation";
const borderWidth = 3;
const outerRadius = 80;
const innerRadius = outerRadius - borderWidth;
const ICON_HIT_SLOP = { top: 7, left: 7, bottom: 7, right: 7 };

const buttonStyles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderRadius: 20,
    maxWidth: 170,
    justifyContent: "center",
    minHeight: 34,
  },
  buttonBlur: {
    backgroundColor: "#c0b4d4",
    borderWidth: 0,
    color: "#FFFFFF",
    borderRadius: 20,
    maxWidth: 170,
    justifyContent: "center",
    minHeight: 34,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    paddingHorizontal: 20,
    fontSize: 12,
  },
});
export function Button({ label, onPress, disabled }) {
  return (
    <TouchableOpacity
      style={!disabled ? buttonStyles.buttonStyle : buttonStyles.buttonBlur}
      activeOpacity={0.8}
      onPress={!disabled ? onPress : null}
    >
      <Text numberOfLines={3} style={buttonStyles.buttonTextStyle}>
        {label ?? ""}
      </Text>
    </TouchableOpacity>
  );
}

const counterStyles = StyleSheet.create({
  counter: {
    flexDirection: "row",
    minHeight: 34,
    borderRadius: 31,
    paddingHorizontal: 14,
    alignItems: "center",
    backgroundColor: "#f3e5f5",
  },
  counterView: {
    width: 12,
    alignItems: "center",
    justifyContent: "center",
  },
  quantitView: {
    width: 41,
    alignItems: "center",
    justifyContent: "center",
  },
  quantityText: {
    fontSize: 16,
    fontWeight: "500",
  },
});

export function Counter({ value = 1, setCount, disableIncreament }) {
  const { updateQuantity } = useCustomisePizza();
  const increment = React.useCallback(() => {
    setCount(value + 1);
    updateQuantity(value + 1);
  }, [value]);
  const decrement = React.useCallback(() => {
    if (value !== 1) {
      setCount(value - 1);
      updateQuantity(value - 1);
    }
  }, [value]);
  return (
    <View style={counterStyles.counter}>
      <TouchableWithoutFeedback
        hitSlop={ICON_HIT_SLOP}
        style={counterStyles.counterView}
        onPress={decrement}
      >
        <View style={counterStyles.infoMinus}>
          <Minus height={23} width={23} fill={"#673ab7"} />
        </View>
      </TouchableWithoutFeedback>
      <View style={counterStyles.quantitView}>
        <Text style={styles.quantityText}>{value}</Text>
      </View>
      <TouchableWithoutFeedback
        style={counterStyles.counterView}
        hitSlop={ICON_HIT_SLOP}
        onPress={increment}
        disabled={disableIncreament}
      >
        <View style={counterStyles.infoPlus}>
          <Plus height={23} width={23} fill={"#673ab7"} />
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

function PizzaSizeText({ style, label, isSelected }) {
  const selectedViewStyle = isSelected ? styles.textViewSelected : {};
  const textStyle = isSelected ? styles.textSelected : styles.text;
  return (
    <View style={[styles.textView, style, selectedViewStyle]}>
      <Text style={textStyle}>{label}</Text>
    </View>
  );
}

export function BuildPizzaPreview() {
  const { basicSelectedItems, image, price, quantity } = useCustomisePizza();
  const [selectedSize] = basicSelectedItems[0]?.[1] ?? [];
  const size = React.useMemo(() => {
    if (selectedSize?.id === "Large") {
      return "L";
    }
    if (selectedSize?.id === "Small") {
      return "S";
    }
    return "M";
  }, [selectedSize]);

  const imageSource = image ? { uri: image } : buildPizzaImage;
  const totalPrice = price * quantity;
  return (
    <View style={styles.preview}>
      <View style={styles.previewImageView}>
        <View style={styles.outerCircle}>
          <View style={styles.imageView}>
            <Image style={styles.image} source={imageSource} />
          </View>
        </View>
        {false && (
          <View style={styles.textContainer}>
            <PizzaSizeText
              label="S"
              style={styles.textView1}
              isSelected={size === "S"}
            />
            <PizzaSizeText
              label="M"
              style={styles.textView2}
              isSelected={size === "M"}
            />
            <PizzaSizeText
              label="L"
              style={styles.textView3}
              isSelected={size === "L"}
            />
          </View>
        )}
        <View style={styles.previewTextView}>
          <Text style={styles.previewTitle}>
            {"Build Your Own Custom Pizza"}
          </Text>
          <View style={styles.costContainer}>
            <Text style={styles.totalCosttxt}>Total Cost:</Text>
            <Text style={styles.totalCostValue}>
              {" "}
              {"$ " + isNaN(totalPrice) ? "" : totalPrice}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
}

function BasicSelectionRowItem(props) {
  const { data, index, onPress, isSelected } = props;
  const { label, secondary } = data;
  const _onPress = React.useCallback(() => {
    onPress(index);
  }, [index, onPress]);

  return (
    <TouchableWithoutFeedback onPress={_onPress}>
      <View
        style={[styles.selectionItem, isSelected ? styles.selectedBorder : {}]}
      >
        <Text style={styles.primarySelectionText}>{label}</Text>
        {secondary ? (
          <Text style={styles.secondaryText}>{secondary}</Text>
        ) : null}
      </View>
    </TouchableWithoutFeedback>
  );
}

function BasicSelectionRow({ data, index: rowIndex }) {
  const { title, items, selectedIndex: defaultSelection } = data;
  const [selectedIndex, selSeletedIndex] = useState(defaultSelection ?? -1);
  const { updateBasicSelection } = useCustomisePizza();

  useEffect(() => {
    selSeletedIndex(defaultSelection);
  }, [defaultSelection]);

  const onPress = React.useCallback(
    (itemIndex) => {
      let newSelection = -1;
      if (itemIndex !== selectedIndex) {
        newSelection = itemIndex;
      }
      selSeletedIndex(newSelection);
      updateBasicSelection(rowIndex, newSelection);
    },
    [selectedIndex, rowIndex]
  );

  return (
    <View>
      <Text style={styles.sectionHeader}>{title}</Text>
      <View style={styles.listView}>
        {items.map((item, idx) => {
          return (
            <BasicSelectionRowItem
              key={idx}
              data={item}
              index={idx}
              isSelected={selectedIndex === idx}
              onPress={onPress}
            />
          );
        })}
      </View>
    </View>
  );
}
function BasicSelectionsView() {
  const { basicSelections } = useCustomisePizza();
  if (basicSelections && basicSelections.length) {
    return (
      <View style={styles.basicSelectionsRoot}>
        {basicSelections.map((item, index) => {
          return <BasicSelectionRow key={index} data={item} index={index} />;
        })}
      </View>
    );
  } else {
    return (
      <View style={styles.noIngredientsText}>
        <Text>No Ingredients were found!.</Text>
      </View>
    );
  }
}
function AdditionalSelectionHeaderRow({ title }) {
  return (
    <View style={styles.additionalSelectionHeader}>
      <View style={styles.additionalSelectionHeadeLeft}>
        <Text style={styles.additionalSelectionHeaderLeftTxt}>{title}</Text>
      </View>
      <View style={styles.additionalSelectionHeaderRight}>
        <Text style={styles.additionalSelectionHeaderRightTxt}>Left</Text>
        <Text style={styles.additionalSelectionHeaderRightTxt}>Right</Text>
        <Text style={styles.additionalSelectionHeaderRightTxt}>Full</Text>
      </View>
    </View>
  );
}

function PizzaSideSelection({ data, sectionIndex, rowIndex, maximum, title }) {
  const [selectedIndex, selSeletedIndex] = useState(data.selectedIndex ?? -1);
  const {
    updateAdditionalSelection,
    updateAdditionalSelectionAlreadySelectedIndex,
    updateAdditionalSelectionTotalSelection,
  } = useCustomisePizza();
  useEffect(() => {
    selSeletedIndex(data.selectedIndex);
  }, [data.selectedIndex]);

  const showErrorToast = (text) => {
    Toast.show({
      type: "error",
      position: "top",
      bottomOffset: 210,
      autoHide: true,
      text1: "Error",
      text2: text,
    });
  };

  const setSelection = React.useCallback(
    (event, itemIndex) => {
      let newSelection = -1;
      if (itemIndex !== selectedIndex) {
        newSelection = itemIndex;
      }
      const existingSelection = updateAdditionalSelectionAlreadySelectedIndex(
        sectionIndex,
        rowIndex
      );
      const maxSelection =
        updateAdditionalSelectionTotalSelection(sectionIndex);
      const maxSelectionCount = JSON.parse(JSON.stringify(maxSelection)).length;

      if (
        existingSelection == -1 &&
        newSelection >= 0 &&
        maxSelectionCount >= maximum
      ) {
        event.preventDefault();

        showErrorToast(
          `You cannot add more than ${maximum} ingredients in ${title} group!`
        );
      } else {
        selSeletedIndex(newSelection);
        updateAdditionalSelection(sectionIndex, rowIndex, newSelection);
      }
    },
    [selectedIndex, rowIndex, sectionIndex]
  );

  return (
    <View style={styles.additionalSelectionValue}>
      <View style={styles.additionalSelectionHeadeLeftValue}>
        <Image
          style={styles.additionalSelectionHeaderLeftImage}
          source={{ uri: data?.image }}
        />
        <Text style={styles.additionalSelectionHeaderLeftValueTxt}>
          {data.label}
        </Text>
      </View>
      <View style={styles.pizzaSideContainer}>
        <TouchableWithoutFeedback onPress={(event) => setSelection(event, 0)}>
          {selectedIndex === 0 ? (
            <CircleLeft
              width={46}
              height={46}
              color="#5e35b1"
              style={styles.circlesSelected}
              viewBox="0 -10 36 36"
            />
          ) : (
            <CircleLeft
              width={46}
              height={46}
              color="#cfd8dc"
              style={styles.circles}
              viewBox="0 -10 36 36"
            />
          )}
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={(event) => setSelection(event, 1)}>
          {selectedIndex === 1 ? (
            <CircleRight
              width={46}
              height={46}
              color="#5e35b1"
              style={styles.circlesSelected}
              viewBox="0 -10 36 36"
            />
          ) : (
            <CircleRight
              width={46}
              height={46}
              color="#cfd8dc"
              style={styles.circles}
              viewBox="0 -10 36 36"
            />
          )}
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={(event) => setSelection(event, 2)}>
          {selectedIndex === 2 ? (
            <CircleFull
              width={46}
              height={46}
              color="#5e35b1"
              style={styles.circlesSelected}
              viewBox="0 -10 36 36"
            />
          ) : (
            <CircleFull
              width={46}
              height={46}
              color="#cfd8dc"
              style={styles.circles}
              viewBox="0 -10 36 36"
            />
          )}
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
}

function AdditionalSelectionView() {
  const { additionalSelections } = useCustomisePizza();
  if (additionalSelections && additionalSelections.length) {
    return (
      <View style={styles.additionalSelection}>
        {additionalSelections.map(
          ({ title, minimum, maximum, items }, sectionIndex) => {
            return (
              <View key={sectionIndex}>
                <AdditionalSelectionHeaderRow
                  title={title}
                  minimum={minimum}
                  maximum={maximum}
                />
                <View>
                  {items.map((item, rowIndex) => {
                    return (
                      <PizzaSideSelection
                        key={rowIndex}
                        data={item}
                        rowIndex={rowIndex}
                        sectionIndex={sectionIndex}
                        minimum={minimum}
                        maximum={maximum}
                        title={title}
                      />
                    );
                  })}
                </View>
              </View>
            );
          }
        )}
      </View>
    );
  } else {
    return null;
  }
}

export function SelectedItemsPreview() {
  const { selectedItemsPreview } = useCustomisePizza();
  if (!selectedItemsPreview?.length) {
    return null;
  }
  return (
    <View style={styles.selectionItemsView}>
      <Text>
        {selectedItemsPreview.map(([key, value], index) => {
          return (
            <Text key={index}>
              <Text style={styles.selectionSectiontxt}>{key + ": "}</Text>
              <Text style={styles.selctionValueTxt}>
                {value[0]?.label
                  ? value.map((i) => i.label).join(", ") + " "
                  : value + " "}
              </Text>
            </Text>
          );
        })}
      </Text>
    </View>
  );
}

const ButtonView = (props) => {
  const cart = useSelector((state) => state.cart.cartItems);
  const loggedInUser = useSelector((state) => state.loggedInUser.loggedInUser);
  const getQuan = () => {
    let filteredCartItem;
    let cartQuantity = 1;
    if (props?.uuid) {
      filteredCartItem = cart.filter((item) => item.uuid == props.uuid);
      cartQuantity = filteredCartItem[0]?.quantity || 1;
      return cartQuantity;
    } else {
      return 1;
    }
  };
  const [showCartModal, setShowCartModal] = useState(false);
  const {
    basicSelections,
    selectedItemsPreview,
    price,
    name,
    id,
    additionalSelections,
  } = useCustomisePizza();
  const dispatch = useDispatch();
  const [quantity, setQuantity] = useState(1);
  const [isCartLimitExceeded, setIsCartLimitExceeded] = useState(
    checkIfCartLimitExceeded()
  );

  const isRequiredBasicSelected = basicSelections.every((i) => {
    if (i.minimum) {
      return !!selectedItemsPreview.find((si) => si[0] === i.title);
    }
    return true;
  });
  const isRequiredAdditionalSelected = additionalSelections.every((i) => {
    if (i.minimum) {
      return !!selectedItemsPreview.find((si) => si[0] === i.title);
    }
    return true;
  });

  const isRequiredSelected =
    isRequiredBasicSelected && isRequiredAdditionalSelected;

  function checkIfCartLimitExceeded(qty) {
    const maxItemPerCart = loggedInUser?.userDetails?.maxItemPerCart;
    if (maxItemPerCart == null) {
      return false;
    }
    const itemInCart = cart.reduce((total, item) => {
      return total + item.quantity;
    }, 0);
    const currentItemsInCart =
      qty == null ? itemInCart + quantity : itemInCart + qty;
    return currentItemsInCart >= maxItemPerCart;
  }

  const showMaximumItemExceededToast = () => {
    Toast.show({
      type: "error",
      position: "top",
      bottomOffset: 210,
      text1: "Error",
      text2: `You exceeded maximum(${loggedInUser?.userDetails?.maxItemPerCart}) item(s) added to the cart!`,
    });
  };

  const addToCart = () => {
    const cartLimitExceeded = checkIfCartLimitExceeded();
    setIsCartLimitExceeded(cartLimitExceeded);
    if (cartLimitExceeded) {
      showMaximumItemExceededToast();
      return;
    }
    dispatch(
      CartActions.addItemsToCart({
        ingredients: selectedItemsPreview,
        name: name,
        id,
        quantity: quantity,
        price: price,
        uuid: new Date().toISOString(),
      })
    );
  };

  useEffect(() => {
    setShowCartModal(false);
    setQuantity(getQuan());
  }, [props]);

  const addItemToCart = () => {
    setShowCartModal(false);
    props.navigateToCart();
  };

  const updateItemsInCart = () => {
    let maxItemPerCart = loggedInUser?.userDetails?.maxItemPerCart;
    let filteredCart = cart.filter((item) => item.uuid != props.uuid);
    let itemInCart = filteredCart.reduce(
      (total, item) => total + item.quantity,
      0
    );
    if (!(itemInCart + quantity <= maxItemPerCart)) {
      showMaximumItemExceededToast();
      return;
    }
    dispatch(
      CartActions.updateItemsToCart({
        ingredients: selectedItemsPreview,
        name: name,
        id: id,
        quantity: quantity,
        price: price,
        uuid: props.uuid,
      })
    );
    props.navigateToCart();
  };

  const addToCartAndNavigateToCheckout = () => {
    const itemInCart = cart.reduce((total, item) => {
      return total + item.quantity;
    }, 0);
    if (itemInCart < loggedInUser?.userDetails?.maxItemPerCart) {
      dispatch(
        CartActions.addItemsToCart({
          ingredients: selectedItemsPreview,
          name: name,
          id,
          quantity: loggedInUser?.userDetails?.maxItemPerCart - itemInCart,
          price: price,
          uuid: new Date().toISOString(),
        })
      );
    }
    props.navigation.navigate("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "Cart",
        params: {
          screen: "checkoutVIPScreen",
          initial: false,
          params: {
            checkOut: true,
          },
        },
      },
    });
  };
  return (
    <View style={styles.actionPanel}>
      <View style={styles.qtyPanel}>
        <View style={styles.elements}>
          <Text style={styles.qtyText}>QTY:</Text>
        </View>
        <View style={styles.elements}>
          <Counter
            value={quantity}
            setCount={(count) => {
              setQuantity(count);
              setIsCartLimitExceeded(checkIfCartLimitExceeded(count));
            }}
            disableIncreament={!props.uuid && isCartLimitExceeded}
          />
        </View>
        {props.uuid && (
          <View style={styles.elements}>
            <Button
              disabled={!isRequiredSelected}
              label={"Save Updates"}
              onPress={() => updateItemsInCart()}
            />
          </View>
        )}
        {!props.uuid && !isCartLimitExceeded && (
          <View style={styles.elements}>
            <Button
              disabled={!isRequiredSelected}
              label={"Add to Cart"}
              onPress={() => addToCart()}
            />
          </View>
        )}
        {!props.uuid && isCartLimitExceeded && (
          <View style={styles.elements}>
            <Button
              label={"Checkout"}
              onPress={() => addToCartAndNavigateToCheckout()}
            />
          </View>
        )}
        <View style={styles.elements}>
          <CBLogoNoFill width={40} height={40} viewBox="0 -15 110 150" />
        </View>
      </View>
      <CartModal
        onPress={addItemToCart}
        showModal={showCartModal}
        setVisible={() => {
          setShowCartModal(false);
        }}
        navigation={props.navigation}
      />
    </View>
  );
};

function CustomisePizzaScreen(props) {
  const { navigation } = props;
  const [showModal, setShowModal] = useState(false);
  const modalClick = () => {
    setShowModal(true);
  };
  const { setTabVisible } = useTabVisible();
  useFocusEffect(
    React.useCallback(() => {
      setTabVisible(true);
    }, [setTabVisible])
  );

  const navigateToCart = () => {
    navigation.push("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "Cart",
      },
    });
  };

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <CallToStore onPress={modalClick} />,
    });
  });

  const isCustomisePizza = props?.route?.params?.pizzaId;

  return (
    <CustomiseScreenProvider
      id={props?.route?.params?.pizzaId}
      uuid={props?.route?.params?.uuid}
    >
      <View style={{ height: "85%" }}>
        <ScrollView>
          <View style={styles.root}>
            <View style={[styles.rootContainer, isCustomisePizza]}>
              {isCustomisePizza ? <PizzaPreview /> : <BuildPizzaPreview />}
              {/* <ButtonView
                navigateToCart={navigateToCart}
                uuid={props?.route?.params?.uuid}
                navigation={navigation}
              /> */}
              <SelectedItemsPreview />
              <BasicSelectionsView />
              <AdditionalSelectionView />
              {showModal && (
                <Modal
                  showModal={showModal}
                  setVisible={() => {
                    setShowModal(false);
                  }}
                />
              )}
            </View>
          </View>

          {isCustomisePizza ? <PizzaPreviewImage /> : null}
        </ScrollView>
      </View>
      <View
        style={{
          height: "15%",
          backgroundColor: "#FFFFFF",
          borderWidth: 1,
          borderColor: "#ede7f6",
          borderRadius: 4,
        }}
      >
        <ButtonView
          navigateToCart={navigateToCart}
          uuid={props?.route?.params?.uuid}
          navigation={navigation}
        />
      </View>
    </CustomiseScreenProvider>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "#fafcfe",
  },
  rootContainer: {
    backgroundColor: "#fff",
    paddingVertical: 10,
    borderRadius: 5,
    elevation: 1,
  },
  additionalSelection: {
    marginTop: 5,
    paddingHorizontal: 25,
  },
  circles: {
    marginTop: 0,
    marginLeft: 6,
  },
  circlesSelected: {
    marginTop: 0,
    marginLeft: 6,
  },
  imageContainer: {
    marginTop: 15,
  },
  pizzaSide: {
    height: 34,
    width: 34,
  },
  additionalSelectionHeaderLeftImage: {
    height: 45,
    width: 45,
    alignItems: "center",
    justifyContent: "center",
  },
  noIngredientsText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#455a64",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
    marginTop: 20,
  },
  infoMinus: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 10,
  },
  infoPlus: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 10,
  },
  additionalSelectionHeader: {
    flexDirection: "row",
    height: 50,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "#eceff1",
    marginTop: 26,
  },
  additionalSelectionValue: {
    flexDirection: "row",
    height: 57,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "#eceff1",
  },
  additionalSelectionHeadeLeft: {
    flex: 6,
  },
  additionalSelectionHeadeLeftValue: {
    flex: 6,
    flexDirection: "row",
    alignItems: "center",
  },
  additionalSelectionHeaderRight: {
    flex: 4,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  pizzaSideContainer: {
    flex: 4,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  additionalSelectionHeaderLeftTxt: {
    fontSize: 16,
    fontWeight: "500",
    color: "#455a64",
  },
  additionalSelectionHeaderLeftValueTxt: {
    fontSize: 14,
    marginLeft: 16,
    fontWeight: "500",
    color: "#212121",
  },
  additionalSelectionHeaderRightTxt: {
    fontSize: 12,
    color: "#78909c",
    marginLeft: 6,
  },
  basicSelectionsRoot: {
    paddingHorizontal: 25,
  },
  sectionHeader: {
    fontSize: 16,
    fontWeight: "500",
    color: "#455a64",
    marginTop: 25,
  },
  listView: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  selectionItem: {
    flexDirection: "row",
    paddingHorizontal: 14,
    paddingVertical: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#cfd8dc",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 15,
    marginTop: 10,
  },
  selectedBorder: {
    borderWidth: 2,
    borderColor: "#a38862",
    paddingHorizontal: 13,
    paddingVertical: 9,
  },
  selectionItemsView: {
    marginTop: 20,
    paddingHorizontal: 25,
    minHeight: 20,
  },
  primarySelectionText: {
    fontSize: 11,
    fontWeight: "500",
    color: "#607d8b",
  },
  secondaryText: {
    fontSize: 11,
    fontWeight: "500",
    color: "#607d8b",
    marginLeft: 12,
  },
  selectionSectiontxt: {
    fontSize: 12,
    fontWeight: "500",
    color: "#607d8b",
  },
  selctionValueTxt: {
    fontSize: 12,
    color: "#607d8b",
  },
  qtyText: {
    color: "#455a64",
    fontSize: 12,
  },
  cbLogo: {},
  favIcon_selected: {
    height: 33,
    width: 33,
    borderRadius: 33,
    backgroundColor: "#ffebee",
    justifyContent: "center",
    alignItems: "center",
  },
  favIcon_unselected: {
    height: 33,
    width: 33,
    borderRadius: 33,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
  },
  actionPanel: {
    marginTop: 20,
    flexDirection: "row",
    paddingHorizontal: 20,
    justifyContent: "space-between",
    alignItems: "center",
  },
  qtyPanel: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  elements: {
    marginLeft: 5,
  },
  preview: {
    paddingLeft: 4,
    paddingRight: 8,
    flexDirection: "row",
    height: outerRadius * 2,
  },
  previewImageView: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  outerCircle: {
    width: outerRadius,
    height: outerRadius * 2,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderRadius: outerRadius,
    borderWidth: borderWidth,
    borderColor: "#a38862",
    marginLeft: outerRadius,
    alignItems: "center",
    justifyContent: "center",
  },
  imageView: {
    marginLeft: -outerRadius,
    padding: 16,
    height: innerRadius * 2,
    width: innerRadius * 2,
    borderRadius: innerRadius,
    backgroundColor: "#fff",
  },
  image: {
    height: "100%",
    width: "100%",
    borderRadius: innerRadius,
  },
  textView: {
    width: 34,
    height: 34,
    borderRadius: 34,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#eceff1",
  },
  text: {
    fontWeight: "500",
    fontSize: 16,
    color: "#455a64",
  },
  textSelected: {
    fontWeight: "500",
    fontSize: 16,
    color: "white",
  },
  textView1: {
    marginLeft: -17,
    backgroundColor: "#eceff1",
  },
  textView2: {
    marginLeft: 7,
  },
  textView3: {
    marginLeft: -17,
  },
  textViewSelected: {
    backgroundColor: "#9c27b0",
  },
  textContainer: {
    height: outerRadius * 2,
    justifyContent: "space-between",
  },
  previewTextView: {
    flex: 1,
    marginLeft: 20,
  },
  previewTitle: {
    color: "#455a64",
    fontSize: 22,
    fontWeight: "500",
  },
  costContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 16,
  },
  totalCosttxt: {
    fontSize: 13,
    color: "#455a64",
  },
  totalCostValue: {
    fontSize: 22,
    fontWeight: "bold",
    color: "#f44336",
    marginLeft: 16,
  },
});

export default CustomisePizzaScreen;
