import React, { useState, useEffect, useCallback, Fragment } from "react";
import { View, Text, SafeAreaView, StyleSheet } from "react-native";
import {
  ScrollView,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import Banner from "./Components/Banner";
import { PizzaCard } from "./Components/TodaySpecial";
import Modal from "./Components/Modal";
import { useDispatch, useSelector } from "react-redux";
import * as PizzaMenuActions from "../store/action/pizzamenu";
import Loader from "./Components/Loader";
import CallToStore from "./Components/CallToStore";

const styles = StyleSheet.create({
  sectionHeader: {
    fontSize: 16,
    marginBottom: 17,
    marginTop: 5,
    marginLeft: 30,
    fontWeight: "500",
    color: "#455a64",
  },
});
const PizzaMenuScreen = (props) => {
  const { navigation } = props;
  const dispatch = useDispatch();
  const menuId = useSelector(
    (state) =>
      state?.loggedInUser?.loggedInUser?.userDetails?.menuId ||
      state?.loggedInUser?.loggedInUser?.user?.menuId
  );
  const loadPizzaMenu = useCallback(() => {
    try {
      dispatch(PizzaMenuActions.fetchPizzaMenu(menuId));
      setTabVisible(true);
    } catch (err) {}
  });

  useEffect(() => {
    const unsubscribe = props.navigation.addListener("focus", () => {
      loadPizzaMenu();
    });
    return unsubscribe;
  }, [props.navigation]);

  const pizzaMenuData = useSelector((state) => state.pizzaMenu.pizzaMenu);
  const loading = useSelector((state) => state.loading.loading);

  const [showModal, setShowModal] = useState(false);
  const modalClick = () => {
    setShowModal(true);
  };
  const openCustomise = React.useCallback(() => {
    navigation.navigate("pizzaScreenCustomise");
  }, []);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <CallToStore onPress={modalClick} />,
    });
  });

  return (
    <SafeAreaView
      style={{ flex: 1, paddingTop: 0, backgroundColor: "#fafcfe" }}
    >
      <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
        <Banner
          touchableOpacity
          onPress={openCustomise}
          imageSource={require("../assets/images/pizzamenu/banner.png")}
          primaryText={"Discover Our"}
          secondaryText={"Menu"}
          touchableOpacityText={"Build A Custom Pizza"}
        />
        <Text style={styles.sectionHeader}>Pizza Menu</Text>
        <Loader loading={loading} />
        {!loading && (
          <Fragment>
            <View>
              {pizzaMenuData?.length > 0 &&
                pizzaMenuData?.map((item, index) => {
                  return (
                    <View style={{ margin: 5 }} key={item.itemId}>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          navigation.navigate("pizzaDetails", {
                            pizzaId: item.id,
                            pizzaName: item.name,
                            price: item.price,
                          });
                        }}
                      >
                        <PizzaCard
                          name={item.name}
                          price={item.price}
                          rating={item.rating}
                          description={item.description}
                          imageUrl={item.imageUrl}
                          enableBottomLogo
                        />
                      </TouchableWithoutFeedback>
                    </View>
                  );
                })}
            </View>
          </Fragment>
        )}
        {showModal && (
          <Modal
            showModal={showModal}
            setVisible={() => {
              setShowModal(false);
            }}
          />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default PizzaMenuScreen;
