import React from "react";
import { View, TouchableOpacity, StyleSheet, Modal, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import _const from "../../data/const";

export function ButtonCheckOut({ label, onPress }) {
  return (
    <TouchableOpacity
      style={styles.modalButtonStyle}
      activeOpacity={0.8}
      onPress={onPress}
    >
      <Text numberOfLines={3} style={styles.modalButtonTextStyle}>
        {label ?? ""}
      </Text>
    </TouchableOpacity>
  );
}
export function ButtonNormal({ label, onPress }) {
  return (
    <TouchableOpacity
      style={styles.buttonStyle}
      activeOpacity={0.8}
      onPress={onPress}
    >
      <Text numberOfLines={3} style={styles.buttonTextStyle}>
        {label ?? ""}
      </Text>
    </TouchableOpacity>
  );
}
const SignoutModal = (props) => {
  const hideModal = () => {
    props.setVisible();
  };

  return (
    <Modal visible={props?.showModal} transparent={true}>
      <SafeAreaView style={[styles.modalContainer]}>
        <View style={styles.whiteContainer}>
          <View style={styles.headerCont}>
            <Text style={styles.title}>Sign Out?</Text>
          </View>
          <View style={styles.contentContainer}>
            <View style={styles.textContainer}>
              <Text style={styles.modalTextInfo}>
                Are you sure you want to sign out?
              </Text>
            </View>
            <View style={styles.buttonContainer}>
              <View style={{ width: "100%" }}>
                <ButtonCheckOut
                  label="Sign Out"
                  onPress={() => {
                    props.onPress();
                  }}
                />
              </View>
              <View style={{ width: "100%" }}>
                <ButtonNormal
                  label="Cancel"
                  onPress={() => {
                    hideModal();
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};
export default SignoutModal;
const styles = StyleSheet.create({
  modalButtonStyle: {
    backgroundColor: "#FF0000",
    borderWidth: 0,
    color: "#FFFFFF",
    borderRadius: 20,
    width: "100%",
    justifyContent: "center",
    height: 45,
    alignItems: "center",
    justifyContent: "space-around",
  },
  modalButtonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    paddingHorizontal: 45,
    fontSize: 14,
  },
  headerCont: {
    marginHorizontal: 30,
    marginTop: 20,
  },
  title: {
    color: "#263238",
    fontSize: 24,
    fontWeight: "600",
  },
  textContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 20,
  },
  modalTextInfo: {
    alignItems: "center",
    justifyContent: "center",
    color: "#263238",
    fontFamily: "Roboto",
    fontWeight: "500",
    fontSize: 20,
    textAlign: "center",
  },
  modalContainer: {
    flex: 1,
    width: "100%",
    backgroundColor: "rgba(0, 0, 0, 0.8)",
    justifyContent: "center",
    alignItems: "center",
  },
  whiteContainer: {
    backgroundColor: "white",
    margin: 20,
    width: "90%",
    borderRadius: 25,
  },
  contentContainer: {
    padding: 20,
    paddingTop: 10,
    alignItems: "center",
  },
  closeContainer: {
    flexDirection: "column",
  },
  alignClose: {
    flex: 1,
    margin: 20,
    alignSelf: "flex-end",
  },
  closeClick: {
    width: 30,
    height: 30,
  },
  imageContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  closeIcon: {
    height: 137,
    width: 137,
  },
  buttonContainer: {
    flexDirection: "column",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  timingContainer: {
    marginBottom: 10,
  },
  infoContainer: {
    flexDirection: "column",
    margin: 30,
    justifyContent: "space-evenly",
  },
  infoAddress: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: 20,
  },
  infoTiming: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: 10,
  },
  infoCloseTime: {
    marginHorizontal: 20,
  },
  infoWeb: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 10,
  },
  infoActiveTime: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: "bold",
    color: "green",
  },
  infoInactiveTime: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: "bold",
    color: "red",
  },
  timingItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 40,
  },
  infoMarginLeft: {
    marginLeft: 20,
  },
  buttonStyle: {
    backgroundColor: "#fff",
    color: "#a38862",
    borderColor: "#a38862",
    borderWidth: 1,
    borderRadius: 20,
    height: 45,
    width: "100%",
    alignItems: "center",
    paddingHorizontal: 10,
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  buttonTextStyle: {
    color: "#a38862",
    paddingVertical: 10,
    fontSize: 13,
  },
});
