import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const CartDetails = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.subTotalContainer}>
        <View>
          <Text style={styles.title}>Subtotal:</Text>
        </View>
        <View style={styles.rightAlign}>
          <Text style={styles.totalAmount}>
            {"$" + parseFloat(props.price * props.quantity, 2).toFixed(2)}
          </Text>
        </View>
      </View>
      <View style={{ ...styles.subTotalContainer, ...styles.marginTop }}>
        <View style={styles.marginTopFive}>
          <Text style={styles.title}>QTY:</Text>
        </View>
        <View style={styles.qtyContainer}>
          <Text style={styles.qtyText}>{props.quantity}</Text>
        </View>
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => {
            props.navigation.navigate("DrawerNavigationRoutes", {
              screen: "homeScreenStack",
              params: {
                screen: "PizzaMenu",
                params: {
                  screen: "pizzaScreenCustomise",
                  params: {
                    pizzaId: props.id,
                    uuid: props.uuid,
                  },
                },
              },
            });
          }}
        >
          <View style={styles.modifyContainer}>
            <Text style={styles.modifyText}>Modify</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CartDetails;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  subTotalContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  marginTop: {
    marginTop: 10,
  },
  title: {
    fontSize: 14,
    color: "#607d8b",
    fontWeight: "600",
  },
  rightAlign: {
    alignSelf: "flex-end",
  },
  totalAmount: {
    fontSize: 24,
    color: "#673ab7",
    fontWeight: "600",
  },
  marginTopFive: {
    marginTop: 5,
  },
  qtyContainer: {
    width: 109,
    backgroundColor: "#f3e5f5",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    height: 34,
  },
  qtyText: {
    fontSize: 16,
    color: "#263238",
    fontWeight: "600",
  },
  modifyContainer: {
    width: 130,
    backgroundColor: "#673ab7",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    height: 34,
  },
  modifyText: {
    fontSize: 12,
    color: "#fff",
  },
});
