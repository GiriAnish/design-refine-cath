import React from "react";
import { View, TouchableOpacity, StyleSheet, Modal, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import _const from "../../data/const";
import Logo from ".././Components/svg/CB_Primary";
import CartCircle from "./svg/CheckCircle.js";
export function ButtonCheckOut({ label, onPress }) {
  return (
    <TouchableOpacity
      style={styles.modalButtonStyle}
      activeOpacity={0.8}
      onPress={onPress}
    >
      <Text numberOfLines={3} style={styles.modalButtonTextStyle}>
        {label ?? ""}
      </Text>
    </TouchableOpacity>
  );
}
export function ButtonNormal({ label, onPress }) {
  return (
    <TouchableOpacity
      style={styles.buttonStyle}
      activeOpacity={0.8}
      onPress={onPress}
    >
      <Text numberOfLines={3} style={styles.buttonTextStyle}>
        {label ?? ""}
      </Text>
    </TouchableOpacity>
  );
}
const CartModal = (props) => {
  const hideModal = () => {
    props.setVisible();
  };
  const navigateToCheckout = () => {
    props.navigation.navigate("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "Cart",
        params: {
          screen: "checkoutVIPScreen",
          initial: false,
          params: {
            checkOut: true,
          },
        },
      },
    });
    hideModal();
  };
  const navigateToPizzaMenu = () => {
    props.navigation.navigate("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "PizzaMenu",
        params: {
          screen: "PizzaMenuScreenHome",
        },
      },
    });
    hideModal();
  };
  return (
    <Modal
      visible={props?.showModal}
      onDismiss={hideModal}
      transparent={true}
      backdropOpacity={0.1}
    >
      <SafeAreaView style={[styles.modalContainer]}>
        <View style={styles.whiteContainer}>
          <View style={styles.contentContainer}>
            <View style={styles.imageContainer}>
              <CartCircle style={styles.cartCircle} height={100} width={100} />
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.modalTextInfo}>
                Item Added to Cart Successfully
              </Text>
            </View>
            <View style={styles.buttonContainer}>
              <View>
                <ButtonCheckOut
                  label="Checkout"
                  onPress={() => {
                    navigateToCheckout();
                  }}
                />
              </View>
              <View>
                <ButtonNormal
                  label="Continue Ordering More"
                  onPress={() => {
                    navigateToPizzaMenu();
                  }}
                />
              </View>
              <View>
                <ButtonNormal
                  label="View Cart"
                  onPress={() => {
                    props.onPress();
                  }}
                />
              </View>
              <View style={{ marginTop: 10 }}>
                <Logo width={200} height={130} viewBox="0 0 620 300" />
              </View>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};
export default CartModal;
const buttonStyles = StyleSheet.create({});
const styles = StyleSheet.create({
  modalButtonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderRadius: 20,
    width: 300,
    justifyContent: "center",
    height: 40,
    alignItems: "center",
    justifyContent: "space-around",
  },
  modalButtonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    paddingHorizontal: 45,
    fontSize: 12,
  },
  textContainer: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    marginTop: 10,
    marginBottom: 20,
  },
  modalTextInfo: {
    alignItems: "center",
    justifyContent: "center",
    color: "#263238",
    fontFamily: "Roboto",
    fontWeight: "500",
    fontSize: 19,
  },
  modalContainer: {
    flex: 1,
    padding: 10,
    backgroundColor: "rgba(0, 0, 0, 0.1)",
    justifyContent: "center",
    flexDirection: "row",
    paddingTop: 15,
    alignItems: "baseline",
  },
  whiteContainer: {
    backgroundColor: "white",
    width: "100%",
    maxWidth: 600,
    borderRadius: 35,
  },
  contentContainer: {
    padding: 20,
    alignItems: "center",
  },
  closeContainer: {
    flexDirection: "column",
  },
  alignClose: {
    flex: 1,
    margin: 20,
    alignSelf: "flex-end",
  },
  closeClick: {
    width: 30,
    height: 30,
  },
  imageContainer: {
    alignItems: "center",
    justifyContent: "center",
    width: 148,
    height: 148,
    backgroundColor: "#fafafa",
    borderRadius: 148,
    marginBottom: 20,
    marginTop: 5,
  },
  cartCircle: {
    marginLeft: 6,
    marginTop: 4,
  },
  closeIcon: {
    height: 100,
    width: 100,
  },
  buttonContainer: {
    flexDirection: "column",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  timingContainer: {
    marginBottom: 10,
  },
  infoContainer: {
    flexDirection: "column",
    margin: 30,
    justifyContent: "space-evenly",
  },
  infoAddress: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: 20,
  },
  infoTiming: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: 10,
  },
  infoCloseTime: {
    marginHorizontal: 20,
  },
  infoWeb: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 10,
  },
  infoActiveTime: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: "bold",
    color: "green",
  },
  infoInactiveTime: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: "bold",
    color: "red",
  },
  timingItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 40,
  },
  infoMarginLeft: {
    marginLeft: 20,
  },
  buttonStyle: {
    backgroundColor: "#fff",
    color: "#a38862",
    borderColor: "#a38862",
    borderWidth: 1,
    borderRadius: 20,
    height: 40,
    width: 300,
    alignItems: "center",
    paddingHorizontal: 10,
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  buttonTextStyle: {
    color: "#a38862",
    paddingVertical: 10,
    fontSize: 13,
  },
});
