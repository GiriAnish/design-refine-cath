import React from "react";
import Toast, { BaseToast } from "react-native-toast-message";

export const toastConfig = {
  success: ({ text1, props, ...rest }) =>
    text1 ? (
      <BaseToast
        {...rest}
        style={{
          borderColor: "#4caf50",
          marginVertical: 0,
          marginHorizontal: "2.5%",
          width: "95%",
          height: 90,
        }}
        contentContainerStyle={{
          paddingHorizontal: 5,
          backgroundColor: "#4caf50",
        }}
        trailingIconContainerStyle={{
          backgroundColor: "#4caf50",
          borderColor: "#4caf50",
        }}
        leadingIconContainerStyle={{ backgroundColor: "#4caf50" }}
        trailingIcon={require("../../assets/images/Close_Icon_5.png")}
        leadingIcon={require("../../assets/images/Check_Circle_Outline_Icon_2.png")}
        trailingIconStyle={{ width: 13, height: 13 }}
        onTrailingIconPress={() => {
          Toast.hide();
        }}
        text1Style={{
          fontSize: 18,
          fontWeight: "600",
          color: "#fff",
        }}
        text2Style={{
          fontSize: 14,
          color: "#fff",
        }}
        text1={text1}
        text2={rest.text2}
      />
    ) : null,

  error: ({ text1, props, ...rest }) => (
    <BaseToast
      {...rest}
      style={{
        borderColor: "#f44336",
        marginVertical: 0,
        marginHorizontal: "2.5%",
        width: "95%",
        height: 90,
      }}
      contentContainerStyle={{
        paddingHorizontal: 5,
        backgroundColor: "#f44336",
      }}
      trailingIconContainerStyle={{
        backgroundColor: "#f44336",
        borderColor: "#f44336",
      }}
      leadingIconContainerStyle={{ backgroundColor: "#f44336" }}
      trailingIcon={require("../../assets/images/Close_Icon_5.png")}
      leadingIcon={require("../../assets/images/Alert_Octagon_Outline_Icon_1.png")}
      trailingIconStyle={{ width: 13, height: 13 }}
      onTrailingIconPress={() => {
        Toast.hide();
      }}
      text1Style={{
        fontSize: 18,
        fontWeight: "600",
        color: "#fff",
      }}
      text2Style={{
        fontSize: 14,
        color: "#fff",
      }}
      text1={text1}
      text2={rest.text2}
    />
  ),
};
