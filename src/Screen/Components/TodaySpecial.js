import React from "react";
import { Text } from "react-native-paper";
import { View, Image, StyleSheet, Dimensions } from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { Rating } from "react-native-ratings";
import { connect } from "react-redux";
import useAxios from "axios-hooks";
import CONST from "../../data/const";
import CBLogoNoFill from "./svg/CBLogoGoldNoFill";

const defaultDescription = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
eiusmod tempor in iscing elit, sed do eiusmod tempor in.`;
const defaultTitle = "Chicken Veggie Supreme";
const defaultImageSource = require("../../assets/images/todayspecial/pizza.png");
export const PizzaCard = (props) => {
  const {
    name = defaultTitle,
    description = defaultDescription,
    price = 2.5,
    rating = 3,
    imageUrl,
    enableBottomLogo,
  } = props;

  const source = imageUrl ? { uri: imageUrl } : defaultImageSource;

  return (
    <View style={styles.subContainer}>
      <View style={styles.leftImageContainer}>
        <Image source={source} style={{ width: 105, height: 105 }} />
      </View>
      <View style={styles.rightContainer}>
        <Text style={styles.title}>{name}</Text>
        <Rating
          imageSize={16}
          ratingBackgroundColor="transparent"
          readonly
          startingValue={rating}
          style={{
            paddingVertical: 0,
            alignSelf: "flex-start",
            backgroundColor: "transparent",
          }}
        />

        <Text style={styles.description}>{description}</Text>
        <View style={styles.contentContainer}>
          <Text style={styles.price}>$ {price}</Text>
          {enableBottomLogo ? (
            <View style={styles.bottomLogo}>
              <CBLogoNoFill width={40} height={40} viewBox="0 -15 110 150" />
            </View>
          ) : null}
        </View>
      </View>
    </View>
  );
};
const TodaySpecial = (props) => {
  const { menuId } = props;
  const [{ data, loading }] = useAxios(
    CONST.baseUrl +
      `api/v1/item?menuId=${menuId}&itemType=special&pageNo=1&size=1`
  );
  const todayspecialItem = data?.length ? data : [];

  return (
    <View style={styles.container}>
      {!loading && (
        <View>
          <Text style={styles.headerTitle}>Today's Special</Text>
          <TouchableWithoutFeedback
            onPress={() => {
              props.navigation.navigate("DrawerNavigationRoutes", {
                screen: "homeScreenStack",
                params: {
                  screen: "PizzaMenu",
                  params: {
                    screen: "pizzaDetails",
                    params: {
                      pizzaId: todayspecialItem.id,
                      pizzaName: todayspecialItem.name,
                      price: todayspecialItem.price,
                    },
                  },
                },
              });
            }}
          >
            <PizzaCard {...todayspecialItem} />
          </TouchableWithoutFeedback>
        </View>
      )}
    </View>
  );
};
export default connect(
  (state) => ({
    menuId: state?.loggedInUser?.loggedInUser?.userDetails?.menuId,
  }),
  null
)(TodaySpecial);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    marginTop: 20,
  },
  contentContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  subContainer: {
    margin: 5,
    marginTop: 0,
    paddingLeft: 15,
    paddingRight: 22,
    paddingTop: 15,
    paddingBottom: 5,
    flexDirection: "row",
    justifyContent: "flex-start",
    borderWidth: 0.5,
    borderColor: "#fff",
    backgroundColor: "#fff",
    borderRadius: 5,
    shadowOpacity: 0.01,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 1,
  },
  leftImageContainer: {
    width: 100,
  },
  rightContainer: {
    paddingLeft: 20,
    width: Dimensions.get('window').width - 140,
  },
  description: {
    color: "#607d8b",
    fontSize: 12,
    marginVertical: 5,
  },
  price: {
    color: "#ef5350",
    fontSize: 13,
    fontWeight: "bold",
  },
  bottomLogo: {
    marginRight: 10
  },
  title: {
    color: "#212121",
    fontSize: 14,
    fontWeight: "600",
    marginBottom: 5,
  },
  headerTitle: {
    color: "#455a64",
    fontWeight: "bold",
    fontSize: 14,
  },
});
