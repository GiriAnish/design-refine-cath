import React, { useState, useEffect } from "react";
import { SafeAreaView, Text, StyleSheet } from "react-native";
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from "react-native-confirmation-code-field";

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    /*marginLeft: 20,
    marginRight: 20*/
  },
  codeFieldRoot: {
    padding: 8,
  },
  cell: {
    width: 70,
    height: 80,
    fontSize: 30,
    borderWidth: 2,
    alignItems: "center",
    textAlign: "center",
    justifyContent: "center",
    paddingVertical: 18,
    backgroundColor: "#fff",
    borderColor: "#a38862",
    borderStyle: "solid",
    borderRadius: 5,
    marginHorizontal: 5,
  },

  filledCell: {
    borderColor: "#4caf50",
  },
  codeTitle: {
    color: "#a38862",
    fontWeight: "400",
    fontSize: 18,
    textAlign: "center",
    marginBottom: 5,
  },
  codeTitleGreen: {
    color: "#4caf50",
  },
});

const CELL_COUNT = 4;

const ConfirmationCodeField = ({ getValue, title, autoFill }) => {
  const [value, setValue] = useState("");
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [useProps, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  useEffect(() => {
    if (value && value.length === CELL_COUNT) {
      getValue(value);
      setValue("");
    }
  }, [value, getValue]);

  useEffect(() => {
    if (autoFill && autoFill.length === CELL_COUNT) {
      setValue(autoFill);
    }
  }, [autoFill]);

  return (
    <SafeAreaView style={styles.root}>
      {/* <Text
        style={[
          styles.codeTitle,
          value && value.length === CELL_COUNT && styles.codeTitleGreen,
        ]}
      >
        {title}
      </Text> */}
      <CodeField
        ref={ref}
        {...useProps}
        value={value}
        onChangeText={setValue}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        renderCell={({ index, symbol, isFocused }) => (
          <Text
            key={index}
            style={[styles.cell, symbol && styles.filledCell]}
            onLayout={getCellOnLayoutHandler(index)}
          >
            {symbol || (isFocused ? <Cursor /> : null)}
          </Text>
        )}
      />
    </SafeAreaView>
  );
};

export default ConfirmationCodeField;
