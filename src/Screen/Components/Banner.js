import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import styled from "@emotion/native";

const SideBar = styled.View`
  background: ${({ theme }) => theme.Colors.purple};
  width: 20px;
  height: 128px;
`;
const Banner = (props) => {
  const {
    onPress,
    touchableOpacity,
    imageSource,
    primaryText,
    secondaryText,
    touchableOpacityText,
  } = props;
  return (
    <View style={styles.root}>
      <SideBar />
      <View style={styles.container}>
        <Image source={imageSource} style={styles.image} />
        <View style={styles.imageOverlay} />
        <View style={styles.contentContainer}>
          <View style={styles.textContainer}>
            <Text style={styles.primaryText}>{primaryText}</Text>
            <Text style={styles.secondaryText}>{secondaryText}</Text>
          </View>
          <View style={styles.rightButton}>
            {touchableOpacity && (
              <TouchableOpacity
                style={styles.buttonStyle}
                activeOpacity={0.8}
                onPress={onPress}
              >
                <Text numberOfLines={3} style={styles.buttonTextStyle}>
                  {touchableOpacityText}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </View>
  );
};

export default Banner;

const styles = StyleSheet.create({
  root: {
    height: 142,
    flexDirection: "row",
  },
  image: {
    position: "absolute",
    width: "100%",
    height: "90%",
  },
  imageOverlay: {
    position: "absolute",
    width: "100%",
    height: "90%",
    backgroundColor: "#263238",
    opacity: 0.27,
  },
  sideBar: {
    backgroundColor: "#673ab7",
    width: 10,
  },
  container: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 18,
    justifyContent: "space-between",
  },
  textContainer: {
    marginTop: 40,
  },
  rightButton: {
    marginTop: 0,
  },
  primaryText: {
    fontSize: 25,
    fontWeight: "300",
    color: "white",
    marginTop: -18,
    marginBottom: 3,
    marginLeft: -8,
  },
  secondaryText: {
    fontSize: 25,
    color: "white",
    fontWeight: "400",
    marginLeft: 10,
    marginBottom: 70,
    textAlign: "left",
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderRadius: 24,
    marginTop: 45,
    marginLeft: 20,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    paddingHorizontal: 27,
    fontSize: 13,
    fontWeight: "500",
  },
});
