import React from "react";
import { View, TouchableOpacity } from "react-native";
import Menu from "../Components/svg/Menu";

const NavigationDrawerHeader = (props) => {
  const toggleDrawer = () => {
    props.navigationProps.toggleDrawer();
  };

  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableOpacity onPress={toggleDrawer}>
        <Menu height={35} width={36} fill={"#37474f"} viewBox="-7 2 25 15" />
      </TouchableOpacity>
    </View>
  );
};
export default NavigationDrawerHeader;
