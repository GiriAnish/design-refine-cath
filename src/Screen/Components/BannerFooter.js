import React from "react";
import { StyleSheet, View, Image } from "react-native";

const ForgotPwdBanner = (props) => {
  const { imageSource } = props;
  return (
    <View style={styles.Container}>
      <Image source={imageSource} style={styles.imageFooter} />
    </View>
  );
};

export default ForgotPwdBanner;

const styles = StyleSheet.create({
  Container: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    width: 500,
    height: 250,
  },
  imageFooter: {
    width: 310,
    height: 200,
  },
});
