import React from "react";
import { ScrollView, View, Text } from "react-native";
import { OrderCard } from "./OrderCard";

const MyOrdersList = (props) => {
  const { now, past, navigation, reOrder } = props;
  return (
    <ScrollView>
      {now.length > 0 ? (
        <>
          <View style={{ fontSize: 21 }}>
            <Text style={{ fontSize: 21, margin: 10 }}>Now</Text>
          </View>
          {now.map((item) => {
            return (
              <OrderCard
                navigation={navigation}
                reOrder={reOrder}
                order={item}
              />
            );
          })}
        </>
      ) : null}
      {past.length > 0 ? (
        <>
          <View style={{ fontSize: 21 }}>
            <Text style={{ fontSize: 21 }}>Past</Text>
          </View>
          {past.map((item) => {
            return (
              <OrderCard
                navigation={navigation}
                reOrder={reOrder}
                order={item}
              />
            );
          })}
        </>
      ) : null}
    </ScrollView>
  );
};
export default MyOrdersList;
