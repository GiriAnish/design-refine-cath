import React from "react";
import { StyleSheet, View, Pressable } from "react-native";
import Phone from "./svg/Phone";

const CallToStore = (props) => {
  const { onPress } = props;
  return (
    <Pressable onPress={onPress}>
      <View pointerEvents="none" style={styles.callIcon}>
        <Phone width={20} height={20} color={"#f44336"} />
      </View>
    </Pressable>
  );
};

export default CallToStore;
const styles = StyleSheet.create({
  callIcon: {
    flex: 1,
    marginRight: 10,
    marginTop: Platform.select({
      ios: 0,
      android: 8,
    }),
    borderRadius: 50,
    width: 38,
    maxHeight: 38,
    backgroundColor: "#ffebee",
    alignItems: "center",
    justifyContent: "center",
  },
});
