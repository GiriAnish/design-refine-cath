import React, { useCallback } from "react";
import styled from "@emotion/native";
import { Text } from "react-native-paper";
import {
  ScrollView,
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  Dimensions,
} from "react-native";
import SocialFollow from "./SocialFollow";
import CBLogoNoFill from "./svg/CBLogoGoldNoFill";

const ImageContainer = styled.Image`
  width: 100px;
  height: 100px;
  border-radius: 10px;
  margin-top: 10px;
`;
export const ChefsList = ({ props }) => {
  const {
    id,
    firstName,
    lastName,
    twitterURL,
    facebookURL,
    instagramURL,
    linkedinURL,
    biography,
    imageURL,
    navigation,
  } = props;
  const openCustomise = useCallback((staffId) => {
    navigation.navigate("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "Staff",
        params: {
          screen: "ourstaffdetails",
          params: {
            staffId: staffId,
          },
        },
      },
    });
  }, []);
  return (
    <ScrollView>
      <TouchableWithoutFeedback
        onPress={() => {
          openCustomise(id);
        }}
      >
        <View key={id} style={styles.subContainer}>
          <View style={styles.leftImageContainer}>
            <ImageContainer source={{ uri: imageURL }} />
          </View>
          <View style={styles.rightContainer}>
            <View style={styles.contentContainer}>
              <View style={styles.staffNameSocial}>
                <Text numberOfLines={1} style={styles.title}>
                  {firstName} {lastName}
                </Text>
                {instagramURL || twitterURL || facebookURL || linkedinURL ? (
                  <View>
                    <SocialFollow
                      twitterURL={twitterURL}
                      facebookURL={facebookURL}
                      instagramURL={instagramURL}
                      linkedinURL={linkedinURL}
                    />
                  </View>
                ) : null}
              </View>
              <View style={styles.rightIconContainer}>
                <CBLogoNoFill width={40} height={40} viewBox="0 0 110 150" />
              </View>
            </View>
            <View style={styles.description}>
              <Text numberOfLines={3}>{biography}</Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </ScrollView>
  );
};
export default ChefsList;
const styles = StyleSheet.create({
  subContainer: {
    display: "flex",
    padding: 10,
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 10,
    marginRight: 13,
    marginLeft: 11,
    marginBottom: 6,
    backgroundColor: "#fff",
    height: 144,
    borderRadius: 5,
    shadowColor: "#000000",
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 2,
  },
  contentContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
  },
  staffNameSocial: {
    width: "86%"
  },
  leftImageContainer: {
    width: 100,
    height: 100,
  },
  rightContainer: {
    overflow: "hidden",
    padding: 15,
    width: Dimensions.get("window").width - 140,
  },
  rightIconContainer: {
    bottom: 10,
    width: "100%",
  },
  description: {
    color: "#263238",
    fontSize: 12,
    marginTop: 7,
    fontFamily: "Roboto",
  },
  title: {
    width: 172,
    height: 25,
    color: "#212121",
    fontSize: 15,
    fontFamily: "Roboto-Medium",
    marginBottom: 7,
    marginTop: -16,
  },
});
