import React, { useCallback } from "react";
import { View, TouchableOpacity, StyleSheet, Linking } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import theme from "../../assets/css/commonThemes";
export const SocialFollow = (props) => {
  const { twitterURL, facebookURL, instagramURL, linkedinURL } = props;
  const OpenURLButton = ({ url, icon }) => {
    const handlePress = useCallback(async () => {
      const supported = await Linking.canOpenURL(url);
      if (supported) {
        await Linking.openURL(url);
      } else {
        Alert.alert(`Don't know how to open this URL: ${url}`);
      }
    }, [url]);
    return (
      <TouchableOpacity style={styles.socialIcon} onPress={handlePress}>
        <Icon name={icon} color={theme.Colors.purple} size={20} />
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.socialContainer}>
      {facebookURL && facebookURL !== "" ? (
        <OpenURLButton icon={"facebook-square"} url={facebookURL} />
      ) : null}
      {twitterURL && twitterURL !== "" ? (
        <OpenURLButton icon={"twitter"} url={twitterURL} />
      ) : null}
      {instagramURL && instagramURL !== "" ? (
        <OpenURLButton icon={"instagram"} url={instagramURL} />
      ) : null}
      {linkedinURL && linkedinURL !== "" ? (
        <OpenURLButton icon={"linkedin-square"} url={linkedinURL} />
      ) : null}
    </View>
  );
};

export default SocialFollow;
const styles = StyleSheet.create({
  socialContainer: {
    alignItems: "center",
    width: "100%",
    flexDirection: "row",
    textAlign: "center",
  },
  socialIcon: {
    paddingRight: 20,
  },
});
