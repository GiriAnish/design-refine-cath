import React from "react";
import { View, Text, StyleSheet } from "react-native";

const ItemToCart = (props) => {
  return (
    <View style={styles.container}>
      {props.cart.map(([key, value], index) => {
        return (
          <View style={styles.itemContainer} key={index}>
            <Text style={styles.activeTitle}>{key}</Text>
            <Text style={styles.inactiveTitle}>
              {value[0]?.label
                ? value.map((i) => i.label).join(", ") + " "
                  : value + " "}
            </Text>
          </View>
        );
      })}
    </View>
  );
};

export default ItemToCart;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  itemContainer: {
    marginTop: 5,
  },
  activeTitle: {
    color: "#455a64",
    fontSize: 12,
    fontWeight: "600",
  },
  inactiveTitle: {
    color: "#607d8b",
    fontSize: 12,
    marginTop: 4,
  },
});
