import React, { useState, useEffect } from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Modal,
  Text,
  Linking,
  Platform,
  Alert,
} from "react-native";
import { connect } from "react-redux";
import DeviceInfo from "react-native-device-info";
import { SafeAreaView } from "react-native-safe-area-context";
import _const from "../../data/const";
import Logo from "../Components/svg/CB_Primary.js";
import Call from "../Components/svg/Phone.js";
import MapMarker from "../Components/svg/MapMarker.js";
import Earth from "../Components/svg/Earth.js";
import ChervonDown from "../Components/svg/ChevronDown.js";
import ClockOutline from "../Components/svg/ClockOutline.js";
import Close from "../Components/svg/Close.js";
import Direction from "../Components/svg/Directions.js";
import parsePhoneNumber from "libphonenumber-js";

const CustomModal = (props) => {
  const [showTimings, setShowTimings] = useState(false);
  const [storeData, setStoreData] = useState(null);
  const { restaurantId, jwtToken } = props;
  const [phoneNumber, setPhoneNumber] = useState(null);

  const hideModal = () => {
    props.setVisible();
    setShowTimings(false);
  };

  const renderTimings = () => {
    const workingDays = storeData?.restaurantTimings?.filter(
      (timing) => !timing.nonWorkingDay
    );
    return (
      <View style={styles.timingContainer}>
        {(storeData?.restaurantTimings || []).map((item) => {
          return (
            <View style={styles.timingItem} key={item.dayId}>
              <View style={styles.timingLeft}>
                <Text
                  style={
                    item.active
                      ? { fontWeight: "600", fontSize: 12 }
                      : { fontSize: 12 }
                  }
                >
                  {item.day}
                </Text>
              </View>
              <View
                style={
                  item.nonWorkingDay ? styles.closedStatus : styles.timingRight
                }
              >
                <Text
                  style={
                    item.active
                      ? { fontWeight: "600", fontSize: 12 }
                      : { fontSize: 12 }
                  }
                >
                  {item.nonWorkingDay
                    ? "Closed"
                    : `${item.startTime} - ${item.endTime}`}
                </Text>
              </View>
            </View>
          );
        })}
      </View>
    );
  };

  const renderOpenCloseComponent = () => {
    return (
      <View style={styles.infoTiming}>
        <ClockOutline height={23} width={23} fill={"#2196f3"} />
        <Text
          style={
            storeData?.status?.toLowerCase() === "closed"
              ? styles.infoInactiveTime
              : styles.infoActiveTime
          }
        >
          {storeData?.status}
        </Text>
        <Text
          style={{
            ...styles.infoCloseTime,
            color:
              storeData?.status?.toLowerCase() === "closed" ? "green" : "red",
          }}
        >
          {storeData?.timingStatus}
        </Text>
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => setShowTimings(!showTimings)}
        >
          <ChervonDown height={23} width={23} fill={"#607d8b"} />
        </TouchableOpacity>
      </View>
    );
  };

  useEffect(() => {
    async function fetchResturant() {
      try {
        const requestParams = {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${jwtToken}`,
          },
        };
        const response = await fetch(
          _const.baseUrl + "api/v1/restaurant/" + restaurantId,
          requestParams
        );
        if (!response.ok) {
          setStoreData(null);
          return;
        }
        const restaurantDetails = await response.json();
        if (restaurantDetails?.contactNumber1) {
          const parsedPhoneNumber = parsePhoneNumber(
            restaurantDetails?.contactNumber1?.toString(),
            "US"
          );
          setPhoneNumber(parsedPhoneNumber.formatNational());
        }

        setStoreData(restaurantDetails);
      } catch (error) {
        Alert.alert("Unable to fetch restaurant timings.");
        console.log(error);
      }
    }

    if (props.showModal) {
      fetchResturant();
    }
  }, [props.showModal]);

  return (
    <Modal visible={props?.showModal} onDismiss={hideModal} transparent={true}>
      <SafeAreaView style={[styles.modalContainer]}>
        <View style={styles.whiteContainer}>
          <View style={styles.closeContainer}>
            <View style={styles.alignClose}>
              <TouchableOpacity
                activeOpacity={0.5}
                style={styles.closeClick}
                onPress={() => {
                  hideModal();
                }}
              >
                <Close height={23} width={23} fill={"#263238"} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.close}>
            <Logo height="147" width="305" />
          </View>
          <View style={styles.buttonContainer}>
            {!DeviceInfo.isTablet() && (
              <View>
                <TouchableOpacity
                  style={styles.buttonStyle}
                  activeOpacity={0.5}
                  onPress={() => {
                    Linking.openURL(`tel:${storeData?.contactNumber1}`);
                  }}
                >
                  <Call height={23} width={23} fill={"#a38862"} />
                  <Text style={styles.buttonTextStyle}>Call Restaurant</Text>
                </TouchableOpacity>
              </View>
            )}
            <View>
              <TouchableOpacity
                style={styles.buttonStyle}
                activeOpacity={0.5}
                onPress={() => {
                  const lat = storeData?.latitude;
                  const lng = storeData?.longitude;
                  const scheme = Platform.select({
                    ios: "maps:0,0?q=",
                    android: "geo:0,0?q=",
                  });
                  const latLng = `${lat},${lng}`;
                  const label = "Custom Label";
                  const url = Platform.select({
                    ios: `${scheme}${label}@${latLng}`,
                    android: `${scheme}${latLng}(${label})`,
                  });
                  Linking.openURL(url);
                }}
              >
                <Direction height={23} width={23} fill={"#a38862"} />
                <Text style={styles.buttonTextStyle}>Get Directions</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.infoContainer}>
            <View style={styles.infoAddress}>
              <Call width={23} height={23} fill={"#2196f3"} />
              <Text style={styles.infoMarginLeft}>{phoneNumber}</Text>
            </View>
            <View style={styles.infoAddress}>
              <MapMarker height={23} width={23} fill={"#2196f3"} />
              <Text style={styles.infoMarginLeft}>{storeData?.address}</Text>
            </View>
            {storeData && renderOpenCloseComponent()}
            {showTimings && renderTimings()}
            <View style={styles.infoWeb}>
              <Earth height={23} width={23} fill={"#2196f3"} />
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL("http://" + storeData?.website)
                    .then()
                    .catch();
                }}
              >
                <Text style={{ ...styles.infoMarginLeft, marginBottom: 20 }}>
                  {storeData?.website}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export default connect(
  (state) => ({
    restaurantId: state?.loggedInUser?.loggedInUser?.userDetails?.restaurantId,
    jwtToken: state?.loggedInUser?.loggedInUser?.userDetails?.jwtToken,
  }),
  null
)(CustomModal);

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    width: "100%",
    backgroundColor: "rgba(96, 125, 139, 0.5)",
    alignItems: "center",
    justifyContent: "center",
  },
  whiteContainer: {
    backgroundColor: "white",
    margin: 20,
    width: "90%",
    borderRadius: 20,
  },
  closeContainer: {
    flexDirection: "column",
  },
  alignClose: {
    flex: 1,
    margin: 20,
    alignSelf: "flex-end",
  },
  closeClick: {
    width: 30,
    height: 30,
  },
  close: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
  },
  closeIcon: {
    height: 147,
    width: 305,
  },
  buttonContainer: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
  },
  timingContainer: {
    marginBottom: 15,
    marginLeft: 5,
  },
  infoContainer: {
    flexDirection: "column",
    margin: 20,
    justifyContent: "space-evenly",
  },
  infoAddress: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: 20,
  },
  timingLeft: { width: "40%" },
  timingRight: { alignItems: "center", width: "60%" },
  infoTiming: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: 10,
  },
  infoCloseTime: {
    marginHorizontal: 20,
  },
  infoWeb: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 10,
  },
  infoActiveTime: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: "bold",
    color: "green",
  },
  infoInactiveTime: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: "bold",
    color: "red",
  },
  timingItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 40,
    marginVertical: 5,
  },
  infoMarginLeft: {
    marginLeft: 20,
  },
  infoMargin: {
    marginLeft: 25,
  },
  buttonStyle: {
    backgroundColor: "#fff",
    color: "#a38862",
    borderColor: "#a38862",
    borderWidth: 1,
    borderRadius: 20,
    height: 42,
    width: 140,
    alignItems: "center",
    paddingHorizontal: 10,
    marginLeft: 10,
    marginTop: -20,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  buttonTextStyle: {
    color: "#a38862",
    paddingVertical: 10,
    fontSize: 13,
  },
  closedStatus: {
    alignItems: "flex-start",
    width: "60%",
    marginLeft: 10,
  },
});
