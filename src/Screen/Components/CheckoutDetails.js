import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const CheckoutDetails = (props) => {
  let calculatedTotal = parseFloat(
    props.cart.reduce((total, item) => total + item.quantity * item.price, 0)
  ).toFixed(2);
  let tax = parseFloat((calculatedTotal * 8.25) / 100).toFixed(2);
  let GrandTotal = parseFloat(
    parseFloat(calculatedTotal) + parseFloat(tax)
  ).toFixed(2);
  const buttonTxt = props.buttonTxt ?? "Checkout";
  return (
    <View style={{ }}>
      <View style={styles.subContainer}>
        <View>
          <Text style={styles.header}>Subtotal</Text>
        </View>
        <View style={styles.flexEnd}>
          <Text style={styles.valueText}>{"$ " + calculatedTotal} {props?.cart?.total}</Text>
        </View>
      </View>
      <View style={styles.subContainer}>
        <View>
          <Text style={styles.header}>Tax</Text>
        </View>
        <View style={styles.flexEnd}>
          <Text style={styles.valueText}>{"$ " + tax}</Text>
        </View>
      </View>
      <View style={styles.subContainer}>
        <View>
          <Text style={styles.totalHeader}>Grand Total</Text>
        </View>
        <View style={styles.flexEnd}>
          <Text style={styles.totalValue}>{"$ " + GrandTotal}</Text>
        </View>
      </View>
      <TouchableOpacity onPress={props.onPress} style={styles.subContainerAdv}>
        <View style={styles.checkoutContainer}>
          <Text style={styles.checkoutText}>{buttonTxt}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default CheckoutDetails;

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
    padding: 15,
    borderWidth: 0.5,
    borderColor: "#fff",
    backgroundColor: "#fff",
    borderRadius: 5,
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 1,
  },
  subContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
    marginLeft: 6,
  },
  subContainerAdv: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignSelf: "center",
    marginVertical: 10,
    marginTop: 30,
  },
  flexEnd: { alignSelf: "flex-end" },
  valueText: { fontSize: 16, color: "#263238", fontWeight: "600" },
  header: { fontSize: 15, color: "#607d8b" },
  totalHeader: { fontSize: 18, color: "#37474f", fontWeight: "600" },
  totalValue: { fontSize: 22, color: "#f44336", fontWeight: "600" },
  checkoutContainer: {
    width: "100%",
    height: 45,
    backgroundColor: "#f44336",
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
  },
  checkoutText: { fontSize: 16, fontWeight: "500", color: "#fff" },
});
