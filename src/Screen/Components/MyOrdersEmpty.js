import React from "react";
import styled from "@emotion/native";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import CbBottomLogo from "../Components/svg/CB_Secondary.js";
import Logo from "../Components/svg/CB_Primary.js";
const MainContainer = styled.View`
  background: ${({ theme }) => theme.Colors.white};
  align-items: center;
  border-radius: 5px;
  elevation: 1;
`;
const InfoContainer = styled.View`
  margin-top: 5px;
  margin-bottom: 5px;
  align-items: center;
  justify-content: center;
`;
const Paragraph = styled.Text`
  color: ${({ theme }) => theme.Colors.hoki};
  font-family: ${({ theme }) => theme.fontFamily};
  font-weight: 400;
  font-size: 15px;
  text-align: center;
  padding-right: 30px;
  padding-left: 30px;
  padding-bottom: 10px;
  margin-bottom: 10px;
`;
const TitleText = styled.Text`
  color: ${({ theme }) => theme.Colors.Atomic};
  font-family: Roboto-Medium;
  font-weight: 900;
  font-size: 21px;
  text-align: center;
  margin-bottom: 10px;
`;
const TouchableButton = styled((props) => <TouchableOpacity {...props} />)`
  background: ${({ theme }) => theme.Colors.purple};
  border-width: 0px;
  color: ${({ theme }) => theme.Colors.white};
  border-radius: 25px;
  width: 230px;
  height: 45px;
  align-items: center;
  justify-content: space-around;
`;
const TouchableButtonText = styled((props) => <Text {...props} />)`
  color: ${({ theme }) => theme.Colors.white};
  padding: 10px 45px;
  font-weight: 500;
  font-size: 14px;
`;
const OrderButton = ({ label, onPress }) => {
  return (
    <TouchableButton activeOpacity={0.8} onPress={onPress}>
      <TouchableButtonText numberOfLines={3}>{label ?? ""}</TouchableButtonText>
    </TouchableButton>
  );
};
const MyOrdersEmpty = (props) => {
  return (
    <SafeAreaView style={{ flex: 1, margin: 10 }}>
      <MainContainer>
        <View
          style={{
            alignItems: "center",
            height: 120,
            width: 300,
            justifyContent: "center",
            padding: 80,
          }}
        >
          <Logo height="140" width="430" style={{ marginTop: 40 }} />
        </View>
        <InfoContainer>
          <TitleText>{props.title}</TitleText>
          <InfoContainer>
            <Paragraph>{props.info}</Paragraph>
          </InfoContainer>
          <OrderButton label="Order New Pizza" onPress={props.onPress} />
        </InfoContainer>
        <View style={orderEmptyStyles.bottomLogo}>
          <CbBottomLogo height={80} width={400} viewBox="20 0 50 120" />
        </View>
      </MainContainer>
    </SafeAreaView>
  );
};

const orderEmptyStyles = StyleSheet.create({
  bottomLogo: {
    marginLeft: 80,
    marginTop: 30,
    marginBottom: 35,
  },
  partnershipText: {
    fontSize: 18,
    color: "#673ab7",
    marginLeft: 90,
  },
});

export default MyOrdersEmpty;
