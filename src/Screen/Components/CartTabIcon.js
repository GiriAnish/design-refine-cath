import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { useSelector } from "react-redux";
import CartOutline from "../Components/svg/CartOutline";
import theme from "../../assets/css/commonThemes";

const CartTabIcon = (props) => {
  const cart = useSelector((state) => state.cart.cartItems);
  return (
    <View>
      {cart && cart.length > 0 && (
        <View style={styles.cartCircleContainer}>
          <Text style={styles.cartText}>
            {cart.reduce((prev, curr) => {
              return prev + curr.quantity;
            }, 0)}
          </Text>
        </View>
      )}
      {props.focused && (
        <CartOutline width={24} height={24} color={theme.Colors.purple} />
      )}
      {!props.focused && (
        <CartOutline width={24} height={24} color={"#607d8b"} />
      )}
    </View>
  );
};

export default CartTabIcon;

const styles = StyleSheet.create({
  cartCircleContainer: {
    position: "absolute",
    height: 20,
    width: 20,
    borderRadius: 15,
    backgroundColor: "#673ab7",
    left: 10,
    bottom: 15,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 2000,
  },
  cartText: {
    color: "white",
    fontWeight: "bold",
  },
});
