import React from "react";
import { StyleSheet, View, Image } from "react-native";

const MailEnvlope = (props) => {
  const { imageSource } = props;
  return (
    <View style={styles.Container}>
      <Image source={imageSource} style={styles.circleImageLayout} />
    </View>
  );
};

export default MailEnvlope;
const styles = StyleSheet.create({
  Container: {
    justifyContent: "center",
    alignItems: "center",
    height: 90,
    width: "100%",
    backgroundColor: "#673ab7",
  },
  circleImageLayout: {
    marginTop: 70,
    width: 140,
    height: 140,
    borderRadius: 70,
    backgroundColor: "#ede7f6",
  },
  text: {
    fontSize: 25,
    textAlign: "center",
  },
});
