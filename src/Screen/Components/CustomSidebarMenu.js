import React, { useState, useEffect } from "react";
import styled from "@emotion/native";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import * as loginActions from "../../store/action/loginActions";
import { useDispatch, useSelector } from "react-redux";
import SignoutModal from "../Components/SignoutModal";
import * as CartActions from "../../store/action/cart";
import Home from "../Components/svg/Home";
import CartCheck from "../Components/svg/CartCheck";
import ChefHat from "../Components/svg/ChefHat";
import Pizza from "../Components/svg/Pizza";
import MapMarker from "../Components/svg/MapMarker";
import Cog from "../Components/svg/Cog";
import Close from "../Components/svg/Close";
import ChevronRight from "../Components/svg/Chevronright";
import CBLogoMain from "../Components/svg/CBLogoMain";
import CBPartnership from "../Components/svg/CB_Partnership";
import * as loadingAction from "../../store/action/loading";
import { HEADER_TITLE_FONT_SIZE } from "../../data/const";

const MenuListContainer = styled.View`
  flex-direction: column;
  margin-horizontal: 40px;
  margin-top: 20px;
  justify-content: space-evenly;
  padding-left: 20px;
`;
const MenuListItem = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  margin-bottom: 20px;
`;
const MenuListLabel = styled.Text`
  margin-left: 20px;
`;
const PizzaIcon = styled((props) => <Pizza {...props} />)`
  transform: rotate(45deg);
`;

const CustomSidebarMenu = (props) => {
  const dispatch = useDispatch();
  const [showSignoutModal, setShowSignoutModal] = useState(false);
  const [appVersion, setAppVersion] = useState("");
  const loginInfo = useSelector((state) => state.loggedInUser.loggedInUser);

  useEffect(() => {
    (async () => {
      const app = await AsyncStorage.getItem("appVersion");
      setAppVersion(app);
    })();
  }, []);

  const logoutClicked = async () => {
    const app = await AsyncStorage.getItem("appVersion");
    AsyncStorage.clear();
    await AsyncStorage.setItem("appVersion", app);
    dispatch(CartActions.resetCart());
    dispatch(loadingAction.setLoading(false));
    dispatch(loginActions.resetUserLogin());
    props.navigation.toggleDrawer();
    props.navigation.replace("Auth");
  };

  const logoutPressed = () => {
    setShowSignoutModal(true);
  };

  function isOrderingFeatureUnavailable() {
    return (
      loginInfo?.userDetails?.maxItemPerCart === 0 ||
      loginInfo?.userDetails?.maxOrderPerDay === 0
    );
  }

  return (
    <SafeAreaView style={stylesSidebar.sideMenuContainer}>
      <View style={stylesSidebar.topContainerWithClose}>
        <View style={{ flexDirection: "row" }}>
          <View style={stylesSidebar.menuTextContainer}>
            <Text style={stylesSidebar.menuText}>Menu</Text>
          </View>
          <View style={stylesSidebar.closeContainer}>
            <View style={stylesSidebar.alignClose}>
              <TouchableOpacity
                activeOpacity={0.5}
                style={stylesSidebar.closeClick}
                onPress={() => {
                  props.navigation.toggleDrawer();
                }}
              >
                <Close width={25} height={25} color={"#000"} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={stylesSidebar.LogoC}>
          <CBLogoMain width="320" height="130" />
        </View>
        <View style={stylesSidebar.linkCont}>
          <TouchableOpacity
            style={stylesSidebar.buttonStyle}
            activeOpacity={0.5}
            onPress={() => {
              props.navigation.navigate("DrawerNavigationRoutes", {
                screen: "homeScreenStack",
                params: {
                  screen: "MyProfileScreenStack",
                },
              });
            }}
          >
            <Text style={stylesSidebar.buttonTextStyle}>My Profile</Text>
            <ChevronRight width={25} height={25} color={"#fff"} />
          </TouchableOpacity>

          <TouchableOpacity
            style={stylesSidebar.buttonStyleLogout}
            activeOpacity={0.5}
            onPress={logoutPressed}
          >
            <Text style={stylesSidebar.buttonLogoutTextStyle}>Sign out</Text>
          </TouchableOpacity>
        </View>

        <ScrollView>
          <MenuListContainer>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                props.navigation.toggleDrawer();
                props.navigation.navigate("DrawerNavigationRoutes", {
                  screen: "homeScreenStack",
                  params: {
                    screen: "Home",
                  },
                });
              }}
            >
              <MenuListItem>
                <Home width={25} height={25} color={"#673ab7"} />
                <MenuListLabel>Home</MenuListLabel>
              </MenuListItem>
            </TouchableOpacity>

            {!isOrderingFeatureUnavailable() && (
              <>
                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => {
                    props.navigation.toggleDrawer();
                    props.navigation.navigate("DrawerNavigationRoutes", {
                      screen: "homeScreenStack",
                      params: {
                        screen: "PizzaMenu",
                      },
                    });
                  }}
                >
                  <MenuListItem>
                    <PizzaIcon width={25} height={25} color={"#673ab7"} />
                    <MenuListLabel>Pizza Menu</MenuListLabel>
                  </MenuListItem>
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => {
                    props.navigation.toggleDrawer();
                    props.navigation.navigate("DrawerNavigationRoutes", {
                      screen: "homeScreenStack",
                      params: {
                        screen: "OrdersMenu",
                        params: {
                          screen: "MyOrders",
                        },
                      },
                    });
                  }}
                >
                  <MenuListItem>
                    <CartCheck width={25} height={25} color={"#673ab7"} />
                    <MenuListLabel>My Orders</MenuListLabel>
                  </MenuListItem>
                </TouchableOpacity>
              </>
            )}

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                props.navigation.toggleDrawer();
                props.navigation.navigate("DrawerNavigationRoutes", {
                  screen: "homeScreenStack",
                  params: {
                    screen: "MyLocationScreenStack",
                  },
                });
              }}
            >
              <MenuListItem>
                <MapMarker width={25} height={25} color={"#673ab7"} />
                <MenuListLabel>My Locations</MenuListLabel>
              </MenuListItem>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                props.navigation.toggleDrawer();
                props.navigation.navigate("DrawerNavigationRoutes", {
                  screen: "homeScreenStack",
                  params: {
                    screen: "Staff",
                  },
                });
              }}
            >
              <MenuListItem>
                <ChefHat width={25} height={25} color={"#673ab7"} />
                <MenuListLabel>Our Staff</MenuListLabel>
              </MenuListItem>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => {
                props.navigation.toggleDrawer();
                props.navigation.navigate("DrawerNavigationRoutes", {
                  screen: "homeScreenStack",
                  params: {
                    screen: "Settings",
                  },
                });
              }}
            >
              <MenuListItem>
                <Cog width={25} height={25} color={"#673ab7"} />
                <MenuListLabel>Settings</MenuListLabel>
              </MenuListItem>
            </TouchableOpacity>
          </MenuListContainer>
        </ScrollView>
        <View>
          <CBPartnership
            width={300}
            height={80}
            viewBox="0 0 400 140"
            marginTop={10}
            marginLeft={-10}
          />
        </View>
        <View style={{ alignItems: "center" }}>
          <Text>{appVersion}</Text>
        </View>
      </View>

      <SignoutModal
        onPress={logoutClicked}
        showModal={showSignoutModal}
        setVisible={() => {
          setShowSignoutModal(false);
        }}
      />
    </SafeAreaView>
  );
};

export default CustomSidebarMenu;

const stylesSidebar = StyleSheet.create({
  cont: { width: 25, alignItems: "center" },
  topContainerWithClose: { flex: 1, flexDirection: "column", height: "100%" },
  menuTextContainer: {
    alignItems: "center",
    width: "90%",
    height: 50,
    flexDirection: "column",
    textAlign: "center",
    marginTop: 20,
  },
  menuText: {
    fontSize: HEADER_TITLE_FONT_SIZE,
  },
  LogoC: {
    alignItems: "center",
    width: "100%",
    flexDirection: "column",
    textAlign: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  linkCont: {
    alignItems: "center",
    width: "100%",
    flexDirection: "row",
    textAlign: "center",
    justifyContent: "center",
    marginTop: -50,
  },
  sideMenuContainer: {
    width: "100%",
    height: "100%",
    backgroundColor: "#fafcfe",
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 1.5,
    borderRadius: 20,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    height: 42,
    alignItems: "center",
    paddingHorizontal: 10,
    marginTop: 35,
    marginBottom: 25,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 12,
    marginRight: 10,
    marginLeft: 10,
  },
  buttonStyleLogout: {
    backgroundColor: "#fff",
    color: "#a38862",
    borderColor: "#a38862",
    borderWidth: 1.5,
    borderRadius: 33,
    height: 42,
    width: "42%",
    alignItems: "center",
    paddingHorizontal: 10,
    marginLeft: 10,
    marginTop: 35,
    marginBottom: 25,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  buttonLogoutTextStyle: {
    color: "#a38862",
    paddingVertical: 10,
    fontSize: 12,
    fontFamily: "Roboto",
  },
  logoCont: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: -60,
    marginRight: 10,
  },
  partnershipText: {
    fontSize: 18,
    color: "#673ab7",
    marginLeft: 30,
  },
  logo: {
    width: "90%",
  },
  closeContainer: {},
  alignClose: {
    marginVertical: 20,
  },
  closeClick: {
    width: 30,
    height: 30,
  },
});
