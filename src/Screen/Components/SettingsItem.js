import React from "react";
import { StyleSheet, View, Text } from "react-native";

const SettingsItem = ({ data }) => {
  return (
    <View>
      {data.map((item) => {
        return (
          <View style={styles.itemContainer} key={item.title}>
            <View style={styles.leftContainer}>
              <Text style={{ ...styles.text, ...styles.bold, ...styles.title }}>
                {item.title}
              </Text>
              <Text style={{ ...styles.colorSubTitle, ...styles.text }}>
                {item.subline1}
              </Text>
              <Text style={{ ...styles.colorSubTitle, ...styles.text }}>
                {item.subline2}
              </Text>
            </View>
            {item.default && (
              <View style={styles.rightContainer}>
                <Text
                  style={{ ...styles.text, ...styles.bold, ...styles.default }}
                >
                  Default
                </Text>
              </View>
            )}
          </View>
        );
      })}
    </View>
  );
};

export default SettingsItem;

const styles = StyleSheet.create({
  itemContainer: {
    padding: 10,
    flexDirection: "row",
    borderWidth: 0.5,
    borderColor: "#fff",
    backgroundColor: "#fff",
    borderRadius: 5,
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 0.2,
  },
  leftContainer: { width: "70%", paddingLeft: 20 },
  rightContainer: { justifyContent: "center", width: "30%" },
  text: { fontSize: 16, marginBottom: 5 },
  bold: { fontWeight: "600" },
  colorTitle: { color: "#37474f" },
  colorSubTitle: { color: "#546e7a" },
  default: { color: "#673ab7" },
});
