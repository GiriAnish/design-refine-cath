import React from "react";
import { View, TouchableOpacity, StyleSheet, Modal, Text } from "react-native";
import { cancelOrder } from "../../store/action/myOrdersAction";
import { connect } from "react-redux";
import { SafeAreaView } from "react-native-safe-area-context";
import _const from "../../data/const";
import SvgUri from "react-native-svg-uri";
import str from "../../assets/images/svg/logo.js";
import CloseOctagon from "../Components/svg/CloseOctagon.js";

export function CancelButtonYes({ onPress }) {
  return (
    <TouchableOpacity
      style={styles.modalButtonStyle}
      activeOpacity={0.8}
      onPress={onPress}
    >
      <Text
        numberOfLines={3}
        style={styles.modalButtonTextStyle}
        onPress={onPress}
      >
        Yes, Cancel Order
      </Text>
    </TouchableOpacity>
  );
}
export function CancelButtonNo({ label, onPress }) {
  return (
    <TouchableOpacity
      style={styles.buttonStyle}
      activeOpacity={0.8}
      onPress={onPress}
    >
      <Text numberOfLines={3} style={styles.buttonTextStyle}>
        Do Not Cancel Order
      </Text>
    </TouchableOpacity>
  );
}
const CancelOrderModal = (props) => {
  const { cancelOrderDispatch, navigation, itemId } = props;
  const RenderImage = () => {
    return <SvgUri width="300" height="120" svgXmlData={str} />;
  };
  const hideModal = () => {
    props.setVisible();
  };
  const handleCancelOrder = () => {
    cancelOrderDispatch(itemId);
    hideModal();
    navigation.navigate("MyOrders");
  };

  return (
    <Modal visible={props?.showModal} onDismiss={hideModal} transparent={true}>
      <SafeAreaView style={[styles.modalContainer]}>
        <View style={styles.whiteContainer}>
          <View style={styles.imageContainer}>
            <CloseOctagon height={65} width={65} />
          </View>
          <Text style={styles.modalTextInfo}>
            Do you want to cancel this order?
          </Text>
          <Text style={styles.modalSubTitle}>
            This action can not be undone.
          </Text>
          <View style={styles.buttonContainer}>
            <CancelButtonYes
              onPress={() => {
                handleCancelOrder();
              }}
            />
            <CancelButtonNo
              onPress={() => {
                hideModal();
              }}
            />
            <View style={{ marginTop: 32, alignItems: "center" }}>
              <RenderImage />
            </View>
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};
export default connect(null, (dispatch, ownProps) => ({
  cancelOrderDispatch: (orderId) => dispatch(cancelOrder(orderId)),
}))(CancelOrderModal);
const styles = StyleSheet.create({
  modalButtonStyle: {
    backgroundColor: "#f44336",
    borderRadius: 31,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "stretch",
  },
  modalButtonTextStyle: {
    color: "#FFFFFF",
    fontSize: 16,
    paddingVertical: 8,
  },
  modalTextInfo: {
    color: "#263238",
    fontFamily: "Roboto-Medium",
    fontWeight: "500",
    fontSize: 20,
    paddingTop: 8,
    textAlignVertical: "center",
  },
  modalSubTitle: {
    color: "#263238",
    fontFamily: "Roboto",
    fontWeight: "500",
    fontSize: 20,
    textAlignVertical: "center",
  },
  modalContainer: {
    flex: 1,
    width: "100%",
    backgroundColor: "rgba(0, 0, 0, 0.8)",
  },
  whiteContainer: {
    backgroundColor: "white",
    margin: 20,
    marginTop: 50,
    borderRadius: 35,
    alignItems: "center",
    padding: 20,
  },
  imageContainer: {
    alignItems: "center",
    justifyContent: "center",
    width: 100,
    height: 100,
    borderRadius: 100,
    backgroundColor: "#ffebee",
  },
  closeIcon: {
    height: 65,
    width: 65,
  },
  buttonContainer: {
    marginTop: 32,
    alignSelf: "stretch",
  },
  buttonStyle: {
    borderRadius: 31,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#a38862",
    borderWidth: 1,
    marginTop: 12,
  },
  buttonTextStyle: {
    color: "#a38862",
    fontSize: 16,
    paddingVertical: 8,
  },
});
