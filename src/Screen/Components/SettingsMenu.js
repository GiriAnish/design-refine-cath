import React, { useEffect } from "react";
import { View, Text, StyleSheet, TouchableWithoutFeedback } from "react-native";
import ChevronRight from "../Components/svg/Chevronright";
import { useTabVisible } from "../../contexts/navigation";

const SettingsMenu = (props) => {
  const { setTabVisible } = useTabVisible();

  useEffect(() => {
    const unsubscribe = props.navigation.addListener("focus", () => {
      setTabVisible(true);
    });
    return unsubscribe;
  }, [props.navigation]);
  return (
    <View style={styles.top}>
      <Text style={styles.title}>{props.name}</Text>
      <View style={styles.container}>
        {props.items.map((item) => {
          return (
            <TouchableWithoutFeedback
              key={item}
              onPress={() => {
                props.navigation.navigate(
                  item.replace(" ", "").replace(" ", "")
                );
              }}
            >
              <View style={styles.itemContainer}>
                <View style={styles.itemLeft}>
                  <Text style={styles.itemTitle}>{item}</Text>
                </View>
                <View style={styles.iconContainer}>
                  <ChevronRight width={25} height={25} color={"#546e7a"} />
                </View>
              </View>
            </TouchableWithoutFeedback>
          );
        })}
      </View>
    </View>
  );
};

export default SettingsMenu;

const styles = StyleSheet.create({
  top: { padding: 10, marginBottom: 10 },
  title: { color: "#455a64", fontSize: 18, fontWeight: "600" },
  container: {
    marginTop: 10,
    borderWidth: 0.5,
    borderColor: "#fff",
    backgroundColor: "#fff",
    borderRadius: 5,
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 0.2,
  },
  itemContainer: {
    padding: 10,
    flexDirection: "row",
    justifyContent: "center",
    borderColor: "#eceff1",
    borderBottomWidth: 0.5,
  },
  itemLeft: { width: "80%" },
  itemTitle: { color: "#546e7a", fontSize: 16, paddingTop: 2 },
  iconContainer: { width: "20%", alignItems: "flex-end" },
});
