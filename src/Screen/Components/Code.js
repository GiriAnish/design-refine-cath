import React, { useState } from "react";
import { TextInput, StyleSheet } from "react-native";
import {
  CodeField,
  useClearByFocusCell,
  useBlurOnFulfill,
} from "react-native-confirmation-code-field";

const Code = (props) => {
  const [value, setValue] = useState("");
  const ref = useBlurOnFulfill({ value, cellCount: 4 });
  const [codeFieldProps, getCellOnLayout] = useClearByFocusCell({
    value,
    setValue,
  });

  const submit = () => {
    props.onCodeChange(value);
  };

  return (
    <CodeField
      ref={ref}
      {...codeFieldProps}
      value={value}
      onChangeText={setValue}
      onEndEditing={submit}
      renderCell={({ index, symbol, isFocused }) => (
        <TextInput
          style={[styles.cell, isFocused && styles.focusCell]}
          returnKeyType={"next"}
          keyboardType="numeric"
          textContentType="oneTimeCode"
          onLayout={getCellOnLayout(index)}
        >
          {symbol}
        </TextInput>
      )}
    />
  );
};

export default Code;

const styles = StyleSheet.create({
  root: { flex: 1, padding: 20 },
  title: { textAlign: "center", fontSize: 30 },
  cell: {
    width: 70,
    height: 80,
    lineHeight: 38,
    fontSize: 40,
    borderWidth: 1,
    borderColor: "#a38862",
    textAlign: "center",
    margin: 5,
    paddingTop: 25,
    borderRadius: 5,
  },
  focusCell: {
    borderColor: "red",
  },
});
