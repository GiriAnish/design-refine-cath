import React from "react";
import { View, TouchableOpacity, StyleSheet, Modal } from "react-native";
import Close from "../Components/svg/Close";
import { WebView } from "react-native-webview";
import CONST from "../../data/const";

import { SafeAreaView } from "react-native-safe-area-context";
const TermsModal = (props) => {
  const hideModal = () => {
    props.setVisible();
  };
  return (
    <Modal
      visible={props?.showModal}
      onDismiss={hideModal}
      transparent={true}
      backdropOpacity={0.1}
    >
      <SafeAreaView style={[styles.modalContainer]}>
        <View style={styles.closeContainer}>
          <View style={styles.whiteContainer}>
            <View style={styles.alignClose}>
              <TouchableOpacity
                activeOpacity={0.5}
                style={styles.closeClick}
                onPress={() => {
                  hideModal();
                }}
              >
                <Close height={23} width={23} fill={"#263238"} />
              </TouchableOpacity>
            </View>
            <View style={styles.webViewContainer}>
              <WebView
                javaScriptEnabled={true}
                source={{
                  uri: CONST.termsAndConditionsUrl,
                }}
                style={styles.webView}
              />
            </View>
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};
export default TermsModal;
const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    width: "100%",
    backgroundColor: "rgba(0, 0, 0, 0.8)",
  },
  whiteContainer: {
    backgroundColor: "white",
    margin: 20,
    marginTop: 50,
    borderRadius: 35,
    alignItems: "center",
    paddingTop: 20,
    paddingBottom: 40,
  },
  closeIcon: {
    height: 65,
    width: 65,
  },
  webViewContainer: {
    overflow: "scroll",
    marginTop: 20,
    width: "100%",
    height: "95%",
  },
  webView: {
    height: "100%",
    width: "100%",
  },
  alignClose: {
    flex: 1,
    marginRight: 15,
    alignSelf: "flex-end",
    marginTop: 5,
  },
  closeClick: {
    width: 30,
    height: 30,
  },
  closeContainer: {
    flexDirection: "column",
  },
});