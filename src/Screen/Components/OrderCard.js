import React, { useCallback } from "react";
import styled from "@emotion/native";
import { SafeAreaView, View } from "react-native";
import LeftLogo from "../Components/svg/CBLogoGoldNoFill";
import AutoRenew from "../Components/svg/Autorenew";
import { convertLocalDateFormat } from "../../utils/date";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

const MainContainer = styled.View`
  background: ${({ theme }) => theme.Colors.white};
  font-family: ${({ theme }) => theme.fontFamily};
  align-items: flex-start;
  border-radius: 5px;
  elevation: 0.5;
  padding: 10px;
  margin-top: 5px;
  margin-bottom: 5px;
  overflow: hidden;
`;
const SubContainer = styled.View`
  width: 100%;
  padding: 5px 15px;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
`;
const MainSubContainer = styled.View`
  width: 100%;
`;
const StatusButtons = styled.View`
  width: 40%;
`;
const ButtonPending = styled.Text`
  font-size: 13px;
  color: ${({ theme }) => theme.Colors.sandal};
  border-color: ${({ theme }) => theme.Colors.sandal};
  border-width: 1px;
  padding: 3px 20px;
  border-radius: 4px;
  font-family: "Roboto";
  align-self: flex-end;
`;
const ButtonCompleted = styled.Text`
  font-size: 13px;
  padding: 3px 18px;
  color: #f44336;
  border-color: #f44336;
  border-width: 1px;
  border-radius: 4px;
  align-self: flex-end;
`;
const DateTimeText = styled.Text`
  color: ${({ theme }) => theme.Colors.hoki};
  font-weight: 400;
  font-size: 18px;
  text-align: left;
`;
const TotalText = styled.Text`
  color: ${({ theme }) => theme.Colors.purple};
  font-weight: 500;
  font-size: 16px;
  text-align: left;
`;
const TotalAmount = styled.Text`
  color: ${({ theme }) => theme.Colors.purple};
  font-weight: 500;
  font-size: 24px;
  text-align: right;
`;
const AddressTitle = styled.Text`
  color: ${({ theme }) => theme.Colors.Atomic};
  font-weight: 500;
  font-size: 16px;
  text-align: left;
  margin: 5px;
`;
const OrderTitle = styled.Text`
  color: ${({ theme }) => theme.Colors.oxfordBlue};
  font-weight: 500;
  font-size: 18px;
  width: 50%;
  margin-left: 6px;
`;
const AddressText = styled.Text`
  width: 210px;
  color: #546e7a;
  font-family: "Roboto";
  font-weight: 400;
  font-size: 16px;
  text-align: left;
  margin-left: 5px;
  padding-bottom: 4px;
`;
const ButtonReorder = styled.Text`
  width: 96px;
  height: 25px;
  color: #2196f3;
  font-family: "Roboto";
  font-weight: 500;
  font-size: 16px;
  text-align: left;
`;
export const OrderCard = ({ navigation, order, reOrder }) => {
  const {
    id,
    orderNumber,
    orderStatus,
    createdOn,
    community,
    state,
    city,
    cancelButtonShow,
    totalAmount,
  } = order;
  const openOrderDetail = useCallback(() => {
    navigation.navigate("OrderDetails", {
      orderId: orderNumber,
      id: id,
    });
  }, []);
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <MainContainer>
        <MainSubContainer>
          <TouchableWithoutFeedback
            onPress={() => {
              openOrderDetail();
            }}
          >
            <SubContainer>
              <LeftLogo height={35} width={35} viewBox="0 0 135 135" />
              <OrderTitle>Order#: {orderNumber}</OrderTitle>
              <StatusButtons>
                {cancelButtonShow ? (
                  <ButtonPending>
                    {orderStatus?.toLowerCase() === "completed"
                      ? "Ready for Delivery"
                      : orderStatus}
                  </ButtonPending>
                ) : (
                  <ButtonCompleted>
                    {orderStatus?.toLowerCase() === "completed"
                      ? "Ready for Delivery"
                      : orderStatus}
                  </ButtonCompleted>
                )}
              </StatusButtons>
            </SubContainer>
            <SubContainer>
              <DateTimeText>{convertLocalDateFormat(createdOn)}</DateTimeText>
            </SubContainer>
            <SubContainer>
              <TotalText>Total Amount</TotalText>
              <TotalAmount>${totalAmount}</TotalAmount>
            </SubContainer>
          </TouchableWithoutFeedback>
        </MainSubContainer>
        <SubContainer style={{ alignItems: "flex-start" }}>
          <TouchableWithoutFeedback
            onPress={() => {
              if (!cancelButtonShow) {
                reOrder(id);
              } else {
                openOrderDetail();
              }
            }}
          >
            <View>
              {cancelButtonShow ? (
                <AddressTitle>{community}</AddressTitle>
              ) : (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    display: "none",
                  }}
                >
                  <AutoRenew height={35} width={35} />
                  <ButtonReorder>Reorder</ButtonReorder>
                </View>
              )}
              {cancelButtonShow ? (
                <AddressText style={{ flexWrap: "wrap-reverse" }}>
                  {city}, {state}
                </AddressText>
              ) : (
                <View></View>
              )}
            </View>
          </TouchableWithoutFeedback>
        </SubContainer>
      </MainContainer>
    </SafeAreaView>
  );
};
