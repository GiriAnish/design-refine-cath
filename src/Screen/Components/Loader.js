import React from "react";
import styled from "@emotion/native";
import { Modal, ActivityIndicator } from "react-native";
import theme from "../../assets/css/commonThemes";

const ModalContainer = styled.View`
  flex: 1;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;
  background-color: #00000040;
`;
const ActivityIndicatorWrapper = styled.View`
background: ${({ theme }) => theme.Colors.white};
height: 100px;
width: 100px;
border-radius: 10px;
display: flex,
align-items: center;
justify-content: space-around;
`;
const LoadingIndicator = styled((props) => <ActivityIndicator {...props} />)`
  align-items: center;
  height: 80px;
`;

const Loader = (props) => {
  const { loading } = props;
  return (
    <Modal transparent={true} animationType={"none"} visible={loading}>
      <ModalContainer>
        <ActivityIndicatorWrapper>
          <LoadingIndicator
            animating={loading}
            color={theme.Colors.purple}
            size="large"
          />
        </ActivityIndicatorWrapper>
      </ModalContainer>
    </Modal>
  );
};

export default Loader;
