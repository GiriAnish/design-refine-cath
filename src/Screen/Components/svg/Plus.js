import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z" fill={props.color} />
    </Svg>
  );
}

export default SvgComponent;
