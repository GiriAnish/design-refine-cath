import * as React from "react";
import Svg, { Path, G, Defs, LinearGradient, Stop } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M391.942 0c54.017 0 82.85 28.814 82.85 82.828v776.344c0 54.002-28.83 82.828-82.85 82.828H86.162c-54.02 0-82.85-28.82-82.85-82.822V82.828C3.313 28.814 32.147 0 86.162 0h305.78z"
        fill="#FBFAFA"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M473.135 223.966c.078-3.231.552-7.213.552-7.213l3.313 1.029v97.477l-3.313 1.208s-.358-1.004-.552-5.773c0-4.417-.078-83.497 0-86.728zM4.969 283.119c.077 3.231 0 45.452 0 49.87-.195 4.768-.553 5.773-.553 5.773l-3.312-1.209v-60.618l3.313-1.029s.474 3.982.552 7.213zM4.969 202.881c.077 3.231 0 44.452 0 48.869-.195 4.768-.553 5.773-.553 5.773l-3.312-1.209v-59.617l3.313-1.03s.474 3.983.552 7.214z"
        fill="#4A4A4A"
      />
      <G filter="url(#prefix__filter0_d_32:61764)">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M4.417 136.619v21.398c0 3.098-.668 5.911-.668 5.911l-2.647-.006L0 162.72v-29.489l1.031-1.029H3.75s.668 1.778.668 4.417z"
          fill="#4A4A4A"
        />
      </G>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M295.66 39.924a6.625 6.625 0 10-12.737-3.653 6.625 6.625 0 0012.737 3.653zM213.657 34.233a3.865 3.865 0 100 7.73h49.686a3.865 3.865 0 100-7.73h-49.686z"
        fill="#4A4A4A"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M118.133 28.711c3.049 0 5.52 2.47 5.52 5.518v3.31c0 13.407 10.874 24.276 24.288 24.276h182.16c13.414 0 24.288-10.869 24.288-24.276v-3.31a5.518 5.518 0 015.52-5.518h43.056c23.779 0 43.056 19.268 43.056 43.035v798.931c0 23.767-19.277 43.034-43.056 43.034H75.077c-23.78 0-43.056-19.267-43.056-43.034V71.746c0-23.767 19.277-43.035 43.056-43.035h43.056z"
        fill="url(#prefix__paint0_linear_32:61764)"
      />
      <Path
        d="M127 882h225"
        stroke="#fff"
        strokeWidth={15}
        strokeLinecap="round"
      />
      <Defs>
        <LinearGradient
          id="prefix__paint0_linear_32:61764"
          x1={239.021}
          y1={28.711}
          x2={239.021}
          y2={913.711}
          gradientUnits="userSpaceOnUse"
        >
          <Stop stopColor="#732DD1" />
          <Stop offset={1} stopColor="#8D95DB" />
        </LinearGradient>
      </Defs>
    </Svg>
  );
}

export default SvgComponent;
