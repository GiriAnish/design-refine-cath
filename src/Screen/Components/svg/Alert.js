import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M8.27 3L3 8.27v7.46L8.27 21h7.46C17.5 19.24 21 15.73 21 15.73V8.27L15.73 3H8.27zm.83 2h5.8L19 9.1v5.8L14.9 19H9.1L5 14.9V9.1L9.1 5zM11 15h2v2h-2v-2zm0-8h2v6h-2V7z"
        fill={props.color}
      />
    </Svg>
  );
}

export default SvgComponent;
