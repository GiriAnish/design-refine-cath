import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M12 2a10 10 0 100 20 10 10 0 000-20zm0 2a8 8 0 010 16V4z"
        fill={props.color}
      />
    </Svg>
  );
}

export default SvgComponent;
