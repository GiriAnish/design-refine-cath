import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M20 12a8 8 0 11-5.8-7.69l1.57-1.57A10 10 0 1022 12h-2zM7.91 10.08L6.5 11.5 11 16 21 6l-1.41-1.42L11 13.17l-3.09-3.09z"
        fill={props.color}
      />
    </Svg>
  );
}

export default SvgComponent;
