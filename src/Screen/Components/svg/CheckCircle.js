import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      fill="#4caf50"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
      viewBox="1 1 23 23"
    >
      <Path
        d="M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
        fill={props.color}
      />
    </Svg>
  );
}

export default SvgComponent;
