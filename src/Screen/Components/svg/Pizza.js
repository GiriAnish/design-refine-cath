import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M12 15a2 2 0 110-4 2 2 0 010 4zM7 7c0-1.11.89-2 2-2a2 2 0 11-2 2zm5-5C8.43 2 5.23 3.54 3 6l9 16 9-16c-2.22-2.46-5.43-4-9-4z"
        fill={props.color}
      />
    </Svg>
  );
}

export default SvgComponent;
