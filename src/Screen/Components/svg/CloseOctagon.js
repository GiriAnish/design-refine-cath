import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      fill="#f44336"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
      viewBox="-1 -1 25  25"
    >
      <Path
        d="M8.27 3L3 8.27v7.46L8.27 21h7.46C17.5 19.24 21 15.73 21 15.73V8.27L15.73 3H8.27zm.83 2h5.8L19 9.1v5.8L14.9 19H9.1L5 14.9V9.1L9.1 5zm.02 2.71L7.71 9.12 10.59 12l-2.88 2.88 1.41 1.41L12 13.41l2.88 2.88 1.41-1.41L13.41 12l2.88-2.88-1.41-1.41L12 10.59"
        fill={props.color}
      />
    </Svg>
  );
}

export default SvgComponent;
