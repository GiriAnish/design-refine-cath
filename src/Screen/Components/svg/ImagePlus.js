import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = (props) => (
  <Svg
    width={24}
    height={24}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
    viewBox="10 -25 60 80 "
  >
    <Path
      d="M5 3a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h9.09c-.06-.33-.09-.66-.09-1 0-.68.12-1.36.35-2H5l3.5-4.5 2.5 3 3.5-4.5 2.23 2.97c.97-.63 2.11-.97 3.27-.97.34 0 .67.03 1 .09V5a2 2 0 0 0-2-2H5Zm14 13v3h-3v2h3v3h2v-3h3v-2h-3v-3h-2Z"
      fill="#90a4ae"
    />
  </Svg>
);

export default SvgComponent;
