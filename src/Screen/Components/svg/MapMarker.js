import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
  return (
    <Svg
      width={props.width}
      height={props.height}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M12 6.5a2.5 2.5 0 110 5 2.5 2.5 0 010-5zM12 2a7 7 0 017 7c0 5.25-7 13-7 13S5 14.25 5 9a7 7 0 017-7zm0 2a5 5 0 00-5 5c0 1 0 3 5 9.71C17 12 17 10 17 9a5 5 0 00-5-5z"
        fill={props.color}
      />
    </Svg>
  );
}

export default SvgComponent;
