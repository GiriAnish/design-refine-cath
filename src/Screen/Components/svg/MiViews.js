import * as React from "react";
import Svg, {
  G,
  Path,
  Defs,
  LinearGradient,
  Stop,
  ClipPath,
} from "react-native-svg";

const SvgComponent = (props) => (
  <Svg
    width={props.width}
    height={props.height}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
    viewBox="0 0 440 250"
  >
    <G clipPath="url(#a)">
      <Path
        d="M333.7 75.7c4-.3 4.3-4.5 4.3-4.5l-22.4-23.4c-6.5-17 12.7-22.3 12.7-22.3h34.1c-7.8-7.7-18.4-12.4-30.1-12.4-23.7 0-43 19.3-43 43.2 0 7 1.6 13.5 4.5 19.3h39.9v.1Z"
        fill="url(#b)"
      />
      <Path
        d="M329.3 37.2c-3 1-2.7 5.1-2.7 5.1 20.1 18.6 22.8 25 22.8 25 3 15.3-13.5 20.4-13.5 20.4h-33.1c7.7 7.4 18.1 11.9 29.6 11.9 23.7 0 43-19.3 43-43.2 0-6.9-1.6-13.5-4.5-19.3l-41.6.1Z"
        fill="url(#c)"
      />
      <Path
        d="m325.4 58.1-5.6 33.8c-.5 3.2 1.7 6.3 4.9 6.9 3.2.5 6.3-1.7 6.9-4.9l4.1-25-10.3-10.8Z"
        fill="url(#d)"
      />
      <Path
        d="M338.8 13.7c-3.2-.5-6.3 1.7-6.9 4.9l-4.1 24.9c1.8 1.7 7.3 6.9 10.4 10.2l5.5-33.1c.6-3.3-1.6-6.4-4.9-6.9Z"
        fill="url(#e)"
      />
      <Path
        d="M340.7 12c3.369 0 6.1-2.686 6.1-6s-2.731-6-6.1-6c-3.369 0-6.1 2.686-6.1 6s2.731 6 6.1 6Z"
        fill="url(#f)"
      />
      <Path
        d="M9.2 26.7h14l11.1 32.2L46 26.7h14.1l8.3 60.2H54.2l-4-34.6h-.1L37 86.9h-5.7L18.8 52.3h-.1l-4.6 34.6H0l9.2-60.2Z"
        fill="url(#g)"
      />
      <Path
        d="M88.2 30.2c0 4.5-3.3 8.1-7.4 8.1-4.1 0-7.4-3.7-7.4-8.1 0-4.4 3.3-8.1 7.4-8.1 4 0 7.4 3.7 7.4 8.1Zm-.8 56.7H74.2v-41h13.2v41Z"
        fill="url(#h)"
      />
      <Path
        d="m119.3 63.3 13.3-36.6H148l-23.3 60.2h-11l-23-60.2h15.4l13.2 36.6Z"
        fill="url(#i)"
      />
      <Path
        d="M166.7 30.2c0 4.5-3.3 8.1-7.4 8.1-4.1 0-7.4-3.7-7.4-8.1 0-4.4 3.3-8.1 7.4-8.1 4.1 0 7.4 3.7 7.4 8.1Zm-.8 56.7h-13.2v-41h13.2v41Z"
        fill="url(#j)"
      />
      <Path
        d="M212.8 68.9H185c0 5.9 2.8 9.1 8.3 9.1 2.8 0 4.9-1 6.3-3.8h12.7c-2.1 9.7-10.4 14.1-18.9 14.1-12.3 0-21.5-7.7-21.5-21.8 0-13.7 8.5-21.9 20.6-21.9 12.9 0 20.4 8.8 20.4 22.8v1.5h-.1Zm-12.3-8.4c-.7-3.9-3.9-6.5-7.5-6.5-3.8 0-7 2.2-7.8 6.5h15.3Z"
        fill="url(#k)"
      />
      <Path
        d="m237.3 68.1 9.4-22.2h7.8l9.4 22.2 9.4-22.2h14.6l-19.4 41h-8.7l-9.2-22.4-9.2 22.4h-8.6l-19.4-41H228l9.3 22.2Z"
        fill="url(#l)"
      />
    </G>
    <Defs>
      <LinearGradient
        id="b"
        x1={289.342}
        y1={44.43}
        x2={362.445}
        y2={44.43}
        gradientUnits="userSpaceOnUse"
      >
        <Stop stopColor="#00AEEF" />
        <Stop offset={1} stopColor="#21409A" />
      </LinearGradient>
      <LinearGradient
        id="c"
        x1={302.678}
        y1={68.311}
        x2={375.283}
        y2={68.311}
        gradientUnits="userSpaceOnUse"
      >
        <Stop stopColor="#00AEEF" />
        <Stop offset={1} stopColor="#21409A" />
      </LinearGradient>
      <LinearGradient
        id="d"
        x1={319.724}
        y1={78.456}
        x2={335.724}
        y2={78.456}
        gradientUnits="userSpaceOnUse"
      >
        <Stop offset={0} stopColor="#58595B" />
        <Stop offset={1} stopColor="#E6E7E8" />
      </LinearGradient>
      <LinearGradient
        id="e"
        x1={327.83}
        y1={33.616}
        x2={343.807}
        y2={33.616}
        gradientUnits="userSpaceOnUse"
      >
        <Stop offset={0} stopColor="#58595B" />
        <Stop offset={1} stopColor="#E6E7E8" />
      </LinearGradient>
      <LinearGradient
        id="f"
        x1={334.6}
        y1={6.035}
        x2={346.775}
        y2={6.035}
        gradientUnits="userSpaceOnUse"
      >
        <Stop offset={0} stopColor="#58595B" />
        <Stop offset={1} stopColor="#E6E7E8" />
      </LinearGradient>
      <LinearGradient
        id="g"
        x1={34.2}
        y1={86.904}
        x2={34.2}
        y2={26.711}
        gradientUnits="userSpaceOnUse"
      >
        <Stop offset={0} stopColor="#58595B" />
        <Stop offset={1} stopColor="#E6E7E8" />
      </LinearGradient>
      <LinearGradient
        id="h"
        x1={80.776}
        y1={86.903}
        x2={80.776}
        y2={22.081}
        gradientUnits="userSpaceOnUse"
      >
        <Stop offset={0} stopColor="#58595B" />
        <Stop offset={1} stopColor="#E6E7E8" />
      </LinearGradient>
      <LinearGradient
        id="i"
        x1={119.305}
        y1={86.904}
        x2={119.305}
        y2={26.711}
        gradientUnits="userSpaceOnUse"
      >
        <Stop stopColor="#00AEEF" />
        <Stop offset={1} stopColor="#21409A" />
      </LinearGradient>
      <LinearGradient
        id="j"
        x1={159.284}
        y1={86.903}
        x2={159.284}
        y2={22.081}
        gradientUnits="userSpaceOnUse"
      >
        <Stop stopColor="#00AEEF" />
        <Stop offset={1} stopColor="#21409A" />
      </LinearGradient>
      <LinearGradient
        id="k"
        x1={192.303}
        y1={88.261}
        x2={192.303}
        y2={44.593}
        gradientUnits="userSpaceOnUse"
      >
        <Stop stopColor="#00AEEF" />
        <Stop offset={1} stopColor="#21409A" />
      </LinearGradient>
      <LinearGradient
        id="l"
        x1={250.55}
        y1={86.903}
        x2={250.55}
        y2={45.95}
        gradientUnits="userSpaceOnUse"
      >
        <Stop stopColor="#00AEEF" />
        <Stop offset={1} stopColor="#21409A" />
      </LinearGradient>
      <ClipPath id="a">
        <Path fill="#fff" d="M0 0h375.4v99.6H0z" />
      </ClipPath>
    </Defs>
  </Svg>
);

export default SvgComponent;
