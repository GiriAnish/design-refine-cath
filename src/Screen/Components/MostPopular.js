import * as React from "react";
import { Text } from "react-native-paper";
import { View, Image, StyleSheet, Dimensions } from "react-native";
import { Rating } from "react-native-ratings";
import {
  ScrollView,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import { connect } from "react-redux";
import useAxios from "axios-hooks";
import CONST from "../../data/const";

const windowWidth = Dimensions.get("window").width;
const MostPopular = (props) => {
  const { menuId } = props;
  const [{ data, loading }] = useAxios(
    CONST.baseUrl +
      `api/v1/item?menuId=${menuId}&itemType=popular&pageNo=1&size=3`
  );
  const items = data?.length ? data : [];
  return (
    <View style={styles.container}>
      {!loading && items.length > 0 && (
        <View>
          <Text style={styles.title}> Most Popular</Text>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.popularItemContainer}>
              {items.map((item) => {
                const { id, name, price, rating, imageUrl } = item;
                return (
                  <TouchableWithoutFeedback
                    onPress={() => {
                      props.navigation.navigate("DrawerNavigationRoutes", {
                        screen: "homeScreenStack",
                        params: {
                          screen: "PizzaMenu",
                          params: {
                            screen: "pizzaDetails",
                            params: {
                              pizzaId: id,
                              pizzaName: name,
                              price: price,
                            },
                          },
                        },
                      });
                    }}
                  >
                    <View style={styles.itemContainer} key={id}>
                      <Image
                        source={{ uri: imageUrl }}
                        style={{ width: 90, height: 90 }}
                      />
                      <Text numberOfLines={1} style={styles.name}>
                        {name}
                      </Text>
                      <View>
                        <Rating
                          imageSize={16}
                          ratingBackgroundColor="transparent"
                          readonly
                          startingValue={rating}
                          style={{
                            paddingVertical: 0,
                            alignSelf: "flex-start",
                            backgroundColor: "transparent",
                          }}
                        />
                      </View>
                      <View style={styles.priceContainer}>
                        <Text style={styles.price}>$ {price}</Text>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                );
              })}
            </View>
          </ScrollView>
        </View>
      )}
    </View>
  );
};
export default connect(
  (state) => ({
    menuId: state?.loggedInUser?.loggedInUser?.userDetails?.menuId,
  }),
  null
)(MostPopular);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    marginTop: 20,
    width: windowWidth,
    flex: 1,
  },
  title: {
    color: "#455a64",
    fontWeight: "bold",
    fontSize: 14,
  },
  popularItemContainer: {
    flexDirection: "row",
    width: windowWidth - 40,
    flex: 1,
    justifyContent: "space-evenly",
  },
  itemContainer: {
    padding: 5,
    width: 130,
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: "#fff",
  },
  name: {
    color: "#546e7a",
    fontSize: 13,
    fontWeight: "600",
    marginBottom: 5,
  },
  priceContainer: {
    flexDirection: "row",
    alignContent: "center",
    justifyContent: "center",
    marginTop: 5,
  },
  price: {
    color: "#ef5350",
    fontSize: 13,
    fontWeight: "bold",
  },
});
