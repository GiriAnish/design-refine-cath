import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { useDispatch } from "react-redux";
import CartRemoveModal from "../Components/CartRemoveModal";
import * as CartActions from "../../store/action/cart";
import Trash from "../Components/svg/Trash";

const CartHeader = (props) => {
  const [showCartModal, setShowCartModal] = useState(false);
  const dispatch = useDispatch();
  const removeCartItems = () => {
    dispatch(CartActions.removeItemsFromCart(props.id));
  };
  const cartModalClick = () => {
    setShowCartModal(true);
  };
  useEffect(() => {
    setShowCartModal(false);
  }, [props]);
  return (
    <View style={styles.headerContainer}>
      <View>
        <Text style={styles.headerTitle}>{props.name}</Text>
      </View>

      <View style={styles.headerIcon}>
        <TouchableOpacity onPress={() => cartModalClick()}>
          <Trash width={26} height={26} />
        </TouchableOpacity>

        <CartRemoveModal
          onPress={removeCartItems}
          showModal={showCartModal}
          setVisible={() => {
            setShowCartModal(false);
          }}
        />
      </View>
    </View>
  );
};

export default CartHeader;

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  headerTitle: {
    fontSize: 18,
    color: "#263238",
    fontWeight: "600",
  },
  headerIcon: {
    alignSelf: "flex-end",
  },
});
