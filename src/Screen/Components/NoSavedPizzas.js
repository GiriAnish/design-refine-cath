import React from "react";
import styled from "@emotion/native";
import { SafeAreaView } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import theme from "../../assets/css/commonThemes";

const Title = "No Saved Pizzas";
const Info = "Looks like you have not saved any pizzas yet.";
const instruction =
  "Click heart on a pizza so that it will appear here for quicker ordering in the future.";

const MainContainer = styled.View`
  background: ${({ theme }) => theme.Colors.white};
  align-items: center;
  border-radius: 5px;
  elevation: 1;
`;
const ImageContainer = styled.View`
  background: ${({ theme }) => theme.Colors.amour};
  align-items: center;
  height: 161px;
  width: 161px;
  border-radius: 80px;
  justify-content: center;
  margin-top: 80px;
`;
const InfoContainer = styled.View`
  margin-top: 30px;
  margin-bottom: 20px;
`;
const Paragraph = styled.Text`
  color: ${({ theme }) => theme.Colors.hoki};
  font-family: ${({ theme }) => theme.fontFamily};
  font-weight: 400;
  font-size: 16px;
  text-align: center;
  padding-right: 10px;
  padding-left: 10px;
`;
const TitleText = styled.Text`
  color: ${({ theme }) => theme.Colors.hoki};
  font-family: ${({ theme }) => theme.fontFamily};
  font-weight: 400;
  font-size: 18px;
  text-align: center;
`;
const NoSavedPizzas = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <MainContainer>
        <ImageContainer>
          <Icon name="heart" color={theme.Colors.redOrange} size={83} />
        </ImageContainer>
        <InfoContainer>
          <TitleText>{Title}</TitleText>
          <InfoContainer>
            <Paragraph style={{ marginBottom: 20 }}>{Info}</Paragraph>
            <Paragraph>{instruction}</Paragraph>
          </InfoContainer>
        </InfoContainer>
      </MainContainer>
    </SafeAreaView>
  );
};

export default NoSavedPizzas;
