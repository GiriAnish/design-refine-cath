import React from "react";
import { View, Text } from "react-native";
import { useSelector } from "react-redux";

const CartTabText = () => {
  const cart = useSelector((state) => state.cart.cartItems);
  return (
    <View>
      <Text style={{ color: "#455a64", fontSize: 18, fontWeight: "600" }}>
        {cart.length > 0
          ? "Cart (" +
            cart.reduce((prev, curr) => {
              return prev + curr.quantity;
            }, 0) +
            ")"
          : "Cart"}
      </Text>
    </View>
  );
};

export default CartTabText;
