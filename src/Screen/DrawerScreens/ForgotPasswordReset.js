import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import { TextInput as NewTextInput } from "react-native-paper";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import { resetPwdCreation } from "../../store/action/forgotpwdAction";
import Toast from "react-native-toast-message";
import { useDispatch } from "react-redux";
import { RESET_FORGET_ALL_PASSWORD } from "../../store/action/forgotpwdAction";

const ForgotPasswordReset = (props) => {
  const dispatch = useDispatch();
  const { emailId } = props.route.params;

  const { finalPwdReserResp, loading, pwdResetFunc, finalLocalotpResp } = props;
  const [isError, setisError] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [form, setForm] = useState({});
  const passwordRef = useRef();
  const handleSubmitPress = () => {
    if (
      form.password === "" ||
      form.confirmpassword === "" ||
      form.password === undefined ||
      form.confirmpassword === undefined ||
      form.password !== form.confirmpassword
    ) {
      setisError(true);
      return;
    }

    var payload = {
      newPassword: form.password,
      newPasswordConfirm: form.confirmpassword,
    };
    pwdResetFunc(emailId, payload);
    setisError(false);
  };

  useEffect(() => {
    if (finalPwdReserResp?.successBody?.message) {
      props.navigation.navigate("ForgotPasswordResetSuccess");
      dispatch({ type: RESET_FORGET_ALL_PASSWORD });
    } else if (finalPwdReserResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: finalPwdReserResp?.errorBody,
      });
      dispatch({ type: RESET_FORGET_ALL_PASSWORD });
    }
  }, [finalPwdReserResp]);

  const EyeComp = () => {
    return (
      <NewTextInput.Icon
        name="eye-outline"
        color="#78909c"
        onPress={() => {
          setShowPassword((prevState) => {
            return !prevState;
          });
        }}
      />
    );
  };

  const TextInputComp = React.useCallback(
    ({ right, placeholderLabel, id, secure, ref, label }) => {
      return (
        <NewTextInput
          style={styles.inputStyle}
          returnKeyType={!secure ? "next" : "default"}
          onSubmitEditing={() => {
            passwordRef.current.focus();
          }}
          ref={passwordRef}
          placeholder={placeholderLabel}
          secureTextEntry={secure && !showPassword ? true : false}
          placeholderTextColor="#546e7a"
          right={
            right == "user" ? (
              <NewTextInput.Icon name="account" color="#78909c" />
            ) : (
              EyeComp()
            )
          }
          underlineColor="transparent"
          onChangeText={(text) => {
            setForm((prevState) => ({ ...prevState, [id]: text }));
          }}
          value={label}
        />
      );
    },
    [showPassword, props]
  );

  return (
    <View style={styles.root}>
      <ScrollView keyboardShouldPersistTaps="handled">
        <BannerAvatar
          touchableOpacity
          imageSource={require("../../assets/images/forgot_pwd.png")}
        />
        <View style={styles.bodyLayout}>
          <View>
            <Text style={styles.secondaryText}>
              Reset password for your account
            </Text>
            <View style={styles.SectionStyle}>
              <TextInputComp
                style={styles.inputStyle}
                right={"eye"}
                placeholderLabel={"Enter New Password"}
                id={"password"}
                secure={true}
                label={form["password"]}
              />
            </View>
            <Text style={isError ? styles.errorTextStyle : styles.subText}>
              Must be at least 8 characters, 1 capital letter, 1 special character
            </Text>
            <View style={styles.SectionStyle}>
              <TextInputComp
                style={styles.inputStyle}
                right={"eye"}
                placeholderLabel={"Retype New Password"}
                id={"confirmpassword"}
                secure={true}
                label={form["confirmpassword"]}
              />
            </View>
            <Text style={isError ? styles.errorTextStyle : styles.subText}>
              Both passwords must match
            </Text>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={handleSubmitPress}
            >
              <Text style={styles.buttonTextStyle}>Reset Password</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.logoContainer}>
          <FooterBanner
            touchableOpacity
            imageSource={require("../../assets/images/bottom_banner.png")}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default connect(
  (state) => ({
    finalLocalotpResp: state.forgotPwd.validateOTPRes,
    finalPwdReserResp: state.forgotPwd.passwordResetResponse,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    pwdResetFunc: (emailId, payload) =>
      dispatch(resetPwdCreation(emailId, payload)),
  })
)(ForgotPasswordReset);

const styles = StyleSheet.create({
  root: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  bodyLayout: {
    color: "#546e7a",
    fontFamily: "Roboto",
    marginTop: 50,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  primaryText: {
    fontSize: 14,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  secondaryText: {
    marginTop: 25,
    fontSize: 20,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#a38862",
    fontFamily: "Roboto",
  },

  SectionStyle: {
    flexDirection: "row",
    height: 55,
    marginTop: 20,
    width: 360,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  logoContainer: {
    alignItems: "center",
    marginTop: -25,
  },
  inputStyle: {
    flex: 1,
    color: "white",
    backgroundColor: "#ede7f6",
    height: 50,
    width: "90%",
    justifyContent: "center",
    borderWidth: 0,
    borderColor: "#ede7f6",
    borderRadius: 0,
  },
  subText: {
    marginLeft: 20,
    marginTop: 10,
    fontSize: 16,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  errorTextStyle: {
    color: "red",
    textAlign: "left",
    fontSize: 16,
    marginLeft: 20,
    marginTop: 10,
    fontWeight: "normal",
  },
  successTextStyle: {
    color: "white",
    textAlign: "center",
    fontSize: 18,
    padding: 30,
  },
  buttonStyle: {
    color: "#FFFFFF",
    borderColor: "#673ab7",
    flexDirection: "row",
    marginTop: 25,
    backgroundColor: "#673ab7",
    borderWidth: 0,
    height: 60,
    alignItems: "center",
    width: "100%",
    borderRadius: 5,
    justifyContent: "center",
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
    fontWeight: "500",
    textAlign: "center",
  },
});
