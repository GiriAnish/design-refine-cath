import React, { useState } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import CBLogoMain from "../Components/svg/CBLogoMain";

const ChangeLanguage = (props) => {
  const [radioSelected, setRadioSelected] = useState(1);
  const language = [
    {
      title: "English",
      id: 1,
    },
  ];

  return (
    <ScrollView style={{ backgroundColor: "#fafcfe", }}>
      <View style={styles.scroll}>
          <View style={styles.top}>
            {language.map((val) => {
              return (
                <View>
                  <View key={val.title} style={styles.radioContainer}>
                    <View style={styles.optCont}>
                      <Text style={styles.optText}>{val.title}</Text>
                    </View>

                    <TouchableOpacity
                      key={val.id}
                      onPress={() => {
                        setRadioSelected(val.id);
                      }}
                    >
                      <View style={styles.outerRadio}>
                        {val.id == radioSelected ? (
                          <View style={styles.innerRadio} />
                        ) : null}
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              );
            })}
          </View>
          <View style={styles.LogoC}>
            <CBLogoMain width="380" height="170" />
          </View>
      </View>
    </ScrollView>
  );
};

export default ChangeLanguage;

const styles = StyleSheet.create({
  scroll: {
    backgroundColor: "#fafcfe",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: Dimensions.get('window').height - 150,
  },
  top: {
    borderWidth: 1,
    borderColor: "#fff",
    backgroundColor: "#ffffff",
    borderRadius: 5,
    shadowOpacity: 0.03,
    margin: 18,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 1,
  },
  LogoC: {
    alignItems: "center",
    width: "100%",
    flexDirection: "column",
    textAlign: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  radioContainer: {
    flexDirection: "row",
    paddingHorizontal: 35,
    paddingVertical: 20,
  },
  optCont: { width: "100%", marginBottom: 5 },
  optText: { color: "#546e7a", fontSize: 16 },
  outerRadio: {
    height: 24,
    width: 24,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: "#673ab7",
    alignItems: "center",
    justifyContent: "center",
  },
  innerRadio: {
    height: 12,
    width: 12,
    borderRadius: 6,
    backgroundColor: "#673ab7",
  },
});
