import React, { useState, useEffect, useRef } from "react";
import RNFetchBlob from "rn-fetch-blob";
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  ImageBackground,
  KeyboardAvoidingView,
  Alert,
} from "react-native";
import Toast from "react-native-toast-message";
import { launchImageLibrary } from "react-native-image-picker";
import { useDispatch, useSelector } from "react-redux";
import CONST from "../../data/const";
import Loader from "../Components/Loader";
import ImagePlus from "../Components/svg/ImagePlus";
import Pencil from "../Components/svg/Pencil";
import ImageEditOutline from "../Components/svg/ImageEditOutline";
import { AsYouType, parsePhoneNumberFromString } from "libphonenumber-js";
import API_LIST from "../../apiServices/apiList";
import AsyncStorage from "@react-native-community/async-storage";
import * as loginActions from "../../store/action/loginActions";
import * as CartActions from "../../store/action/cart";

const MyProfile = (props) => {
  const [edit, setEdit] = useState(false);
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState({});
  const [localImageUri, setLocalImageUri] = useState("");
  const [imageFile, setUploadFile] = useState(null);
  const login = useSelector((state) => state.loggedInUser.loggedInUser);
  const scrollViewRef = useRef();
  const [isKeyboardActive, setIsKeyboardActive] = useState(false);
  const [phoneNumberText, setPhoneNumberText] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    setLoading(true);
    fetch(CONST.baseUrl + "api/v1/user/profile", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${login?.userDetails?.jwtToken}`,
      },
    })
      .then((response) => response.json())
      .then((json) => {
        setData(json);
        setForm(json);
        setPhoneNumberText(new AsYouType("US").input(json.phone));
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setData(null);
        setLoading(false);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [props]);

  const editClicked = () => {
    setEdit(!edit);
  };

  const saveClicked = async () => {
    try {
      const phoneNumber = parsePhoneNumberFromString(phoneNumberText, "US");
      if (!(!!phoneNumber && phoneNumber.isPossible())) {
        Toast.show({
          type: "error",
          text1: "Error",
          text2: "Please enter a valid Phone number.",
        });
        return;
      }
      form.phone = phoneNumber.nationalNumber;
      const token = login?.userDetails?.jwtToken;
      if (!token) {
        return;
      }
      setLoading(true);
      let uri = null;
      if (localImageUri) {
        uri = await uploadFile(imageFile, login?.userDetails?.jwtToken);
      }
      if (uri) {
        form.imageUrl = uri;
      }
      const putMethod = {
        method: "PUT",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(form),
      };
      const response = await fetch(
        `${CONST.baseUrl}api/v1/user/profile`,
        putMethod
      );
      if (response?.ok && response?.status === 200) {
        setEdit(!edit);
        Toast.show({
          type: "success",
          text1: "Success",
          text2: "Profile updated",
        });
        return;
      }
      Toast.show({
        type: "error",
        text1: "Error",
        text2: "Profile updating failed.",
      });
    } catch (error) {
      console.log(error);
      Toast.show({
        type: "error",
        text1: "Error",
        text2: "Profile updating failed.",
      });
    } finally {
      setLoading(false);
    }
  };

  const deleteUser = async () => {
    try {
      setLoading(true);
      const { jwtToken, id } = login?.userDetails;
      const request = {
        method: "DELETE",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${jwtToken}`,
        },
        body: JSON.stringify(form),
      };
      const response = await fetch(
        `${CONST.baseUrl}${API_LIST.DELETE_USER}/${id}`,
        request
      );

      if (!response.ok) {
        Toast.show({
          type: "error",
          text1: "Error",
          text2: "Account Deletion failed. Please try again",
        });
        return;
      }

      const app = await AsyncStorage.getItem("appVersion");
      AsyncStorage.clear();
      await AsyncStorage.setItem("appVersion", app);
      dispatch(CartActions.resetCart());
      dispatch(loginActions.resetUserLogin());

      Alert.alert(
        "Your account is deleted",
        "You will now be redirected to the login screen.",
        [
          {
            text: "OK",
            onPress: () => {
              props.navigation.toggleDrawer();
              props.navigation.replace("Auth");
              setLoading(false);
            },
          },
        ]
      );
    } catch (error) {
      console.log(error);
      Toast.show({
        type: "error",
        text1: "Error",
        text2: "Account Deletion failed. Please try again",
      });
    } finally {
      setLoading(false);
    }
  };

  const onDeleted = () => {
    Alert.alert(
      "Delete User Account",
      "Are you sure you want to delete your account? All your personal information, including your order history, will be permanently removed.",
      [
        {
          text: "Cancel",
        },
        {
          text: "Delete",
          onPress: () => {
            deleteUser();
          },
        },
      ]
    );
  };

  const uploadFile = async (image, token) => {
    try {
      let url = CONST.baseUrl + "api/v1/files";
      const resp = await RNFetchBlob.fetch(
        "POST",
        url,
        {
          Authorization: "Bearer " + token,
          "Content-Type": "application/octet",
        },
        image.base64
      );
      const data = JSON.parse(resp.data);
      console.log("--File upload response---", data);
      if (resp.respInfo.status === 200 && data.uri) {
        return data.uri;
      }
      return "";
    } catch (e) {
      return { error: true, errorBody: e };
    }
  };

  const launchImageFromLibrary = () => {
    let options = {
      mediaType: "photo",
      includeBase64: true,
    };
    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        setUploadFile(response.assets[0]);
        setLocalImageUri(response.assets[0].uri);
      }
    });
  };

  const _handleMultiInput = (id) => {
    return (text) => {
      setForm((prevState) => ({ ...prevState, [id]: text }));
    };
  };

  const Title = ({ label }) => {
    return <Text style={styles.title}>{label}</Text>;
  };

  const onPhoneNumberTextChange = (number) => {
    const num = parsePhoneNumberFromString(number, "US");
    const reg = /^[0-9]/;
    if (
      !!num &&
      phoneNumberText?.length > number.length &&
      !reg.test(phoneNumberText[phoneNumberText.length - 1])
    ) {
      let phone = num.nationalNumber.split("");
      phone.pop();
      phone = phone.join("");
      setPhoneNumberText(phone);
      return;
    }
    setPhoneNumberText(new AsYouType("US").input(number));
  };

  const SubTitle = React.useCallback(
    (props) => {
      return (
        <TextInput
          editable={props.isEditable && edit ? true : false}
          style={props.isEditable && edit ? styles.textEdit : styles.subTitle}
          onChangeText={_handleMultiInput(props.id)}
          onFocus={onFocusChange}
          keyboardType={props.keyboardType}
        >
          {props.label}
        </TextInput>
      );
    },
    [props.isEditable, props.label, edit]
  );

  const onFocusChange = () => {
    setIsKeyboardActive(true);
  };

  useEffect(() => {
    if (isKeyboardActive) {
      scrollViewRef.current.scrollToEnd({ animated: true });
    }
  }, [isKeyboardActive]);

  const SubTitle2 = React.useCallback(
    (props) => {
      return (
        <TextInput
          editable={props.isEditable && edit ? true : false}
          style={styles.subTitle}
        >
          {props.label}
        </TextInput>
      );
    },
    [props.isEditable, props.label, edit]
  );

  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={100}
      style={{ flex: 1 }}
      enabled={Platform.OS === "ios"}
      behavior={"position"}
    >
      <View style={styles.parentView}>
        <ScrollView
          style={styles.scrollView}
          keyboardShouldPersistTaps="handled"
          ref={scrollViewRef}
        >
          <View style={styles.topContainer}>
            <Loader loading={loading} />
            {!loading && data && (
              <View>
                <View style={styles.topC}>
                  <View style={styles.headerCont}>
                    <View style={styles.modalContainer}>
                      {localImageUri != "" ? (
                        <ImageBackground
                          source={{ uri: localImageUri }}
                          style={styles.imageBackground}
                        >
                          {edit && (
                            <TouchableWithoutFeedback
                              onPress={() => {
                                launchImageFromLibrary();
                              }}
                            >
                              <View
                                style={{
                                  position: "absolute",
                                  Left: 95,
                                  top: 90,
                                  marginLeft: 90,
                                }}
                              >
                                <ImageEditOutline
                                  fill={"#673ab7"}
                                  style={{
                                    borderRadius: 50,
                                    backgroundColor: "white",
                                    width: 50,
                                    height: 50,
                                    marginLeft: 100,
                                  }}
                                />
                              </View>
                            </TouchableWithoutFeedback>
                          )}
                        </ImageBackground>
                      ) : data?.imagePath ? (
                        <ImageBackground
                          source={{ uri: data?.imagePath }}
                          style={styles.imageBackground}
                        >
                          {edit && (
                            <TouchableWithoutFeedback
                              onPress={() => {
                                launchImageFromLibrary();
                              }}
                            >
                              <View
                                style={{
                                  position: "absolute",
                                  Left: 95,
                                  top: 90,
                                  marginLeft: 90,
                                }}
                              >
                                <ImageEditOutline
                                  fill={"#673ab7"}
                                  style={{
                                    borderRadius: 50,
                                    backgroundColor: "white",
                                    width: 50,
                                    height: 50,
                                    marginLeft: 100,
                                  }}
                                />
                              </View>
                            </TouchableWithoutFeedback>
                          )}
                        </ImageBackground>
                      ) : (
                        <ImageBackground style={styles.imageBackground}>
                          <ImagePlus
                            height={145}
                            width={145}
                            fill={"#90a4ae"}
                            style={{ marginLeft: 46, marginBottom: 40 }}
                          />
                        </ImageBackground>
                      )}
                      {edit && (
                        <TouchableWithoutFeedback
                          onPress={() => {
                            launchImageFromLibrary();
                          }}
                        >
                          <View
                            style={{
                              borderRadius: 40,
                              backgroundColor: "white",
                              width: 40,
                              height: 40,
                              marginLeft: 100,
                              marginTop: -65,
                            }}
                          >
                            <ImageEditOutline
                              fill={"#673ab7"}
                              style={{ marginLeft: 8, marginTop: 8 }}
                            />
                          </View>
                        </TouchableWithoutFeedback>
                      )}
                    </View>
                    <Text style={styles.header}>Profile Information</Text>
                    <View style={styles.subContainer}>
                      <Title label="Account type" />
                      <SubTitle2
                        label={form?.accountTypeName}
                        isEditable={false}
                        id={"accountType"}
                      />
                    </View>

                    <View style={styles.subContainer}>
                      <Title label="Company Name" />
                      {form?.accountLogoURL?.length > 0 && (
                        <Image
                          source={{ uri: form?.accountLogoURL }}
                          style={{
                            flex: 1,
                            width: 80,
                            height: 26,
                            marginBottom: 10,
                            resizeMode: "contain",
                          }}
                        />
                      )}
                      {form?.accountLogoURL?.length === 0 && (
                        <SubTitle2
                          label={form?.accountName}
                          isEditable={false}
                          id={"companyName"}
                        />
                      )}
                    </View>
                    <View style={styles.subContainer}>
                      <Title label="First Name" />
                      <SubTitle
                        label={form?.firstName}
                        isEditable={true}
                        id={"firstName"}
                      />
                    </View>
                    <View style={styles.subContainer}>
                      <Title label="Last Name" />
                      <SubTitle
                        label={form?.lastName}
                        isEditable={true}
                        id={"lastName"}
                      />
                    </View>
                    <View style={styles.subContainer}>
                      <Title label="Email" />
                      <SubTitle2
                        label={form?.email}
                        isEditable={false}
                        id={"email"}
                      />
                    </View>
                    <View style={styles.subContainer}>
                      <Title label="Phone Number" />
                      <TextInput
                        keyboardType="phone-pad"
                        maxLength={15}
                        editable={edit}
                        style={
                          props.isEditable && edit
                            ? styles.textEdit
                            : styles.subTitle
                        }
                        onChangeText={(number) =>
                          onPhoneNumberTextChange(number)
                        }
                        value={phoneNumberText}
                      ></TextInput>
                    </View>
                    <View style={styles.buttonCont}>
                      <TouchableWithoutFeedback onPress={onDeleted}>
                        {edit ? (
                          <View style={styles.delteButton}>
                            <View>
                              <Text style={styles.checkoutText}>Delete</Text>
                            </View>
                          </View>
                        ) : (
                          <View></View>
                        )}
                      </TouchableWithoutFeedback>
                      <TouchableWithoutFeedback onPress={saveClicked}>
                        {edit ? (
                          <View style={styles.saveButton}>
                            <View>
                              <Text style={styles.checkoutText}>Save</Text>
                            </View>
                          </View>
                        ) : (
                          <View></View>
                        )}
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                </View>
              </View>
            )}
          </View>
        </ScrollView>
        {!edit ? (
          <TouchableWithoutFeedback onPress={editClicked}>
            <View style={styles.editSticky}>
              <Pencil
                fill={"white"}
                style={styles.editIcon}
                resizeMode={"stretch"}
              />
            </View>
          </TouchableWithoutFeedback>
        ) : null}
      </View>
    </KeyboardAvoidingView>
  );
};

export default MyProfile;

const styles = StyleSheet.create({
  parentView: {
    backgroundColor: "#fafcfe",
    height: "100%",
  },
  scrollView: { backgroundColor: "#fafcfe" },
  topContainer: { marginBottom: 10 },
  header: {
    marginTop: 0,
    marginBottom: 20,
    fontWeight: "600",
    fontSize: 18,
    color: "#673ab7",
  },
  headerCont: {
    paddingHorizontal: 30,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: "#ffff",
    marginTop: 15,
    borderRadius: 5,
    borderColor: "#eceff1",
    borderWidth: 0.5,
  },
  subContainer: { marginTop: -5 },
  topC: { paddingHorizontal: 20, marginTop: -28 },
  buttonCont: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
    width: "100%",
    alignSelf: "flex-end",
  },
  modalContainer: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    marginBottom: 10,
  },
  editSticky: {
    borderRadius: 60,
    backgroundColor: "#673ab7",
    height: 60,
    width: 60,
    position: "absolute",
    flex: 0.1,
    right: 25,
    bottom: 10,
    flexDirection: "row",
  },
  editIcon: {
    marginVertical: 17,
    marginLeft: 18,
    width: 24,
    height: 24,
    resizeMode: "contain",
  },
  saveButton: {
    width: "40%",
    height: 45,
    marginTop: 5,
    backgroundColor: "#673ab7",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  delteButton: {
    width: "40%",
    height: 45,
    marginTop: 5,
    backgroundColor: "red",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  checkoutText: { fontSize: 16, color: "#fff", fontWeight: "600" },
  subTitle: {
    fontSize: 16,
    color: "#546e7a",
    marginLeft: -20,
    paddingLeft: 20,
    height: 40,
    top: -5,
  },
  textEdit: {
    fontSize: 16,
    height: 45,
    color: "#546e7a",
    backgroundColor: "#ede7f6",
    marginLeft: -20,
    paddingLeft: 20,
    borderRadius: 5,
    borderWidth: 0.5,
    marginVertical: 5,
    borderColor: "#fff",
    shadowColor: "#000000",
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 2,
  },
  textNotEdit: {
    fontSize: 16,
    height: 45,
    color: "#546e7a",
    backgroundColor: "#eceff1",
    marginLeft: -20,
    paddingLeft: 20,
    borderRadius: 5,
    borderWidth: 0.5,
    marginVertical: 5,
    borderColor: "#fff",
    shadowColor: "#000000",
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 2,
  },
  title: {
    fontWeight: "600",
    fontSize: 16,
    color: "#37474f",
    marginVertical: 5,
  },
  imageBackground: {
    width: 145,
    height: 145,
    overflow: "hidden",
    borderWidth: 10,
    position: "relative",
    top: -10,
    borderColor: "#ffffff",
    backgroundColor: "#f5f5f5",
    borderRadius: 145,
  },
});
