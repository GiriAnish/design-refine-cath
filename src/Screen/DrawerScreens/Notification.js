import React, { useState } from "react";
import { StyleSheet, View, ScrollView, Text, Switch } from "react-native";
import SvgUri from "react-native-svg-uri";
import str from "../../assets/images/svg/logo.js";

const data = [
  {
    label: "Pizza Status Changes",
    id: "pizza",
  },
  {
    label: "Delivery Status Changes",
    id: "delivery",
  },
  {
    label: "Weekly Menu Updates",
    id: "week",
  },
  {
    label: "Sales and Promotional Offers",
    id: "sales",
  },
  {
    label: "Privacy Policy Changes",
    id: "privacy",
  },
  {
    label: "Application Updates",
    id: "app",
  },
];

const Notification = (props) => {
  const RenderImage = () => {
    return <SvgUri width="400" height="180" svgXmlData={str} />;
  };
  const [enabled, setEnabled] = useState({});
  const [allNoti, setAllNoti] = useState(false);
  const toggleSwitch = (id) =>
    setEnabled((prevState) => ({ ...prevState, [id]: !prevState[id] }));
  const RowItem = (props) => {
    return (
      <View style={styles.rowCont}>
        <View style={styles.textCont}>
          <Text style={styles.text}>{props.item.label}</Text>
        </View>
        <View style={styles.switchCont}>
          <Switch
            thumbColor={enabled[props.item.id] ? "#673ab7" : "#ffffff"}
            trackColor={{ false: "#eceff1", true: "#d1c4e9" }}
            onValueChange={() => {
              toggleSwitch(props.item.id);
            }}
            value={enabled[props.item.id]}
          />
        </View>
      </View>
    );
  };

  const ShowAllNotification = () => {
    return (
      <View style={{ ...styles.rowCont, ...styles.highlightColor }}>
        <View style={styles.textCont}>
          <Text
            style={{
              fontSize: 18,
              color: "#ffffff",
              fontWeight: "600",
              fontFamily: "Roboto",
              width: 290,
              height: 35,
              marginLeft: 15,
            }}
          >
            {"Show all Notifications"}
          </Text>
        </View>
        <View style={{ width: "22%", alignItems: "flex-end" }}>
          <Switch
            thumbColor={allNoti ? "#fefefe" : "#ffffff"}
            trackColor={{ false: "#eceff1", true: "#d1c4e9" }}
            onValueChange={() => {
              setAllNoti(!allNoti);
            }}
            value={allNoti}
          />
        </View>
      </View>
    );
  };

  return (
    <ScrollView style={styles.scrollView}>
      <View style={styles.topContainer}>
        <ShowAllNotification />
        <View style={styles.mainContainer}>
          {data.map((item) => {
            return <RowItem key={item.id} item={item} />;
          })}
        </View>
      </View>
      <View style={{ marginTop: 80 }}>
        <RenderImage />
      </View>
    </ScrollView>
  );
};

export default Notification;

const styles = StyleSheet.create({
  scrollView: { backgroundColor: "#fafcfe" },
  topContainer: { marginTop: 0 },
  rowCont: {
    flexDirection: "row",
    paddingHorizontal: 30,
    paddingVertical: 10,
    backgroundColor: "white",
  },
  highlightColor: { backgroundColor: "#673ab7" },
  textCont: { width: "77%" },
  text: { color: "#37474f", fontSize: 16 },
  highText: { color: "#ffffff", fontWeight: "500" },
  switchCont: { width: "23%", alignItems: "flex-end", marginLeft: 10 },
  mainContainer: { marginTop: 10, marginLeft: 15, marginRight: 15 },
});
