import React, { useState, useEffect, createRef } from "react";
import { connect, useDispatch } from "react-redux";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
} from "react-native";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import { pwdResetReq } from "../../store/action/forgotpwdAction";
import Toast from "react-native-toast-message";
import { useTabVisible } from "../../contexts/navigation";
import Lock from "../Components/svg/lock";
import GoldLogo from "../Components/svg/CBLogoGoldNoFill";
import { RESET_FORGET_ALL_PASSWORD } from "../../store/action/forgotpwdAction";
import Loader from "../Components/Loader";

const ResetEmail = (props) => {
  const { setTabVisible } = useTabVisible();
  const dispatch = useDispatch();
  const { finalLocalResp, loading, resetMyPwdFunc } = props;

  const [workEmailId, setWorkEmailId] = useState("");
  const [errortext, setErrortext] = useState("");

  const emailInputRef = createRef();
  const handleSubmitPress = () => {
    setErrortext("");
    if (!workEmailId) {
      setErrortext("Please enter a valid work email address");
      return;
    } else {
      resetMyPwdFunc(workEmailId);
    }
  };
  Toast.hide();

  useEffect(() => {
    setTabVisible(false);
    if (finalLocalResp?.successBody?.message) {
      setTabVisible(false);
      props.navigation.navigate("EmailVerifyCode", {
        emailId: workEmailId,
      });
      dispatch({ type: RESET_FORGET_ALL_PASSWORD });
    } else if (finalLocalResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: finalLocalResp?.errorBody,
      });
      dispatch({ type: RESET_FORGET_ALL_PASSWORD });
    }
  }, [finalLocalResp]);

  return (
    <View style={styles.mainBody}>
      {loading && <Loader />}
      <ScrollView keyboardShouldPersistTaps="handled">
        <BannerAvatar touchableOpacity />
        <View style={styles.lockLayout}>
          <Lock height={100} width={110} viewBox="0 18 250 200" />
        </View>
        <View style={styles.goldLogoLayout}>
          <GoldLogo height={36} width={40} viewBox="10 0 100 127" />
        </View>
        <View style={styles.bodyLayout}>
          <Text numberOfLines={3} style={styles.primaryText}>
            Enter the work email associated with your account. We will send you
            instructions and a link to help you reset your password.
          </Text>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Work Email Address*</Text>
            <TextInput
              style={styles.inputStyle}
              onChangeText={(workEmailId) => setWorkEmailId(workEmailId)}
              underlineColorAndroid="#f000"
              placeholder=""
              placeholderTextColor="#8b9cb5"
              keyboardType="email-address"
              ref={emailInputRef}
              returnKeyType="next"
              blurOnSubmit={false}
            />
          </View>
          {errortext != "" ? (
            <Text style={styles.errorTextStyle}> {errortext} </Text>
          ) : null}
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={handleSubmitPress}
            >
              <Text style={styles.buttonTextStyle}>
                Send Reset Password Instructions
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.logoContainer}>
          <FooterBanner
            touchableOpacity
            imageSource={require("../../assets/images/bottom_banner.png")}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default connect(
  (state) => ({
    finalLocalResp: state.forgotPwd.passwordResetReq,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    resetMyPwdFunc: (workEmailId) => dispatch(pwdResetReq(workEmailId)),
  })
)(ResetEmail);

const styles = StyleSheet.create({
  mainBody: {
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "#fff",
    alignItems: "center",
  },
  lockLayout: {
    bottom: 60,
    left: 5,
    alignSelf: "center",
  },
  goldLogoLayout: {
    bottom: 118,
    left: 2,
    backgroundColor: "white",
    alignItems: "center",
    alignSelf: "center",
    width: 30,
    borderRadius: 40 / 2,
  },
  bodyLayout: {
    fontFamily: "Roboto",
    marginTop: -75,
    justifyContent: "center",
    margin: 15,
  },
  inputContainer: {
    alignItems: "flex-start",
    marginTop: 30,
  },
  buttonContainer: {
    marginTop: 30,
  },
  logoContainer: {
    alignItems: "center",
    marginTop: -25,
  },
  primaryText: {
    fontSize: 16,
    fontWeight: "400",
    color: "#546e7a",
    marginTop: 20,
  },
  inputLabel: {
    marginBottom: 10,
    fontSize: 20,
    fontWeight: "400",
    color: "#a38862",
  },
  inputStyle: {
    backgroundColor: "#ede7f6",
    height: 60,
    width: "100%",
    justifyContent: "center",
    borderWidth: 0,
    borderRadius: 4,
    padding: 8,
    elevation: 2,
    color: "#000000",
    fontSize: 20,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "left",
    fontSize: 18,
    marginTop: 10,
    fontWeight: "normal",
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    height: 60,
    alignItems: "center",
    width: "100%",
    borderRadius: 5,
    justifyContent: "center",
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
    fontWeight: "500",
    textAlign: "center",
  },
});
