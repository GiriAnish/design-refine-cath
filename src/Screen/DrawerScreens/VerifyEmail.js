import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
} from "react-native";
import FooterBanner from "../Components/BannerFooter";
import { connect } from "react-redux";
import MailEnvelope from "../Components/svg/MailEnvelop";
import GoldLogo from "../Components/svg/CBLogoGoldNoFill";
import { resendEmailVerification } from "../../store/action/userRegistrationAction";
import Loader from "../Components/Loader";
import Toast from "react-native-toast-message";
import parseUrl from "url-parse";
import qs from "qs";
const VerifyEmail = (props) => {
  const { registeredUser, emailResp, loading, resendEmail } = props;

  const [email, setEmail] = useState(null);
  const handleSubmitPress = () => {
    props.navigation.navigate("VerifyEmailSuccess");
  };
  const handleResendEmail = () => {
    resendEmail(registeredUser?.user?.email);
  };
  const getEmail = (url) => {
    if (url) {
      const { navigate } = props.navigation;
      const urlObj = parseUrl(url);
      const qsParsed = qs.parse(urlObj.query.replace("?", ""));
      setEmail(qsParsed.id);
    }
  };

  useEffect(() => {
    getEmail(props?.route?.params?.id);
  }, [props]);

  useEffect(() => {
    if (emailResp?.successBody?.user) {
      Toast.show({
        type: "success",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Success",
        text2: emailResp?.successBody?.message,
      });
    } else if (emailResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: emailResp?.errorBody,
      });
    }
  }, [emailResp]);

  return (
    <ScrollView style={styles.scrollView}>
      <Loader loading={loading} />
      <View style={{ height: 185 }}>
        <View style={styles.topContainer}></View>
        <View style={styles.absCont}>
          <MailEnvelope height={250} width={138} viewBox="-4 26 430 240" />
        </View>
        <View style={styles.goldlogo}>
          <GoldLogo height={50} width={100} viewBox="5 0 100 126" />
        </View>
      </View>
      <View style={{ margin: 20, alignItems: "center" }}>
        <Text style={{ color: "#546e7a", fontSize: 20 }}>
          A verification email has been sent to:
        </Text>
        <Text
          style={{
            color: "#263238",
            fontSize: 22,
            fontWeight: "600",
            marginTop: 20,
          }}
        >
          {email || registeredUser?.user?.email || registeredUser?.user?.email}
        </Text>
        <Text
          style={{
            color: "#546e7a",
            fontSize: 20,
            marginTop: 20,
            textAlign: "center",
          }}
        >
          Please click the verify link in your email to continue the sign up
          process....
        </Text>
        <TouchableWithoutFeedback onPress={handleResendEmail}>
          <Text style={{ color: "#2196f3", fontSize: 17, marginTop: 20 }}>
            Resend Verification OTP
          </Text>
        </TouchableWithoutFeedback>
        <View style={styles.buttonCont}>
          <TouchableWithoutFeedback onPress={handleSubmitPress}>
            <View style={styles.checkoutContainer}>
              <Text style={styles.checkoutText}>Next</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
      <View
        style={{
          alignContent: "center",
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          height: 190,
          marginBottom: 20,
        }}
      >
        <FooterBanner
          touchableOpacity
          imageSource={require("../../assets/images/bottom_banner.png")}
        />
      </View>
    </ScrollView>
  );
};

export default connect(
  (state) => ({
    registeredUser: state.registeredUser.registeredUser,
    emailResp: state.registeredUser.emailResp,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    resendEmail: (email) => dispatch(resendEmailVerification(email)),
  })
)(VerifyEmail);

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#fff",
  },
  topContainer: {
    backgroundColor: "#673ab7",
    marginTop: -20,
    marginBottom: 20,
    height: 135,
    overflow: "visible",
  },
  absCont: {
    height: 185,
    width: "46%",
    position: "absolute",
    top: 10,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    backgroundColor: "#ede7f6",
    borderRadius: 100,
    marginLeft: 105,
  },
  goldlogo: {
    bottom: 63,
    left: 170,
    backgroundColor: "white",
    alignItems: "center",
    width: 50,
    borderRadius: 120,
  },

  buttonCont: { alignItems: "center", marginTop: 20, width: "100%" },
  checkoutContainer: {
    width: "100%",
    height: 45,
    backgroundColor: "#90a4ae",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  checkoutText: { fontSize: 16, color: "#fff", fontWeight: "600" },
  logo: {
    width: "90%",
    height: undefined,
    aspectRatio: 1,
  },
});
