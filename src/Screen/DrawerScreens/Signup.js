import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
} from "react-native";
import CONST from "../../data/const";
import Loader from "../Components/Loader";
import LeftLogo from "../Components/svg/CBLogoGoldNoFill";
import RightLogo from "../Components/svg/CBLogoPurpleNoFill";
import CBLogoFullWhite from "../Components/svg/CBLogoFullWhite";

const Signup = (props) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    fetch(CONST.baseUrl + "api/v1/accounttype")
      .then((response) => response.json())
      .then((json) => {
        setData(json);
        setLoading(false);
      })
      .catch((error) => {
        setData(null);
        setLoading(false);
      });
  }, [props]);

  const AccounType = (props) => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          props.navigation.navigate("RegistrationBuilder", {
            id: props.id,
            label: props.label,
          });
        }}
      >
        <View style={styles.accountType}>
          <View style={styles.accountImage}>
            <LeftLogo height={40} width={50} viewBox="0 0 100 126" />
          </View>
          <View style={styles.textCont}>
            <Text style={styles.accountText}>{props.label}</Text>
          </View>
          <View style={styles.accountImage}>
            <RightLogo height={41} width={50} viewBox="4 -5 100 95" />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <ScrollView style={styles.scrollView}>
      <View style={styles.topContainer}>
        <View style={styles.topC}>
          <View style={styles.logo}>
            <CBLogoFullWhite height={100} width={"100%"} viewBox="0 0 378 89" />
          </View>
          <View style={styles.titleCont}>
            <Text style={styles.title}>Select Account Type</Text>
          </View>
          <View>
            {data &&
              data?.filter(item => item?.active === true).map((item) => {
                return (
                  <AccounType
                    label={item.type}
                    key={item.id}
                    {...props}
                    id={item.id}
                  />
                );
              })}
            {loading && <Loader />}
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Signup;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#673ab7",
    paddingTop: 12,
  },
  topContainer: {
    width: "100%",
    height: "100%",
  },
  header: {
    fontWeight: "600",
    fontSize: 19,
    color: "#37474f",
  },
  topC: {
    paddingHorizontal: 20
  },
  logo: {
    paddingVertical: 10,
  },
  titleCont: {
    textAlign: "center",
    justifyContent: "center",
    alignContent: "center",
    width: "100%",
    flex: 1,
    flexDirection: "row",
    paddingBottom: 30,
  },
  title: {
    fontSize: 25,
    fontWeight: "600",
    color: "#fff",
  },
  accountType: {
    height: 70,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignContent: "center",
    width: "100%",
    flex: 1,
    flexDirection: "row",
    marginVertical: 10,
    borderRadius: 5,
  },
  accountText: {
    fontSize: 23,
    color: "#263238",
    alignSelf: "center",
  },
  accountImage: {
    alignSelf: "center",
    width: "20%",
    alignItems: "center",
  },
  textCont: {
    alignSelf: "center",
    width: "60%",
  },
});
