import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  View,
  Image,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { useDispatch } from "react-redux";
import { SET_USER_LOGIN } from "../../store/action/loginActions";
import { connect } from "react-redux";
import Logo from "../Components/svg/CB_Primary";

const RegistrationSuccess = (props) => {
  const { otpResp } = props;
  const dispatch = useDispatch();
  const bannerImageURL = props?.route?.params?.bannerImageURL;

  const handleNavigation = () => {
    dispatch({
      type: SET_USER_LOGIN,
      loggedInUser: {
        message: otpResp.successBody?.message,
        userDetails: otpResp.successBody?.user,
      },
    });
    props.navigation.navigate("DrawerNavigationRoutes");
  };
  return (
    <ScrollView>
      <SafeAreaView style={styles.root}>
        <ImageBackground
            resizeMode={"contain"}
            source={{ uri: props?.route?.params?.bannerImageURL }}
            style={styles.bannerCont}
          >
          </ImageBackground>
        <View
          style={{
            alignContent: "center",
            width: "100%",
            alignItems: "center",
            justifyContent: "center",
            height: 180,
          }}
        >
          <Logo width={600} height={145} viewBox="0 0 600 300" />
        </View>
        <View style={styles.bodyLayout}>
          <View style={styles.bodyLayoutInner}>
            <Text style={styles.successText}>Congratulations!</Text>
            <Text style={styles.primaryText}>
              Your registration is completed.
            </Text>
            <Text style={styles.secondaryText}>
              Please check your email to retrieve the VIP code for placing pizza orders.
            </Text>
            <Text style={styles.secondaryText}>
              Enjoy the exclusive benefits of the Cathedral Plumbing VIP Program!
            </Text>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={handleNavigation}
            >
              <Text style={styles.buttonTextStyle}>Finish</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <Text style={{ marginBottom: 100 }}></Text>
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};
export default connect(
  (state) => ({
    otpResp: state.registeredUser.otpResp,
  }),
  null
)(RegistrationSuccess);

const styles = StyleSheet.create({
  root: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
  bodyLayout: {
    color: "#546e7a",
    width: "90%",
  },
  bodyLayoutInner: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  successText: {
    marginTop: 5,
    fontSize: 22,
    fontWeight: "500",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#a38862",
    fontFamily: "Roboto",
    fontWeight: "bold",
  },
  primaryText: {
    fontSize: 19,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#5e35b1",
    fontFamily: "Roboto",
    fontWeight: "bold",
    marginTop: 10,
  },
  secondaryText: {
    marginTop: 20,
    fontSize: 20,
    fontWeight: "bold",
    flexDirection: "row",
    textAlign: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  diffEmailText: {
    marginTop: "5%",
    marginLeft: "25%",
    fontSize: 17,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#2196f3",
    fontFamily: "Roboto",
  },
  SectionStyle: {
    flexDirection: "row",
    height: 40,
    marginTop: "5%",
  },
  inputStyle: {
    flex: 1,
    color: "black",
    backgroundColor: "#ede7f6",
    height: 45,
    width: "90%",
    justifyContent: "center",
    borderWidth: 0,
    borderColor: "#ede7f6",
    borderRadius: 0,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "left",
    fontSize: 14,
    marginTop: 10,
    fontWeight: "normal",
  },
  successTextStyle: {
    color: "white",
    textAlign: "center",
    fontSize: 18,
    padding: 30,
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    height: 50,
    alignItems: "center",
    width: "100%",
    borderRadius: 5,
    marginTop: 25,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 12,
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: "Roboto",
    textAlign: "center",
  },
  buttonSkipStyle: {
    borderWidth: 0,
    borderColor: "#a38862",
    borderWidth: 1,
    color: "#ffffff",
    height: 40,
    alignItems: "center",
    width: "90%",
    marginLeft: "5%",
    borderRadius: 5,
    marginTop: "5%",
  },
  buttonSkipTextStyle: {
    borderColor: "#a38862",
    color: "#a38862",
    paddingVertical: 10,
    fontSize: 14,
    fontWeight: "bold",
  },
  bannerCont: {
    width: "100%",
    height: 270,
    alignItems: "center",
    marginBottom: 5,
    marginVertical: -20,
  },
});
