import React, { useState, useEffect, createRef } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import CONST from "../../data/const";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import { useDispatch } from "react-redux";
import {
  PASSWORD_RESET_REQUEST_DEFAULT,
  SET_USER_NEWPASSWORD_TO_DEFAULT,
  VALIDATE_OTP_RESET,
} from "../../store/action/forgotpwdAction";
const ForgotPasswordResetSuccess = (props) => {
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const [workEmailId, setWorkEmailId] = useState("");
  const [errortext, setErrortext] = useState("");

  const emailInputRef = createRef();
  const handleSubmitPress = () => {
    setErrortext("");
    if (!workEmailId) {
      setErrortext("Please enter a valid work email address");
      return;
    }
  };

  useEffect(() => {
    fetch(CONST.baseUrl + "api/v1/accounts")
      .then((response) => response.json())
      .then((json) => {
        setData(json.accounts);
      })
      .catch((error) => {
        setData([]);
      });
  }, [props]);

  useEffect(() => {
    dispatch({ type: PASSWORD_RESET_REQUEST_DEFAULT });
    dispatch({ type: VALIDATE_OTP_RESET });
    dispatch({ type: SET_USER_NEWPASSWORD_TO_DEFAULT });
  }, []);

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      () => true
    );
    return () => {
      backHandler.remove();
    };
  }, []);

  return (
    <View style={styles.root}>
      <ScrollView keyboardShouldPersistTaps="handled">
        <BannerAvatar
          touchableOpacity
          imageSource={require("../../assets/images/forgot-pwd-success.png")}
        />
        <View style={styles.bodyLayout}>
          <View>
            <Text style={styles.successText}>Congratulations!</Text>
            <Text style={styles.primaryText}>
              Your password was reset successfully!
            </Text>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={() => {
                props.navigation.navigate("LoginScreen");
              }}
            >
              <Text style={styles.buttonTextStyle}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.logoContainer}>
          <FooterBanner
            touchableOpacity
            imageSource={require("../../assets/images/bottom_banner.png")}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default ForgotPasswordResetSuccess;

const styles = StyleSheet.create({
  root: {
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  bodyLayout: {
    width: "100%",
    backgroundColor: "white",
    color: "#546e7a",
    fontFamily: "Roboto",
    marginTop: 80,
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  successText: {
    width: "100%",
    fontSize: 25,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#4caf50",
    fontFamily: "Roboto",
    marginLeft: 65,
    fontWeight: "bold",
  },
  logoContainer: {
    alignItems: "center",
    flexDirection: "column",
    marginTop: 120,
  },
  primaryText: {
    fontSize: 18,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
    marginTop: "5%",
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    height: 45,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "10%",
    borderRadius: 5,
    width: "100%",
  },
  buttonTextStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 14,
    fontWeight: "bold",
  },
});
