import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import * as CartActions from "../../store/action/cart";
import Modal from "../Components/Modal";
import {
  CustomiseScreenProvider,
  useCustomisePizza,
} from "../../contexts/customise";
import { TouchableOpacity } from "react-native-gesture-handler";
import CallToStore from "../Components/CallToStore";
import CartModal from "../Components/CartModal";
import { SelectedItemsPreview } from "../CustomisePizza";
import CBLogoNoFill from "../Components/svg/CBLogoGoldNoFill";
import Toast from "react-native-toast-message";
import Minus from "../Components/svg/Minus.js";
import Plus from "../Components/svg/Plus.js";
import { useFocusEffect } from "@react-navigation/native";
import { useTabVisible } from "../../contexts/navigation";

export const PIZZA_PREVIEW_IMAGE_RADIUS = 65;
const ICON_HIT_SLOP = { top: 7, left: 7, bottom: 7, right: 7 };

const buttonStyles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderRadius: 20,
    maxWidth: 160,
    justifyContent: "center",
    minHeight: 34,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    paddingHorizontal: 20,
    fontSize: 12,
  },
});
export function Button({ label, onPress }) {
  return (
    <TouchableOpacity
      style={buttonStyles.buttonStyle}
      activeOpacity={0.8}
      onPress={onPress}
    >
      <Text numberOfLines={3} style={buttonStyles.buttonTextStyle}>
        {label ?? ""}
      </Text>
    </TouchableOpacity>
  );
}
export function ButtonNormal({ label, onPress }) {
  return (
    <TouchableOpacity
      style={styles.buttonStyleLogout}
      activeOpacity={0.8}
      onPress={onPress}
    >
      <Text numberOfLines={3} style={styles.buttonLogoutTextStyle}>
        {label ?? ""}
      </Text>
    </TouchableOpacity>
  );
}

const counterStyles = StyleSheet.create({
  counter: {
    flexDirection: "row",
    minHeight: 34,
    borderRadius: 31,
    paddingHorizontal: 14,
    alignItems: "center",
    backgroundColor: "#f3e5f5",
  },
  counterView: {
    width: 12,
    alignItems: "center",
    justifyContent: "center",
  },
  quantitView: {
    width: 41,
    alignItems: "center",
    justifyContent: "center",
  },
  quantityText: {
    fontSize: 16,
    fontWeight: "500",
  },
});
export function Counter({ value = 1, setCount, disableIncreament }) {
  const { updateQuantity } = useCustomisePizza();
  const increment = React.useCallback(() => {
    setCount(value + 1);
    updateQuantity(value + 1);
  }, [value]);
  const decrement = React.useCallback(() => {
    if (value !== 1) {
      setCount(value - 1);
      updateQuantity(value - 1);
    }
  }, [value]);
  return (
    <View style={counterStyles.counter}>
      <TouchableWithoutFeedback
        hitSlop={ICON_HIT_SLOP}
        style={counterStyles.counterView}
        onPress={decrement}
      >
        <View style={counterStyles.infoMinus}>
          <Minus height={23} width={23} fill={"#673ab7"} />
        </View>
      </TouchableWithoutFeedback>
      <View style={counterStyles.quantitView}>
        <Text style={styles.quantityText}>{value}</Text>
      </View>
      <TouchableWithoutFeedback
        style={counterStyles.counterView}
        hitSlop={ICON_HIT_SLOP}
        onPress={increment}
        disabled={disableIncreament}
      >
        <View style={counterStyles.infoPlus}>
          <Plus height={23} width={23} fill={"#673ab7"} />
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

export function PizzaPreviewImage() {
  const { image } = useCustomisePizza();
  return <Image style={styles.imageView} source={{ uri: image }} />;
}

export function PizzaPreview() {
  const { name, price, quantity } = useCustomisePizza();

  return (
    <View style={styles.previewImageView}>
      <Text style={styles.previewTitle}>{name}</Text>
      <View style={styles.costContainer}>
        <Text style={styles.totalCosttxt}>Total Cost:</Text>
        <Text style={styles.totalCostValue}> {"$ " + price * quantity}</Text>
      </View>
    </View>
  );
}
const ButtonView = (props) => {
  const cart = useSelector((state) => state.cart.cartItems);
  const loggedInUser = useSelector((state) => state.loggedInUser.loggedInUser);
  const getQuan = () => {
    let filteredCartItem;
    let cartQuantity = 1;
    if (props?.uuid) {
      filteredCartItem = cart.filter((item) => item.uuid == props.uuid);
      cartQuantity = filteredCartItem[0]?.quantity || 1;
      return cartQuantity;
    } else {
      return 1;
    }
  };
  const [showCartModal, setShowCartModal] = useState(false);
  const { selectedItemsPreview, price, name, id } = useCustomisePizza();
  const [quantity, setQuantity] = useState(1);
  const [isCartLimitExceeded, setIsCartLimitExceeded] = useState(
    checkIfCartLimitExceeded()
  );

  function checkIfCartLimitExceeded(qty) {
    const maxItemPerCart = loggedInUser?.userDetails?.maxItemPerCart;
    if (maxItemPerCart == null) {
      return false;
    }
    const itemInCart = cart.reduce((total, item) => {
      return total + item.quantity;
    }, 0);
    const currentItemsInCart =
      qty == null ? itemInCart + quantity : itemInCart + qty;
    return currentItemsInCart >= maxItemPerCart;
  }

  const showMaximumItemExceededToast = () => {
    Toast.show({
      type: "error",
      position: "top",
      bottomOffset: 210,
      text1: "Error",
      text2: `You exceeded maximum(${loggedInUser?.userDetails?.maxItemPerCart}) item(s) added to the cart!`,
    });
  };

  const addToCart = () => {
    const cartLimitExceeded = checkIfCartLimitExceeded();
    setIsCartLimitExceeded(cartLimitExceeded);
    if (cartLimitExceeded) {
      showMaximumItemExceededToast();
      return;
    }
    dispatch(
      CartActions.addItemsToCart({
        ingredients: selectedItemsPreview,
        name: name,
        id,
        quantity: quantity,
        price: price,
        uuid: new Date().toISOString(),
      })
    );
  };

  useEffect(() => {
    setShowCartModal(false);
    setQuantity(getQuan());
  }, [props]);
  const dispatch = useDispatch();

  const addItemToCart = () => {
    setShowCartModal(false);
    props.navigateToCart();
  };

  const updateItemsInCart = () => {
    const maxItemPerCart = loggedInUser?.userDetails?.maxItemPerCart;
    const filteredCart = cart.filter((item) => item.uuid != props.uuid);
    const itemInCart = filteredCart.reduce(
      (total, item) => total + item.quantity,
      0
    );
    if (!(itemInCart + quantity <= maxItemPerCart)) {
      showMaximumItemExceededToast();
      return;
    }
    dispatch(
      CartActions.updateItemsToCart({
        ingredients: selectedItemsPreview,
        name: name,
        id: id,
        quantity: quantity,
        price: price,
        uuid: props.uuid,
      })
    );
    props.navigateToCart();
  };

  const addToCartAndNavigateToCheckout = () => {
    const itemInCart = cart.reduce((total, item) => {
      return total + item.quantity;
    }, 0);
    if (itemInCart < loggedInUser?.userDetails?.maxItemPerCart) {
      dispatch(
        CartActions.addItemsToCart({
          ingredients: selectedItemsPreview,
          name: name,
          id,
          quantity: loggedInUser?.userDetails?.maxItemPerCart - itemInCart,
          price: price,
          uuid: new Date().toISOString(),
        })
      );
    }
    props.navigation.navigate("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "Cart",
        params: {
          screen: "checkoutVIPScreen",
          initial: false,
          params: {
            checkOut: true,
          },
        },
      },
    });
  };
  return (
    <View style={styles.actionPanel}>
      <View style={styles.qtyPanel}>
        <View style={styles.elements}>
          <Text style={styles.qtyText}>QTY:</Text>
        </View>
        <View style={styles.elements}>
          <Counter
            value={quantity}
            setCount={(count) => {
              setQuantity(count);
              setIsCartLimitExceeded(checkIfCartLimitExceeded(count));
            }}
            disableIncreament={!props.uuid && isCartLimitExceeded}
          />
        </View>
        {props.uuid && (
          <View style={styles.elements}>
            <Button
              label={"Save Updates"}
              onPress={() => updateItemsInCart()}
            />
          </View>
        )}
        {!props.uuid && !isCartLimitExceeded && (
          <View style={styles.elements}>
            <Button label={"Add to Cart"} onPress={() => addToCart()} />
          </View>
        )}
        {!props.uuid && isCartLimitExceeded && (
          <View style={styles.elements}>
            <Button
              label={"Checkout"}
              onPress={() => addToCartAndNavigateToCheckout()}
            />
          </View>
        )}
        <View style={styles.elements}>
          <CBLogoNoFill width={40} height={40} viewBox="0 -15 110 150" />
        </View>
      </View>
      <CartModal
        onPress={addItemToCart}
        showModal={showCartModal}
        setVisible={() => {
          setShowCartModal(false);
        }}
        navigation={props.navigation}
      />
    </View>
  );
};
const PizzaIngredients = () => {
  const { basicSelections } = useCustomisePizza();
  const ingredients = [];
  basicSelections.map((item) => {
    ingredients.push(
      <Text style={styles.selectionSectionTxt}> {item?.title}: </Text>
    );
    item?.items.map((ing) => {
      if (ing?.isDefault === false) {
        ingredients.push(
          <Text style={styles.selectionValueTxt}>{ing?.label}, </Text>
        );
      }
    });
  });
  return ingredients;
};
function PizzaDetailsScreen(props) {
  const { navigation } = props;
  const pizzaId = props?.route?.params?.pizzaId;
  const [showModal, setShowModal] = useState(false);
  const modalClick = () => {
    setShowModal(true);
  };
  const { setTabVisible } = useTabVisible();
  useFocusEffect(
    React.useCallback(() => {
      setTabVisible(true);
    }, [setTabVisible])
  );

  const navigateToCart = () => {
    navigation.push("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "Cart",
      },
    });
  };
  const navigateToCustomizePizza = () => {
    navigation.navigate("pizzaScreenCustomise", {
      pizzaId: pizzaId,
    });
  };
  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <CallToStore onPress={modalClick} />,
    });
  });

  return (
    <CustomiseScreenProvider id={pizzaId} uuid={props?.route?.params?.uuid}>
      <View style={styles.root}>
        <ScrollView contentContainerStyle={styles.rootContainer}>
          <PizzaPreviewImage />
          <PizzaPreview />
          <ButtonView
            navigateToCart={navigateToCart}
            uuid={props?.route?.params?.uuid}
            navigation={navigation}
          />
          <SelectedItemsPreview />
          <View style={styles.customizeButton}>
            <ButtonNormal
              label={"Customize Pizza"}
              onPress={navigateToCustomizePizza}
            />
          </View>
          {showModal && (
            <Modal
              showModal={showModal}
              setVisible={() => {
                setShowModal(false);
              }}
            />
          )}
        </ScrollView>
      </View>
    </CustomiseScreenProvider>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "#fafcfe",
    padding: 15,
  },
  rootContainer: {
    backgroundColor: "#fff",
    paddingVertical: 10,
    borderRadius: 5,
    elevation: 1,
  },
  additionalSelection: {
    marginTop: 5,
    paddingHorizontal: 25,
  },
  customizeButton: {
    alignItems: "center",
    marginTop: 20,
    marginBottom: 10,
  },
  buttonStyleLogout: {
    backgroundColor: "#fff",
    color: "#a38862",
    borderColor: "#a38862",
    borderWidth: 2,
    borderRadius: 20,
    height: 40,
    alignItems: "center",
    paddingHorizontal: 100,
    flexDirection: "row",
    textAlign: "center",
    justifyContent: "space-around",
  },
  buttonLogoutTextStyle: {
    color: "#a38862",
    fontSize: 13,
  },
  selectionItemsView: {
    marginTop: 20,
    paddingHorizontal: 26,
    minHeight: 20,
  },
  ingredientsContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 20,
    paddingHorizontal: 10,
  },
  selectionSectionTxt: {
    color: "#607d8b",
    fontSize: 14,
    letterSpacing: 0.3,
    fontWeight: "600",
  },
  selectionValueTxt: {
    color: "#607d8b",
    fontSize: 14,
    letterSpacing: 0.3,
  },
  pizzaSide: {
    height: 34,
    width: 34,
  },
  additionalSelectionHeaderLeftImage: {
    height: 45,
    width: 45,
    alignItems: "center",
    justifyContent: "center",
  },
  additionalSelectionHeader: {
    flexDirection: "row",
    height: 50,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "#eceff1",
    marginTop: 26,
  },
  additionalSelectionValue: {
    flexDirection: "row",
    height: 57,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "#eceff1",
  },
  additionalSelectionHeadeLeft: {
    flex: 6,
  },
  additionalSelectionHeadeLeftValue: {
    flex: 6,
    flexDirection: "row",
    alignItems: "center",
  },
  additionalSelectionHeaderRight: {
    flex: 4,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  pizzaSideContainer: {
    flex: 4,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  additionalSelectionHeaderLeftTxt: {
    fontSize: 16,
    fontWeight: "500",
    color: "#455a64",
  },
  additionalSelectionHeaderLeftValueTxt: {
    fontSize: 14,
    marginLeft: 16,
    fontWeight: "500",
    color: "#212121",
  },
  additionalSelectionHeaderRightTxt: {
    fontSize: 12,
    color: "#78909c",
  },
  basicSelectionsRoot: {
    paddingHorizontal: 25,
  },
  sectionHeader: {
    fontSize: 16,
    fontWeight: "500",
    color: "#455a64",
    marginTop: 25,
  },
  listView: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  selectionItem: {
    flexDirection: "row",
    paddingHorizontal: 14,
    paddingVertical: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#cfd8dc",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 15,
    marginTop: 10,
  },
  selectedBorder: {
    borderWidth: 2,
    borderColor: "#a38862",
    paddingHorizontal: 13,
    paddingVertical: 9,
  },
  selectionItemsView: {
    marginTop: 20,
    paddingHorizontal: 25,
    minHeight: 20,
  },
  primarySelectionText: {
    fontSize: 11,
    fontWeight: "500",
    color: "#607d8b",
  },
  secondaryText: {
    fontSize: 11,
    fontWeight: "500",
    color: "#607d8b",
    marginLeft: 12,
  },
  qtyText: {
    color: "#455a64",
    fontSize: 12,
  },
  favIcon_selected: {
    height: 33,
    width: 33,
    borderRadius: 33,
    backgroundColor: "#ffebee",
    justifyContent: "center",
    alignItems: "center",
  },
  favIcon_unselected: {
    height: 33,
    width: 33,
    borderRadius: 33,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
  },
  actionPanel: {
    marginTop: 10,
    flexDirection: "row",
    paddingHorizontal: 20,
    justifyContent: "space-between",
    alignItems: "center",
  },
  qtyPanel: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  elements: {
    marginLeft: 5,
  },
  previewImageView: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    margin: 5,
    marginTop: PIZZA_PREVIEW_IMAGE_RADIUS + 60,
  },
  imageView: {
    padding: 16,
    height: PIZZA_PREVIEW_IMAGE_RADIUS * 2,
    width: PIZZA_PREVIEW_IMAGE_RADIUS * 2,
    borderRadius: PIZZA_PREVIEW_IMAGE_RADIUS,
    backgroundColor: "#fff",
    position: "absolute",
    alignSelf: "center",
  },
  textView: {
    width: 34,
    height: 34,
    borderRadius: 34,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#eceff1",
  },
  text: {
    fontWeight: "500",
    fontSize: 16,
    color: "#455a64",
  },
  textSelected: {
    fontWeight: "500",
    fontSize: 16,
    color: "white",
  },
  textView1: {
    marginLeft: -17,
    backgroundColor: "#eceff1",
  },
  textView2: {
    marginLeft: 7,
  },
  textView3: {
    marginLeft: -17,
  },
  textViewSelected: {
    backgroundColor: "#9c27b0",
  },
  previewTextView: {
    flexDirection: "row",
    marginLeft: 20,
  },
  previewTitle: {
    color: "#455a64",
    fontSize: 22,
    fontFamily: "Roboto-Medium",
  },
  costContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10,
  },
  totalCosttxt: {
    fontSize: 13,
    color: "#455a64",
  },
  totalCostValue: {
    fontSize: 22,
    fontWeight: "bold",
    color: "#f44336",
    marginLeft: 16,
  },
});

export default PizzaDetailsScreen;
