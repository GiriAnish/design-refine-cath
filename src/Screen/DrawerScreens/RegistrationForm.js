import React, { useState, useEffect, useRef } from "react";
import { connect, useDispatch } from "react-redux";
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Text,
  TouchableWithoutFeedback,
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import { TextInput as NewTextInput, Checkbox } from "react-native-paper";
import {
  setNewUser,
  RESET_ALL_USER_REGISTRATION,
} from "../../store/action/userRegistrationAction";
import Loader from "../Components/Loader";
import Toast from "react-native-toast-message";
import CBLogoPurpleNoFill from "../Components/svg/CBLogoPurpleNoFill";
import TermsModal from "../Components/TermsModal";
import PrivacyPolicyModel from "../Components/PrivacyPolicyModel";

const validateEmail = (email) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
const RegForm = (props) => {
  const scrollViewRef = useRef();

  const dispatch = useDispatch();
  const { accountId } = props?.route?.params;

  const { createNewUser, data, loading } = props;
  const [checked, setChecked] = useState(false);
  const [showTermsModal, setShowTermsModal] = useState(false);
  const [showPrivacyModal, setShowPrivacyModal] = useState(false);
  const [isKeyboardActive, setIsKeyboardActive] = useState(false);
  const [form, setForm] = useState({
    firstName: "",
    lastName: "",
    email: "",
  });
  const [error, setError] = useState({});
  Toast.hide();
  const Right = () => {
    return (
      <CBLogoPurpleNoFill
        height="38"
        width="40"
        opacity="0.5"
        viewBox="0 -5 98 100"
      />
    );
  };
  const TextInputComp = React.useCallback(
    (props) => {
      return (
        <NewTextInput
          style={styles.inputStyle}
          onChangeText={(text) => {
            setForm((prevState) => ({ ...prevState, [props.id]: text }));
          }}
          value={props.value}
          underlineColor="transparent"
          onFocus={onFocusChange}
        />
      );
    },
    [props]
  );
  const onFocusChange = () => {
    setIsKeyboardActive(true);
  };
  useEffect(() => {
    if (isKeyboardActive) {
      scrollViewRef.current.scrollToEnd({ animated: true });
    }
  }, [isKeyboardActive, scrollViewRef]);
  const FormComp = React.useCallback(
    ({ label, id, value, first, errorText }) => {
      return (
        <View style={{ ...styles.CompCont, marginTop: first ? 25 : 15 }}>
          <Text style={styles.title}>{label}</Text>
          <View style={{ flexDirection: "row" }}>
            <TextInputComp id={id} value={value} />
            <View style={styles.rightIcon}>
              <Right />
            </View>
          </View>
          {error[id] && (
            <View style={styles.errorTextCont}>
              <Text style={styles.errorText}>{errorText + " is required"}</Text>
            </View>
          )}
        </View>
      );
    },
    [props, error]
  );

  const nextClicked = () => {
    if (!checked) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: "Please agree to the terms below",
      });
    } else {
      let keys = Object.keys(form);
      const formErrors = Object.keys(error);
      keys.map((item) => {
        if (!form[item] || form[item].length == 0) {
          setError((prevState) => ({ ...prevState, [item]: true }));
        } else {
          if (item === "email") {
            validateEmail(form[item])
              ? setError((prevState) => ({ ...prevState, [item]: false }))
              : setError((prevState) => ({ ...prevState, [item]: true }));
          } else {
            setError((prevState) => ({ ...prevState, [item]: false }));
          }
        }
      });
      {
        const payload = {
          firstName: form["firstName"],
          lastName: form["lastName"],
          email: form["email"],
          accountId: accountId,
        };
        createNewUser(payload);
      }
    }
  };

  useEffect(() => {
    if (data?.successBody?.user) {
      props.navigation.navigate("RegEmailVerify", {
        emailId: form["email"],
        userId: data?.successBody?.user?.id,
      });
      dispatch({ type: RESET_ALL_USER_REGISTRATION });
    } else if (data?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: data?.errorBody,
      });
      dispatch({ type: RESET_ALL_USER_REGISTRATION });
    }
  }, [data]);

  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={100}
      style={{ flex: 1 }}
      enabled={Platform.OS === "ios"}
      behavior="padding"
    >
      <ScrollView
        style={styles.scrollView}
        keyboardShouldPersistTaps="handled"
        ref={scrollViewRef}
      >
        <Loader loading={loading} />
        <View style={styles.topContainer}>
          <View style={styles.logo}>
            <ImageBackground
              resizeMode={"contain"}
              source={{ uri: props?.route?.params?.bannerImageURL }}
              style={styles.bannerCont}
            >
              <View style={styles.cblogopurple}>
                <CBLogoPurpleNoFill
                  height="40"
                  width="40"
                  viewBox="0 0 98 100"
                />
              </View>
              <View style={styles.titleCont1}>
                <Text style={styles.title1}>
                  {props?.route?.params?.bannerText}&nbsp;
                  {props?.route?.params?.year}
                </Text>
              </View>
              <View style={styles.absCont}>
                {props?.route?.params?.logoUrl?.length > 0 && (
                  <View style={styles.accountImageCen}>
                    <Image
                      resizeMode={"contain"}
                      source={{ uri: props?.route?.params?.logoUrl }}
                      style={{ width: 200, height: 60 }}
                    />
                  </View>
                )}
                {props?.route?.params?.logoUrl?.length == 0 && (
                  <View style={styles.textCont}>
                    <Text style={styles.accountText}>
                      {props?.route?.params?.label}
                    </Text>
                  </View>
                )}
              </View>
            </ImageBackground>
          </View>
          <View>
            <FormComp
              keyBoardType="web-search"
              id={"firstName"}
              label={"First Name*"}
              value={form?.["firstName"]}
              first={true}
              errorText={"First Name"}
            />
            <FormComp
              id={"lastName"}
              label={"Last Name*"}
              value={form?.["lastName"]}
              errorText={"Last Name"}
            />
            <FormComp
              keyBoardType="email-address"
              id={"email"}
              label={"Work Email Address*"}
              value={form?.["email"]}
              errorText={"A work email"}
            />
          </View>
          <View style={styles.container}>
            <View style={styles.checkboxContainer}>
              <Checkbox.Android
                style={styles.checkbox}
                status={checked ? "checked" : "unchecked"}
                onPress={() => {
                  setChecked(!checked);
                }}
              />
              <Text style={styles.label}>
                <Text>I agree to the </Text>
                <TouchableWithoutFeedback
                  onPress={() => setShowTermsModal(true)}
                >
                  <Text style={styles.linking}>Terms of Service</Text>
                </TouchableWithoutFeedback>
                <Text> and </Text>
                <TouchableWithoutFeedback
                  onPress={() => setShowPrivacyModal(true)}
                >
                  <Text style={styles.linking}>Privacy Policy</Text>
                </TouchableWithoutFeedback>
                <Text> </Text>
                <Text style={{ color: "red", textDecorationLine: "none" }}>
                  *
                </Text>
              </Text>
            </View>
          </View>
          <View style={styles.buttonCont}>
            <TouchableWithoutFeedback
              onPress={() => {
                nextClicked();
              }}
            >
              <View style={styles.checkoutContainer}>
                <Text style={styles.checkoutText}>Next</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
        <TermsModal
          showModal={showTermsModal}
          setVisible={() => {
            setShowTermsModal(false);
          }}
        />
        <PrivacyPolicyModel
          showModal={showPrivacyModal}
          setVisible={() => {
            setShowPrivacyModal(false);
          }}
        />
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default connect(
  (state) => ({
    data: state.registeredUser.registeredUser,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    createNewUser: (payload) => dispatch(setNewUser(payload)),
  })
)(RegForm);

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#fff",
  },
  topContainer: {
    marginTop: -20,
    marginBottom: 20,
  },
  logo: {
    width: "100%",
    height: "auto",
  },
  bannerCont: {
    width: "100%",
    height: 270,
    alignItems: "center",
    marginBottom: 5,
    marginVertical: -20,
  },
  cblogopurple: {
    marginBottom: -40,
    marginTop: 50,
    marginLeft: 345,
    opacity: 0.9,
  },
  absCont: {
    height: 64,
    width: "90%",
    backgroundColor: "#fff",
    borderRadius: 7,
    marginTop: 130,
    position: "absolute",
    top: 100,
    justifyContent: "center",
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 2,
  },
  titleCont1: {
    justifyContent: "center",
    alignContent: "center",
    width: "80%",
    flex: 1,
    flexDirection: "row",
    marginTop: 70,
  },
  title1: {
    fontSize: 25,
    color: "#fff",
    fontWeight: "600",
    textAlign: "center",
  },
  accountImageCen: {
    alignSelf: "center",
    width: "100%",
    alignItems: "center",
  },
  inputStyle: {
    flex: 1,
    color: "white",
    height: 50,
    marginBottom: 10,
    marginTop: 6,
    width: "100%",
    paddingTop: 0,
    backgroundColor: "#ede7f6",
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  buttonCont: { alignItems: "center", marginTop: 0 },
  checkoutContainer: {
    width: "90%",
    height: 45,
    backgroundColor: "#673ab7",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  checkoutText: { fontSize: 16, color: "#fff", fontWeight: "600" },
  CompCont: { marginHorizontal: "5%", marginTop: 10 },
  title: { color: "#a38862", fontSize: 18 },
  rightIcon: {
    backgroundColor: "#ede7f6",
    height: 50,
    justifyContent: "center",
    marginTop: 6,
    alignContent: "center",
    paddingRight: 10,
    borderTopEndRadius: 5,
    borderBottomRightRadius: 5,
    marginLeft: -2,
  },
  errorTextCont: { marginTop: -10, padding: 0 },
  errorText: { color: "#f44336", fontSize: 14 },
  accountText: {
    fontSize: 23,
    color: "#263238",
    alignSelf: "center",
    height: 400,
  },
  textCont: {
    alignSelf: "center",
    width: "60%",
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
    alignItems: "center",
  },
  checkbox: {
    width: 22,
    height: 22,
    backgroundColor: "#fafafa",
    borderColor: "#90a4ae",
    borderWidth: 2,
    borderStyle: "solid",
  },
  label: {
    margin: 8,
    fontFamily: "Roboto",
    fontSize: 14,
  },
  linking: {
    color: "#2196f3",
    textAlign: "center",
    marginTop: 10,
    fontSize: 14,
    fontFamily: "Roboto",
    fontSize: 14,
  },
});
