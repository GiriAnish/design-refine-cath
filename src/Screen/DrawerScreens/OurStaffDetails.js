import React, { useEffect, useState, useCallback } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  Linking,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import RenderHtml from "react-native-render-html";
import Loader from "../Components/Loader";
import theme from "../../assets/css/commonThemes";

import _const from "../../data/const";

const OpenURLButton = ({ url, icon }) => {
  const handlePress = useCallback(async () => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  }, [url]);
  return (
    <TouchableOpacity onPress={handlePress}>
      <Icon name={icon} color={theme.Colors.purple} size={30} />
    </TouchableOpacity>
  );
};

const OurStaffDetails = (props) => {
  const { staffId } = props.route.params;
  const [isLoading, setLoading] = useState(true);
  const [staffData, setStaffData] = useState({});
  useEffect(() => {
    fetch(_const.baseUrl + "api/v1/staff/" + staffId)
      .then((response) => response.json())
      .then((json) => setStaffData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, [staffId]);
  const {
    facebookURL,
    twitterURL,
    instagramURL,
    linkedinURL,
    imageURL,
    firstName,
    lastName,
    title,
    biography,
  } = staffData;
  const source = {
    html: `
      <p>
        ${biography}
      </p>`,
  };
  return (
    <ScrollView style={styles.scrollView}>
      <View style={styles.modalContainer}>
        <View>
          <Loader loading={isLoading} />
          {!isLoading && (
            <View style={styles.rootContainer}>
              <View style={styles.flexContainer}>
                <Image source={{ uri: imageURL }} style={styles.heroImage} />
              </View>
              <View style={styles.flexContainer}>
                <Text style={styles.name}>
                  {firstName} {lastName}
                </Text>
              </View>
              <View style={styles.flexContainer}>
                <Text style={styles.title}>{title}</Text>
              </View>
              {instagramURL || twitterURL || facebookURL || linkedinURL ? (
                <View style={styles.socialContainer}>
                  {facebookURL && facebookURL !== "" ? (
                    <OpenURLButton icon={"facebook"} url={facebookURL} />
                  ) : null}
                  {twitterURL && twitterURL !== "" ? (
                    <OpenURLButton icon={"twitter"} url={twitterURL} />
                  ) : null}
                  {instagramURL && instagramURL !== "" ? (
                    <OpenURLButton icon={"instagram"} url={instagramURL} />
                  ) : null}
                  {linkedinURL && linkedinURL !== "" ? (
                    <OpenURLButton icon={"linkedin"} url={linkedinURL} />
                  ) : null}
                </View>
              ) : null}
              <View style={styles.flexContainer}>
                <RenderHtml source={source} baseStyle={styles.biography} />
              </View>
            </View>
          )}
        </View>
      </View>
    </ScrollView>
  );
};

export default OurStaffDetails;
const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    width: "97%",
    padding: 10,
    alignItems: "center",
    backgroundColor: "#fff",
    shadowColor: "#000000",
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 2,
    borderRadius: 5,
    marginVertical: 6,
    marginHorizontal: 6,
  },
  scrollView: {
    backgroundColor: "#fafcfe",
  },
  heroImage: {
    width: 140,
    height: 140,
    aspectRatio: 1,
    borderRadius: 100,
  },
  flexContainer: {
    flexDirection: "row",
    justifyContent: "center",
    margin: 5,
  },
  name: {
    fontSize: 22,
    color: "#212121",
    fontWeight: "500",
  },
  title: {
    fontFamily: "Roboto-Medium",
    fontSize: 20,
    color: "#673ab7",
  },
  biography: {
    backgroundColor: "#fff",
    color: "#607d8b",
    borderRadius: 5,
    fontFamily: "Roboto",
    fontWeight: "400",
    fontSize: 14,
    textAlign: "left",
  },
  rootContainer: {
    marginBottom: 213,
  },

  socialContainer: {
    justifyContent: "space-evenly",
    marginTop: 12,
    flexDirection: "row",
  },
});
