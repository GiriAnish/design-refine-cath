import React, { useEffect, useCallback, useRef } from "react";
import styled from "@emotion/native";
import { SafeAreaView } from "react-native";
import MyOrdersEmpty from "../Components/MyOrdersEmpty";
import { connect } from "react-redux";
import {
  fetchMyOrders,
  reOrder,
  RESET_RE_ORDER,
  RESET_CANCEL_ORDER,
} from "../../store/action/myOrdersAction";
import Loader from "../Components/Loader";
import MyOrdersList from "../Components/MyOrdersList";
import Toast from "react-native-toast-message";
import * as CartActions from "../../store/action/cart";
import { useDispatch } from "react-redux";
import { getSelectionPreviewFromServerData } from "../../contexts/customise";
import { useTabVisible } from "../../contexts/navigation";

const Container = styled.View`
  flex: 1;
  padding: 10px;
`;
const MyOrdersScreen = (props) => {
  const {
    myOrder,
    loading,
    userData,
    navigation,
    getMyOrders,
    cancelOrder,
    repeatOrder,
    reOrderStatus,
  } = props;
  const { setTabVisible } = useTabVisible();
  const userId = userData?.userDetails?.id;
  const now = myOrder?.now?.length > 0 ? myOrder.now : [];
  const past = myOrder?.past?.length > 0 ? myOrder.past : [];
  const dispatch = useDispatch();

  const isMounted = useRef();

  useEffect(() => {
    if (!loading) {
      isMounted.current = true;
    }
  }, [loading]);

  useEffect(() => {
    setTabVisible(true);
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getMyOrders(userId);
      setTabVisible(true);
    });
    return unsubscribe;
  }, [navigation, userId]);
  useEffect(() => {
    if (cancelOrder?.successBody?.message) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Order Cancelled",
        text2: cancelOrder?.successBody?.message,
      });
      dispatch({ type: RESET_CANCEL_ORDER});
      getMyOrders(userId);
    }
  }, [cancelOrder]);
  useEffect(() => {
    if (reOrderStatus?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: reOrderStatus?.errorBody,
      });
    } else if (reOrderStatus?.successBody?.items?.length && isMounted.current) {
      const cartItems = reOrderStatus?.successBody?.items?.map((orderItem) => {
        return {
          ingredients: getSelectionPreviewFromServerData(orderItem),
          name: orderItem.name,
          id: orderItem.id,
          price: orderItem.price,
          uuid: new Date().toISOString(),
          quantity: 1,
        };
      });
      dispatch({ type: RESET_RE_ORDER });
      //dispatch(CartActions.resetCart());
      dispatch(CartActions.addItemsToCart(cartItems));
      navigation.navigate("DrawerNavigationRoutes", {
        screen: "homeScreenStack",
        params: {
          screen: "Cart",
        },
      });
    }
  }, [reOrderStatus]);

  const navigateToPizzaMenu = () => {
    navigation.navigate("PizzaMenu");
  };
  const repeatMyOrder = useCallback((id) => {
    repeatOrder(id);
  }, []);
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#fafcfe" }}>
      <Loader loading={loading} />
      {!loading && (
        <Container>
          {myOrder?.now?.length > 0 || myOrder?.past?.length > 0 ? (
            <MyOrdersList
              navigation={navigation}
              now={now}
              past={past}
              reOrder={repeatMyOrder}
            />
          ) : (
            <MyOrdersEmpty
              onPress={navigateToPizzaMenu}
              imageSource={require("../../assets/images/my_orders.png")}
              title={"No Order History"}
              info={"Looks like you have not placed any orders in the past!"}
            />
          )}
        </Container>
      )}
    </SafeAreaView>
  );
};

export default connect(
  (state) => ({
    myOrder: state.myOrders.myOrders,
    cancelOrder: state.myOrders.cancelOrder,
    reOrderStatus: state.myOrders.reOrder,
    loading: state.loading.loading,
    userData: state.loggedInUser.loggedInUser,
  }),
  (dispatch, ownProps) => ({
    getMyOrders: (id) => dispatch(fetchMyOrders(id)),
    repeatOrder: (id) => dispatch(reOrder(id)),
  })
)(MyOrdersScreen);
