import React, { useCallback, useEffect, useState, Fragment } from "react";
import styled from "@emotion/native";
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import _const from "../../data/const";
import LeftLogo from "../Components/svg/CBLogoGoldNoFill";
import CancelOrderModal from "../Components/CancelOrder";
import { connect, useDispatch } from "react-redux";
import {
  getOneOrderDetail,
  trackOrder,
} from "../../store/action/myOrdersAction";
import { convertLocalDateFormat, convertTime } from "../../utils/date";
import { useTabVisible } from "../../contexts/navigation";
import CircleCheck from "../Components/svg/CircleCheck";

const MainContainer = styled.View`
  background: ${({ theme }) => theme.Colors.white};
  font-family: ${({ theme }) => theme.fontFamily};
  align-items: center;
  border-radius: 5px;
  border-color: white;
  border-width 0.1px;
  elevation: 0.5;
  padding: 15px;
  overflow: hidden;
  margin: 5px;
`;
const SubContainer = styled.View`
  width: 100%;
  padding: 5px 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
const OrderTitle = styled.Text`
  color: ${({ theme }) => theme.Colors.oxfordBlue};
  font-weight: 500;
  font-size: 18px;
  text-align: left;
  margin-left: 10px;
`;
const ButtonPending = styled.Text`
  font-size: 13px;
  color: ${({ theme }) => theme.Colors.sandal};
  border-color: ${({ theme }) => theme.Colors.sandal};
  border-width: 1px;
  padding: 5px 10px;
  border-radius: 4px;
`;
const ButtonCompleted = styled.Text`
  font-size: 13px;
  padding: 10px;
  color: #f44336;
  border-color: #f44336;
  border-width: 1px;
  border-radius: 4px;
`;
const OrderIcon = styled.View`
  width: 35px;
  height: 35px;
  align-items: center;
  justify-content: center;
  border-radius: 30px;
`;
const ValueText = styled.Text`
  color: ${({ theme }) => theme.Colors.oxfordBlue};
  font-weight: 400;
  font-size: 16px;
  text-align: right;
`;
const LabelText = styled.Text`
  color: ${({ theme }) => theme.Colors.oxfordBlue};
  font-weight: 500;
  font-size: 16px;
  text-align: left;
`;
const TotalText = styled.Text`
  color: ${({ theme }) => theme.Colors.purple};
  font-weight: 500;
  font-size: 16px;
  text-align: left;
`;
const TotalAmount = styled.Text`
  color: ${({ theme }) => theme.Colors.purple};
  font-weight: 500;
  font-size: 18px;
  text-align: right;
`;
const QtyText = styled.Text`
  color: ${({ theme }) => theme.Colors.sanJuan};
  font-weight: 400;
  font-size: 12px;
  text-align: left;
  margin-right: 20px;
`;
const Qty = styled.Text`
  width: 56px;
  height: 34px;
  padding: 8px;
  background: ${({ theme }) => theme.Colors.selago};
  border-radius: 20px;
  text-align: center;
`;
const TouchableButton = styled((props) => <TouchableOpacity {...props} />)`
  background: ${({ theme }) => theme.Colors.darkOrchid};
  color: ${({ theme }) => theme.Colors.white};
  border-radius: 10px;
  height: 40px;
  align-items: center;
  justify-content: space-around;
`;
const TouchableButtonText = styled((props) => <Text {...props} />)`
  color: ${({ theme }) => theme.Colors.white};
  padding: 10px 30px;
  font-weight: 500;
  font-size: 14px;
`;
const CancelTouchableButton = styled((props) => (
  <TouchableOpacity {...props} />
))`
  border-width: 1px;
  background: ${({ theme }) => theme.Colors.white};
  border-color: ${({ theme }) => theme.Colors.sandal};
  border-radius: 10px;
  height: 40px;
  align-items: center;
  justify-content: space-around;
`;
const CancelTouchableButtonText = styled((props) => <Text {...props} />)`
  color: ${({ theme }) => theme.Colors.black};
  padding: 10px 30px;
  font-weight: 500;
  font-size: 14px;
`;
const DeliveryTrackingIcon = styled((props) => <Image {...props} />)`
  margin-left: 15px;
  margin-right: 15px;
`;
const DeliveryText = styled.Text`
  color: ${({ theme }) => theme.Colors.sanJuan};
  font-weight: 500;
  font-size: 14px;
  text-align: left;
  width: 80%;
`;
const ActionContainer = styled((props) => <SubContainer {...props} />)`
  margin-top: 10px;
  margin-bottom: 10px;
`;
const TimeLineContainer = styled((props) => <SubContainer {...props} />)`
  margin-top: 10px;
  margin-bottom: 10px;
`;
const DeliveryTimeText = styled.Text`
  color: ${({ theme }) => theme.Colors.bermudaGrey};
  font-weight: 400;
  font-size: 14px;
  text-align: right;
`;
const AddressTitle = styled.Text`
  color: ${({ theme }) => theme.Colors.Atomic};
  font-weight: 500;
  font-size: 16px;
  text-align: left;
  margin: 5px;
`;
const AddressText = styled.Text`
  width: 210px;
  color: #546e7a;
  font-family: "Roboto";
  font-weight: 400;
  font-size: 16px;
  text-align: left;
  margin-left: 5px;
  padding-bottom: 4px;
`;
const OrderDetails = (props) => {
  const dispatch = useDispatch();
  const { orderId, id } = props?.route?.params;
  const { setTabVisible } = useTabVisible();
  const { getOneOrderDetail, loading, getOneOrderDetailDispatch, navigation } =
    props;

  const [showCancelModal, setShowCancelModal] = useState(false);

  const onTrackorder = useCallback(() => {
    setTabVisible(true);
    navigation.navigate("deliveryTrackingHome", {
      orderId: orderId,
      id: id,
    });
  }, []);

  useEffect(() => {
    setTabVisible(true);
  }, []);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getOneOrderDetailDispatch(orderId);
      setTabVisible(true);
    });
  }, [orderId]);

  const trackOrderReverse = getOneOrderDetail?.track;

  return (
    <ScrollView style={{ flex: 1, backgroundColor: "#fafcfe" }}>
      {!loading && (
        <Fragment>
          <View style={{ margin: 5 }}>
            <MainContainer>
              <SubContainer>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <OrderIcon>
                    <LeftLogo height={30} width={37} viewBox="1 1 135 135" />
                  </OrderIcon>
                  <OrderTitle>Order Details</OrderTitle>
                </View>
                {getOneOrderDetail?.cancelButtonShow ? (
                  <ButtonPending>
                    {getOneOrderDetail?.orderStatus}
                  </ButtonPending>
                ) : (
                  <ButtonCompleted>
                    {getOneOrderDetail?.orderStatus}
                  </ButtonCompleted>
                )}
              </SubContainer>
              <SubContainer>
                <LabelText>Order ID</LabelText>
                <ValueText>#{getOneOrderDetail?.orderNumber}</ValueText>
              </SubContainer>
              <SubContainer>
                <LabelText>Order Date</LabelText>
                <ValueText>
                  {convertLocalDateFormat(getOneOrderDetail?.createdOn)}
                </ValueText>
              </SubContainer>
              <SubContainer>
                <TotalText>Total Amount</TotalText>
                <TotalAmount>${getOneOrderDetail?.totalAmount}</TotalAmount>
              </SubContainer>
            </MainContainer>
            <MainContainer>
              <SubContainer style={{ alignItems: "flex-start" }}>
                <View>
                  <LabelText style={{ paddingLeft: 4 }}>
                    Delivery Address
                  </LabelText>
                  <AddressTitle>{getOneOrderDetail?.community}</AddressTitle>
                  <AddressText style={{ flexWrap: "wrap-reverse" }}>
                    {getOneOrderDetail?.city}, {getOneOrderDetail?.state}
                  </AddressText>
                </View>
              </SubContainer>
            </MainContainer>
            {getOneOrderDetail?.orderItems?.map((item, idx) => {
              return (
                <MainContainer>
                  <SubContainer>
                    <LabelText>{item?.name}</LabelText>
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <QtyText>QTY:</QtyText>
                      <Qty>{item?.quantity}</Qty>
                    </View>
                  </SubContainer>
                  <Text style={styles.ingTxt}>
                    <Text style={styles.selctionValueTxt}>
                      {item?.ingredientNames
                        ? Object.keys(JSON.parse(item?.ingredientNames)).map(
                            (ingKey, i) =>
                              `${ingKey}: ${
                                JSON.parse(item?.ingredientNames)[ingKey]
                              } `
                          )
                        : ""}
                    </Text>
                  </Text>
                </MainContainer>
              );
            })}

            {trackOrderReverse?.length > 0 ||
            getOneOrderDetail?.cancelButtonShow ? (
              <MainContainer>
                {trackOrderReverse?.length > 0 && (
                  <SubContainer>
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <LeftLogo height={30} width={37} viewBox="1 1 135 135" />
                      <OrderTitle>Order Timeline</OrderTitle>
                    </View>
                  </SubContainer>
                )}
                {trackOrderReverse?.length > 0 &&
                  trackOrderReverse.map((order) => {
                    return (
                      <TimeLineContainer key={order.orderStatusId}>
                        <DeliveryTimeText>
                          {convertTime(order?.time)}
                        </DeliveryTimeText>
                        <CircleCheck
                          width={42}
                          height={42}
                          color={
                            order?.orderStatusId == 5 ? "#f44336" : "#4caf50"
                          }
                          style={styles.circlesSelected}
                          viewBox="-6 -6 36 36"
                        />
                        <DeliveryText>
                          {order?.orderStatus?.toLowerCase() === "completed"
                            ? "Ready for Delivery"
                            : order?.orderStatus}
                        </DeliveryText>
                      </TimeLineContainer>
                    );
                  })}

                <View style={styles.orderActionContainer}>
                  {getOneOrderDetail?.cancelButtonShow ? (
                    <TouchableOpacity
                      style={styles.cancelOrderBtn}
                      onPress={() => setShowCancelModal(true)}
                    >
                      <Text style={styles.cancelOrderBtnText}>
                        Cancel Order
                      </Text>
                    </TouchableOpacity>
                  ) : null}
                  <TouchableOpacity
                    onPress={onTrackorder}
                    style={styles.viewOrderBtn}
                  >
                    <Text style={styles.viewOrderBtnText}>
                      Track Order on Map
                    </Text>
                  </TouchableOpacity>
                </View>
              </MainContainer>
            ) : null}
          </View>
        </Fragment>
      )}
      <CancelOrderModal
        showModal={showCancelModal}
        itemId={id}
        navigation={navigation}
        setVisible={() => {
          setShowCancelModal(false);
        }}
      />
    </ScrollView>
  );
};
export default connect(
  (state) => ({
    getOneOrderDetail: state.myOrders.getOneOrderDetail,
    trackOrder: state.myOrders.trackOrder,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    getOneOrderDetailDispatch: (id) => dispatch(getOneOrderDetail(id)),
    trackOrderDispatch: (id) => dispatch(trackOrder(id)),
  })
)(OrderDetails);

const styles = StyleSheet.create({
  selctionValueTxt: {
    fontSize: 12,
    color: "#607d8b",
  },
  ingTxt: {
    width: "100%",
    paddingTop: 10,
    marginLeft: 15,
  },
  orderActionContainer: {
    flexDirection: "row",
    marginTop: 30,
  },
  viewOrderBtn: {
    backgroundColor: "#673ab7",
    borderRadius: 8,
    paddingVertical: 8,
    alignItems: "center",
    flex: 1,
    marginLeft: 4,
    justifyContent: "center",
  },
  cancelOrderBtn: {
    backgroundColor: "#fff",
    borderRadius: 8,
    paddingVertical: 8,
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#a38862",
    flex: 1,
    marginRight: 4,
    justifyContent: "center",
  },
  viewOrderBtnText: {
    color: "#fff",
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    textAlign: "center",
  },
  cancelOrderBtnText: {
    fontFamily: "Roboto-Medium",
    color: "#263238",
    fontSize: 16,
    textAlign: "center",
  },
});
