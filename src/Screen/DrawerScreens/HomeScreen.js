import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  SafeAreaView,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import CallToStore from "../Components/CallToStore";
import Modal from "../Components/Modal";
import MostPopular from "../Components/MostPopular";
import TodaySpecial from "../Components/TodaySpecial";
import Logo from "../Components/svg/CB_Primary";

const HomeScreen = ({ navigation }) => {
  const { setTabVisible } = useTabVisible();
  const [showModal, setShowModal] = useState(false);
  const modalClick = () => {
    setShowModal(true);
  };

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <CallToStore onPress={modalClick} />,
    });
  }, []);

  return (
    <SafeAreaView style={styles.SafeAreaViewContainer}>
      <ScrollView>
        <View style={styles.scrollContainer}>
          <View style={styles.scrollParent}>
            <View style={styles.container}>
              <View style={styles.backgroundContainer}>
                <Image
                  source={require("../../assets/images/banner.png")}
                  resizeMode="cover"
                  style={styles.backdrop}
                />
              </View>
              <View style={styles.overlay}>
                <View style={styles.logoSVG}>
                  <Logo width={700} height={120} viewBox="0 0 600 300" />
                </View>
                <TouchableOpacity
                  style={styles.buttonStyle}
                  activeOpacity={0.5}
                  onPress={() => {
                    navigation.navigate("DrawerNavigationRoutes", {
                      screen: "homeScreenStack",
                      params: {
                        screen: "PizzaMenu",
                        params: {
                          screen: "pizzaScreenCustomise",
                        },
                      },
                    });
                  }}
                >
                  <Text style={styles.buttonTextStyle}>
                    Build A Custom Pizza
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.subContainer}>
            <TodaySpecial navigation={navigation} />
            <MostPopular navigation={navigation} />
          </View>
          {showModal && (
            <Modal
              showModal={showModal}
              setVisible={() => {
                setShowModal(false);
              }}
            />
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  SafeAreaViewContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    backgroundColor: "#fff",
    paddingTop: 5,
  },
  logoSVG: {
    alignItems: "center",
    backgroundColor: "#fff",
    width: 300,
    height: 140,
    paddingTop: 10,
    borderRadius: 10,
  },
  scrollContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    backgroundColor: "#fff",
  },
  scrollParent: {
    height: 307,
    width: "100%",
    backgroundColor: "#fff",
  },
  subContainer: {
    flexDirection: "column",
    marginTop: 20,
    backgroundColor: "#fff",
  },
  backgroundContainer: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center",
    height: 307,
  },
  container: {
    flex: 1,
    alignItems: "center",
    height: 307,
  },
  overlay: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 50,
    backgroundColor: "rgba(0,0,0,0)",
  },
  logo: {
    backgroundColor: "rgba(0,0,0,0)",
    width: 250,
    height: 121,
  },
  backdrop: {
    flex: 1,
    flexDirection: "column",
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    borderRadius: 20,
    height: 40,
    width: 232,
    alignItems: "center",
    marginLeft: 35,
    marginRight: 35,
    marginTop: 35,
    marginBottom: 25,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
});
