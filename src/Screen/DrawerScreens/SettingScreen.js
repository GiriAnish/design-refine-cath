import React, { useEffect, useState } from "react";
import { SafeAreaView, StyleSheet, ScrollView } from "react-native";
import Modal from "../Components/Modal";
import CallToStore from "../Components/CallToStore";
import SettingsMenu from "../Components/SettingsMenu";
import { useTabVisible } from "../../contexts/navigation";

const data = [
  {
    name: "Account Information",
    items: [
      "My Profile",
      "Reset Password",
      "My Locations",
      //"My Payment methods",
      //"Notifications",
    ],
  },
  {
    name: "Support",
    items: [
      "Change Language",
      "About Application",
      "Terms & Conditions",
      "Privacy Policy",
    ],
  },
];

const SettingsScreen = (props) => {
  const { setTabVisible } = useTabVisible();

  const { navigation } = props;
  const [showModal, setShowModal] = useState(false);
  const modalClick = () => {
    setShowModal(true);
  };
  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <CallToStore onPress={modalClick} />,
    });
  });

  return (
    <SafeAreaView style={styles.safeView}>
      <ScrollView style={styles.scrollView}>
        {data.map((item) => {
          return (
            <SettingsMenu
              {...item}
              key={item.name}
              navigation={props.navigation}
            />
          );
        })}
      </ScrollView>
      {showModal && (
        <Modal
          showModal={showModal}
          setVisible={() => {
            setShowModal(false);
          }}
        />
      )}
    </SafeAreaView>
  );
};

export default SettingsScreen;

const styles = StyleSheet.create({
  safeView: { flex: 1, backgroundColor: "#fafcfe" },
  scrollView: { flex: 1, padding: 10, marginBottom: 50 },
});
