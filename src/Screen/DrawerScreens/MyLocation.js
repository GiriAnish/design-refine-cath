import React, { useEffect, useState, useCallback } from "react";
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  InteractionManager,
  TouchableWithoutFeedback,
} from "react-native";
import { FlatList, TextInput } from "react-native-gesture-handler";
import { useDispatch, useSelector } from "react-redux";
import { AddressContainer } from "./CheckoutScreen";
import { Checkbox } from "react-native-paper";
import Icon from "react-native-vector-icons/Ionicons";
import * as locationActions from "../../store/action/location";
import Logo from "../Components/svg/CB_Secondary.js";

const searchBarStyles = StyleSheet.create({
  root: {
    backgroundColor: "white",
    alignItems: "center",
    borderRadius: 4,
    flexDirection: "row",
    marginHorizontal: 16,
    marginVertical: 16,
    paddingHorizontal: 16,
    shadowColor: "#000000",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    elevation: 2,
  },
  textInput: {
    paddingVertical: 16,
    textAlignVertical: "center",
    flexGrow: 1,
    flexShrink: 1,
    fontSize: 16,
    backgroundColor: "white",
    flex: 1,
  },
  searchIcon: {
    marginRight: 4,
  },
});

const SearchBar = (props) => {
  const [text, setText] = useState("");
  const onChangeText = React.useCallback((txt) => {
    setText(txt);
    if (props.onChangeText) {
      props.onChangeText(txt);
    }
  }, []);
  const styles = searchBarStyles;
  return (
    <View style={[styles.root]}>
      <TextInput
        value={text}
        underlineColorAndroid="transparent"
        style={styles.textInput}
        placeholder="Search"
        placeholderTextColor="#607d8b"
        blurOnSubmit
        onChangeText={onChangeText}
      />
      <View style={styles.searchIcon}>
        <Icon name="search" size={21} color="#455a64" />
      </View>
    </View>
  );
};

const rowItemStyles = StyleSheet.create({
  root: {
    paddingHorizontal: 16,
    paddingVertical: 16,
    backgroundColor: "white",
    borderRadius: 5,
    borderRadius: 5,
    shadowColor: "#000000",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    elevation: 2,
    marginHorizontal: 16,
    justifyContent: "space-between",
  },
  flexrow: {
    flexDirection: "row",
  },
  textinput: {
    color: "#546e7a",
    fontSize: 16,
    paddingTop: 8,
    paddingBottom: 24,
  },
});

export const RowItem = (props) => {
  const styles = rowItemStyles;
  const { onPress, field1, field2, field3, containerStyle, selected, id } =
    props;
  return (
    <TouchableWithoutFeedback onPress={() => onPress(id)}>
      <View style={[styles.root, styles.flexrow, containerStyle]}>
        <AddressContainer field1={field1} field2={field2} field3={field3} />
        <Checkbox.Android
          status={selected ? "checked" : "unchecked"}
          color="#9c27b0"
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

const DeliveryAddress = ({ navigation }) => {
  const cart = useSelector((state) => state.cart.cartItems);
  const login = useSelector((state) => state.loggedInUser.loggedInUser);
  const userLocation = useSelector((state) => state.locations.locations);
  const dispatch = useDispatch();
  const fetchLocations = useCallback(() => {
    try {
      dispatch(locationActions.getLocation(login?.userDetails?.jwtToken));
    } catch (err) {}
  });

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      fetchLocations();
    });
    return unsubscribe;
  }, [navigation]);

  const [searchResult, setSearchResult] = useState([]);
  const [searchtext, setSearchText] = useState("");
  const [selectedItemId, setSelectedId] = useState("");

  const cartTotal = React.useMemo(() => {
    const calculatedTotal = parseFloat(
      cart.reduce((total, item) => total + item.quantity * item.price, 0)
    ).toFixed(2);
    const tax = parseFloat((calculatedTotal * 2) / 100).toFixed(2);
    return parseFloat(parseFloat(calculatedTotal) + parseFloat(tax)).toFixed(2);
  }, [cart]);

  useEffect(() => {
    setSelectedId(
      userLocation?.communities?.filter((item) => item.isDefault)?.[0]
        .communityId
    );
  }, [userLocation]);

  const performSearch = React.useCallback((text) => {
    InteractionManager.runAfterInteractions(() => {
      const searchresult = userLocation?.communities?.filter((item) => {
        const searchVal = text.toLowerCase().trim();

        return (
          item.communityName?.toLowerCase().includes(searchVal) ||
          item.address?.toLowerCase().includes(searchVal) ||
          item.city?.toLowerCase().includes(searchVal)
        );
      });
      setSearchResult(searchresult);
    }, []);
  }, []);

  const onSearchTextChange = React.useCallback((text) => {
    setSearchText(text);
    performSearch(text);
  }, []);

  const onNext = React.useCallback(() => {
    navigation.goBack();
  }, []);

  const onItemSelection = React.useCallback((id) => {
    dispatch(
      locationActions.setDefaultLocally(id, login?.userDetails?.jwtToken)
    );
    setSelectedId(id);
  }, []);

  return (
    <SafeAreaView style={styles.root}>
      <View style={{width: "100%"}}>
        <Logo
          height={130}
          width={"100%"}
          viewBox="0 0 400 200"
          style={{ marginTop: 20, marginBottom: -20 }}
        />
      </View>
      <View style={{marginTop: -30}}>
        <Text style={styles.commText}>Choose your Community</Text>
      </View>
      <FlatList
        style={{ width: "100%" }}
        showsVerticalScrollIndicator={false}
        data={
          searchtext.length ? searchResult : userLocation?.communities ?? []
        }
        keyExtractor={(item, index) => `${item.field1}${index}`}
        renderItem={({ item }) => {
          return (
            <RowItem
              containerStyle={styles.itemStyle}
              field1={item.communityName}
              //field2={`${item.address} ${item.secondaryAddress}`}
              field2={`${item.city}, ${item.state} ${item.zipcode}`}
              selected={item.communityId === selectedItemId}
              onPress={onItemSelection}
              id={item.communityId}
            />
          );
        }}
      />
    </SafeAreaView>
  );
};

export default DeliveryAddress;

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "100%",
    backgroundColor: "#fafcfe",
  },
  commText: {
    width: 300,
    height: 55,
    fontSize: 21,
    color: "#3b018a",
    fontWeight: "400",
    fontFamily: "Arial",
    textAlign: "center",
  },
  itemStyle: {
    marginBottom: 16,
  },
  inputItemStyle: {
    marginBottom: 16,
  },
});
