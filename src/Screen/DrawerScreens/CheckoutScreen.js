import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  Alert,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { connect, useDispatch } from "react-redux";
import CheckoutDetails from "../Components/CheckoutDetails";
import { getLocation } from "../../store/action/location";
import { useTabVisible } from "../../contexts/navigation";
import {
  createNewOrder,
  resetNewOrder,
  orderDateIsValidOrNot,
  RESET_UPDATE_TEMP_ORDER,
} from "../../store/action/myOrdersAction";
import Loader from "../Components/Loader";
import Toast from "react-native-toast-message";
import Chevronright from "../Components/svg/Chevronright";
import CONST from "../../data/const";
import DateTimePicker from "@react-native-community/datetimepicker";
import { convertUTCtoLocal, convertUTCtoAPI } from "../../utils/date";
import Calendar from "../Components/svg/Calendar";
import { PaymentSheetError, useStripe } from "@stripe/stripe-react-native";
import {
  getPaymentIntent,
  confirmOrderPayment,
  RESET_PAYMENT_CONFIRMED,
} from "../../store/action/paymentAction";
import { updateTempOrder } from "../../store/action/myOrdersAction";

const RESET_CLIENT_SECRET = "RESET_CLIENT_SECRET";
const RESET_NEW_ORDER = "RESET_NEW_ORDER";
const RESET_ORDER_DATE_VALID_OR_NOT = "RESET_ORDER_DATE_VALID_OR_NOT";

const rowItemStyles = StyleSheet.create({
  root: {
    paddingVertical: 10,
    backgroundColor: "white",
    justifyContent: "space-between",
    width: "100%",
  },
  flexrow: {
    flexDirection: "row",
    alignItems: "center",
  },
  field1: {
    color: "#37474f",
    fontSize: 16,
    fontWeight: "600",
  },
  field2: {
    color: "#546e7a",
    fontSize: 16,
    paddingTop: 8,
  },
  field3: {
    color: "#546e7a",
    fontSize: 16,
    paddingTop: 8,
  },
  icon: {
    height: 32,
    width: 32,
  },
  leftContainer: {
    justifyContent: "space-between",
    flexGrow: 1,
    flexShrink: 1,
  },
  textinput: {
    color: "#546e7a",
    fontSize: 16,
    paddingTop: 8,
    paddingBottom: 24,
  },
});

export const AddressContainer = (props) => {
  const { field1, field2, field3, field4 } = props;
  const styles = rowItemStyles;
  return (
    <View style={styles.leftContainer}>
      <Text style={styles.field1}>{field1}</Text>
      <Text style={styles.field2}>{field2}</Text>
    </View>
  );
};

export const RowItem = (props) => {
  const styles = rowItemStyles;
  const { onPress, field1, field2, field3, field4, containerStyle } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.root, styles.flexrow, containerStyle]}
    >
      <AddressContainer
        field1={field1}
        field2={field2}
        field3={field3}
        field4={field4}
      />
      <Chevronright height={20} width={37} fill={"#37474f"} />
    </TouchableOpacity>
  );
};

const CheckoutScreen = (props) => {
  const {
    loading,
    cartDetails,
    userLogin,
    userLocation,
    fetchLocation,
    createOrder,
    paymentConfirmed,
    paymentIntentDetails,
    isValidDateSelected,
    fetchPaymentIntent,
    confirmOrderPaymentDetails,
    validateOrderDate,
    tempOrder,
    updateOrder,
    updatedTempOrder,
  } = props;
  const dispatch = useDispatch();
  const { setTabVisible } = useTabVisible();
  const [jwtToken] = useState(userLogin?.userDetails?.jwtToken);
  const [defaultLocation, setDefaultLocation] = useState({});
  const [selectedLocationId, setSelectedLocationId] = useState(0);
  const { initPaymentSheet, presentPaymentSheet } = useStripe();
  const [orderUUIDIs, setOrderUUIDIs] = useState();
  const [mydate, setDate] = useState(new Date());
  const [dummy] = useState();
  const [displaymode, setMode] = useState("date");
  const [isDisplayDate, setIsDisplayDate] = useState(false);

  const changeSelectedDate = (_, selectedDate) => {
    setIsDisplayDate(!isDisplayDate);
    const currentDate = selectedDate || mydate;
    updateOrder(orderUUIDIs, { date: currentDate }, jwtToken);
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    setIsDisplayDate(!isDisplayDate);
    setMode(currentMode);
  };

  const displayDatepicker = () => {
    showMode("date");
  };

  useEffect(() => {
    Toast.hide();
    setTabVisible(false);
    dispatch({ type: RESET_CLIENT_SECRET });
    dispatch({ type: RESET_NEW_ORDER });
    dispatch({ type: RESET_UPDATE_TEMP_ORDER });
    dispatch({ type: RESET_PAYMENT_CONFIRMED });
  }, []);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener("beforeRemove", (e) => {
      e.preventDefault();
      setTabVisible(false);
      props.navigation.dispatch(e.data.action);
    });
    return unsubscribe;
  }, [props.navigation]);

  useEffect(() => {
    if (props.route.params.params.checkOut) {
      fetchLocation(userLogin?.userDetails?.jwtToken);
    }
  }, [props.navigation]);

  useEffect(() => {
    const location = userLocation?.communities?.filter(
      (item) => item.isDefault
    )?.[0];
    const communityId = userLocation?.communities?.filter(
      (item) => item.isDefault
    )?.[0]?.communityId;
    setDefaultLocation(location);
    setSelectedLocationId(communityId);
  }, [userLocation]);

  useEffect(() => {
    if (paymentConfirmed?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: paymentConfirmed?.errorBody,
      });
      return;
    }
    if (paymentConfirmed?.successBody) {
      props.navigation.replace("orderSuccessScreen", {
        orderNumber: paymentConfirmed?.successBody?.orderNumber,
        id: paymentConfirmed?.successBody?.id,
      });
      return;
    }
  }, [paymentConfirmed]);

  useEffect(() => {
    if (tempOrder?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: tempOrder?.errorBody,
      });
      dispatch({ type: RESET_NEW_ORDER });
      return;
    }
    if (tempOrder?.successBody) {
      setOrderUUIDIs(tempOrder?.successBody?.orderUuid);
      fetchPaymentIntent(tempOrder?.successBody?.orderUuid, jwtToken);
      return;
    }
  }, [tempOrder]);

  useEffect(() => {
    if (updatedTempOrder?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: tempOrder?.errorBody,
      });
      dispatch({ type: RESET_UPDATE_TEMP_ORDER });
      return;
    }
    if (updatedTempOrder?.successBody) {
      Toast.show({
        type: "success",
        position: "top",
        bottomOffset: 150,
        autoHide: true,
        text1: "Success",
        text2: "Order updated successfully.",
      });
      dispatch({ type: RESET_UPDATE_TEMP_ORDER });
      return;
    }
  }, [updatedTempOrder]);

  useEffect(() => {
    if (paymentIntentDetails?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: paymentIntentDetails?.errorBody,
      });
      dispatch({ type: RESET_NEW_ORDER });
      return;
    }
  }, [paymentIntentDetails]);

  useEffect(() => {
    async function checkIfValidDateIsSelected() {
      if (isValidDateSelected?.errorBody) {
        Toast.show({
          type: "error",
          position: "top",
          bottomOffset: 210,
          autoHide: true,
          text1: "Error",
          text2: isValidDateSelected?.errorBody,
        });
        dispatch({ type: RESET_ORDER_DATE_VALID_OR_NOT });
        return;
      }

      if (isValidDateSelected?.successBody) {
        dispatch({ type: RESET_ORDER_DATE_VALID_OR_NOT });
        setTimeout(() => openPaymentSheet(), 100);
        return;
      }
    }
    checkIfValidDateIsSelected();
  }, [isValidDateSelected]);

  const validateSelectedOrderDate = () => {
    validateOrderDate(convertUTCtoAPI(mydate), jwtToken);
  };

  const openPaymentSheet = async () => {
    try {
      if (!defaultLocation?.communityName) {
        Alert.alert("Please select the delivery address.");
        return;
      }
      const { error: paymentSheetInitError } = await initPaymentSheet({
        customerId: paymentIntentDetails?.successBody?.stripeUserKey,
        paymentIntentClientSecret:
          paymentIntentDetails?.successBody?.clientSecret,
        ephemeralKey: paymentIntentDetails?.successBody?.ephemeralKey,
        allowsDelayedPaymentMethods: false,
        merchantDisplayName: "Pizza",
      });
      if (paymentSheetInitError) {
        if (paymentSheetInitError.code === PaymentSheetError.Failed) {
          showPaymentTryAgainAlert();
        }
        return;
      }
      const { error } = await presentPaymentSheet();
      if (error) {
        if (error.code === PaymentSheetError.Failed) {
          showPaymentTryAgainAlert();
        }
        return;
      }
      confirmOrderPaymentDetails(orderUUIDIs, jwtToken);
      dispatch({ type: RESET_CLIENT_SECRET });
      dispatch({ type: RESET_NEW_ORDER });
      dispatch({ type: RESET_PAYMENT_CONFIRMED });
      dispatch({ type: RESET_UPDATE_TEMP_ORDER });
    } catch (error) {
      console.log("Something went wrong while opening payment form ", error);
      showPaymentTryAgainAlert();
    }
  };

  const showPaymentTryAgainAlert = () => {
    Alert.alert("Unable to make payment please try again.");
  };

  const onPlaceOrder = () => {
    const { id, communityId } = userLogin?.userDetails;
    const { secondaryAddress, city, state } = defaultLocation;
    const address =
      (secondaryAddress || "") + "," + (city || "") + "," + (state || "");
    let totalPrice = 0;
    const items = [];
    cartDetails.map((itemMain) => {
      const itemId = itemMain?.id;
      const price = itemMain?.price;
      const quantity = itemMain?.quantity;
      totalPrice += price * quantity;
      const ingredients = [];
      const ingredientNames = {};
      itemMain?.ingredients?.forEach((item) => {
        let ingId = item[2];
        if (ingId === CONST.additionalPizzaItems) {
          let names = "";
          item[1].forEach((i) => {
            names = names + i.label + ", ";

            ingredients.push({
              ingredientId: i.id,
              mode: i.mode,
            });
          });

          ingredientNames[item[0]] = names;
        } else {
          ingredientNames[item[0]] = item[1][0];
          ingredients.push({
            ingredientId: ingId,
            mode: 0,
          });
        }
      });

      items.push({
        itemId: itemId,
        ingredients: ingredients,
        quantity: quantity,
        ingredientNames: JSON.stringify(ingredientNames),
      });
    });
    let tax = parseFloat(totalPrice * (8.25 / 100)).toFixed(2);
    const payload = {
      userId: id,
      communityId: communityId,
      address: address || "",
      zipcode: defaultLocation?.zipcode || "",
      orderTypeId: 1,
      isFavourite: true,
      rating: 0,
      date: mydate,
      totalPrice: parseFloat(
        parseFloat(parseFloat(totalPrice) + parseFloat(tax)).toFixed(2)
      ),
      items: items,
    };
    createOrder(payload, jwtToken);
  };

  useEffect(() => {
    onPlaceOrder();
  }, []);

  const onSelectDeliveryAddress = () => {
    props.navigation.navigate("deliveryAddressHome", {
      selectedLocationId: selectedLocationId,
      orderId: orderUUIDIs,
    });
  };

  const addressHandler = (t1, t2, t3) => {
    if (t1 && t2 && t3) {
      return `${t1}, ${t2}, ${t3}`;
    } else if (t1 && t2) {
      return `${t1}, ${t2}`;
    } else if (t1 && t3) {
      return `${t1}, ${t3}`;
    } else if (t2 && t3) {
      return `${t2}, ${t3}`;
    } else if (t1) {
      return `${t1}`;
    } else if (t2) {
      return `${t2}`;
    } else if (t3) {
      return `${t3}`;
    } else {
      return null;
    }
  };
  return (
    <SafeAreaView style={styles.root}>
      {loading}
      <Loader loading={loading} />
      <KeyboardAvoidingView
        enabled={Platform.OS === "ios"}
        behavior="position"
        style={{ flex: 1 }}
        contentContainerStyle={{
          flex: 1,
          flexDirection: "row",
          alignItems: "baseline",
          justifyContent: "center",
        }}
      >
        <ScrollView
          style={styles.scrollView}
          showsVerticalScrollIndicator={false}
        >
          <View style={{ padding: 20 }}>
            <View style={styles.SectionStyleWithoutMargin}>
              <Text style={styles.title1}>
                Delivery Address <Text style={styles.selectAddress}>*</Text>
              </Text>
            </View>
            {defaultLocation?.communityName != "" && (
              <View style={styles.SectionStyleWithoutMargin}>
                <RowItem
                  containerStyle={styles.itemStyle}
                  field1={addressHandler(defaultLocation?.communityName)}
                  field2={addressHandler(
                    defaultLocation?.city,
                    defaultLocation?.state,
                    defaultLocation?.zipcode
                  )}
                  onPress={onSelectDeliveryAddress}
                />
              </View>
            )}
            {defaultLocation?.communityName == "" && (
              <Text style={styles.selectAddress}>
                Please select a delivery address
              </Text>
            )}
            <View style={styles.SectionStyle}>
              <Text style={styles.title1}>Delivery Date</Text>
              <View style={styles.deliveryStyle}>
                <View style={styles.deliveryBox}>
                  <Text
                    style={{
                      color: "#37474f",
                      fontSize: 16,
                      fontWeight: "400",
                    }}
                  >
                    {convertUTCtoLocal(mydate)}
                  </Text>
                </View>
                <TouchableOpacity
                  disabled={Platform.OS === "ios"}
                  style={styles.datepicker}
                  activeOpacity={0.5}
                  onPress={displayDatepicker}
                >
                  {Platform.OS === "ios" ? (
                    <View style={styles.hiddenDatePicker}>
                      <DateTimePicker
                        testID="dateTimePicker"
                        value={mydate}
                        mode={displaymode}
                        is24Hour={true}
                        display="default"
                        onChange={changeSelectedDate}
                        minimumDate={new Date()}
                      />
                    </View>
                  ) : null}
                  <Calendar
                    height={25}
                    width={25}
                    fill="#263238"
                    style={{ marginLeft: 4, marginTop: 5 }}
                  />
                </TouchableOpacity>
              </View>
              {isDisplayDate && Platform.OS !== "ios" ? (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={mydate}
                  mode={displaymode}
                  is24Hour={true}
                  display="default"
                  onChange={changeSelectedDate}
                  minimumDate={new Date()}
                />
              ) : null}
            </View>
            <CheckoutDetails
              containerStyle={styles.checkoutDetailsContainer}
              buttonTxt="Make Payment"
              cart={cartDetails}
              onPress={validateSelectedOrderDate}
            />
            <Text style={styles.finalSales}>
              All sales are final! No refunds given. {dummy}
            </Text>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default connect(
  (state) => ({
    isValidDateSelected: state?.myOrders?.orderDateValidation,
    tempOrder: state?.myOrders?.newOrder,
    updatedTempOrder: state?.myOrders?.updatedOrder,
    paymentIntentDetails: state?.payment.paymentIntentDetails,
    userLogin: state.loggedInUser.loggedInUser,
    loading: state.loading.loading,
    userLocation: state.locations.locations,
    cartDetails: state.cart.cartItems,
    paymentConfirmed: state.payment.paymentConfirmed,
  }),
  (dispatch) => ({
    fetchLocation: (jwtToken) => dispatch(getLocation(jwtToken)),
    createOrder: (payload, jwtToken) =>
      dispatch(createNewOrder(payload, jwtToken)),
    resetOrder: () => dispatch(resetNewOrder()),
    fetchPaymentIntent: (orderUUID, jwtToken) =>
      dispatch(getPaymentIntent(orderUUID, jwtToken)),
    confirmOrderPaymentDetails: (orderUUID, jwtToken) =>
      dispatch(confirmOrderPayment(orderUUID, jwtToken)),
    validateOrderDate: (deliveryDate, jwtToken) =>
      dispatch(orderDateIsValidOrNot(deliveryDate, jwtToken)),
    updateOrder: (orderId, updatedOrder, jwtToken) =>
      dispatch(updateTempOrder(orderId, updatedOrder, jwtToken)),
  })
)(CheckoutScreen);

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "white",
  },
  hiddenDatePicker: {
    backgroundColor: "green",
    opacity: 1,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0,
  },
  contentContainer: {
    flexDirection: "row-reverse",
    position: "absolute",
    marginTop: 150,
    marginLeft: 350,
    backgroundColor: "#fff",
    borderRadius: 50,
    width: 35,
    justifyContent: "space-between",
  },
  iconContentStyle: {
    backgroundColor: "#b0bec5",
  },
  iconStyle: {},
  datepicker: {
    borderRadius: 5,
    borderBottomLeftRadius: 0,
    borderTopLeftRadius: 0,
    padding: 8,
    borderColor: "#b0bec5",
    borderStyle: "solid",
    borderWidth: 2,
    width: 50,
    height: 45,
    position: "absolute",
    right: 0,
    top: 0,
    backgroundColor: "#eeeeee",
  },
  inputStyle: {
    color: "black",
    fontSize: 14,
    fontWeight: "normal",
    backgroundColor: "#fff",
    height: 35,
    width: 336,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#ede7f6",
    borderRadius: 5,
  },
  SectionStyleWithoutMargin: {
    flexDirection: "row",
    width: "100%",
  },
  SectionStyle: {
    flexDirection: "row",
    width: "100%",
    marginVertical: 10,
  },
  checkoutImage: {
    alignSelf: "center",
    height: 200,
  },
  scrollView: {
    flex: 1,
  },
  itemStyle: {
    marginBottom: 10,
  },
  inputItemStyle: {
    marginBottom: 16,
  },
  checkoutDetailsContainer: {
    padding: 15,
    paddingLeft: 30,
    borderWidth: 0.5,
    borderColor: "#fff",
    backgroundColor: "#fff",
    elevation: 0,
  },
  primaryText: {
    fontSize: 14,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },

  deliveryStyle: {
    alignContent: "center",
    display: "flex",
    flexWrap: "nowrap",
    marginTop: 35,
    flexDirection: "row",
    marginLeft: -95,
    width: "100%",
    height: 50,
    borderRadius: 5,
    borderColor: "#b0bec5",
  },
  title1: {
    fontSize: 16,
    color: "#37474f",
    textAlign: "center",
    fontFamily: "Roboto-Medium",
  },
  selectAddress: {
    color: "red",
    fontSize: 18,
    fontWeight: "normal",
    flexDirection: "row",
    alignSelf: "flex-start",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  finalSales: {
    color: "red",
    fontSize: 18,
    fontWeight: "normal",
    flexDirection: "row",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  deliveryBox: {
    width: "100%",
    height: 45,
    borderRadius: 5,
    borderColor: "#b0bec5",
    borderWidth: 2,
    padding: 10,
  },
});
