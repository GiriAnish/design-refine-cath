import React, { useEffect, useState } from "react";
import { StyleSheet, View, Image, Text, TextInput } from "react-native";
import MiView from "../Components/svg/MiViews.js";
import CBPlumbingLogo from "../Components/svg/CBPlumbingLogo";
import CBLogoPurpleNoFill from "../Components/svg/CBLogoPurpleNoFill";
import DeviceInfo from "react-native-device-info";
import { convertLocalDateFormat } from "../../utils/date.js";

const MyProfile = (props) => {
  const [applicationInformation, setApplicationInformation] = useState({
    developer: "MiViewIS",
    buildNumber: null,
    releaseVersion: null,
    releaseDate: null,
  });
  const Title = ({ label }) => {
    return <Text style={styles.title}>{label}</Text>;
  };
  const SubTitle = ({ label }) => {
    return (
      <TextInput editable={false} style={styles.SubTitle}>
        {label}
      </TextInput>
    );
  };

  useEffect(() => {
    async function prepareApplicationInformation() {
      const releaseTime = await DeviceInfo.getLastUpdateTime();
      setApplicationInformation({
        developer: "MiViewIS",
        buildNumber: DeviceInfo.getBuildNumber(),
        releaseVersion: DeviceInfo.getVersion(),
        releaseDate: convertLocalDateFormat(releaseTime),
      });
    }
    prepareApplicationInformation();
  }, []);
  return (
    <View style={styles.scrollView}>
      <View style={styles.topC}>
        <View style={styles.headerCont}>
          <View style={styles.subContainer}>
            <Title label="Application Developer" />
            <MiView height={55} width={100} style={{ marginBottom: -20 }} />
          </View>
          <View style={styles.subContainer}>
            <Title label="Build Number" />
            <SubTitle label={applicationInformation?.buildNumber} />
          </View>
          <View style={styles.subContainer}>
            <Title label="Release Version" />
            <SubTitle
              label={applicationInformation?.releaseVersion}
              isEditable={true}
            />
          </View>
          <View style={styles.subContainer}>
            <Title label="Release Date" />
            <SubTitle label={applicationInformation?.releaseDate} />
          </View>
        </View>
      </View>

      <View style={styles.topContainer2}>
        <View style={styles.absCont}>
          <CBLogoPurpleNoFill height={160} width={200} viewBox="0 0 90 90" />
        </View>
        <View style={styles.goldlogo}>
          <CBPlumbingLogo height={270} width={320} />
        </View>
      </View>
    </View>
  );
};

export default MyProfile;

const styles = StyleSheet.create({
  topContainer2: {
    marginTop: 80,
    height: 150,
    width: "100%",
  },
  absCont: {
    height: 185,
    position: "relative",
    bottom: 100,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    opacity: 0.15,
  },
  goldlogo: {
    bottom: 270,
    alignSelf: "center",
  },
  title: { fontWeight: "600", fontSize: 16, color: "#37474f", marginBottom: 5 },
  SubTitle: { fontSize: 16, color: "#546e7a", paddingVertical: 5 },
  scrollView: { backgroundColor: "#fafcfe", width: "100%", height: "100%" },
  topContainer: {
    margin: 18,
    marginHorizontal: 15,
    borderWidth: 0.1,
    borderTopWidth: 0,
    borderColor: "#fff",
    backgroundColor: "#fff",
    borderRadius: 4,
    shadowOpacity: 0.5,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 2,
  },
  topC: { paddingHorizontal: 20, marginBottom: 20 },
  headerCont: { paddingHorizontal: 10, paddingTop: 30 },
  subContainer: { marginBottom: 5 },
  logoContainer: {
    alignContent: "center",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    height: 250,
    marginBottom: 70,
  },
  logo: {
    width: "90%",
    marginTop: 0,
    height: undefined,
    aspectRatio: 1,
  },
});
