import React, { useState, useEffect } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Text,
  TouchableWithoutFeedback,
  ImageBackground,
  KeyboardAvoidingView,
} from "react-native";
import {
  checkVIPCode,
  resetVIPCode,
  RESET_NEW_ORDER,
  RESET_UPDATE_TEMP_ORDER,
} from "../../store/action/myOrdersAction";
import { resetError } from "../../store/action/error";
import Toast from "react-native-toast-message";
import Loader from "../Components/Loader";
import { useTabVisible } from "../../contexts/navigation";
import CBPlumbingLogo from "../Components/svg/CBPlumbingLogo";
import CONST from "../../data/const";
import ConfirmationCodeField from "../Components/ConfirmationCodeField";
import {
  RESET_CLIENT_SECRET,
  RESET_PAYMENT_CONFIRMED,
} from "../../store/action/paymentAction";

const CheckoutVIPScreen = (props) => {
  const [mailLoading, setLoading] = useState(false);
  const loggedInUser = useSelector((state) => state.loggedInUser.loggedInUser);
  const { setTabVisible } = useTabVisible();
  const dispatch = useDispatch();
  const { checkCode, userLogin, vipCode, resetCode } = props;
  const logoUrl = loggedInUser?.userDetails?.accountLogoURL || "";
  const logoUrlLength = logoUrl.length;

  const TextComp = (props) => {
    return (
      <TouchableWithoutFeedback onPress={props.click ? props.click : null}>
        <View style={props.view}>
          <Text style={props.text}>{props.label}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };
  const resendEmail = () => {
    setLoading(true);
    fetch(
      CONST.baseUrl +
        `/api/v1/user/resendcurrentcode/${loggedInUser?.userDetails?.id}`
    )
      .then((response) => response.json())
      .then((json) => {
        setLoading(false);

        if (json) {
          Toast.show({
            type: "success",
            position: "top",
            bottomOffset: 150,
            autoHide: true,
            text1: "Success",
            text2: "A new VIP number has been sent to your email",
          });
        }
      })
      .catch((error) => {
        setLoading(false);
      });
  };

  useEffect(() => {
    setTabVisible(false);
  }, [setTabVisible]);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener("beforeRemove", (e) => {
      e.preventDefault();
      setTabVisible(true);
      props.navigation.dispatch(e.data.action);
    });

    return unsubscribe;
  }, [props.navigation, setTabVisible]);
  useEffect(() => {
    if (vipCode?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: vipCode?.errorBody,
      });
    } else if (vipCode?.successBody) {
      dispatch({ type: RESET_CLIENT_SECRET });
      dispatch({ type: RESET_NEW_ORDER });
      dispatch({ type: RESET_UPDATE_TEMP_ORDER });
      dispatch({ type: RESET_PAYMENT_CONFIRMED });
      props.navigation.navigate("checkoutScreenHome", {
        params: {
          checkOut: true,
        },
      });
      resetCode();
    }
  }, [vipCode]);
  const getValue = React.useCallback((code) => {
    if (code) {
      checkCode(userLogin?.userDetails?.id, code);
    }
  }, []);

  return (
    <ScrollView style={styles.scrollView} keyboardShouldPersistTaps="handled">
      <Loader loading={mailLoading} />
      <KeyboardAvoidingView enabled>
        <View style={styles.topContainer}>
          <View style={styles.logo}>
            <ImageBackground
              resizeMode={"contain"}
              source={require("../../assets/images/Registration/builderBanner.png")}
              style={styles.bannerCont}
            >
              <View style={styles.titleCont1}>
                <Text style={styles.title1}>
                  {"Thank you for being a VIP Cathedral Customer"}
                </Text>
              </View>
              <View style={styles.absCont}>
                {logoUrlLength > 0 && (
                  <View style={styles.accountImageCen}>
                    <Image
                      source={{ uri: logoUrl }}
                      style={styles.topLogo}
                      resizeMode={"stretch"}
                    />
                  </View>
                )}
                {logoUrlLength == 0 && (
                  <View style={styles.textCont}>
                    <Text style={styles.accountText}>
                      {loggedInUser?.userDetails?.accountName || "MiViewIs"}
                    </Text>
                  </View>
                )}
              </View>
            </ImageBackground>
          </View>
          <View style={styles.bottomContainer}>
            <Text style={styles.bottomHeader}>Enter Weekly Code</Text>
            <View style={styles.bottomCode}>
              <ConfirmationCodeField getValue={getValue} />
            </View>
            <TextComp
              view={styles.info}
              text={styles.infoText}
              label={
                'The Weekly Code is sent to your work email address at the start of each week. Check your Junk or Spam folder if you cannot find the email or click the "Resend Code to Email" option to have the email sent again.'
              }
            />
            <TextComp
              view={styles.info}
              text={styles.infoEmail}
              label={loggedInUser?.userDetails?.email.toLowerCase()}
            />
            <TextComp
              view={styles.info}
              text={styles.resendLink}
              label={"Resend Code to Email"}
              click={resendEmail}
            />
            <View style={styles.logoCont}>
              <CBPlumbingLogo width={500} height={150} />
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default connect(
  (state) => ({
    vipCode: state.myOrders.vipCode,
    userLogin: state.loggedInUser.loggedInUser,
    loading: state.loading.loading,
    error: state.error.err,
  }),
  (dispatch, ownProps) => ({
    checkCode: (id, code) => dispatch(checkVIPCode(id, code)),
    resetCode: () => dispatch(resetVIPCode()),
    resetErr: () => dispatch(resetError()),
  })
)(CheckoutVIPScreen);

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#fff",
  },
  topContainer: {
    marginTop: -30,
    marginBottom: 20,
  },
  logo: {
    width: "100%",
    height: "auto",
  },
  topLogo: {
    width: 200,
    height: 40,
  },
  bannerCont: {
    width: "100%",
    height: 200,
    alignItems: "center",
  },
  titleCont1: {
    justifyContent: "center",
    alignContent: "center",
    width: "80%",
    flex: 1,
    flexDirection: "row",
    marginTop: 70,
  },
  title1: {
    fontSize: 23,
    color: "#fff",
    fontWeight: "600",
    textAlign: "center",
  },
  absCont: {
    height: 70,
    width: "80%",
    backgroundColor: "#fff",
    borderRadius: 10,
    marginTop: 20,
    position: "absolute",
    top: 130,
    justifyContent: "center",
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 2,
  },
  accountImageCen: {
    alignSelf: "center",
    width: "60%",
    alignItems: "center",
  },
  textCont: {
    alignSelf: "center",
    width: "60%",
  },
  accountText: {
    fontSize: 23,
    color: "#263238",
    alignSelf: "center",
  },

  logoCont: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: -60,
    marginRight: 10,
  },
  bottomContainer: {
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  bottomHeader: {
    color: "#a38862",
    fontSize: 24,
    marginTop: 40,
    marginBottom: 10,
  },
  bottomCode: { marginVertical: 0 },
  info: { width: "90%", marginVertical: 13 },
  infoText: { color: "#455a64", fontSize: 19, textAlign: "center" },
  infoEmail: {
    color: "#263238",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "600",
  },
  resendLink: {
    color: "#2196f3",
    fontSize: 18,
    textAlign: "center",
  },
});
