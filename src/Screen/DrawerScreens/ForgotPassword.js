import React, { useState, useEffect, createRef } from "react";
import { connect, useDispatch } from "react-redux";
import {
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
} from "react-native";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import { pwdResetReq } from "../../store/action/forgotpwdAction";
import Toast from "react-native-toast-message";
import MailEnvelope from "../Components/svg/MailEnvelop";
import GoldLogo from "../Components/svg/CBLogoGoldNoFill";
import { RESET_FORGET_ALL_PASSWORD } from "../../store/action/forgotpwdAction";

const ForgotPassword = (props) => {
  const { finalLocalResp, loading, resetMyPwdFunc } = props;
  const dispatch = useDispatch();
  const [workEmailId, setWorkEmailId] = useState("");
  const [errortext, setErrortext] = useState("");

  const emailInputRef = createRef();
  const handleSubmitPress = () => {
    setErrortext("");
    if (!workEmailId) {
      setErrortext("Please enter a valid work email address");
      return;
    } else {
      resetMyPwdFunc(workEmailId);
    }
  };
  Toast.hide();

  useEffect(() => {
    if (finalLocalResp?.successBody?.message) {
      props.navigation.navigate("ForgotPasswordEmailSend", {
        emailId: workEmailId,
      });
      dispatch({ type: RESET_FORGET_ALL_PASSWORD });
    } else if (finalLocalResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: finalLocalResp?.errorBody,
      });
    }
    dispatch({ type: RESET_FORGET_ALL_PASSWORD });
  }, [finalLocalResp]);

  return (
    <SafeAreaView style={styles.root}>
      <View style={styles.topContainer}>
        <BannerAvatar touchableOpacity />
        <View style={styles.absCont}>
          <MailEnvelope height={250} width={107} viewBox="-4 24 430 240" />
        </View>
        <View style={styles.goldlogo}>
          <GoldLogo height={50} width={50} viewBox="10 0 100 126" />
        </View>
      </View>
      <View style={styles.bodyLayout}>
        <Text numberOfLines={3} style={styles.primaryText}>
          Enter the work email associated with your account. We will send you
          instructions and a link to help you reset your password.
        </Text>
        <View style={styles.inputContainer}>
          <Text style={styles.inputLabel}>Work Email Address*</Text>
          <TextInput
            style={styles.inputStyle}
            onChangeText={(workEmailId) => setWorkEmailId(workEmailId)}
            underlineColorAndroid="#f000"
            placeholder=""
            placeholderTextColor="#8b9cb5"
            keyboardType="email-address"
            ref={emailInputRef}
            returnKeyType="next"
            blurOnSubmit={false}
          />
        </View>
        {errortext != "" ? (
          <Text style={styles.errorTextStyle}> {errortext} </Text>
        ) : null}
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={handleSubmitPress}
          >
            <Text style={styles.buttonTextStyle}>
              Send Reset Password Instructions
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.logoContainer}>
        <FooterBanner
          touchableOpacity
          imageSource={require("../../assets/images/bottom_banner.png")}
        />
      </View>
    </SafeAreaView>
  );
};

export default connect(
  (state) => ({
    finalLocalResp: state.forgotPwd.passwordResetReq,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    resetMyPwdFunc: (workEmailId) => dispatch(pwdResetReq(workEmailId)),
  })
)(ForgotPassword);

const styles = StyleSheet.create({
  root: {
    backgroundColor: "#fff",
    alignItems: "center",
    flex: 1,
    width: "100%",
  },
  topContainer: {
    height: 10,
    width: "100%",
  },
  absCont: {
    height: 185,
    position: "relative",
    bottom: 100,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  goldlogo: {
    bottom: 225,
    backgroundColor: "white",
    alignSelf: "center",
    borderRadius: 50 / 2,
  },

  bodyLayout: {
    color: "#546e7a",
    fontFamily: "Roboto",
    width: "100%",
    justifyContent: "center",
    padding: 20,
  },
  inputContainer: {
    alignItems: "flex-start",
    marginTop: 30,
  },
  inputLabel: {
    marginBottom: 10,
    fontSize: 18,
    fontWeight: "400",
    color: "#a38862",
  },
  inputStyle: {
    backgroundColor: "#ede7f6",
    height: 60,
    width: "100%",
    justifyContent: "center",
    borderWidth: 0,
    borderRadius: 4,
    padding: 8,
    elevation: 2,
    color: "#000000",
    fontSize: 20,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "left",
    fontSize: 18,
    marginTop: 10,
    fontWeight: "normal",
  },
  buttonContainer: {
    marginTop: 30,
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    height: 60,
    alignItems: "center",
    width: "100%",
    borderRadius: 5,
    justifyContent: "center",
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
    fontWeight: "500",
    textAlign: "center",
  },
  logoContainer: {
    alignItems: "center",
    marginTop: -25,
  },
  primaryText: {
    fontSize: 16,
    fontWeight: "400",
    color: "#546e7a",
    marginTop: 130,
  },
});
