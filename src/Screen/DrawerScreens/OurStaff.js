import React, { useEffect, useState } from "react";
import { View, Text, SafeAreaView, FlatList, StyleSheet } from "react-native";
import Banner from "../Components/Banner";
import Modal from "../Components/Modal";
import CallToStore from "../Components/CallToStore";
import ChefsList from "../Components/ChefsList";
import { fetchAllStaffs } from "../../apiServices/apiService";
import Loader from "../Components/Loader";
import { ScrollView } from "react-native-gesture-handler";
const OurStaffScreen = (props) => {
  const { navigation } = props;
  const [showModal, setShowModal] = useState(false);
  const [{ data, loading }] = fetchAllStaffs();
  const modalClick = () => {
    setShowModal(true);
  };
  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <CallToStore onPress={modalClick} />,
    });
  });
  return (
    <SafeAreaView style={styles.SafeAreaViewContainer}>
      <ScrollView style={styles.ScrollView}>
        <Loader loading={loading} />
        <View style={styles.banner}>
          <Banner
            imageSource={require("../../assets/images/Our_Staff_Banner.png")}
          />
          <Text style={styles.meet}>MEET THE TEAM</Text>
          <Text style={styles.world}>WORLD CLASS STAFF</Text>
        </View>
        {!loading && (
          <FlatList
            data={data?.staffs}
            keyExtractor={({ id }, index) => index}
            renderItem={({ item }) => (
              <View>
                <Text style={styles.title}>{item.title}</Text>

                <FlatList
                  data={item?.objects}
                  keyExtractor={({ id }, index) => id}
                  renderItem={(chefs) => (
                    <ChefsList
                      props={{ ...chefs.item, navigation: navigation }}
                    />
                  )}
                />
              </View>
            )}
          />
        )}
        {showModal && (
          <Modal
            showModal={showModal}
            setVisible={() => {
              setShowModal(false);
            }}
          />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default OurStaffScreen;
const styles = StyleSheet.create({
  SafeAreaViewContainer: {
    flex: 1,
    backgroundColor: "#fefefe",
  },
  ScrollView: {
    backgroundColor: "#fafcfe",
  },
  banner: {
    marginBottom: 40,
  },
  meet: {
    marginLeft: 65,
    marginTop: -95,
    color: "#fff",
    fontWeight: "400",
    fontSize: 12,
  },
  world: {
    color: "#fff",
    marginLeft: 65,
    fontWeight: "700",
    fontSize: 29,
    bottom: 5,
  },
  title: {
    width: "89%",
    height: 25,
    color: "#455a64",
    fontFamily: "Roboto-Medium",
    fontSize: 18,
    textAlign: "left",
    marginBottom: 5,
    marginTop: 10,
    marginLeft: 23,
  },

  activityIndicator: {
    alignItems: "center",
    top: "25%",
    height: 80,
  },
});
