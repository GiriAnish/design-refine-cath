import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import CONST from "../../data/const";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import { useTabVisible } from "../../contexts/navigation";
import { useDispatch, useSelector } from "react-redux";
import Lock from "../Components/svg/lock";
import CheckCircle from "../Components/svg/CheckCircle";
import {
  PASSWORD_RESET_REQUEST_DEFAULT,
  SET_USER_NEWPASSWORD_TO_DEFAULT,
  VALIDATE_OTP_RESET,
} from "../../store/action/forgotpwdAction";
import * as CartActions from "../../store/action/cart";
import * as loadingAction from "../../store/action/loading";
import * as loginActions from "../../store/action/loginActions";
import AsyncStorage from "@react-native-community/async-storage";

const SettingsResetPasswordSuccess = (props) => {
  const { setTabVisible } = useTabVisible();
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const response = useSelector(
    (state) => state.forgotPwd.passwordResetResponse
  );

  const handleLoginNavigation = async () => {
    const app = await AsyncStorage.getItem("appVersion");
    AsyncStorage.clear();
    await AsyncStorage.setItem("appVersion", app);
    dispatch(CartActions.resetCart());
    dispatch(loadingAction.setLoading(false));
    dispatch(loginActions.resetUserLogin());
    props.navigation.toggleDrawer();
    props.navigation.replace("Auth");
  };

  useEffect(() => {
    dispatch({ type: PASSWORD_RESET_REQUEST_DEFAULT });
    dispatch({ type: VALIDATE_OTP_RESET });
    dispatch({ type: SET_USER_NEWPASSWORD_TO_DEFAULT });
  }, []);

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      () => true
    );
    return () => {
      backHandler.remove();
    };
  }, []);

  useEffect(() => {
    setTabVisible(false);
    fetch(CONST.baseUrl + "api/v1/accounts")
      .then((response) => response.json())
      .then((json) => {
        setData(json.accounts);
      })
      .catch((error) => {
        setData([]);
      });
  }, [props]);

  return (
    <View style={styles.root}>
      <ScrollView keyboardShouldPersistTaps="handled">
        <BannerAvatar touchableOpacity />
        <View style={styles.lockLayout}>
          <Lock height={100} width={110} viewBox="0 18 250 200" />
        </View>
        <View style={styles.check}>
          <CheckCircle height={40} width={40} />
        </View>
        <View style={styles.bodyLayout}>
          <View>
            <Text style={styles.successText}>Congratulations!</Text>
            <Text style={styles.primaryText}>
              Your password was reset successfully!
            </Text>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={handleLoginNavigation}
            >
              <Text style={styles.buttonTextStyle}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.logoContainer}>
          <FooterBanner
            touchableOpacity
            imageSource={require("../../assets/images/bottom_banner.png")}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default SettingsResetPasswordSuccess;

const styles = StyleSheet.create({
  root: {
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  bodyLayout: {
    width: "100%",
    backgroundColor: "white",
    color: "#546e7a",
    fontFamily: "Roboto",
    marginTop: -50,
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  lockLayout: {
    bottom: 60,
    left: 5,
    alignSelf: "center",
  },
  check: {
    bottom: 115,
    left: 2,
    backgroundColor: "white",
    alignItems: "center",
    alignSelf: "center",
    width: 30,
    height: 30,
    justifyContent: "center",
    borderRadius: 40 / 2,
  },
  successText: {
    width: "100%",
    fontSize: 25,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#4caf50",
    fontFamily: "Roboto",
    marginLeft: 65,
    fontWeight: "bold",
  },
  logoContainer: {
    alignItems: "center",
    flexDirection: "column",
    marginTop: 120,
  },
  primaryText: {
    fontSize: 18,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
    marginTop: "5%",
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    height: 45,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "10%",
    borderRadius: 5,
  },
  buttonTextStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 14,
    fontWeight: "bold",
  },
});
