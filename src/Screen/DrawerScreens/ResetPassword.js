import React, { useState, useRef } from "react";
import { StyleSheet, View, ScrollView, Text, Image } from "react-native";
import { TextInput as NewTextInput } from "react-native-paper";

const ResetPassword = (props) => {
  const [form, setForm] = useState({});
  const newpasswordRef = useRef();
  const confirmpasswordRef = useRef();
  const [showPassword, setShowPassword] = useState({
    oldPassword: true,
    newPassword: true,
    confirmPassword: true,
  });

  const EyeComp = (id) => {
    return (
      <NewTextInput.Icon
        name="eye"
        color="#78909c"
        onPress={() => {
          setShowPassword((prevState) => ({
            ...prevState,
            [id]: !prevState[id],
          }));
        }}
      />
    );
  };

  const TextInputComp = React.useCallback(
    ({ right, placeholderLabel, id, label }) => {
      return (
        <NewTextInput
          style={styles.inputStyle}
          returnKeyType={id != "confirmPassword" ? "next" : "default"}
          onSubmitEditing={() => {
            if (id == "oldPassword") {
              newpasswordRef.current.focus();
            }
            if (id == "newPassword") {
              confirmpasswordRef.current.focus();
            }
          }}
          ref={
            id == "newPassword"
              ? newpasswordRef
              : id == "confirmPassword"
              ? confirmpasswordRef
              : null
          }
          placeholder={placeholderLabel}
          secureTextEntry={showPassword[id]}
          placeholderTextColor="#546e7a"
          right={right ? EyeComp(id) : null}
          onChangeText={(text) => {
            setForm((prevState) => ({ ...prevState, [id]: text }));
          }}
          value={label}
          underlineColor="transparent"
        />
      );
    },
    [showPassword, props]
  );

  const PasswordRules = ({ label }) => {
    return (
      <View style={styles.ruleContainer}>
        <Image
          style={styles.image}
          source={require("../../assets/images/settings/Rectangle_269.png")}
        />
        <Text style={{ ...styles.textColor, ...styles.defaultFont }}>
          {label}
        </Text>
      </View>
    );
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.modalContainer}>
        <Text style={{ ...styles.textColor, ...styles.title }}>
          Reset Your Password
        </Text>
        <Text
          style={{
            ...styles.textColor,
            ...styles.defaultFont,
            ...styles.subTitle,
          }}
        >
          Password Requirements
        </Text>
        <View style={styles.contPadd}>
          <PasswordRules label="Must be at least 8 characters" />
          <PasswordRules label="Must have one upper case & Lower case letter" />
          <PasswordRules label="Must have one number" />
          <PasswordRules label="Must have one special character (!@#$%&?)" />
        </View>
      </View>
      <View style={{ alignItems: "center" }}>
        <TextInputComp
          placeholderLabel={"Enter Current Password"}
          right={true}
          id="oldPassword"
          label={form["oldPassword"]}
        />
        <TextInputComp
          placeholderLabel={"Enter New Password"}
          right={true}
          id="newPassword"
          label={form["newPassword"]}
        />
        <TextInputComp
          placeholderLabel={"Retype New Password"}
          right={true}
          id="confirmPassword"
          label={form["confirmPassword"]}
        />
      </View>
      <View style={{ alignItems: "center" }}>
        <View style={styles.checkoutContainer}>
          <Text style={styles.checkoutText}>Reset Password</Text>
        </View>
      </View>
    </ScrollView>
  );
};

export default ResetPassword;

const styles = StyleSheet.create({
  container: { backgroundColor: "#fff" },
  modalContainer: {
    flex: 1,
    width: "100%",
    paddingHorizontal: 20,
    backgroundColor: "#fff",
  },
  textColor: { color: "#546e7a" },
  defaultFont: { fontSize: 16 },
  ruleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 5,
  },
  image: { marginRight: 20 },
  contPadd: { padding: 20 },
  title: { fontSize: 27, fontWeight: "600", marginBottom: 20 },
  subTitle: { fontWeight: "600", marginBottom: 10 },
  inputStyle: {
    flex: 1,
    color: "white",
    height: 61,
    marginBottom: 20,
    width: "90%",
    paddingTop: 0,
    borderWidth: 0.5,
    borderColor: "#fff",
    backgroundColor: "#fff",
    borderRadius: 5,
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 1,
  },
  checkoutContainer: {
    width: 350,
    height: 45,
    backgroundColor: "#673ab7",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  checkoutText: { fontSize: 16, color: "#fff", fontWeight: "600" },
});
