import React, { useState, useEffect, useCallback, useRef } from "react";
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Button,
} from "react-native";
import MapView, { Marker, Polyline } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import Icon from "react-native-vector-icons/Ionicons";
import CommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import EntyPoIcon from "react-native-vector-icons/Entypo";
import CancelOrderModal from "../Components/CancelOrder";
import Phone from "../Components/svg/Phone.js";
import { useTabVisible } from "../../contexts/navigation";
import {
  getOneOrderDetail,
  trackOrder,
} from "../../store/action/myOrdersAction";
import { connect } from "react-redux";
import DeviceInfo from "react-native-device-info";
import Loader from "../Components/Loader";
import parsePhoneNumber from "libphonenumber-js";
import { useIsFocused } from "@react-navigation/native";

const DeliveryTrackingScreen = (props) => {
  const { orderId, id } = props?.route?.params;
  const { getOneOrderDetailDispatch, getOneOrderDetail, loading } = props;
  const mapRef = useRef(null);
  const { navigation } = props;
  const { setTabVisible } = useTabVisible();
  const [showCancelModal, setShowCancelModal] = useState(false);
  const [deliveryRoutePoints, setDeliveryRoutePoints] = useState([
    {
      latitude: 0,
      longitude: 0,
    },
    {
      latitude: 0,
      longitude: 0,
    },
  ]);
  const [driverPhoneNumber, setDriverPhoneNumber] = useState();
  const isFocused = useIsFocused();

  const [deliveryLeftTime, setDeliveryLeftTime] = useState(
    deliveryRoutePoints.length * 2
  );
  const [driverCurrentLocation, setDriverCurrentLoc] = React.useState(null);

  const [deliveryLocation, setDeliveryLocation] = useState({
    ...deliveryRoutePoints[0],
    latitudeDelta: 0.1,
    longitudeDelta: 0.1,
  });

  const [shopLocation, setShopLocation] = useState({
    ...deliveryRoutePoints[deliveryRoutePoints.length - 1],
    latitudeDelta: 0.1,
    longitudeDelta: 0.1,
  });
  const viewOrderDetails = useCallback(() => {
    navigation.push("OrderDetails", {
      orderId: orderId,
      id: id,
    });
  }, []);

  const [driverSnappedPoints, setDriverSnappedPoints] = React.useState([]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("beforeRemove", (e) => {
      e.preventDefault();
      setTabVisible(false);
      navigation.dispatch(e.data.action);
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    setTabVisible(true);
  }, []);

  useEffect(() => {
    if (isFocused) {
      getOneOrderDetailDispatch(orderId);
    }
  }, [isFocused]);

  useEffect(() => {
    if (getOneOrderDetail?.vanDetails?.driverPhone) {
      try {
        const parsedPhoneNumber = parsePhoneNumber(
          getOneOrderDetail?.vanDetails?.driverPhone.toString(),
          "US"
        );
        setDriverPhoneNumber(parsedPhoneNumber.number);
      } catch {
        setDriverPhoneNumber(getOneOrderDetail?.vanDetails?.driverPhone);
      }
    }

    if (getOneOrderDetail?.sourceLatitude != null) {
      setDeliveryRoutePoints([
        {
          latitude: getOneOrderDetail?.sourceLatitude,
          longitude: getOneOrderDetail?.sourceLongitude,
        },
        {
          latitude: getOneOrderDetail?.destinationLatitude,
          longitude: getOneOrderDetail?.destinationLongitude,
        },
      ]);

      setDeliveryLocation({
        latitude: getOneOrderDetail?.destinationLatitude,
        longitude: getOneOrderDetail?.destinationLongitude,
        latitudeDelta: 0.1,
        longitudeDelta: 0.1,
      });

      setShopLocation({
        latitude: getOneOrderDetail?.sourceLatitude,
        longitude: getOneOrderDetail?.sourceLongitude,
        latitudeDelta: 0.1,
        longitudeDelta: 0.1,
      });

      mapRef.current?.animateToRegion({
        latitude: getOneOrderDetail?.sourceLatitude,
        longitude: getOneOrderDetail?.sourceLongitude,
        latitudeDelta: 0.1,
        longitudeDelta: 0.1,
      });
    }
  }, [getOneOrderDetail]);

  return (
    <SafeAreaView style={styles.root}>
      <Loader loading={loading} />
      {!loading && (
        <>
          <MapView
            ref={mapRef}
            style={styles.map}
            loadingEnabled
            initialRegion={shopLocation}
            rotateEnabled={false}
            mapPadding={styles.mapPadding}
            showsCompass
          >
            {driverSnappedPoints && (
              <Polyline
                coordinates={driverSnappedPoints}
                strokeWidth={3}
                strokeColor={"blue"}
              />
            )}
            {driverCurrentLocation && (
              <Marker coordinate={driverCurrentLocation}>
                <CommunityIcon name="van-utility" color="blue" size={24} />
              </Marker>
            )}
            <MapViewDirections
              origin={deliveryRoutePoints[0]}
              destination={deliveryRoutePoints[deliveryRoutePoints.length - 1]}
              apikey={"AIzaSyBbmnBQN9_tONY35MVvLRGeFS6GJQWgY0A"}
              strokeWidth={4}
              strokeColor={getOneOrderDetail?.vanDetails?.color}
            />
            <Marker coordinate={deliveryLocation}>
              <Icon name="location" color="#e53935" size={32} />
            </Marker>
            <Marker coordinate={shopLocation}>
              <EntyPoIcon name="shop" color="#008000" size={24} />
            </Marker>
          </MapView>
          <View style={styles.lowerContainer}>
            <View style={styles.lowerContainerFirstRow}>
              <View>
                <Text style={styles.orderNo}>
                  Order#:{getOneOrderDetail?.orderNumber}
                </Text>
                <Text style={styles.status}>
                  Order {getOneOrderDetail?.orderStatus}
                </Text>
              </View>
              {!DeviceInfo.isTablet() && (
                <TouchableOpacity
                  disabled={
                    getOneOrderDetail?.vanDetails?.driverPhone == null
                      ? true
                      : false
                  }
                  activeOpacity={0.5}
                  onPress={() => {
                    Linking.openURL(`tel:${driverPhoneNumber}`);
                  }}
                  style={
                    getOneOrderDetail?.vanDetails?.driverPhone == null
                      ? styles.phoneIconContainerDisabled
                      : styles.phoneIconContainer
                  }
                >
                  <Phone
                    height={38}
                    width={21}
                    color={
                      getOneOrderDetail?.vanDetails?.driverPhone == null
                        ? "#78909C"
                        : "#673ab7"
                    }
                  />
                  <Text style={styles.callDriverText}>Call Driver</Text>
                </TouchableOpacity>
              )}
            </View>
            <View style={styles.driverInfoContainer}>
              {getOneOrderDetail?.vanDetails?.imageUrl ? (
                <Image
                  style={styles.vanImage}
                  source={{ uri: getOneOrderDetail?.vanDetails?.imageUrl }}
                />
              ) : (
                <Image
                  style={styles.vanImageDefault}
                  source={require("../../assets/images/orders/order_Confirmation.png")}
                />
              )}
              <View style={styles.drivernameContainer}>
                <Text style={styles.driverName}>
                  {getOneOrderDetail?.vanDetails?.driverName
                    ? getOneOrderDetail?.vanDetails?.driverName
                    : "Driver Not Assigned Yet..."}
                </Text>
                <Text style={styles.vanInfo}>
                  {getOneOrderDetail?.vanDetails?.driverName
                    ? getOneOrderDetail?.vanDetails?.make +
                      " " +
                      getOneOrderDetail?.vanDetails?.model +
                      "(" +
                      getOneOrderDetail?.vanDetails?.color +
                      ")"
                    : "#-------"}
                </Text>
              </View>
            </View>
            <View style={styles.orderActionContainer}>
              <TouchableOpacity
                style={styles.cancelOrderBtn}
                onPress={() => setShowCancelModal(true)}
              >
                <Text style={styles.cancelOrderBtnText}>Cancel Order</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={viewOrderDetails}
                style={styles.viewOrderBtn}
              >
                <Text style={styles.viewOrderBtnText}>View Order Details</Text>
              </TouchableOpacity>
            </View>
          </View>
          <CancelOrderModal
            showModal={showCancelModal}
            itemId={id}
            navigation={navigation}
            setVisible={() => {
              setShowCancelModal(false);
            }}
          />
        </>
      )}
    </SafeAreaView>
  );
};

export default connect(
  (state) => ({
    getOneOrderDetail: state.myOrders.getOneOrderDetail,
    trackOrder: state.myOrders.trackOrder,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    getOneOrderDetailDispatch: (id) => dispatch(getOneOrderDetail(id)),
    trackOrderDispatch: (id) => dispatch(trackOrder(id)),
  })
)(DeliveryTrackingScreen);

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
  mapPadding: {
    top: 100,
    right: 0,
    bottom: 110,
    left: 0,
  },
  lowerContainer: {
    position: "absolute",
    bottom: 16,
    right: 0,
    left: 0,
    backgroundColor: "#ffffff",
    paddingHorizontal: 24,
    paddingVertical: 20,
    borderRadius: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 4,
  },
  lowerContainerFirstRow: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  orderNo: {
    color: "#263238",
    fontSize: 18,
    fontFamily: "Roboto",
    fontWeight: "bold",
    marginTop: 8,
  },
  status: {
    color: "#455a64",
    fontSize: 15,
    paddingTop: 4,
  },
  driverInfoContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexShrink: 1,
  },
  vanImage: {
    width: 170,
    height: 100,
  },
  vanImageDefault: {
    width: 170,
    height: 100,
  },
  drivernameContainer: {
    marginLeft: 10,
    flexShrink: 1,
    justifyContent: "center",
    maxHeight: 2000,
    overflow: "scroll",
  },
  driverName: {
    fontFamily: "Roboto-Medium",
    fontWeight: "600",
    fontSize: 14,
    color: "#000000",
  },
  vanInfo: {
    fontFamily: "Roboto-Medium",
    color: "#5e35b1",
    fontSize: 16,
    marginTop: 15,
  },
  phoneIconContainer: {
    alignItems: "center",
    borderColor: "#a38862",
    backgroundColor: "#fff",
    borderWidth: 1.5,
    borderRadius: 10,
    height: 75,
    padding: 10,
    width: 106,
  },
  phoneIconContainerDisabled: {
    alignItems: "center",
    borderColor: "#78909C",
    backgroundColor: "#eceff1",
    borderWidth: 1.5,
    borderRadius: 10,
    height: 75,
    padding: 10,
    width: 106,
  },
  callDriverText: {
    fontSize: 12,
    fontFamily: "Roboto-Medium",
    color: "#000",
    marginTop: -2,
  },
  orderActionContainer: {
    flexDirection: "row",
    marginTop: 30,
  },
  viewOrderBtn: {
    backgroundColor: "#673ab7",
    borderRadius: 8,
    paddingVertical: 8,
    alignItems: "center",
    flex: 1,
    marginLeft: 4,
    justifyContent: "center",
  },
  cancelOrderBtn: {
    backgroundColor: "#fff",
    borderRadius: 8,
    paddingVertical: 8,
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#a38862",
    flex: 1,
    marginRight: 4,
    justifyContent: "center",
  },
  viewOrderBtnText: {
    color: "#fff",
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    textAlign: "center",
  },
  cancelOrderBtnText: {
    fontFamily: "Roboto-Medium",
    color: "#263238",
    fontSize: 16,
    textAlign: "center",
  },
  marker: {
    opacity: 1,
  },
  inactiveMarker: {
    opacity: 0.3,
  },
});
