import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Platform,
} from "react-native";
import Toast from "react-native-toast-message";
import { TextInput as NewTextInput } from "react-native-paper";
import { connect } from "react-redux";
import { addPhoneNumber } from "../../store/action/userRegistrationAction";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import PhoneImg from "../Components/svg/Mobile";
import GoldLogo from "../Components/svg/CBLogoGoldNoFill";
import SmsRetriever from "react-native-sms-retriever";
import { useDispatch } from "react-redux";
import { RESET_ALL_USER_REGISTRATION } from "../../store/action/userRegistrationAction";

const RegistrationAddPhoneNo = (props) => {
  const { emailId, userId } = props.route.params;
  const dispatch = useDispatch();
  const [isError, setisError] = useState(false);
  const { phoneResp, loading, registeredUser, addPhoneNumber } = props;
  const [showPassword, setShowPassword] = useState(false);
  const [form, setForm] = useState({});
  const passwordRef = useRef();
  const handleSubmitPress = () => {
    if (form.phoneNo === "" || form.phoneNo === undefined) {
      setisError(true);
      return;
    } else {
      setisError(false);
    }
    if (!isError) {
      addPhoneNumber(form?.phoneNo, userId);
    }
  };
  const onTextChange = (text) => {
    var cleaned = ("" + text).replace(/\D/g, "");
    var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      var intlCode = match[1] ? "+1 " : "",
        number = [intlCode, "(", match[2], ") ", match[3], "-", match[4]].join(
          ""
        );
      setForm({
        phoneNo: number,
      });
      return;
    }
    setForm({
      phoneNo: text,
    });
  };
  const EyeComp = () => {
    return (
      <NewTextInput.Icon
        name="eye"
        color="#78909c"
        onPress={() => {
          setShowPassword((prevState) => {
            return !prevState;
          });
        }}
      />
    );
  };
  useEffect(() => {
    (async () => {
      try {
        if (Platform.OS === "android") {
          const phoneNumber = await SmsRetriever.requestPhoneNumber();
          onTextChange(
            phoneNumber.startsWith("+91")
              ? phoneNumber.split("+91")[1]
              : phoneNumber.startsWith("+1")
              ? phoneNumber.split("+1")[1]
              : phoneNumber
          );
        }
      } catch (error) {
        console.log(`Phone Number Error: ${JSON.stringify(error)}`);
      }
    })();
  }, []);

  useEffect(() => {
    if (phoneResp?.successBody?.message) {
      props.navigation.navigate("RegistrationSmsVerifyCode", {
        phoneNo: form.phoneNo,
        emailId: emailId,
        userId: userId,
      });
      dispatch({ type: RESET_ALL_USER_REGISTRATION });
    } else if (phoneResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: phoneResp?.errorBody,
      });
      dispatch({ type: RESET_ALL_USER_REGISTRATION });
    }
  }, [phoneResp]);
  const TextInputComp = React.useCallback(
    ({ right, placeholderLabel, id, secure, ref, label }) => {
      return (
        <NewTextInput
          keyboardType="phone-pad"
          maxLength={14}
          style={styles.inputStyle}
          returnKeyType={!secure ? "next" : "default"}
          onSubmitEditing={() => {
            passwordRef.current.focus();
          }}
          ref={passwordRef}
          textContentType="telephoneNumber"
          autoCompleteType="tel"
          autoCorrect
          placeholder={placeholderLabel}
          secureTextEntry={secure && !showPassword ? true : false}
          placeholderTextColor="#546e7a"
          right={right == "eye" ? EyeComp() : ""}
          underlineColor="transparent"
          onChangeText={(text) => onTextChange(text)}
          value={label}
        />
      );
    },
    [showPassword, props]
  );

  useEffect(() => {}, [props]);

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <SafeAreaView style={styles.root}>
        <BannerAvatar touchableOpacity />
        <View style={styles.mobileLayout}>
          <PhoneImg height={120} width={150} viewBox="0 0 130 950" />
        </View>
        <View style={styles.goldLogoLayout}>
          <GoldLogo height={37} width={40} viewBox="10 0 90 126" />
        </View>
        <View style={styles.bodyLayout}>
          <View>
            <Text style={styles.secondaryText}>What is your phone number?</Text>
            <View style={styles.SectionStyle}>
              <TextInputComp
                style={styles.inputStyle}
                right={"phone"}
                placeholderLabel={" "}
                id={"phoneNo"}
                secure={false}
                label={form["phoneNo"]}
              />
            </View>
            {isError ? (
              <Text style={styles.errorTextStyle}>
                Please enter a valid 10-digit phone number
              </Text>
            ) : null}

            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={handleSubmitPress}
            >
              <Text style={styles.buttonTextStyle}>Next</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ backgroundColor: "white", marginTop: 55 }}>
          <FooterBanner
            touchableOpacity
            imageSource={require("../../assets/images/bottom_banner.png")}
          />
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

export default connect(
  (state) => ({
    phoneResp: state.registeredUser.phoneResp,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    addPhoneNumber: (phoneNumber, userId) =>
      dispatch(addPhoneNumber(phoneNumber, userId)),
  })
)(RegistrationAddPhoneNo);

const styles = StyleSheet.create({
  root: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
  },
  mobileLayout: {
    bottom: 69,
    right: 21,
  },
  goldLogoLayout: {
    bottom: 166,
    left: 0,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    width: 30,
    height: 30,
    borderRadius: 100,
  },
  bodyLayout: {
    color: "#546e7a",
    fontFamily: "Roboto",
    marginTop: -75,
    width: "90%",
  },
  primaryText: {
    fontSize: 14,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  secondaryText: {
    width: "100%",
    height: 32,
    color: "#a38862",
    fontFamily: "Roboto",
    fontWeight: "400",
    fontSize: 20,
    letterSpacing: 0.1,
    textAlign: "center",
  },

  SectionStyle: {
    flexDirection: "row",
    height: 55,
    marginTop: 20,
    width: "100%",
  },
  inputStyle: {
    flex: 1,
    color: "black",
    fontSize: 18,
    fontWeight: "normal",
    backgroundColor: "#ede7f6",
    height: "65%",
    width: "100%",
    flexDirection: "column",
    textAlign: "center",
    justifyContent: "center",
    borderWidth: 0,
    borderColor: "#ede7f6",
    borderRadius: 5,
  },
  subText: {
    marginLeft: "2%",
    marginTop: 10,
    fontSize: 12,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  errorTextStyle: {
    color: "#f44336",
    textAlign: "left",
    fontSize: 16,
    marginLeft: "2%",
    marginTop: 5,
    fontWeight: "400",
    fontFamily: "Roboto",
    marginTop: 5,
    marginLeft: 45,
  },
  successTextStyle: {
    color: "white",
    textAlign: "center",
    fontSize: 18,
    padding: 30,
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    height: 50,
    alignItems: "center",
    width: "100%",
    marginTop: 30,
    borderRadius: 5,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 15,
    fontSize: 16,
    fontWeight: "500",
    fontFamily: "Roboto",
    textAlign: "center",
  },
});
