import React, { useEffect, useState, useCallback } from "react";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  InteractionManager,
  FlatList,
} from "react-native";
import { useDispatch, useSelector, connect } from "react-redux";
import { Checkbox } from "react-native-paper";
import * as locationActions from "../../store/action/location";
import Logo from "../Components/svg/CB_Secondary.js";
import { updateTempOrder } from "../../store/action/myOrdersAction";

export const AddressContainer = (props) => {
  const { field1, field2, field3, field4 } = props;
  const styles = rowItemStyles;
  return (
    <View style={styles.leftContainer}>
      <Text style={styles.field1}>{field1}</Text>
      <Text style={styles.field2}>{field2}</Text>
    </View>
  );
};

const rowItemStyles = StyleSheet.create({
  root: {
    paddingHorizontal: 30,
    paddingVertical: 15,
    backgroundColor: "white",
    borderTopWidth: 1,
    borderColor: "#eceff1",
    justifyContent: "space-between",
  },
  flexrow: {
    flexDirection: "row",
  },
  textinput: {
    color: "#546e7a",
    fontSize: 16,
    paddingTop: 8,
    paddingBottom: 24,
  },
});

export const RowItem = (props) => {
  const styles = rowItemStyles;
  const { onPress, field1, field2, field3, containerStyle, selected, id } =
    props;
  return (
    <TouchableOpacity
      onPress={() => onPress(id)}
      style={[styles.root, styles.flexrow, containerStyle]}
    >
      <AddressContainer field1={field1} field2={field2} field3={field3} />
      <Checkbox.Android
        status={selected ? "checked" : "unchecked"}
        color="#673ab7"
      />
    </TouchableOpacity>
  );
};

const DeliveryAddress = ({ navigation, updateOrder, route }) => {
  const cart = useSelector((state) => state.cart.cartItems);
  const login = useSelector((state) => state.loggedInUser.loggedInUser);
  const userLocation = useSelector((state) => state.locations.locations);
  const dispatch = useDispatch();
  const [searchResult, setSearchResult] = useState([]);
  const [searchtext, setSearchText] = useState("");
  const [selectedItemId, setSelectedId] = useState("");
  const [jwtToken] = useState(login?.userDetails?.jwtToken);

  const fetchLocations = useCallback(() => {
    try {
      dispatch(locationActions.getLocation(login?.userDetails?.jwtToken));
    } catch (err) {}
  });

  useEffect(() => {
    if (!userLocation) {
      return navigation.addListener("focus", () => {
        fetchLocations();
      });
    }
  }, [navigation]);

  const cartTotal = React.useMemo(() => {
    const calculatedTotal = parseFloat(
      cart.reduce((total, item) => total + item.quantity * item.price, 0)
    ).toFixed(2);
    const tax = parseFloat((calculatedTotal * 8.25) / 100).toFixed(2);
    return parseFloat(parseFloat(calculatedTotal) + parseFloat(tax)).toFixed(2);
  }, [cart]);

  useEffect(() => {
    const communityId = userLocation?.communities?.filter(
      (item) => item.isDefault
    )?.[0]?.communityId;
    setSelectedId(communityId);
  }, [userLocation]);

  const performSearch = React.useCallback((text) => {
    InteractionManager.runAfterInteractions(() => {
      const searchresult = userLocation?.communities?.filter((item) => {
        const searchVal = text.toLowerCase().trim();

        return (
          item.communityName?.toLowerCase().includes(searchVal) ||
          item.address?.toLowerCase().includes(searchVal) ||
          item.city?.toLowerCase().includes(searchVal)
        );
      });
      setSearchResult(searchresult);
    }, []);
  }, []);

  const onSearchTextChange = React.useCallback((text) => {
    setSearchText(text);
    performSearch(text);
  }, []);

  const onNext = () => {
    dispatch(locationActions.setLocally(selectedItemId));
    updateOrder(
      route?.params?.orderId,
      {
        communityId: selectedItemId,
      },
      jwtToken
    );
    navigation.navigate("checkoutScreenHome", {
      checkOut: false,
    });
  };

  const onItemSelection = (id) => {
    setSelectedId(id);
  };

  const flatListdata = React.useMemo(() => {
    return searchtext.length ? searchResult : userLocation?.communities ?? [];
  }, [searchtext, searchResult]);

  const renderItem = React.useCallback(
    ({ item }) => {
      return (
        <TouchableOpacity
          onPress={() => {
            onItemSelection(item.communityId);
          }}
          style={[rowItemStyles.root, rowItemStyles.flexrow, styles.itemStyle]}
        >
          <AddressContainer
            field1={item.communityName}
            field2={`${item.city}, ${item.state}`}
          />
          <Checkbox.Android
            status={
              item.communityId === selectedItemId ? "checked" : "unchecked"
            }
            color="#673ab7"
          />
        </TouchableOpacity>
      );
    },
    [flatListdata, selectedItemId]
  );

  return (
    <SafeAreaView style={styles.root}>
      {/* <Loader loading={loading} /> */}
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
          alignContent: "center",
          width: "100%",
          margin: 20,
          marginBottom: 5,
        }}
      >
        <Logo height={98} width={380} viewBox="20 0 50 120" />
      </View>
      <View style={{ width: "100%" }}>
        <Text
          style={{
            height: 55,
            fontSize: 21,
            color: "#3b018a",
            fantFamily: "Arial",
            textAlign: "center",
            fontWeight: "400",
          }}
        >
          Choose your Community
        </Text>
      </View>

      <FlatList
        showsVerticalScrollIndicator={false}
        data={flatListdata}
        keyExtractor={(item, index) => `${item.field1}${index}`}
        renderItem={renderItem}
      />
      <View style={styles.bottomBanner}>
        <Text style={styles.totalValue}>{"$ " + cartTotal}</Text>
        <TouchableOpacity onPress={onNext} style={styles.nextButton}>
          <Text style={styles.nextButtonText}>Save</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default connect(
  (state) => ({
    loading: state.loading.loading,
  }),
  (dispatch) => ({
    updateOrder: (orderId, updatedOrder, jwtToken) =>
      dispatch(updateTempOrder(orderId, updatedOrder, jwtToken)),
  })
)(DeliveryAddress);

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "white",
  },
  itemStyle: {
    marginBottom: 20,
  },
  inputItemStyle: {
    marginBottom: 16,
  },
  bottomBanner: {
    height: 75,
    backgroundColor: "green",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 24,
    justifyContent: "space-between",
    backgroundColor: "#ffffff",
    marginBottom: 8,
    shadowColor: "#000000",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    elevation: 2,
  },
  totalValue: { fontSize: 22, color: "#f44336", fontWeight: "600" },
  nextButtonText: {
    paddingHorizontal: 60,
    paddingVertical: 8,
    fontSize: 16,
    color: "#fff",
  },
  nextButton: {
    backgroundColor: "#f44336",
    borderRadius: 30,
  },
  leftContainer: {
    justifyContent: "space-between",
    flexGrow: 1,
    flexShrink: 1,
  },
  field1: {
    color: "#37474f",
    fontSize: 16,
    fontWeight: "600",
  },
  field2: {
    color: "#546e7a",
    fontSize: 16,
    paddingTop: 8,
  },
  field3: {
    color: "#546e7a",
    fontSize: 16,
    paddingTop: 8,
  },
});
