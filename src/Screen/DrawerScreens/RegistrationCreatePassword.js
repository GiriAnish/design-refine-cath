import React, { useState, useEffect, useRef } from "react";
import { connect, useDispatch } from "react-redux";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import { TextInput as NewTextInput } from "react-native-paper";
import {
  createPassword,
  RESET_ALL_USER_REGISTRATION,
} from "../../store/action/userRegistrationAction";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import Lock from "../Components/svg/lock";
import GoldLogo from "../Components/svg/CBLogoGoldNoFill";
import Loader from "../Components/Loader";
import Toast from "react-native-toast-message";

const PASSWORD_FORMAT_ERROR =
  "Must have eight characters, at least 1 uppercase letter and 1 special character";
const PASSWORD_NOT_MATCHING_ERROR = "Passwords not matching";
const PASSWORD_REQUIRED_ERROR = "Password is required";
const CONFORM_PASSWORD_REQUIRED_ERROR = "Confirm Password is required";

const RegistrationCreatePassword = (props) => {
  const { emailId, userId } = props?.route?.params;
  const dispatch = useDispatch();
  const { passwordResp, loading, createNewPassword } = props;
  const [showPassword, setShowPassword] = useState(false);
  const [form, setForm] = useState({
    password: null,
    passwordError: null,
    confirmPassword: null,
    confirmPasswordError: null,
  });
  const passwordRef = useRef();

  Toast.hide();
  const handleSubmitPress = () => {
    const formData = {
      ...form,
      passwordError: null,
      confirmPasswordError: null,
    };
    const passwordValidationRegex = /^(?=.*?[A-Z])(?=.*?[#?!@$%^&*-]).{8,}$/;

    if (!form.password) {
      formData.passwordError = PASSWORD_REQUIRED_ERROR;
    }

    if (!form.confirmPassword) {
      formData.confirmPasswordError = CONFORM_PASSWORD_REQUIRED_ERROR;
    }

    const doesPasswordAlignsWithCriteria = passwordValidationRegex.test(
      form.password
    );

    if (form.password && !doesPasswordAlignsWithCriteria) {
      formData.passwordError = PASSWORD_FORMAT_ERROR;
    }

    if (formData.passwordError || formData.confirmPasswordError) {
      setForm(formData);
      return;
    }

    if (form.password !== form.confirmPassword) {
      formData.passwordError = PASSWORD_NOT_MATCHING_ERROR;
      formData.confirmPasswordError = PASSWORD_NOT_MATCHING_ERROR;
    }

    setForm(formData);
    if (formData.passwordError || formData.confirmPasswordError) {
      return;
    }
    const payload = {
      newPassword: form.password,
      newPasswordConfirm: form.confirmPassword,
    };
    createNewPassword(payload, userId);
  };

  const EyeComp = () => {
    return (
      <NewTextInput.Icon
        name="eye-outline"
        color="#78909c"
        onPress={() => {
          setShowPassword((prevState) => {
            return !prevState;
          });
        }}
      />
    );
  };

  const TextInputComp = React.useCallback(
    ({ right, placeholderLabel, id, secure, label }) => {
      return (
        <NewTextInput
          style={styles.inputStyle}
          returnKeyType={!secure ? "next" : "default"}
          onSubmitEditing={() => {
            passwordRef.current.focus();
          }}
          ref={passwordRef}
          placeholder={placeholderLabel}
          secureTextEntry={secure && !showPassword ? true : false}
          placeholderTextColor="#546e7a"
          right={right == "eye" ? EyeComp() : ""}
          underlineColor="transparent"
          onChangeText={(text) => {
            setForm((prevState) => ({ ...prevState, [id]: text }));
          }}
          value={label}
        />
      );
    },
    [showPassword, props]
  );

  useEffect(() => {
    if (passwordResp?.successBody?.message) {
      Toast.show({
        type: "success",
        text1: "Success",
        text2: passwordResp?.successBody?.message,
      });
      props.navigation.navigate("RegistrationAddPhoneNo", {
        emailId: emailId,
        userId: userId,
      });
      dispatch({ type: RESET_ALL_USER_REGISTRATION });
    } else if (passwordResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: passwordResp?.errorBody,
      });
      dispatch({ type: RESET_ALL_USER_REGISTRATION });
    }
  }, [passwordResp]);
  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <SafeAreaView style={styles.root}>
        <Loader loading={loading} />
        <BannerAvatar touchableOpacity />
        <View style={styles.lockLayout}>
          <Lock height={100} width={110} viewBox="0 18 250 200" />
        </View>
        <View style={styles.goldLogoLayout}>
          <GoldLogo height={36} width={40} viewBox="10 0 100 127" />
        </View>
        <View style={styles.bodyLayout}>
          <View>
            <Text style={styles.secondaryText}>
              Create a password for your account...
            </Text>
            <View style={styles.SectionStyle}>
              <TextInputComp
                style={styles.inputStyle}
                right={"eye"}
                placeholderLabel={"Enter New Password"}
                id={"password"}
                secure={true}
                label={form["password"]}
              />
            </View>
            {form.passwordError ? (
              <Text style={styles.errorTextStyle}>{form.passwordError}</Text>
            ) : (
              <Text style={styles.subText}>{PASSWORD_FORMAT_ERROR}</Text>
            )}
            <View style={styles.SectionStyle}>
              <TextInputComp
                style={styles.inputStyle}
                right={"eye"}
                placeholderLabel={"Retype New Password"}
                id={"confirmPassword"}
                secure={true}
                label={form["confirmPassword"]}
              />
            </View>
            {form.confirmPasswordError ? (
              <Text style={styles.errorTextStyle}>
                {form.confirmPasswordError}
              </Text>
            ) : (
              <Text style={styles.subText}>Both passwords must match</Text>
            )}
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={handleSubmitPress}
            >
              <Text style={styles.buttonTextStyle}>Next</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            alignContent: "center",
            width: "90%",
            alignItems: "center",
            justifyContent: "center",
            height: 180,
            marginBottom: 20,
          }}
        >
          <FooterBanner
            touchableOpacity
            imageSource={require("../../assets/images/bottom_banner.png")}
          />
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

export default connect(
  (state) => ({
    passwordResp: state.registeredUser.createPasswordRes,
    loading: state.loading.loading,
    registeredUser: state.registeredUser.registeredUser,
  }),
  (dispatch) => ({
    createNewPassword: (payload, userId) =>
      dispatch(createPassword(payload, userId)),
  })
)(RegistrationCreatePassword);

const styles = StyleSheet.create({
  root: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
  bodyLayout: {
    color: "#546e7a",
    fontFamily: "Roboto",
    marginTop: -50,
    bottom: 30,
    width: "90%",
  },
  lockLayout: {
    bottom: 60,
    left: 5,
  },
  logo: {
    marginBottom: 22,
    alignItems: "center",
    top: 10,
  },
  goldLogoLayout: {
    bottom: 118,
    left: 2,
    backgroundColor: "white",
    alignItems: "center",
    width: 30,
    borderRadius: 120,
  },
  primaryText: {
    fontSize: 14,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  secondaryText: {
    marginTop: 20,
    fontSize: 19,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#a38862",
    fontFamily: "Roboto",
  },

  SectionStyle: {
    flexDirection: "row",
    height: 55,
    marginVertical: 22,
    marginTop: 0,
    top: 25,
    width: "100%",
    alignContent: "stretch",
  },
  inputStyle: {
    flex: 1,
    color: "black",
    fontSize: 17,
    fontWeight: "normal",
    backgroundColor: "#ede7f6",
    height: "65%",
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "stretch",
    borderWidth: 10,
    borderColor: "#ede7f6",
    borderRadius: 5,
  },
  subText: {
    marginLeft: "2%",
    marginTop: 5,
    fontSize: 15,
    top: 10,
    left: 15,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  errorTextStyle: {
    color: "red",
    textAlign: "left",
    fontSize: 17,
    top: 10,
    left: 15,
    marginLeft: "2%",
    marginTop: 8,
    fontWeight: "normal",
  },
  successTextStyle: {
    color: "white",
    textAlign: "center",
    fontSize: 18,
    padding: 30,
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    height: 50,
    top: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    borderRadius: 5,
    marginBottom: 15,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 12,
    fontSize: 16,
    fontWeight: "bold",
  },
});
