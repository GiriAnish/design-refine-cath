import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  SafeAreaView,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import CBLogo from "../Components/svg/CB_Primary.js";
import VipBenefitsModal from "../Components/VipBenefitsModal.js";
import CallToStore from "../Components/CallToStore";
import Modal from "../Components/Modal";
import { useSelector, useDispatch } from "react-redux";
import CBLogoFullWhite from "../Components/svg/CBLogoFullWhite";
import { useTabVisible } from "../../contexts/navigation";
import * as loadingAction from "../../store/action/loading";
import { useFocusEffect } from "@react-navigation/native";

const HomeVipScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const [showVipModal, setShowVipModal] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const loggedInUser = useSelector((state) => state.loggedInUser.loggedInUser);
  const { setTabVisible, tabVisible } = useTabVisible();
  const vipBannerImageURL = loggedInUser?.userDetails?.vipBannerImageURL;

  const modalClick = () => {
    setShowModal(true);
  };
  useEffect(() => {
    dispatch(loadingAction.setLoading(false));
    navigation.setOptions({
      headerRight: () => <CallToStore onPress={modalClick} />,
    });
  });

  useFocusEffect(
    React.useCallback(() => {
      setTabVisible(true);
    }, [setTabVisible])
  );
  return (
    <ScrollView style={{ backgroundColor: "#673ab7", paddingTop: 12 }}>
      <SafeAreaView>
        <View style={styles.root}>
          <View style={styles.bodyLayout}>
            <CBLogo
              height={150}
              width={378}
              style={{ marginTop: 20, marginBottom: -20 }}
            />
            <View style={styles.vipMembershipContainer}>
              <Text style={styles.vipMembership}>VIP Membership Card</Text>
            </View>
            <View style={styles.accountTypeContainer}>
              {vipBannerImageURL ? (
                <Image
                  style={styles.logoImage}
                  resizeMode={"contain"}
                  source={{ uri: vipBannerImageURL }}
                />
              ) : (
                <Text style={styles.primaryText}>
                  {loggedInUser?.userDetails?.vipBannerText}
                </Text>
              )}
              <Text style={styles.accountTypeSubText}>
                {loggedInUser?.userDetails?.vipBannerSubText}
              </Text>
            </View>
            <View style={styles.userDetailsContainer}>
              <Text style={styles.userDetailsText}>
                {loggedInUser?.userDetails?.firstName}{" "}
                {loggedInUser?.userDetails?.lastName}
              </Text>
            </View>
            <View style={{}}>
              <Image
                style={styles.qrcodeImage}
                source={{
                  uri: loggedInUser?.userDetails?.accountBarcodeBase64,
                }}
              />
            </View>
            <View style={styles.barcodeDetailsContainer}>
              <Text style={styles.titleBarcode}>
                {loggedInUser?.userDetails?.barCodeImageURL}
              </Text>
            </View>
            <View style={styles.membershipBenefits}>
              <TouchableOpacity onPress={() => setShowVipModal(true)}>
                <Text style={styles.linkText}>VIP Membership Benefits</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              width: "100%",
              height: "auto",
              marginTop: 30,
              marginBottom: 30,
            }}
          >
            <CBLogoFullWhite height={70} viewBox="0 0 378 89" />
          </View>
        </View>

        <VipBenefitsModal
          showModal={showVipModal}
          setVisible={() => {
            setShowVipModal(false);
          }}
        />
        {showModal && (
          <Modal
            showModal={showModal}
            setVisible={() => {
              setShowModal(false);
            }}
          />
        )}
      </SafeAreaView>
    </ScrollView>
  );
};

export default HomeVipScreen;
const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
  },
  bodyLayout: {
    width: "90%",
    backgroundColor: "white",
    borderRadius: 20,
    paddingTop: 10,
    alignItems: "center",
  },
  vipMembershipContainer: {
    width: "100%",
    backgroundColor: "#ede7f6",
    fontWeight: "bold",
    textAlign: "center",
    padding: 10,
  },
  vipMembership: {
    color: "#f44336",
    textAlign: "center",
    fontSize: 20,
  },
  logoImage: {
    width: 200,
    height: 60,
    marginTop: 5,
  },
  accountTypeContainer: {
    width: "100%",
    backgroundColor: "white",
    alignItems: "center",
    marginTop: 15,
  },
  userDetailsContainer: {
    alignItems: "center",
    width: "100%",
    marginBottom: 8,
  },
  barcodeDetailsContainer: {
    alignItems: "center",
    width: "100%",
    marginTop: 8,
  },
  titleBarcode: {
    fontSize: 18,
    fontWeight: "600",
  },
  userDetailsText: {
    width: 321,
    height: 37,
    color: "#455a64",
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 24,
    textAlign: "center",
  },
  membershipBenefits: {
    margin: 6,
    marginTop: 0,
  },
  banner: {
    alignContent: "center",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    height: 130,
  },
  qrcodeImage: {
    height: 40,
    width: 190,
  },
  accountTypeSubText: {
    color: "#a38862",
    margin: 15,
    fontSize: 18,
    fontFamily: "Roboto",
  },

  linkText: {
    width: 320,
    height: 37,
    color: "#2196f3",
    textAlign: "center",
    marginTop: 10,
    fontSize: 14,
    textDecorationLine: "underline",
  },
});
