import React, { useEffect } from "react";
import { SafeAreaView, View } from "react-native";
import NoSavedPizzas from "../Components/NoSavedPizzas";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { fetchSavedPizzas } from "../../store/action/savedPizzasAction";
import Loader from "../Components/Loader";
import Banner from "../Components/Banner";
import { PizzaCard } from "../Components/TodaySpecial";

const SavedPizzaScreen = (props) => {
  const { data, loading, getSavedPizzas, navigation } = props;

  useEffect(() => {
    const unsubscribe = props.navigation.addListener("focus", () => {
      getSavedPizzas();
    });
    return unsubscribe;
  }, [props.navigation]);
  return (
    <SafeAreaView style={{ flex: 1 }}>
      {!loading && data?.items?.length && (
        <Banner
          imageSource={require("../../assets/images/savedPizza/savedPizzaBanner.png")}
          primaryText={"Your Saved"}
          secondaryText={"Pizza Favorites"}
        />
      )}
      <Loader loading={loading} />
      {!loading && data?.items?.length ? (
        data.items.map((item) => {
          return (
            <View>
              <TouchableWithoutFeedback
                key={item.itemId}
                onPress={() => {
                  props.navigation.navigate("DrawerNavigationRoutes", {
                    screen: "homeScreenStack",
                    params: {
                      screen: "PizzaMenu",
                      params: {
                        screen: "pizzaDetails",
                        params: {
                          pizzaId: item.itemId,
                          pizzaName: item.itemName,
                          price: item.price,
                        },
                      },
                    },
                  });
                }}
              >
                <PizzaCard
                  name={item.itemName}
                  price={item.price}
                  rating={item.rating}
                  description={item.description}
                  imageUrl={item.imageUrl}
                />
              </TouchableWithoutFeedback>
            </View>
          );
        })
      ) : (
        <NoSavedPizzas />
      )}
    </SafeAreaView>
  );
};

export default connect(
  (state) => ({
    data: state.savedPizzas.savedPizzas,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    getSavedPizzas: () => dispatch(fetchSavedPizzas(1)),
  })
)(SavedPizzaScreen);
