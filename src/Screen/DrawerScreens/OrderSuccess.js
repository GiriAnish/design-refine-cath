import React, { useCallback, useEffect } from "react";
import {
  View,
  Text,
  SafeAreaView,
  Image,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { useTabVisible } from "../../contexts/navigation";
import * as CartActions from "../../store/action/cart";
import { useDispatch } from "react-redux";

const RESET_CLIENT_SECRET = "RESET_CLIENT_SECRET";
const RESET_NEW_ORDER = "RESET_NEW_ORDER";
const RESET_PAYMENT_CONFIRMED = "RESET_PAYMENT_CONFIRMED";

const OrderSuccessScreen = (props) => {
  const dispatch = useDispatch();
  const { navigation } = props;
  const { setTabVisible } = useTabVisible();
  const { orderNumber, id } = props?.route?.params;
  const onTrackorder = useCallback(() => {
    //navigation.dispatch(StackActions.popToTop());
    navigation.push("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "OrdersMenu",
        params: {
          screen: "deliveryTrackingHome",
          params: {
            orderId: orderNumber,
            id: id,
          },
        },
      },
    });
  }, []);

  const onOrderInfo = useCallback(() => {
    //navigation.dispatch(StackActions.popToTop());
    navigation.push("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "OrdersMenu",
        params: {
          screen: "OrderDetails",
          params: {
            orderId: orderNumber,
            id: id,
          },
        },
      },
    });
  }, []);

  useEffect(() => {
    dispatch({ type: RESET_CLIENT_SECRET });
    dispatch({ type: RESET_NEW_ORDER });
    dispatch({ type: RESET_PAYMENT_CONFIRMED });
  }, []);

  useEffect(() => {
    dispatch(CartActions.resetCart());
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      () => true
    );
    return () => {
      //dispatch(CartActions.resetCart());
      backHandler.remove();
    };
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      setTabVisible(true);
    });
    return unsubscribe;
  }, []);

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollContainer}
      >
        <Image
          style={styles.orderSuccessImage}
          source={require("../../assets/images/orders/order_Confirmation.png")}
        />
        <View style={styles.succsessTextContainer}>
          <Image
            style={styles.successIcon}
            source={require("../../assets/images/orders/success_icon.png")}
          />
          <Text style={styles.successText}>Order Successful!</Text>
        </View>
        <Text style={styles.orderNo}>Order: #{orderNumber}</Text>
        <Text style={styles.orderMsg}>
          Your order is processing. Click on View Order Info below to see the
          updated progress for your order.
        </Text>
        <TouchableOpacity onPress={onTrackorder} style={styles.trackOrderBtn}>
          <Text style={styles.trackOrderBtnText}>Track Order on Map</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onOrderInfo} style={styles.viewOrderBtn}>
          <Text style={styles.viewOrderBtnText}>View Order Info</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

export default OrderSuccessScreen;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "#fff",
    width: "100%",
    height: "100%",
  },
  scrollContainer: {
    alignItems: "center",
    paddingHorizontal: 24,
    paddingVertical: 16,
  },
  orderSuccessImage: {
    height: 200,
    width: 325,
  },
  succsessTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 16,
  },
  successIcon: {
    height: 24,
    width: 24,
    marginRight: 12,
  },
  successText: {
    color: "#4caf50",
    fontSize: 26,
    fontWeight: "600",
  },
  orderNo: {
    color: "#37474f",
    fontSize: 20,
    fontWeight: "600",
    marginTop: 16,
  },
  orderMsg: {
    color: "#546e7a",
    fontSize: 18,
    marginVertical: 16,
    textAlign: "center",
  },
  trackOrderBtn: {
    height: 45,
    backgroundColor: "#673ab7",
    alignSelf: "stretch",
    borderRadius: 8,
    paddingVertical: 10,
    marginTop: 20,
    alignItems: "center",
  },
  viewOrderBtn: {
    height: 45,
    backgroundColor: "#fff",
    alignSelf: "stretch",
    marginTop: 20,
    borderRadius: 8,
    paddingVertical: 10,
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#a38862",
  },
  trackOrderBtnText: {
    color: "#fff",
    fontSize: 16,
    fontWeight: "600",
  },
  viewOrderBtnText: {
    color: "#263238",
    fontSize: 16,
    fontWeight: "600",
  },
});
