import React, { useState, useEffect, useCallback } from "react";
import { View, SafeAreaView, StyleSheet } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { useSelector } from "react-redux";
import { useTabVisible } from "../../contexts/navigation";
import * as CartActions from "../../store/action/cart";
import CallToStore from "../Components/CallToStore";
import Modal from "../Components/Modal";
import CartHeader from "../Components/CartHeader";
import ItemToCart from "../Components/ItemToCart";
import CartDetails from "../Components/CartDetails";
import CheckoutDetails from "../Components/CheckoutDetails";
import MyOrdersEmpty from "../Components/MyOrdersEmpty";
import { useFocusEffect } from "@react-navigation/native";

const CartScreen = ({ navigation }) => {
  const { setTabVisible } = useTabVisible();
  const [showModal, setShowModal] = useState(false);
  const cart = useSelector((state) => state.cart.cartItems);
  const modalClick = () => {
    setShowModal(true);
  };

  const goToCheckout = useCallback(() => {
    setTabVisible(false);
    setTimeout(() => {
      navigation.navigate("DrawerNavigationRoutes", {
        screen: "homeScreenStack",
        params: {
          screen: "Cart",
          params: {
            screen: "checkoutVIPScreen",
            params: {
              checkOut: true,
            },
          },
        },
      });
    }, 1);
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      setTabVisible(true);
    }, [setTabVisible])
  );

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <CallToStore onPress={modalClick} />,
    });
  });
  const navigateToPizzaMenu = () => {
    navigation.navigate("DrawerNavigationRoutes", {
      screen: "homeScreenStack",
      params: {
        screen: "PizzaMenu",
        params: {
          screen: "PizzaMenuScreenHome",
        },
      },
    });
  };
  return (
    <SafeAreaView style={styles.topContainer}>
      {cart && cart?.length == 0 && (
        <MyOrdersEmpty
          onPress={navigateToPizzaMenu}
          title={"Your cart is Empty"}
          info={"Looks like you have not added anything to your cart yet"}
        />
      )}
      {cart && cart?.length > 0 && (
        <SafeAreaView style={styles.topContainer}>
          <ScrollView
            style={styles.scrollView}
            showsVerticalScrollIndicator={false}
          >
            {cart.map((item) => {
              return (
                <View key={item.uuid} style={styles.container}>
                  <CartHeader name={item.name} id={item.uuid} />
                  <ItemToCart cart={item.ingredients} />
                  <CartDetails
                    quantity={item.quantity}
                    price={item.price}
                    id={item.id}
                    uuid={item.uuid}
                    navigation={navigation}
                  />
                </View>
              );
            })}
            <CheckoutDetails onPress={goToCheckout} cart={cart} />
          </ScrollView>
        </SafeAreaView>
      )}
      {showModal && (
        <Modal
          showModal={showModal}
          setVisible={() => {
            setShowModal(false);
          }}
        />
      )}
    </SafeAreaView>
  );
};

export default CartScreen;

const styles = StyleSheet.create({
  topContainer: { flex: 1, paddingTop: 5, backgroundColor: "#fafcfe" },
  scrollView: { marginHorizontal: 15, paddingVertical: 2 },
  container: {
    marginBottom: 15,
    padding: 20,
    borderWidth: 0.5,
    borderColor: "#fff",
    backgroundColor: "#fff",
    borderRadius: 5,
    shadowOpacity: 0.03,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    elevation: 1,
  },
});
