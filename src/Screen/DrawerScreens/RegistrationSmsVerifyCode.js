import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Platform,
} from "react-native";
import { connect } from "react-redux";
import BannerAvatar from "../Components/BannerAvatar";
import Toast from "react-native-toast-message";
import ConfirmationCodeField from "../Components/ConfirmationCodeField";
import {
  smsOtpVerification,
  resendPhoneOtpVerification,
} from "../../store/action/userRegistrationAction";
import PhoneImg from "../Components/svg/Mobile";
import GoldLogo from "../Components/svg/CBLogoGoldNoFill";
import SmsRetriever from "react-native-sms-retriever";
import Loader from "../Components/Loader";
import { useDispatch } from "react-redux";
import { RESET_ALL_USER_REGISTRATION } from "../../store/action/userRegistrationAction";

const RegistrationSmsVerifyCode = (props) => {
  const { phoneNo, emailId, userId } = props.route.params;
  const dispatch = useDispatch();
  const {
    otpResp,
    verifyOTP,
    loading,
    registeredUser,
    resendOTP,
    otpResendResp,
  } = props;

  Toast.hide();
  const [isError, setIsError] = useState(false);
  const [isEnable, setEnable] = useState(false);
  const [otpReceived, setOTPReceived] = useState("");
  const handleSubmitPress = () => {
    if (!isError && isEnable) {
      setTimeout(() => {
        props.navigation.push("RegistrationSuccess");
      }, 2000);
    }
  };
  useEffect(() => {
    (async () => {
      try {
        if (Platform.OS === "android") {
          const registered = await SmsRetriever.startSmsRetriever();
          if (registered) {
            SmsRetriever.addSmsListener((event) => {
              const otp = event?.message?.match(/\b\d{4}\b/)[0];
              if (otp) {
                setOTPReceived(otp);
                SmsRetriever.removeSmsListener();
              }
            });
          }
        }
      } catch (error) {
        console.log("--OTP Error----", JSON.stringify(error));
      }
    })();
    if (Platform.OS === "android") {
      return () => SmsRetriever.removeSmsListener();
    }
  });

  const onResendCodePress = () => {
    resendOTP(emailId, phoneNo);
  };
  useEffect(() => {
    if (otpResp?.successBody?.message) {
      Toast.show({
        type: "success",
        text1: "Success",
        text2: otpResp?.successBody?.message,
      });
      setIsError(false);
      setEnable(true);
      props.navigation.push("RegistrationSuccess", {
        bannerImageURL: "https://mivprodblob.blob.core.windows.net/images/f50c6361-9eb8-415a-93ef-0228c4dbced8",  //otpResp?.successBody?.BannerImageURL,
        bannerText: "Proudly Serving Since"  //, otpResp?.successBody?.BannerText
      });
    } else if (otpResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: otpResp?.errorBody,
      });
      setIsError(true);
      setEnable(false);
      dispatch({ type: RESET_ALL_USER_REGISTRATION });
    }
  }, [otpResp, props.navigation]);

  useEffect(() => {
    if (otpResendResp?.successBody?.message) {
      Toast.show({
        type: "success",
        text1: "Success",
        text2: otpResendResp?.successBody?.message,
      });
      dispatch({ type: RESET_ALL_USER_REGISTRATION });
    } else if (otpResendResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: otpResendResp?.errorBody,
      });
      dispatch({ type: RESET_ALL_USER_REGISTRATION });
      setIsError(true);
    }
  }, [otpResendResp]);

  const getValue = React.useCallback((phoneOTP) => {
    if (phoneOTP) {
      verifyOTP(userId, phoneNo, phoneOTP);
    }
  }, []);
  return (
    <ScrollView>
      <SafeAreaView style={styles.root}>
        <Loader loading={loading} />
        <BannerAvatar touchableOpacity />
        <View style={styles.mobileLayout}>
          <PhoneImg height={120} width={150} viewBox="0 0 130 950" />
        </View>
        <View style={styles.goldLogoLayout}>
          <GoldLogo height={37} width={40} viewBox="10 0 90 126" />
        </View>
        <View style={styles.bodyLayout}>
          <View style={styles.bodyLayoutInner}>
            <Text style={styles.primaryText}>
              An SMS verification code has been sent to:
            </Text>
            <Text style={styles.phoneText}>{phoneNo}</Text>
            {!isError ? (
              <Text style={styles.subText}>
                Please enter the 4 digit code sent to your device
              </Text>
            ) : null}
            {isError ? (
              <Text style={styles.errorTextStyle}>
                The code entered is incorrect. Please re-enter the code and try
                again.
              </Text>
            ) : null}
            <View style={styles.SectionStyle}>
              <ConfirmationCodeField getValue={getValue} autoFill={otpReceived} />
            </View>
            <View style={styles.centerflex}>
            <Text style={styles.resendCode} onPress={onResendCodePress}>
              Resend Code
            </Text>
            </View>
            <TouchableOpacity
              style={isEnable ? styles.buttonStyleEnabled : styles.buttonStyle}
              activeOpacity={0.5}
              onPress={handleSubmitPress}
            >
              <Text style={styles.buttonTextStyle}>Next</Text>
            </TouchableOpacity>
          </View>
          <Text style={{ backgroundColor: "white", marginBottom: 120 }}></Text>
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

export default connect(
  (state) => ({
    otpResp: state.registeredUser.otpResp,
    loading: state.loading.loading,
    registeredUser:
      state.registeredUser.registeredUser ||
      state?.loggedInUser?.loggedInUser?.userDetails,
    otpResendResp: state.registeredUser.otpResendResp,
  }),
  (dispatch, ownProps) => ({
    verifyOTP: (userId, phoneNo, phoneOTP) =>
      dispatch(smsOtpVerification(userId, phoneNo, phoneOTP)),
    resendOTP: (email, phoneNumber) =>
      dispatch(resendPhoneOtpVerification(email, phoneNumber)),
  })
)(RegistrationSmsVerifyCode);
const styles = StyleSheet.create({
  root: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
  },
  bodyLayout: {
    color: "#546e7a",
    fontFamily: "Roboto",
    marginTop: -70,
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    width: "90%",
  },
  bodyLayoutInner: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
  },
  mobileLayout: {
    bottom: 69,
    right: 21,
  },
  goldLogoLayout: {
    bottom: 166,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    width: 30,
    height: 30,
    borderRadius: 100,
  },
  phoneText: {
    width: "100%",
    fontSize: 18,
    textAlign: "center",
    fontWeight: "300",
    color: "#546e7a",
    marginBottom: 10,
    fontFamily: "Roboto",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  centerflex: {
    textAlign: "center"
  },
  primaryText: {
    fontSize: 18,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  secondaryText: {
    marginLeft: 80,
    marginTop: 25,
    fontSize: 16,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#a38862",
    fontFamily: "Roboto",
  },

  SectionStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  inputStyle: {
    color: "black",
    fontSize: 18,
    fontWeight: "normal",
    backgroundColor: "#ffffff",
    marginLeft: 20,
    height: 70,
    width: 55,
    borderWidth: 1.5,
    borderRadius: 5,
    borderColor: "#a38862",
  },
  inputerrorStyle: {
    color: "black",
    fontSize: 18,
    fontWeight: "normal",
    backgroundColor: "red",
    marginLeft: 20,
    height: 70,
    width: 55,
    borderWidth: 1.5,
    borderRadius: 5,
    borderColor: "#f44336",
    textAlign: "center",
  },
  resendCode: {
    width: "100%",
    marginTop: "5%",
    fontSize: 17,
    fontWeight: "300",
    color: "#2196f3",
    fontFamily: "Roboto",
  },
  codeErrorText: {
    marginLeft: 60,
    marginTop: 10,
    fontSize: 8,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  subText: {
    width: "100%",
    color: "#546e7a",
    fontFamily: "Roboto",
    fontWeight: "400",
    fontSize: 17,
    letterSpacing: 0.1,
    textAlign: "center",
  },
  errorTextStyle: {
    color: "red",
    width: "100%",
    fontFamily: "Roboto",
    fontWeight: "400",
    fontSize: 17,
    letterSpacing: 0.1,
    textAlign: "center",
  },
  successTextStyle: {
    color: "white",
    textAlign: "center",
    fontSize: 18,
    padding: 30,
  },
  buttonStyle: {
    backgroundColor: "#78909c",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    height: 50,
    alignItems: "center",
    width: "100%",
    borderRadius: 5,
    marginTop: 25,
  },
  buttonStyleEnabled: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    height: 50,
    alignItems: "center",
    width: "100%",
    borderRadius: 5,
    marginTop: 25,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 12,
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: "Roboto",
    textAlign: "center",
  },
});
