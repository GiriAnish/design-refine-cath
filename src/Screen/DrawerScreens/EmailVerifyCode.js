import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { ScrollView, StyleSheet, View, Text, SafeAreaView ,TouchableWithoutFeedback } from "react-native";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import ConfirmationCodeField from "../Components/ConfirmationCodeField";
import { otpVerify } from "../../store/action/forgotpwdAction";
import Toast from "react-native-toast-message";
import { useDispatch } from "react-redux";
import { RESET_FORGET_ALL_PASSWORD ,pwdResetReq,PASSWORD_RESET_REQUEST_DEFAULT} from "../../store/action/forgotpwdAction";
import MailEnvelope from "../Components/svg/MailEnvelop";
import GoldLogo from "../Components/svg/CBLogoGoldNoFill";
import Loader from "../Components/Loader";

const EmailVerifyCode = (props) => {
  Toast.hide();
  const dispatch = useDispatch();
  const { emailId } = props.route.params;
  const { finalLocalotpResp, loading, verifyOTPFunc,resendEmailOtp,passwordResetReq } = props;
  const [isError, setIsError] = useState(false);
  const handleSubmitPress = () => {
    resendEmailOtp(emailId);
    };
    useEffect(()=>{
      if(passwordResetReq?.successBody?.message){
        Toast.show({
        type: "success",
        text1: "Success",
        text2: passwordResetReq?.successBody?.message,
      });
    setIsError(false);
    dispatch({type:PASSWORD_RESET_REQUEST_DEFAULT})
      }
        else if(passwordResetReq?.errorBody){
        Toast.show({
        type: "success",
        text1: "Success",
        text2: passwordResetReq?.errorBody,
      });
      dispatch({type:PASSWORD_RESET_REQUEST_DEFAULT})
      }
    })
  useEffect(() => {
    if (finalLocalotpResp?.successBody?.message) {
      Toast.show({
        type: "success",
        text1: "Success",
        text2: finalLocalotpResp?.successBody?.message,
      });
      setIsError(false);
      props.navigation.navigate("SettingsResetPassword", {
        emailId: emailId,
      });
      dispatch({ type: RESET_FORGET_ALL_PASSWORD });
    } else if (finalLocalotpResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: finalLocalotpResp?.errorBody,
      });
      setIsError(true);
      dispatch({ type: RESET_FORGET_ALL_PASSWORD });
    }
  }, [finalLocalotpResp]);

  const getValue = React.useCallback((phoneOTP) => {
    if (phoneOTP) {
      verifyOTPFunc(emailId, phoneOTP);
    }
  }, []);

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView  style={{width: "100%"}} keyboardShouldPersistTaps="handled">
        {loading && <Loader />}
        <View style={styles.topContainer}>
          <BannerAvatar touchableOpacity />
          <View style={styles.absCont}>
            <MailEnvelope height={250} width={107} viewBox="-4 24 430 240" />
          </View>
          <View style={styles.goldlogo}>
            <GoldLogo height={50} width={50} viewBox="10 0 100 126" />
          </View>
        </View>
        <View style={styles.bodyLayout}>
          <View style={styles.subContent}>
            <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
              <Text style={styles.instructText}>We sent an email to </Text>
              <Text style={styles.email}>{emailId}. </Text>
              <Text style={styles.instructText}>
                Please follow the instructions to reset your password.
              </Text>
            </View>
            <View style={styles.codeContainer}>
              <ConfirmationCodeField
                getValue={getValue}
                title={"Enter Code from Email"}
              />
            </View>
            
            <View style={styles.bottomTextLayout}>
              <Text style={styles.secondaryText}>
                Did not receive a password reset email?
              </Text>
              <TouchableWithoutFeedback
              onPress={handleSubmitPress}>
                <Text
                style={styles.diffEmailText}
              >
                Resend Verification OTP
              </Text>
              </TouchableWithoutFeedback>
            
            </View>
          </View>
        </View>
        <FooterBanner
          touchableOpacity
          imageSource={require("../../assets/images/bottom_banner.png")}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default connect(
  (state) => ({
    finalLocalotpResp: state.forgotPwd.validateOTPRes,
    loading: state.loading.loading,
    passwordResetReq: state.forgotPwd.passwordResetReq,

  }),
  (dispatch, ownProps) => ({
    verifyOTPFunc: (emailId, getValue) =>
      dispatch(otpVerify(emailId, getValue)),
    resendEmailOtp: (emailId) => dispatch(pwdResetReq(emailId)),
  })
)(EmailVerifyCode);

const styles = StyleSheet.create({
  codeContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  root: {
    backgroundColor: "#fff",
    alignItems: "center",
    flex: 1,
    width: "100%",
  },
  topContainer: {
    height: 10,
    width: "100%",
  },
  absCont: {
    height: 185,
    position: "relative",
    bottom: 100,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  goldlogo: {
    bottom: 225,
    alignSelf: "center",
  },
  bodyLayout: {
    color: "#546e7a",
    fontFamily: "Roboto",
    width: "100%",
    justifyContent: "center",
    padding: 20,
  },
  subContent: {
    marginTop: 130,
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
  },
  bottomTextLayout: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    marginTop: 10,
    marginBottom: -30,
  },
  instructText: {
    fontSize: 20,
    fontWeight: "400",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  email: {
    fontSize: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
    fontWeight: "bold",
  },
  secondaryText: {
    fontSize: 18,
    fontWeight: "300",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  diffEmailText: {
    fontSize: 18,
    marginTop: 10,
    fontWeight: "300",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#2196f3",
    fontFamily: "Roboto",
  },
});
