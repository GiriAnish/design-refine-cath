import React, { useState, useEffect, useRef } from "react";
import {
  SafeAreaView,
  StyleSheet,
  View,
  Image,
  Text,
  ScrollView,
  TouchableWithoutFeedback,
  Animated,
} from "react-native";
import CONST from "../../data/const";
import Loader from "../Components/Loader";
import RightLogo from "../Components/svg/CBLogoGoldNoFill";
import LeftLogo from "../Components/svg/CBLogoPurpleNoFill.js";
const HEADER_MAX_HEIGHT = 44;
const HEADER_MIN_HEIGHT = 44;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const RegBuilder = (props) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  // Animation related code starts here
  const offset = useRef(new Animated.Value(0)).current;

  const scrollY = useRef(new Animated.Value(0)).current;
  const headerTranslateY = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE],
    extrapolate: "clamp",
  });

  const imageOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0],
    extrapolate: "clamp",
  });
  const imageTranslateY = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 100],
    extrapolate: "clamp",
  });

  const titleScale = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0.9],
    extrapolate: "clamp",
  });
  const titleTranslateY = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -130],
    extrapolate: "clamp",
  });
  // Animation related code ends here

  useEffect(() => {
    setLoading(true);
    fetch(
      CONST.baseUrl +
        "api/v1/accounts/accounttype?accountTypeId=" +
        props?.route?.params?.id
    )
      .then((response) => response.json())
      .then((json) => {
        setData(json.details);
        setLoading(false);
      })
      .catch((error) => {
        setData([]);
        setLoading(false);
      });
  }, [props]);

  useEffect(() => {
    // If data length is 1
    if (data?.accounts && data?.accounts?.length == 1) {
      props.navigation.navigate("RegForm", {
        logoUrl: data.accounts[0].logoURL,
        accountId: data.accounts[0].id,
        label: data.accounts[0].name,
        year: data.accounts[0].servingYear,
        bannerImageURL: data.bannerImageURL,
        bannerText: data.bannerText,
      });
    }
  }, [data]);

  const AccounType = (props) => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          props.navigation.navigate("RegForm", {
            logoUrl: props.logoUrl,
            accountId: props.id,
            label: props.label,
            year: props.year,
            bannerImageURL: props.bannerImageURL,
            bannerText: props.bannerText,
          });
        }}
      >
        <View style={styles.accountType}>
          <View style={styles.accountImage}>
            <RightLogo height={40} width={40} viewBox="0 0 130 130" />
          </View>
          {props?.logoUrl?.length > 0 && (
            <View style={styles.accountImageCen}>
              <Image
                source={{ uri: props.logoUrl }}
                style={{ width: 200, height: 60 }}
                resizeMode={"contain"}
              />
            </View>
          )}
          {props?.logoUrl?.length == 0 && (
            <View style={styles.textCont}>
              <Text style={styles.accountText}>{props.label}</Text>
            </View>
          )}
          <View style={styles.accountImage}>
            <LeftLogo height={41} width={41} viewBox="-5 -1 100 100" />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <SafeAreaView style={styles.saveArea}>
      <Animated.ScrollView
        contentContainerStyle={{
          paddingTop: HEADER_MAX_HEIGHT,
          alignItems: "center",
        }}
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
          { useNativeDriver: true }
        )}
      >
        <View style={styles.horiPad}>
          {data?.accounts?.length > 0 &&
            data?.accounts.filter(item => item?.active === true).map((item) => {
              return (
                <AccounType
                  id={item.id}
                  label={item.name}
                  year={item.servingYear}
                  key={item.id}
                  logoUrl={item.logoURL}
                  bannerImageURL={data?.bannerImageURL}
                  bannerText={data?.bannerText}
                  {...props}
                />
              );
            })}
          {loading && <Loader />}
        </View>
      </Animated.ScrollView>
      <View style={styles.header}>
        <Text style={styles.title}>
          {"Select Your " + props?.route?.params?.label}
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default RegBuilder;

const styles = StyleSheet.create({
  saveArea: {
    flex: 1,
    backgroundColor: "#673ab7",
  },
  header: {
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: "#673ab7",
    overflow: "hidden",
    height: HEADER_MAX_HEIGHT,
  },
  topBar: {
    marginTop: 40,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: "white",
    fontSize: 20,
  },
  title1: {
    fontSize: 25,
    color: "#fff",
    fontWeight: "600",
    textAlign: "center",
  },
  titleCont1: {
    justifyContent: "center",
    alignContent: "center",
    width: "75%",
    height: 100,
    marginTop: 50,
  },
  accountType: {
    height: 70,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignContent: "center",
    width: "100%",
    flex: 1,
    flexDirection: "row",
    marginVertical: 10,
    borderRadius: 5,
  },
  horiPad: {
    paddingHorizontal: 20,
  },
  accountImage: {
    alignSelf: "center",
    width: "20%",
    alignItems: "center",
  },
  accountImageCen: {
    alignSelf: "center",
    width: "60%",
    alignItems: "center",
  },
  accountText: {
    fontSize: 23,
    color: "#263238",
    alignSelf: "center",
  },
  textCont: {
    alignSelf: "center",
    width: "60%",
  },
});
