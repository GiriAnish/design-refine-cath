import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
} from "react-native";
import { connect, useDispatch } from "react-redux";
import { TextInput as NewTextInput } from "react-native-paper";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import {
  resetPwdCreation,
  RESET_FORGET_ALL_PASSWORD,
} from "../../store/action/forgotpwdAction";
import Toast from "react-native-toast-message";
import { useTabVisible } from "../../contexts/navigation";
import Loader from "../Components/Loader";
import Lock from "../Components/svg/lock";
import GoldLogo from "../Components/svg/CBLogoGoldNoFill";

const SettingsResetPassword = (props) => {
  const { setTabVisible } = useTabVisible();
  const { emailId } = props.route.params;
  const dispatch = useDispatch();
  const { finalPwdResetResp, loading, pwdResetFunc } = props;
  const [isError, setIsError] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [form, setForm] = useState({});
  const passwordRef = useRef();
  const handleSubmitPress = () => {
    if (
      form.password === "" ||
      form.confirmPassword === "" ||
      form.password === undefined ||
      form.confirmPassword === undefined ||
      form.password !== form.confirmPassword
    ) {
      setIsError(true);
      return;
    }
    var payload = {
      newPassword: form.password,
      newPasswordConfirm: form.confirmPassword,
    };
    pwdResetFunc(emailId, payload);
    setIsError(false);
  };

  useEffect(() => {
    if (finalPwdResetResp?.successBody?.message) {
      setTabVisible(false);
      props.navigation.navigate("SettingsResetPasswordSuccess");
      dispatch({ type: RESET_FORGET_ALL_PASSWORD });
    } else if (finalPwdResetResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: finalPwdResetResp?.errorBody,
        numberofLines: 12
      });
      dispatch({ type: RESET_FORGET_ALL_PASSWORD });
    }
  }, [finalPwdResetResp]);

  const EyeComp = () => {
    return (
      <NewTextInput.Icon
        name="eye-outline"
        color="#78909c"
        onPress={() => {
          setShowPassword((prevState) => {
            return !prevState;
          });
        }}
      />
    );
  };

  const TextInputComp = React.useCallback(
    ({ right, placeholderLabel, id, secure, ref, label }) => {
      return (
        <NewTextInput
          style={styles.inputStyle}
          returnKeyType={!secure ? "next" : "default"}
          onSubmitEditing={() => {
            passwordRef.current.focus();
          }}
          ref={passwordRef}
          placeholder={placeholderLabel}
          secureTextEntry={secure && !showPassword ? true : false}
          placeholderTextColor="#546e7a"
          fontSize="40"
          right={right == "eye" ? EyeComp() : ""}
          underlineColor="transparent"
          onChangeText={(text) => {
            setForm((prevState) => ({ ...prevState, [id]: text }));
          }}
          value={label}
        />
      );
    },
    [showPassword, props]
  );
  return (
    <View style={styles.root}>
      <ScrollView keyboardShouldPersistTaps="handled">
        <View>
          <Loader loading={loading} />
          <BannerAvatar touchableOpacity />
          <View style={styles.lockLayout}>
            <Lock height={100} width={110} viewBox="0 18 250 200" />
          </View>
          <View style={styles.goldLogoLayout}>
            <GoldLogo height={36} width={40} viewBox="10 0 100 127" />
          </View>
          <View style={styles.bodyLayout}>
            <View>
              <Text style={styles.secondaryText}>
                Reset password for your account
              </Text>
              <View style={styles.SectionStyle}>
                <TextInputComp
                  style={styles.inputStyle}
                  right={"eye"}
                  placeholderLabel={"Enter New Password"}
                  id={"password"}
                  secure={true}
                  label={form["password"]}
                />
              </View>
              <Text style={isError ? styles.errorTextStyle : styles.subText}>
                Must be at least 8 characters, 1 capital letter, 1 special character
              </Text>
              <View style={styles.SectionStyle}>
                <TextInputComp
                  style={styles.inputStyle}
                  right={"eye"}
                  placeholderLabel={"Retype New Password"}
                  id={"confirmPassword"}
                  secure={true}
                  label={form["confirmPassword"]}
                />
              </View>
              <Text style={isError ? styles.errorTextStyle : styles.subText}>
                Both passwords must match
              </Text>
              <TouchableOpacity
                style={styles.buttonStyle}
                activeOpacity={0.5}
                onPress={handleSubmitPress}
              >
                <Text style={styles.buttonTextStyle}>Reset Password</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.logoContainer}>
            <FooterBanner
              touchableOpacity
              imageSource={require("../../assets/images/bottom_banner.png")}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default connect(
  (state) => ({
    finalPwdResetResp: state.forgotPwd.passwordResetResponse,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    pwdResetFunc: (emailId, payload) =>
      dispatch(resetPwdCreation(emailId, payload)),
  })
)(SettingsResetPassword);

const styles = StyleSheet.create({
  root: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  lockLayout: {
    bottom: 60,
    left: 5,
    alignSelf: "center",
  },
  goldLogoLayout: {
    bottom: 118,
    left: 2,
    backgroundColor: "white",
    alignItems: "center",
    alignSelf: "center",
    width: 30,
    borderRadius: 40 / 2,
  },
  bodyLayout: {
    color: "#546e7a",
    fontFamily: "Roboto",
    marginTop: -80,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 15,
  },
  primaryText: {
    fontSize: 14,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  secondaryText: {
    marginTop: 25,
    fontSize: 20,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#a38862",
    fontFamily: "Roboto",
  },
  SectionStyle: {
    flexDirection: "row",
    height: 55,
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  logoContainer: {
    alignItems: "center",
    marginTop: -25,
  },
  inputStyle: {
    color: "black",
    fontSize: 16,
    fontWeight: "normal",
    backgroundColor: "#ede7f6",
    height: 50,
    width: "100%",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "stretch",
    borderWidth: 5,
    borderColor: "#ede7f6",
    borderRadius: 5,
  },
  subText: {
    marginLeft: 17,
    marginTop: 10,
    fontSize: 16,
    fontWeight: "300",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  errorTextStyle: {
    color: "red",
    textAlign: "left",
    fontSize: 16,
    marginLeft: 17,
    marginTop: 10,
    fontWeight: "normal",
  },
  successTextStyle: {
    color: "white",
    textAlign: "center",
    fontSize: 18,
    padding: 30,
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#673ab7",
    height: 45,
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 14,
    fontWeight: "bold",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
});
