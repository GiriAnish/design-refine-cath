import React from "react";
import { StyleSheet, View, SafeAreaView } from "react-native";
import { WebView } from "react-native-webview";
import CONST from "../../data/const";

const TermsAndCondition = (props) => {
  return (
    <SafeAreaView style={styles.SafeAreaViewContainer}>
      <View style={{ height: "100%" }}>
        <WebView
          javaScriptEnabled={true}
          source={{ uri: CONST.termsAndConditionsUrl }}
          style={styles.webView}
        />
      </View>
    </SafeAreaView>
  );
};

export default TermsAndCondition;

const styles = StyleSheet.create({
  SafeAreaViewContainer: {
    flex: 1,
    backgroundColor: "#fefefe",
    padding: 5,
  },
  webView: {
    height: "100%",
    width: "100%",
  },
});