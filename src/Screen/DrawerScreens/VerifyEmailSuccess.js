import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  SafeAreaView,
  Text,
  TouchableWithoutFeedback,
} from "react-native";
import FooterBanner from "../Components/BannerFooter";
import MailEnvelope from "../Components/svg/MailEnvelop";
import CheckCircle from "../Components/svg/CheckCircle";
import MailEnvlope from "../Components/MailEnvlope";
import Loader from "../Components/Loader";
import Lock from "../Components/svg/lock";
import GoldLogo from "../Components/svg/CBLogoGoldNoFill";
import BannerAvatar from "../Components/BannerAvatar";

const VerifyEmailSuccess = (props) => {
  const { emailId, userId } = props.route.params;

  const handleSubmitPress = () => {
    props.navigation.navigate("RegistrationCreatePassword", {
      emailId: emailId,
      userId: userId,
    });
  };
  return (
      <SafeAreaView style={styles.root}>
        <View style={styles.topContainer}>
          <BannerAvatar touchableOpacity />
          <View style={styles.absCont}>
            <MailEnvelope height={250} width={107} viewBox="-4 24 430 240" />
          </View>
          <View style={styles.goldlogo}>
            <CheckCircle color={"#4caf50"} height={60} width={60} />
          </View>
        </View>
        <View style={styles.bodyLayout}>
          <View style={{ margin: 20, alignItems: "center" }}>
            <Text style={{ color: "#546e7a", fontSize: 17, textAlign: "center" }}>
              Your email has been successfully verified!
            </Text>
            <Text
              style={{
                color: "#263238",
                fontSize: 22,
                fontWeight: "600",
                marginTop: 20,
              }}
            >
              {emailId}
            </Text>
            <View style={styles.buttonCont}>
              <TouchableWithoutFeedback onPress={handleSubmitPress}>
                <View style={styles.checkoutContainer}>
                  <Text style={styles.checkoutText}>Next</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
          <View
            style={{
              alignContent: "center",
              width: "100%",
              alignItems: "center",
              justifyContent: "center",
              height: 190,
              marginBottom: 20,
            }}
            >
              <FooterBanner
                touchableOpacity
                imageSource={require("../../assets/images/bottom_banner.png")}
              />
            </View>
          </View>
      </SafeAreaView>
  );
};

export default VerifyEmailSuccess;

const styles = StyleSheet.create({
  root: {
    backgroundColor: "#fff",
    alignItems: "center",
    flex: 1,
    width: "100%",
  },
  lockLayout: {
    bottom: 40,
    left: 5,
  },
  logo: {
    marginBottom: 22,
    alignItems: "center",
    top: 10,
  },
  goldlogo: {
    bottom: 225,
    alignSelf: "center",
  },
  goldLogoLayout: {
    bottom: 118,
    left: 2,
    backgroundColor: "white",
    alignItems: "center",
    width: 30,
    borderRadius: 120,
  },
  scrollView: {
    backgroundColor: "#fff",
  },
  topContainer: {
    height: 150,
    width: "100%",
  },
  bodyLayout: {
    color: "#546e7a",
    fontFamily: "Roboto",
    width: "100%",
    justifyContent: "center",
    padding: 20,
  },
  absCont: {
    height: 185,
    position: "relative",
    bottom: 100,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  checkCircle: {
    bottom: 53,
    left: 173,
    backgroundColor: "white",
    alignItems: "center",
    width: 45,
    borderRadius: 150,
    height: 47,
    justifyContent: "center",
  },

  buttonCont: { alignItems: "center", marginTop: 20, width: "100%" },
  checkoutContainer: {
    width: "100%",
    height: 45,
    backgroundColor: "#673ab7",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  checkoutText: { fontSize: 16, color: "#fff", fontWeight: "600" },
  logo: {
    width: "90%",
    height: undefined,
    aspectRatio: 1,
  },
});
