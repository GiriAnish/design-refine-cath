import React, { useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";
import {
  CardField,
  useStripe,
  initStripe,
  useConfirmPayment,
} from "@stripe/stripe-react-native";
import {
  ScrollView,
  Button,
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
} from "react-native";
import {
  getPaymentIntent,
  getSavedCards,
  confirmOrderPayment,
  RESET_PAYMENT_CONFIRMED,
} from "../../store/action/paymentAction";
import { FlatList } from "react-native-gesture-handler";
import { RadioButton } from "react-native-paper";
import CardVisa from "../Components/svg/CardVisa";
import Toast from "react-native-toast-message";
import Loader from "../Components/Loader";

export const CardContainer = (props) => {
  const { brand, last4Digit } = props;
  return (
    <View style={[styles.cardCont, styles.flexRow]}>
      <CardVisa width={78} height={30} color="#5e35b1" viewBox="0 0 780 500" />
      <Text>{last4Digit}</Text>
    </View>
  );
};

export const RowItem = (props) => {
  const [checked, setChecked] = React.useState("first");
  const [showButton, setShowButton] = useState(false);
  const [last4DigitText, setLast4DigitText] = useState("???");

  const {
    fetchPaymentIntent,
    selectedCardInfo,
    jwtToken,
    onPress,
    brand,
    last4Digit,
    field3,
    containerStyle,
    selected,
    id,
    ccId,
  } = props;

  const setButtonStatus = (id) => {
    setChecked(!showButton ? id : "");
    setShowButton(!showButton);
  };
  
  const handlePayPressExistingCards = (id) => {
    selectedCardInfo(id);
    setLast4DigitText(id);
  };
  return (
    <TouchableWithoutFeedback>
      <View style={[styles.rowItem, containerStyle]}>
        <View style={[styles.flexRow]}>
          <CardContainer
            brand={brand}
            last4Digit={last4Digit}
          />
          <RadioButton
            value="first"
            status={checked === id ? "checked" : "unchecked"}
            onPress={(id) => setButtonStatus(id)}
          />
        </View>
        { showButton && <View>

          <Button onPress={() => handlePayPressExistingCards(ccId)} title={last4DigitText} />
        </View>}
      </View>
    </TouchableWithoutFeedback>
  );
};
const MakePayment = (props) => {
  const [clientSecret, setClientSecret] = useState();
  const [paymentIntentDetails, setPaymentIntentDetails] = useState();
  const [cardDetails, setCardDetails] = useState();
  const [selectedCardId, setSelectedCardId] = useState();
  const [dummy, setDummy] = useState();
  const dispatch = useDispatch();
  const { orderUUID } = props?.route?.params;
  const {
    paymentConfirmed,
    clientSecretData,
    cardsData,
    fetchPaymentIntent,
    fetchSavedCards,
    confirmOrderPaymentDetails,
    userLogin,
    loadingLocal,
  } = props;
  const { jwtToken, email } = userLogin?.userDetails;
  const { confirmPayment, loading } = useConfirmPayment();
  const { initPaymentSheet, presentPaymentSheet } = useStripe();

  // Init Stripe
  useEffect(() => {
    fetchSavedCards(jwtToken);
  }, []);

  const onItemSelection = React.useCallback((id) => {
  }, []);

  
  const selectedCardInfo = (id) => {
    setSelectedCardId(id);  
  };

  const handlePayPressNewCard = () => {
    fetchPaymentIntent(orderUUID, jwtToken);
  };

  const submitPayment = async () => {
    const billingDetails = {
      email: email,
    };

    if(clientSecret != null) {
      /*
      const { paymentIntent, error } = await confirmPayment(clientSecret, {
        type: "Card",
        billingDetails,
        setupFutureUsage: "OffSession",
      });
      */


      const { error } = await initPaymentSheet({
        customerId: "cus_KzgCy88VA2IGSb",
        paymentIntentClientSecret: clientSecret,
        allowsDelayedPaymentMethods: false,
        merchantDisplayName: "Pizza"
      });
      if (!error) {
        setLoading(true);
      }

      /*
      const { paymentIntent, error } = await confirmPayment(clientSecret, {
        type: "Card",
        paymentMethodId: "pm_1KI7EvBNWb7Uy4rcGvmAtscN"
      });
      */

      setPaymentIntentDetails(error);
      setDummy(clientSecret);
      if (error) {
        Toast.show({
          type: "error",
          position: "top",
          bottomOffset: 210,
          autoHide: true,
          text1: "Error",
          text2: "Please try again.",
        });
      } else if (paymentIntent) {
        setPaymentIntentDetails(paymentIntent);
        confirmOrderPaymentDetails(orderUUID, jwtToken)
      }
    }
  };



  useEffect(() => {
    //submitPayment();
    fetchPaymentIntent(orderUUID, jwtToken);
  }, [orderUUID]);


  const openPaymentSheet = async () => {
    const { error } = await presentPaymentSheet();

    setDummy(error.message);
    if (error) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: error.message,
      });
    } else {
      Alert.alert('Success', 'Your order is confirmed!');
    }
  };


  // useEffect(() => {

    useEffect(() => {
      if (paymentConfirmed?.errorBody) {
        Toast.show({
          type: "error",
          position: "top",
          bottomOffset: 210,
          autoHide: true,
          text1: "Error",
          text2: paymentConfirmed?.errorBody,
        });
        dispatch({ type: RESET_PAYMENT_CONFIRMED });
      } else if (paymentConfirmed?.successBody) {
        //setDummy("dddddddddddddd " + paymentConfirmed?.successBody?.order?.orderNumber);
        dispatch({ type: RESET_PAYMENT_CONFIRMED });
        props.navigation.navigate("orderSuccessScreen", {
          orderNumber: paymentConfirmed?.successBody?.order?.orderNumber,
          id: paymentConfirmed?.successBody?.order?.id
        });
        
        
      }
  
    }, [paymentConfirmed]);

  useEffect(() => {

    if (clientSecretData?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: clientSecretData?.errorBody,
      });
    } else if (clientSecretData?.successBody) {
      setClientSecret(clientSecretData?.successBody?.clientSecret);
      
      submitPayment();

    }

  }, [clientSecretData]);

  useEffect(() => {
    if (cardsData) {
      //setResponseIs("ss");
      setCardDetails(cardsData);
    } else {
      //setResponseIs("ssddd");
    }

  }, [cardsData]);

  return (
    <ScrollView>
      <View style={styles.root}>
        <Loader loading={loading || loadingLocal} />
        {/* <View>
          <Text>
            {clientSecret} {JSON.stringify(props.route)} // {paymentIntentDetails?.description}  ===== 
            {JSON.stringify(paymentIntentDetails)}
          </Text>
          <Text>-----------------------------------------------</Text>
          <Text>{clientSecretData?.successBody?.clientSecret}</Text>
          <Text>-----------------------------------------------</Text>
          <Text>
            {orderUUID} {selectedCardInfo}
          </Text>
          <Text>
            {dummy}
          </Text>
        </View> */}
        {/* {cardDetails &&
          cardDetails.length > 0 &&
          cardDetails[0]?.cardDetails && (
            <Text style={styles.title}>Preferred Payment</Text>
          )}
        <FlatList
          showsVerticalScrollIndicator={false}
          data={
            (cardDetails &&
              cardDetails.length > 0 &&
              cardDetails[0]?.cardDetails) ??
            []
          }
          keyExtractor={(item, index) => `${item.brand}${index}`}
          renderItem={({ item }) => {
            return (
              <RowItem
                containerStyle={styles.itemStyle}
                brand={item.brand}
                last4Digit={`**** ${item.last4}`}
                id={cardDetails[0].customerId}
                onPress={onItemSelection}
                orderUUID={orderUUID}
                jwtToken={jwtToken}
                selectedCardInfo={selectedCardInfo}
                fetchPaymentIntent={fetchPaymentIntent}
              />
            );
          }}
        /> */}
        <Text style={styles.title}>Credit & Debit Cards</Text>
        <View style={[styles.root2, styles.boxing]}>
          <View style={{ width: "100%", }}>
            <CardField
              postalCodeEnabled={true}
              placeholder={{
                number: "4242 4242 4242 4242",
              }}
              cardStyle={{
                backgroundColor: "#FFFFFF",
                textColor: "#000000",
              }}
              style={{
                width: "100%",
                height: 50,
                marginVertical: 30,
              }}
              onCardChange={(cardDetails) => {
                console.log("cardDetails", cardDetails);
              }}
              onFocus={(focusedField) => {
                console.log("focusField", focusedField);
              }}
            />
          </View>
          <View style={{ width: "100%", }}>
            <Button
              style={{ width: "100%" }}
              //onPress={handlePayPressNewCard}
              onPress={openPaymentSheet}
              title="Add new card & Pay"
              disabled={loading || loadingLocal}
            />
          </View>
          <View><Text>{dummy}</Text></View>
        </View>
      </View>
    </ScrollView>
  );
};
export default connect(
  (state) => ({
    userLogin: state.loggedInUser.loggedInUser,
    loadingLocal: state.loading.loading,
    clientSecretData: state.payment.clientSecret,
    paymentConfirmed: state.payment.paymentConfirmed,
    cardsData: state.payment.cards,
  }),
  (dispatch, ownProps) => ({
    fetchPaymentIntent: (orderUUID, jwtToken) =>
      dispatch(getPaymentIntent(orderUUID, jwtToken)),
    fetchSavedCards: (jwtToken) =>
      dispatch(getSavedCards(jwtToken)),
    confirmOrderPaymentDetails: (orderUUID, jwtToken) =>
      dispatch(confirmOrderPayment(orderUUID, jwtToken)),
  })
)(MakePayment);

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "100%",
    backgroundColor: "#fafcfe",
  },
  root2: {
    alignItems: "center",
    justifyContent: "center",
    width: "90%",
    backgroundColor: "#fafcfe",
  },
  rowItem: {
    paddingHorizontal: 16,
    paddingVertical: 16,
    backgroundColor: "white",
    borderRadius: 5,
    shadowColor: "#000000",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    elevation: 2,
    marginHorizontal: 16,
    justifyContent: "space-between",
  },
  flexRow: {
    flexDirection: "row",
  },
  cardCont: {
    alignItems: "center",
    justifyContent: "center",
    width: "90%",
  },
  boxing: {
    paddingHorizontal: 16,
    paddingVertical: 16,
    backgroundColor: "white",
    borderRadius: 5,
    shadowColor: "#000000",
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 5,
    elevation: 2,
    marginHorizontal: 16,
    justifyContent: "space-between",
    marginBottom: 30,
  },
  commText: {
    width: 300,
    height: 55,
    fontSize: 21,
    color: "#3b018a",
    fontWeight: "400",
    fontFamily: "Arial",
    textAlign: "center",
  },
  itemStyle: {
    marginBottom: 16,
  },
  inputItemStyle: {
    marginBottom: 16,
  },
  title: {
    color: "#455a64",
    fontSize: 18,
    fontWeight: "600",
    justifyContent: "flex-start",
    marginTop: 20,
    marginBottom: 20,
  },
});
