import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { StyleSheet, View, Text, SafeAreaView,TouchableWithoutFeedback } from "react-native";
import FooterBanner from "../Components/BannerFooter";
import BannerAvatar from "../Components/BannerAvatar";
import ConfirmationCodeField from "../Components/ConfirmationCodeField";
import {
  emailOtpVerify,
  RESET_EMAIL_OTP_ACTION,
  
} from "../../store/action/userRegistrationAction";
import { pwdResetReq,PASSWORD_RESET_REQUEST_DEFAULT } from "../../store/action/forgotpwdAction";
import Toast from "react-native-toast-message";
import { useDispatch } from "react-redux";

const RegEmailVerify = (props) => {
  Toast.hide();
  const dispatch = useDispatch();
  const { emailId, userId } = props.route.params;
  const { finalLocalotpResp, loading, verifyOTPFunc,resendEmailOtp,passwordResetReq } = props;
  const [isError, setIsError] = useState(false);
 const handleSubmitPress = () => {
    resendEmailOtp(emailId);
    };
    useEffect(()=>{
      if(passwordResetReq?.successBody?.message){
        Toast.show({
          type: "success",
          text1: "Success",
          text2: passwordResetReq?.successBody?.message,
        });
        setIsError(false);
        dispatch({type:PASSWORD_RESET_REQUEST_DEFAULT})
      } else if(passwordResetReq?.errorBody){
        Toast.show({
        type: "success",
        text1: "Success",
        text2: passwordResetReq?.errorBody,
      });
      setIsError(true);
       dispatch({type:PASSWORD_RESET_REQUEST_DEFAULT})
      }
    })
  useEffect(() => {
    if (finalLocalotpResp?.successBody?.message) {
      setIsError(false);
      props.navigation.navigate("VerifyEmailSuccess", {
        emailId: emailId,
        userId: userId,
      });
      dispatch({ type: RESET_EMAIL_OTP_ACTION });
    } else if (finalLocalotpResp?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: finalLocalotpResp?.errorBody,
      });
      setIsError(true);
      dispatch({ type: RESET_EMAIL_OTP_ACTION });
    }
  }, [finalLocalotpResp]);

  const getValue = React.useCallback((phoneOTP) => {
    if (phoneOTP) {
      verifyOTPFunc(emailId, phoneOTP);
    }
  }, []);

  return (
    <SafeAreaView style={styles.root}>
      <View style={styles.topContainer}>
        <BannerAvatar
          touchableOpacity
          imageSource={require("../../assets/images/email-password.png")}
        />
      </View>
      <View style={styles.bodyLayout}>
        <View style={styles.subContent}>
          <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <Text style={styles.instructText}>We sent an email to </Text>
            <Text style={styles.email}>{emailId} </Text>
            <Text style={styles.instructText}>
              Please follow the instructions to verify your email address.
            </Text>
          </View>
          <View style={styles.codeContainer}>
            <ConfirmationCodeField
              getValue={getValue}
              title={"Enter Code from Email"}
            />
          </View>
          <View style={styles.bottomTextLayout}>
            <Text style={styles.secondaryText}>Did not receive code?{JSON.stringify(passwordResetReq?.successBody?.message)}</Text>
 <TouchableWithoutFeedback
            onPress={handleSubmitPress}>
               <Text style={styles.diffEmailText}>Resend verification code</Text>
            </TouchableWithoutFeedback>
           
          </View>
        </View>
      </View>
      <FooterBanner
        touchableOpacity
        imageSource={require("../../assets/images/bottom_banner.png")}
      />
    </SafeAreaView>
  );
};

export default connect(
  (state) => ({
    finalLocalotpResp: state.registeredUser.verifyEmailOtp,
    loading: state.loading.loading,
    passwordResetReq: state.forgotPwd.passwordResetReq,
  }),
  (dispatch, ownProps) => ({
    verifyOTPFunc: (emailId, getValue) =>
      dispatch(emailOtpVerify(emailId, getValue)),
      resendEmailOtp: (emailId) => dispatch(pwdResetReq(emailId)),
  })
)(RegEmailVerify);

const styles = StyleSheet.create({
  codeContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  root: {
    backgroundColor: "#fff",
    alignItems: "center",
    flex: 1,
    width: "100%",
  },
  topContainer: {
    height: 10,
    width: "100%",
  },
  bodyLayout: {
    color: "#546e7a",
    fontFamily: "Roboto",
    width: "100%",
    justifyContent: "center",
    padding: 20,
  },
  subContent: {
    marginTop: 130,
  },
  bottomTextLayout: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    marginTop: 10,
    marginBottom: -30,
  },
  instructText: {
    fontSize: 18,
    fontWeight: "400",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  email: {
    fontSize: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
    fontWeight: "bold",
  },
  secondaryText: {
    fontSize: 18,
    fontWeight: "300",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#546e7a",
    fontFamily: "Roboto",
  },
  diffEmailText: {
    fontSize: 18,
    marginTop: 10,
    fontWeight: "300",
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    color: "#2196f3",
    fontFamily: "Roboto",
  },
});
