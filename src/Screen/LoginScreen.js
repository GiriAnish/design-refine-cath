import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { getUserLogin, RESET_USER_LOGIN } from "../store/action/loginActions";
import { TextInput as NewTextInput } from "react-native-paper";
import Toast from "react-native-toast-message";
import Loader from "./Components/Loader";
import Logo from "./Components/svg/CB_Primary";
import { useDispatch } from "react-redux";
import CBPlumbingLogo from "./Components/svg/CBPlumbingLogo";
import CBLogoPurpleNoFill from "./Components/svg/CBLogoPurpleNoFill";
import * as loadingActions from "../store/action/loading";

const LoginScreen = (props) => {
  const { userLogin, data, loading } = props;
  const dispatch = useDispatch();
  const [errortext, setErrortext] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [form, setForm] = useState({});
  const [btnClick, setbtnClick] = useState(false);
  const [appVersion, setAppVersion] = useState("");
  const passwordRef = useRef();
  const handleSubmitPress = () => {
    if (
      form.username &&
      form.username != "" &&
      form.password &&
      form.password != ""
    ) {
      setbtnClick(true);
      userLogin(form.username, form.password);
    } else {
      if (!form.username || form.username == "") {
        Toast.show({
          type: "error",
          position: "top",
          bottomOffset: 210,
          autoHide: true,
          text1: "Error",
          text2: "Please enter username",
        });
      } else if (!form.password || form.password == "") {
        Toast.show({
          type: "error",
          position: "top",
          bottomOffset: 210,
          autoHide: true,
          text1: "Error",
          text2: "Please enter password",
        });
      }
    }
  };

  useEffect(() => {
    dispatch(loadingActions.setLoading(false));
    (async () => {
      const app = await AsyncStorage.getItem("appVersion");
      setAppVersion(app);
    })();
  }, []);

  useEffect(() => {
    if (data?.errorBody) {
      Toast.show({
        type: "error",
        position: "top",
        bottomOffset: 210,
        autoHide: true,
        text1: "Error",
        text2: data?.errorBody,
      });

      dispatch({ type: RESET_USER_LOGIN });
    } else {
      if (data?.userDetails && data?.userDetails?.phoneNumberConfirmed) {
        props.navigation.replace("DrawerNavigationRoutes");
      } else if (
        data?.userDetails &&
        data?.userDetails?.phoneNumberConfirmed == false
      ) {
        props.navigation.navigate("RegistrationAddPhoneNo", {
          emailId: data?.userDetails?.email,
          userId: data?.userDetails?.id,
        });
      }
    }
  }, [data]);
  const EyeComp = () => {
    return (
      <NewTextInput.Icon
        name="eye-outline"
        color="#78909c"
        onPress={() => {
          setShowPassword((prevState) => {
            return !prevState;
          });
        }}
      />
    );
  };

  const TextInputComp = React.useCallback(
    ({ right, placeholderLabel, id, secure, ref, label }) => {
      return (
        <NewTextInput
          style={styles.inputStyle}
          returnKeyType={!secure ? "next" : "default"}
          onSubmitEditing={() => {
            passwordRef.current.focus();
          }}
          ref={passwordRef}
          placeholder={placeholderLabel}
          secureTextEntry={secure && !showPassword ? true : false}
          placeholderTextColor="#546e7a"
          right={
            right == "user" ? (
              <NewTextInput.Icon name="account" color="#78909c" />
            ) : (
              EyeComp()
            )
          }
          underlineColor="transparent"
          onChangeText={(text) => {
            setForm((prevState) => ({ ...prevState, [id]: text }));
            setbtnClick(false);
          }}
          value={label}
        />
      );
    },
    [showPassword, props]
  );

  return (
    <View style={styles.mainBody}>
      {loading && <Loader />}
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          flex: 1,
        }}
      >
        <View>
          <KeyboardAvoidingView enabled>
            <View
              style={{ alignItems: "center", marginTop: 40, marginBottom: 20 }}
            >
              <Logo width={800} height={145} viewBox="0 0 600 300" />
            </View>
            {!data?.userDetails && (data?.message || data?.errors) && btnClick
              ? Toast.show({
                  type: "error",
                  position: "top",
                  bottomOffset: 210,
                  autoHide: true,
                  text1: "Error",
                  text2: data?.message || data?.errors["UserName"][0],
                })
              : null}
            <View>
              <View style={styles.SectionStyle}>
                <TextInputComp
                  right={"user"}
                  placeholderLabel={"Enter Username"}
                  id={"username"}
                  label={form["username"]}
                />
              </View>
              <View style={styles.SectionStyle}>
                <TextInputComp
                  right={"password"}
                  placeholderLabel={"Enter Password"}
                  id={"password"}
                  secure={true}
                  label={form["password"]}
                />
              </View>
              {errortext != "" ? (
                <Text style={styles.errorTextStyle}> {errortext} </Text>
              ) : null}
              <TouchableOpacity
                style={styles.buttonStyle}
                activeOpacity={0.5}
                onPress={handleSubmitPress}
              >
                <Text style={styles.buttonTextStyle}>Login</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.registerButtonStyle}
                activeOpacity={0.5}
                onPress={() => {
                  props.navigation.navigate("Registration");
                }}
              >
                <Text style={styles.registerTextStyle}>Sign Up</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.forgotLinkText}
                activeOpacity={0.5}
                onPress={() => {
                  props.navigation.navigate("ForgotPassword");
                }}
              >
                <Text style={styles.forgotPasswordText}>Forgot Password</Text>
              </TouchableOpacity>
              <View style={{ alignItems: "center" }}>
                <Text>{appVersion}</Text>
              </View>
              <View style={styles.topContainer2}>
                <View style={styles.absCont}>
                  <CBLogoPurpleNoFill
                    height={160}
                    width={200}
                    viewBox="0 0 90 90"
                  />
                </View>
                <View style={styles.goldlogo}>
                  <CBPlumbingLogo height={270} width={320} />
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
    </View>
  );
};

export default connect(
  (state) => ({
    data: state.loggedInUser.loggedInUser,
    loading: state.loading.loading,
  }),
  (dispatch, ownProps) => ({
    userLogin: (username, password) =>
      dispatch(getUserLogin(username, password)),
  })
)(LoginScreen);

const styles = StyleSheet.create({
  topContainer2: {
    marginTop: 80,
    height: 150,
    width: "100%",
  },
  absCont: {
    height: 185,
    position: "relative",
    bottom: 100,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    opacity: 0.15,
  },
  goldlogo: {
    bottom: 270,
    alignSelf: "center",
  },
  mainBody: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#fff",
    alignContent: "center",
  },
  SectionStyle: {
    flexDirection: "row",
    width: "90%",
    marginVertical: 10,
    marginLeft: "5%",
  },
  bottomLogo: {
    alignItems: "center",
    width: "100%",
    marginTop: 40,
  },
  splashImage: {
    width: 383,
    height: 165,
    marginHorizontal: 10,
    marginTop: 10,
  },
  buttonStyle: {
    backgroundColor: "#673ab7",
    color: "#FFFFFF",
    borderColor: "#673ab7",
    borderWidth: 1,
    height: 45,
    alignItems: "center",
    width: "90%",
    marginLeft: "5%",
    marginTop: 20,
    borderRadius: 5,
  },
  registerButtonStyle: {
    backgroundColor: "#fff",
    color: "#FFFFFF",
    borderColor: "#a38862",
    borderWidth: 1,
    height: 45,
    alignItems: "center",
    width: "90%",
    marginLeft: "5%",
    marginTop: 20,
    borderRadius: 5,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "white",
    backgroundColor: "#ede7f6",
    height: 50,
    width: "90%",
    justifyContent: "center",
    borderWidth: 0,
    borderColor: "#ede7f6",
    borderRadius: 4,
  },
  registerTextStyle: {
    color: "#a38862",
    textAlign: "center",
    fontWeight: "normal",
    fontSize: 16,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  forgotPasswordText: {
    height: 60,
    color: "#2196f3",
    fontSize: 16,
    textAlign: "center",
    marginTop: 20,
  },
  forgotLinkText: {
    borderWidth: 0,
    color: "#FFFFFF",
    alignItems: "center",
    width: "90%",
    marginLeft: "5%",
  },
  logo: {
    width: "70%",
    top: -80,
    position: "absolute",
    aspectRatio: 1,
    height: undefined,
  },
});
